<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPrice extends Model
{

    protected $fillable = ['user_id', 'tag_id', 'price', 'price_type', 'user'];
    protected $with = ['tag'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function tag()
    {
        return $this->belongsTo('Spatie\Tags\Tag', 'tag_id');
    }

    public function getTypeForFrontAttribute(){
        $config_name = 'portfolio.price_types.' . $this->price_type;
        return config($config_name, '');
    }
}
