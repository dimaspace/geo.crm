<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;
use Config;
use DB;
use App\BalanceTransaction;

class CashFlow extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'carried',
        'comment'
    ];

    function transactions(){
        return $this->belongsTo('App\BalanceTransaction', 'id', 'cash_flow_id');
    }

    function user(){
        return $this->belongsTo('App\User');
    }

    function get_type_by_code($code){
        return array_flip(Config::get('transaction.cash_flow_types'))[$code];
    }

    static function type_by_code($code){
        return array_flip(Config::get('transaction.cash_flow_types'))[$code];
    }

    public function getNotCarried(){
        return $this->amount - $this->carried;
    }

    public function scopeNotCarried($query)
    {
        return $query->whereRaw('amount > carried');
    }

    public function scopeFullNotCarriedValue($query, $value)
    {
        return $query->whereRaw('(amount - carried) = ' . $value);
    }

    public function scopePartialNotCarriedValue($query, $value, $full = true)
    {
        if($full){
            return $query->whereRaw('(amount - carried) > ' . $value);
        }else{
            return $query->whereRaw('(amount - carried) < ' . $value);
        }
    }

    public function scopeNotUserId($query, $user_id)
    {
        return $query->where('user_id', '<>', $user_id);
    }

    public function scopeNotType($query, $type_id)
    {
        return $query->where('type', '<>', $type_id);
    }

    public function scopeByRegionCode($query, $region_code = 'yar')
    {
        return $query->whereHas('user', function ($query) use($region_code){
            return $query->whereHas('regions', function ($query) use($region_code){
                return $query->where('code', $region_code);
            });
        });
        //('user.regions.id = user.regions.id');
    }

    static function checkCrossFindByMode($notCarried, $mode = 'one'){
        if($mode == 'one'){
            return $notCarried > 0;
        }else{
            return false;
        }
    }

    static function crossFind($mode = 'one', $debug = false){
        self::clear_self_flow();
        // Поиск первого в очереди на вывод
        $first = self::NotCarried()->join('users as user', 'user.id', '=', 'cash_flows.user_id')->select('cash_flows.*', 'user.cf_priority')->orderBy('user.cf_priority', 'desc')->orderBy('cash_flows.updated_at', 'asc')->where('type', self::type_by_code('TAKE'))->first();
        if(!$first){
            return false;
        }
        $notCarried = $first->getNotCarried();
        if($debug){
            print "(NC " . $notCarried . ")";
        }
        $check = true;
        while($check && ($second = self::findOpposite($first, $debug))){
            if($debug) {
                print $first->id;
                print  " ".$second->id;
                print  " ".$second->value."\n";
            }
            $notCarried = $notCarried - $second->value;

            DB::transaction(function () use($first, $second) {
                $opposite = self::find($second->id);
                $opposite->carried = $second->carried + $second->value;
                $opposite->update();

                $first->carried = $first->carried + $second->value;
                $first->update();
                $desc = [
                    '[заявка №{*CASH_FLOW_ID*}] Передайте {*USER_T_CROSS*} {*VALUE*} руб.',
                    '[заявка №{*CASH_FLOW_ID*}] {*USER_T_CROSS*} передаст вам {*VALUE*} руб.'
                ];
                BalanceTransaction::make_payment($second->value, $first->user_id, $second->user_id, NULL, 'CASH_FLOW', 'TRIGGER', $desc, 0, [$second->id, $first->id]);
            });

            if($debug) {
                print "(NC ".$notCarried.")";
            }

            $check = self::checkCrossFindByMode($notCarried, $mode);
        }

        $check_recharge = self::NotCarried()->where('type', self::type_by_code('RECHARGE'))->count();
        $check_take = self::NotCarried()->where('type', self::type_by_code('TAKE'))->count();
        if($check_recharge == 0 || $check_take == 0){
            return false;
        }else{
            return true;
        }

    }

    static function clear_self_flow(){
        $not_carried = self::NotCarried()->get();
        $by_user = $not_carried->groupBy('user_id');
        $by_user->each(function($user_flows, $user_id){
            $by_type = $user_flows->groupBy('type');
            if($by_type != null && $by_type->count() > 1){
                $by_type[1]->each(function($flow, $key) use(&$by_type, $user_id){
                    $not_carried_take = $flow->getNotCarried();
                    if($not_carried_take > 0){
                        foreach($by_type[0] as &$recharge_flow){
                            $not_carried_recharge = $recharge_flow->getNotCarried();
                            if($not_carried_recharge > 0){
                                $value = ($not_carried_recharge > $not_carried_take) ? $not_carried_take :  $not_carried_recharge;
                                DB::transaction(function () use($value, $recharge_flow, $flow, $user_id) {
                                    $flow->carried = $flow->carried + $value;
                                    $recharge_flow->carried = $recharge_flow->carried + $value;
                                    $desc = '[заявка №{*CASH_FLOW_ID*}] Гашение противопложной заявки';
                                    BalanceTransaction::make_payment($value, $user_id, $user_id, NULL, 'CASH_FLOW', 'AUTO', $desc, 1, [$recharge_flow->id, $flow->id]);
                                    $flow->update();
                                    $recharge_flow->update();
                                });
                            }
                        }
                    }
                });
            }
        });
    }

    static function findOpposite($flow, $debug = false){
        $value = (float) ($flow->amount - $flow->carried);

        $region_code = '';
        $region = $flow->user->regions->first();
        if($region){
            $region_code = $region->code;
        }

        // Поиск полного покрытия в том же регионе
        $result = self::NotCarried()->FullNotCarriedValue($value)->NotUserId($flow->user_id)->NotType($flow->type)->ByRegionCode($region_code)->join('users as user', 'user.id', '=', 'cash_flows.user_id')->select('cash_flows.*', 'user.cf_priority')->orderBy('user.cf_priority', 'desc')->orderBy('cash_flows.updated_at', 'asc')->first();
        if($result){
            if($debug) {
                print 1 ."::";
            }
            $result->value = $value;
            return $result;
        }
        // Поиск частичного покрытия целой заявкой в том же регионе
        $result = self::NotCarried()->PartialNotCarriedValue($value)->NotUserId($flow->user_id)->NotType($flow->type)->ByRegionCode($region_code)->join('users as user', 'user.id', '=', 'cash_flows.user_id')->select('cash_flows.*', 'user.cf_priority')->orderBy('user.cf_priority', 'desc')->orderBy('cash_flows.created_at', 'asc')->first();
        if($result){
            if($debug) {
                print 2 ."::";
            }
            $result->value = min($value, $result->getNotCarried());
            return $result;
        }

        // Поиск полного покрытия в любом регионе
        $result = self::NotCarried()->FullNotCarriedValue($value)->NotUserId($flow->user_id)->NotType($flow->type)->join('users as user', 'user.id', '=', 'cash_flows.user_id')->select('cash_flows.*', 'user.cf_priority')->orderBy('user.cf_priority', 'desc')->orderBy('cash_flows.created_at', 'asc')->first();
        if($result){
            if($debug) {
                print 3 ."::";
            }
            $result->value = $value;
            return $result;
        }

        // Поиск частичного покрытия целой заявкой в любом регионе
        $result = self::NotCarried()->PartialNotCarriedValue($value)->NotUserId($flow->user_id)->NotType($flow->type)->join('users as user', 'user.id', '=', 'cash_flows.user_id')->select('cash_flows.*', 'user.cf_priority')->orderBy('user.cf_priority', 'desc')->orderBy('cash_flows.created_at', 'asc')->first();
        if($result){
            if($debug) {
                print 4 ."::";
            }
            $result->value = min($value, $result->getNotCarried());
            return $result;
        }

        // Поиск частичного покрытия частью заявки в любом регионе
        $result = self::NotCarried()->PartialNotCarriedValue($value, false)->NotUserId($flow->user_id)->NotType($flow->type)->join('users as user', 'user.id', '=', 'cash_flows.user_id')->select('cash_flows.*', 'user.cf_priority')->orderBy('user.cf_priority', 'desc')->orderBy('cash_flows.created_at', 'asc')->first();
        if($result){
            if($debug) {
                print 5 ."::";
            }
            $max_sum = (float) $result->amount - $result->carried;
            $result->value = $max_sum ;
            return $result;
        }

        return false;
    }
}
