<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;


class ProjectCords extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;

    protected $fillable = ['type_code', 'user_id'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function project(){
        return $this->belongsTo('App\Project', 'id', 'project_id');
    }

    static function getTypeNameByCode($code){
        $type_names = config('project.cord_type');
        return isset($type_names[$code]) ? $type_names[$code] : '???';
    }
}
