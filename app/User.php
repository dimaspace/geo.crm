<?php namespace App;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Acoustep\EntrustGui\Contracts\HashMethodInterface;
use Hash;
use Illuminate\Notifications\Notifiable;
use Cache;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

use App\Notifications\PasswordReset;
use Spatie\Image\Manipulations;
use Overtrue\LaravelFollow\Traits\CanVote;

/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $readNotifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Model implements HasMedia, AuthenticatableContract, CanResetPasswordContract, HashMethodInterface
{
  use \Spiritix\LadaCache\Database\LadaCacheTrait;
  use Authenticatable, CanResetPassword, EntrustUserTrait, Notifiable, UserBalance, HasMediaTrait;
  use CanVote;

    protected $throwValidationExceptions = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'is_staff', 'hidden', 'ed_fee_cash', 'ed_fee_noncash', 'vk_id', 'threshold', 'primary_region_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'telegram_find_code', 'cf_priority'];

    protected $hashable = ['password'];

    protected $rulesets = [

        'creating' => [
            'email'      => 'required|email|unique:users',
            'password'   => 'required',
        ],

        'updating' => [
            'email'      => 'required|unique:users',
            'password'   => '',
        ],
    ];

    public static function flushPermissionCache(){
            Cache::tags([
                config('entrust.role_user_table'),
                config('entrust.permission_role_table')
            ])->flush();
    }

    public function entrustPasswordHash()
    {
        $this->password = Hash::make($this->password);
        $this->save();
    }


    public function price()
    {
        return $this->hasMany('App\UserPrice', 'user_id');
    }

    public function regions()
    {
        return $this->belongsToMany('App\Region', 'user_region');
    }


    public function primary_region()
    {
        return $this->hasOne('App\Region', 'code', 'primary_region_code');
    }

    public function getRegionsIds()
    {
        return $this->regions->pluck('id');
    }

    public function getRegionsCodes()
    {
        return $this->regions->pluck('code');
    }

    public function info(){
        return $this->hasOne('App\UserInfo', 'user_id');
    }

    public function getWeight(){

        $weight = 0;

        if($this->is_staff == 1){
            $weight += 1000;
        }else{
            $weight += 0;
        }

        if($this->hidden == 1){
            $weight += 0;
        }else{
            $weight += 10;
        }

        return $weight;
    }

    static function getNameById($id){
        return Cache::tags(['user_names'])->rememberForever('user_name_'.$id, function () use($id) {
            return User::findOrNew($id, ['name'])->name;
        });

    }

    public function isDebt(){
        return (($this->balanceCached() + $this->threshold) < 0);
    }

    public function getBalanceColorAttribute(){
        return $this->balanceCached() >= 0 ? 'light-green' : ($this->isDebt() ? 'red' : 'orange');
    }

    public function balanceCached(){
        return self::getBalanceById($this->id);
    }

    static function getBalanceById($id){
        return Cache::tags(['user_balances'])->rememberForever('user_balance_'.$id, function () use($id) {
            return User::findOrNew($id)->balance();
        });

    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('portfolio')
            ->useDisk('s3');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb_100')
            ->width(100)
            ->height(100)
            ->format('jpg')
            ->performOnCollections('portfolio');

        $this->addMediaConversion('thumb_fit_250')
            ->fit(Manipulations::FIT_CROP, 387, 255)
            ->format('jpg')
            ->performOnCollections('portfolio');

        $this->addMediaConversion('thumb_250')
            ->width(250)

            ->format('jpg')
            ->performOnCollections('portfolio');

        $this->addMediaConversion('thumb_crop_100_100')
            ->fit(Manipulations::FIT_CROP, 100, 100)
            ->format('jpg')
            ->performOnCollections('portfolio');

        $this->addMediaConversion('main')
            //->width(1920)
            ->fit(Manipulations::FIT_MAX, 1920, 1920)
            ->format('jpg')
            //->watermark(storage_path('app/watermark_full.png'))
            //->watermarkHeight(100, Manipulations::UNIT_PERCENT)
            //->watermarkWidth(100, Manipulations::UNIT_PERCENT)
            //->watermarkFit(Manipulations::FIT_CROP)
            ->performOnCollections('portfolio');
    }

    static function getByRole($role, $return_query = false){
        return self::getByRoles([$role], $return_query);
    }

    static function getByRoles(array $roles, $return_query = false){
        $users = User::whereHas('roles', function ($query) use ($roles) {
            $query->whereIn('name', $roles);
        });
        if($return_query){
            return $users;
        }else{
            return $users->get();
        }
    }

    public function getPriceByTagId($tag_id){
        $price = $this->price->where('tag_id', $tag_id)->first();
        if($price){
            return $price->price;
        }else{
            return false;
        }
    }

    public function isSuperuser(){
        return ($this->id === 1);
    }

    public function next(){
        // get next user
        $user = static::where('id', '>', $this->id)->orderBy('id','asc')->first();
        return $user ?? false;

    }

    public function prev(){
        // get next user
        $user = static::where('id', '<', $this->id)->orderBy('id','desc')->first();
        return $user ?? false;

    }
}
