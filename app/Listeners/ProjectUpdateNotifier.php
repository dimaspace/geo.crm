<?php

namespace App\Listeners;

use App\Events\ProjectUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\GeoLinkSendToModerVkChat;

class ProjectUpdateNotifier implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProjectUpdated  $event
     * @return void
     */
    public function handle(ProjectUpdated $event)
    {
        //$event->project;
        //$event->changes;
        if($event->changes->has('geo_link') && trim($event->project->geo_link) != ''){
            $virtual_user = new User();
            $virtual_user->vk_id = config('VKAPI.moderate_chat_id');
            Notification::send($virtual_user, new GeoLinkSendToModerVkChat($event->project));
        }

    }
}
