<?php

namespace App\Listeners;

//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSending;

class MessageSendingCustomHeader
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSending  $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        // Get Swift_Message obj
        $message = $event->message;

        // Get Message Headers
        $headers = $message->getHeaders();

        // Append custom header
        $headers->addTextHeader('Reply-To', config('mail.reply.name'). ' <' . config('mail.reply.address'). '>');
    }
}
