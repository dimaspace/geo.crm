<?php

namespace App\Listeners\Vk;

use App\Events\Notification\Vk\VkDenied;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VkDeniedAdminNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VkDenied  $event
     * @return void
     */
    public function handle(VkDenied $event)
    {
        //
    }
}
