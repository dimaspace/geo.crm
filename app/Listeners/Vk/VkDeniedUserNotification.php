<?php

namespace App\Listeners\Vk;

use App\Client;
use App\Events\Notification\Vk\VkDenied;
use App\Notifications\Vk\VkMsgDeny;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;
use stdClass;

class VkDeniedUserNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  VkDenied  $event
     * @return void
     */
    public function handle(VkDenied $event)
    {

        $notifiable = new stdClass();
        $notifiable->vk_id = $event->user_id;

        Notification::send($notifiable, new VkMsgDeny($event->region_code, $event->group_id, $event->group_name, $event->group_screen_name));
    }
}
