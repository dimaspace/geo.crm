<?php


namespace App\Listeners\Notification;


use App\Events\Project\ProjectReceivedToClient;
use App\Events\Project\ProjectReceivedToWorker;
use App\Notifications\ProjectSendToClient;
use App\Notifications\Vk\ProjectSendToWorkerVk;
use App\Channels\VkChannel;

class NotificationEventSubscriber
{

    public function onNotificationSent($event) {
        $notifiable = $event->notifiable;
        $notification = $event->notification;
        $channel = $event->channel; // App\Channels\VkChannel
        $response = $event->response;

        if($notification instanceof ProjectSendToWorkerVk && $channel === VkChannel::class){
                event(new ProjectReceivedToWorker($notification->project, $response));
        }

        if($notification instanceof ProjectSendToClient){
            event(new ProjectReceivedToClient($notification->project, $response));
        }

    }

    public function onNotificationFailed($event) {

    }

    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Notifications\Events\NotificationSent',
            'App\Listeners\Notification\NotificationEventSubscriber@onNotificationSent'
        );

        $events->listen(
            'Illuminate\Notifications\Events\NotificationFailed',
            'App\Listeners\Notification\NotificationEventSubscriber@onNotificationFailed'
        );
    }
}