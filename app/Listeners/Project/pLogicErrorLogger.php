<?php

namespace App\Listeners\Project;

use App\Events\Project\pLogicError;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GeoCrmEventLogger\GeoCrmEventLoggerBase;

class pLogicErrorLogger extends GeoCrmEventLoggerBase
{

    public function __construct(){
        $this->init(__NAMESPACE__ , __CLASS__);
    }

    /**
     * Handle the event.
     *
     * @param  pLogicError  $event
     * @return void
     */
    public function handle(pLogicError $event)
    {
        $msg = $event->error_code . ': ' . $event->error_msg . ' ' . $event->project->toJson();

        $this->log->warning($msg);
    }
}
