<?php

namespace App\Listeners\Project;

use App\Events\Project\pClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class pCloseNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  pClosed  $event
     * @return void
     */
    public function handle(pClosed $event)
    {
        //
    }
}
