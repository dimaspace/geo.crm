<?php

namespace App\Listeners\Project;

use App\Events\Project\pLogicError;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class pLogicErrorNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  pLogicError  $event
     * @return void
     */
    public function handle(pLogicError $event)
    {
        //
    }
}
