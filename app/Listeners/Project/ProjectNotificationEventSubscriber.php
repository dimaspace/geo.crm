<?php


namespace App\Listeners\Project;



class ProjectNotificationEventSubscriber
{

    public function onProjectReceivedToWorker($event) {
        $project = $event->project;
        $response = $event->response;
        $project->vk_msg_id = is_int($response) ? $response : 0;
        $project->save();
    }

    public function onProjectReceivedToClient($event) {
        $project = $event->project;
        $project->client_notified = 1;
        $project->pass_type_id = 2;
        $project->update();
        $project->close();
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Project\ProjectReceivedToWorker',
            'App\Listeners\Project\ProjectNotificationEventSubscriber@onProjectReceivedToWorker'
        );
        $events->listen(
            'App\Events\Project\ProjectReceivedToClient',
            'App\Listeners\Project\ProjectNotificationEventSubscriber@onProjectReceivedToClient'
        );
    }
}