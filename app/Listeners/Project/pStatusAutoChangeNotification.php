<?php

namespace App\Listeners\Project;

use App\Events\Project\pStatusAutoChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class pStatusAutoChangeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  pStatusAutoChanged  $event
     * @return void
     */
    public function handle(pStatusAutoChanged $event)
    {
        //
    }
}
