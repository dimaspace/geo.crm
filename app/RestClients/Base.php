<?php

namespace App\RestClients;

use GuzzleHttp\Client;
use \GuzzleHttp\Exception\ClientException;

class Base extends Client

{
    public function __construct($default)
    {
        parent::__construct($default);
    }


    public static function call($endpoint, $params = [])
    {
        $default = static::defaults();
        if (array_has(static::endpoints(), $endpoint.'.default') && is_array(array_get(static::endpoints(),
                $endpoint.'.default'))) {
            $default = array_replace_recursive(array_get(static::endpoints(), $endpoint.'.default'), $default);
        }

        $client_name = static::class;
        $client = new $client_name($default);

        return (object) $client->request(null, $endpoint, $params);
    }


    public static function defaults()
    {
        return [
            'base_uri' => 'https://restapi.moedelo.org/accounting/api/',
            'headers'  => [
                'md-api-key' => config('rest_api.moedelo.api_key'),
                'Accept'     => 'application/json'
            ]
        ];
    }


    public static function endpoints()
    {
        return [];
    }


    public function request($method = null, $uri = null, array $query = [])
    {
        if ($method === null) {
            $method = 'get';
        }
        // overwrite from endpoints
        if (array_has(static::endpoints(), $uri)) {
            $key = $uri;
            $uri = array_get(static::endpoints(), $key.'.uri');
            if (array_has(static::endpoints(), $key.'.method')) {
                $method = array_get(static::endpoints(), $key.'.method');
            }

            $options = [
                'query' => $query
            ];

            if (array_has(static::endpoints(), $key.'.options') && is_array(array_get(static::endpoints(),
                    $key.'.options'))) {
                $options = array_get(static::endpoints(), $key.'.options');
                if (array_has(static::endpoints(), $key.'.options.query') && is_array(array_get(static::endpoints(),
                        $key.'.options.query'))) {
                    $options['query'] = array_replace_recursive(array_get(static::endpoints(), $key.'.options.query'),
                        $query);
                }
            }
        }

        // overwrite from query params
        $uri = preg_replace_callback('/{:(.*)}/U', function ($matches) use (&$options) {
            if (isset($options['query'][$matches[1]])) {
                $key = $matches[1];
                $value = array_pull($options['query'], $key);

                return $value;
              } else {
                return $matches[0];
            }
        }, $uri);

        try {
            $response = parent::request($method, $uri, $options);

        } catch (ClientException $e) {

            $response = $e->getResponse();

        }

        $return = [
            'code'   => $response->getStatusCode(),
            'msg'    => $response->getReasonPhrase(),
            'result' => $response->getBody()
        ];

        if ($response->hasHeader('Content-Type')) {
            $contentType = $response->getHeader('Content-Type')[0];
            if (str_contains($contentType, '/json')) {
                $return['result'] = (object) json_decode($response->getBody());
            }
        }

        return $return;

    }
}