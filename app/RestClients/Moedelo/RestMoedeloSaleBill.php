<?php

namespace App\RestClients\Moedelo;

class RestMoedeloSaleBill extends RestMoedelo{

    public static function endpoints()
    {

        return [
            'get_bill_by_id' => [
                'uri'     => 'v1/sales/bill/{:id}',
                'options' => [
                    'query' => []
                ]
            ],
            'get_bills' => [
                'uri'     => 'v1/sales/bill',
                'options' => [
                    'query' => [
                        'afterDate' => '2018-01-01T00:00:00',
                        'pageSize' => 999
                    ]
                ]
            ]
        ];
    }
}