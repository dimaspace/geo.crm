<?php

namespace App\RestClients\Moedelo;

class RestMoedeloKontragent extends RestMoedelo{

    public static function endpoints()
    {

        return [
            'get_kontragent_by_id' => [
                'uri'     => '/kontragents/api/v1/kontragent/{:id}',
                'options' => [
                    'query' => []
                ]
            ]
        ];
    }
}