<?php

namespace App\RestClients\Moedelo;

use App\RestClients\Base;

class RestMoedelo extends Base{

    public static function defaults()
    {
        return [
            'base_uri' => config('rest_api.moedelo.api_url'),
            'headers'  => [
                'md-api-key' => config('rest_api.moedelo.api_key'),
                'Accept'     => 'application/json'
            ]
        ];
    }
}