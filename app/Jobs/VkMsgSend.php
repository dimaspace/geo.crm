<?php

namespace App\Jobs;

use dimaspace\VKAPI\VKAPIException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use VKAPI;
use App\FailedDispatch;

class VkMsgSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $msg = [];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = [
            'user_id' => isset($this->msg['user_id']) ? $this->msg['user_id'] : '2967049',
            'message' => isset($this->msg['message']) ? $this->msg['message'] : ''
        ];
        if(isset($this->msg['attachment'])){
            $params['attachment'] = $this->msg['attachment'];
        }

        try {
            VKAPI::setAccessToken(config('project.vk_tokens.' . $this->msg['region_code']));
            $msg_id = VKAPI::call('messages.send', $params);
        } catch (VKAPIException $e) {
                $problem = 'Код ошибки: ' . $e->code . '<br>';
                $problem .= 'Пояснение: ' . $e->message;
            if ($e->code == 901) {
                $problem = 'Требуется написать в группу. Регион ' . $this->msg['region_code'];
            }
            $fail_info = new FailedDispatch();
            $fail_info->channel = 'VK';
            $fail_info->to = $params['user_id'];
            $fail_info->msg = $params['message'];
            $fail_info->problem = $problem;
            $fail_info->save();
        }
    }
}
