<?php

/**
 * Очистка кеша привилегий
 *
 * @param $time
 * @param $tz
 *
 * @return void
 */
function perm_flush_cache()
{
    \App\User::flushPermissionCache();
}