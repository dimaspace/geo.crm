<?php

namespace App;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use Config;
use Carbon\CarbonPeriod;
use Spatie\Period\Boundaries;
use Spatie\Period\Period;
use Spatie\Period\Precision;
use Spatie\Period\Visualizer;
use Spatie\Period\PeriodCollection;

/**
 * App\Busy
 *
 * @property int $id
 * @property int|null $user_id
 * @property \Carbon\Carbon $start
 * @property \Carbon\Carbon $end
 * @property int $busy
 * @property string|null $comment
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereBusy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Busy whereUserId($value)
 * @mixin \Eloquent
 */
class Busy extends Model
{

    protected $dates = [
        'created_at',
        'updated_at',
        'started_at',
        'ended_at'
    ];

    protected $fillable = [
        'user_id',
        'started_at',
        'ended_at',
        'busy',
        'comment'
    ];
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            self::clearPeriod($model, 'updating');
        });
        static::updating(function($model)
        {
            self::clearPeriod($model, 'updating');
        });
        static::saving(function($model)
        {
            self::clearPeriod($model, 'saving');
        });
    }

    public static function clearPeriod($model, $event): void
    {
        $period = CarbonPeriod::create($model->started_at, $model->ended_at);
        $overlaps = self::overlaps($period);
        $overlaps =  $overlaps->where('user_id', $model->user_id);
        if($model->id){
            $overlaps->where('id', '<>', $model->id);
        }

        $overlaps = $overlaps->get();
        if($overlaps) {
            foreach($overlaps as $overlap){
                $model_period = Period::make($model->started_at, $model->ended_at, Precision::MINUTE);
                $overlap_period = Period::make($overlap->started_at, $overlap->ended_at, Precision::MINUTE);
                $diff = $overlap_period->diff($model_period);
                    $overlap_data = $overlap;
                    $overlap->delete();
                    foreach ($diff as $period) {
                        self::create([
                            'user_id' => $overlap_data['user_id'],
                            'started_at' => $period->getStart(),
                            'ended_at'=> $period->getEnd(),
                            'busy' => $overlap_data['busy'],
                            'comment' => $overlap_data['comment'],
                        ])->save();
                }
            }
        }
    }

    function user(){
        return $this->hasOne('App\User');
    }

    static function get_code_by_busy($busy){
        return array_flip(Config::get('project.busy_code'))[$busy];
    }
}
