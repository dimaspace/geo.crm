<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;

/**
 * App\Region
 *
 * @property integer                                                                 $id
 * @property string                                                                  $title
 * @property string                                                                  $code
 * @property integer                                                                 $geo_id
 * @property \Carbon\Carbon                                                          $created_at
 * @property \Carbon\Carbon                                                          $updated_at
 * @property float                                                                   $balance
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereGeoId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Region whereBalance($value)
 * @mixin \Eloquent
 */
class Region extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;

    protected $table = 'regions';

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_region', 'region_id', 'user_id');
    }


}
