<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;

class ProjectDesire extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;
    protected $fillable = ['user_id', 'project_id', 'rating'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function project(){
        return $this->belongsTo('App\User');
    }
}
