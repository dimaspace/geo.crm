<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;

class UserInfo extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;
    /**
    * Связанная с моделью таблица.
    *
    * @var string
    */
    protected $table = 'users_info';
    protected $primaryKey = 'user_id';
    //public $incrementing = false;
    protected $hidden = ['passport'];
}
