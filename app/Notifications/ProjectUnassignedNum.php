<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\Channels\VkChannel;


class ProjectUnassignedNum extends Notification implements ShouldQueue
{
    use Queueable;

    protected $projects24, $projects72, $msg = '', $stiker = false;
    /**
     * Create a new notification instance.
     *
     * @param array $projects24
     * @param array $projects72
     * @return void
     */
    public function __construct($projects24, $projects72)
    {
        $this->projects24 = $projects24;
        $this->projects72 = $projects72;

        if($this->projects24->count() > 0 || $this->projects72->count() > 0 ){
            $this->msg .= "Найдены проекты с неназначенными исполнителями." . "\n";
            $this->msg .= "Менее суток: " . $projects24->count() . "\n";
            $this->msg .= "Менее 3 суток: " . $projects72->count() . "\n";
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        $return[] = VkChannel::class;
        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            $return[] = TelegramChannel::class;
        }


        return $return;
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Убить всех человеков!</b>'."\n";
            $msg .= $this->msg;

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

            sleep(1);

        return $TMsg;

    }
    public function toVk($notifiable)
    {
        return ['stiker' => $this->stiker, 'text' => $this->msg];
    }
}
