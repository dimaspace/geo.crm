<?php

namespace App\Notifications;

use Illuminate\Support\Collection;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Channels\VkChannel;


class GeoLinkSendToModerVkChat extends Notification implements ShouldQueue
{
    use Queueable;

    protected $project, $msg = '', $stiker = false;
    /**
     * Create a new notification instance.
     *
     * @param array $projects12
     * @return void
     */
    public function __construct($project)
    {
        $this->project = $project;

            $this->msg .= "В систему поступила информация о новой выгруженной съёмке.\n";
            $this->msg .= "==========\n\n";

            if(trim($project->geo_link)){
                $this->msg .= "\n";
                $this->msg .= "Публикация на геометрии:\n ";
                $this->msg .= $project->geo_link;
            }

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->vk_id) != '') {
            $return[] = VkChannel::class;
        }

        return $return;
    }

    public function toVk($notifiable)
    {
        return ['stiker' => $this->stiker, 'text' => $this->msg];
    }
}
