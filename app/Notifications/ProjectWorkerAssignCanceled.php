<?php

namespace App\Notifications;

use Illuminate\Support\Collection;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\Channels\VkChannel;


class ProjectWorkerAssignCanceled extends Notification implements ShouldQueue
{
    use Queueable;

    protected $project;
    public $region = false;
    /**
     * Create a new notification instance.
     *
     * @param \App\Project $project
     * @return void
     */
    public function __construct(\App\Project $project)
    {
        $this->project = $project;
        $this->region = $project->region_code;
    }

}
