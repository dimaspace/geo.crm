<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SysWorks extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     * @param  mixed  $msg
     * @param  mixed  $title
     * @param  integer  $time
     * @param  string  $color
     * @param  mixed  $msg2
     * @return void
     */
    protected $msg, $msg2, $title, $time, $color, $icon;

    public function __construct($msg, $title = 'Технический отдел', $time = 5, $color='deep-orange', $msg2=false, $icon=false)
    {
        $this->time = $time;

        $this->msg = str_replace('. ', '.<br>', $msg);
        $this->msg = str_replace('! ', '!<br>', $this->msg);

        $this->msg2 = str_replace('. ', '.<br>', $msg2);
        $this->msg2 = str_replace('! ', '!<br>', $this->msg2);

        $this->title = $title;

        $this->color = $color;
        $this->icon = $icon;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the broadcast representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => $this->title,
            'msg' => $this->msg,
            'msg2' => $this->msg2,
            'time' => $this->time,
            'color' => $this->color,
            'icon' => $this->icon
        ]);
    }
}
