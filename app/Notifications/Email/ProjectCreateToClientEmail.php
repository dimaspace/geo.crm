<?php

namespace App\Notifications\Email;

use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\ProjectCreateToClient;


class ProjectCreateToClientEmail extends ProjectCreateToClient
{


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->email) !== '') {
            $return[] = 'mail';
        }

        return $return;
    }

    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->subject('Ваша заявка занесена в систему распределения съемок Геометрии')
            ->greeting('Добрый день!')
            ->line(nl2br($this->msg));

        return $mail;
    }
}
