<?php

namespace App\Notifications\Email;


use App\Notifications\ProjectWorkerAssignCanceled;

use Illuminate\Notifications\Messages\MailMessage;



class ProjectWorkerAssignCanceledEmail extends ProjectWorkerAssignCanceled
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('email', $notify_channels)){
            $return[] = 'mail';
        }

        return $return;
    }

    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->subject('Ваше назначение исполнителем отменено')
            ->greeting('Приветствуем!')
            ->line('Ваше назначение исполнителем на проект № ' . $this->project->id . '' . " отменено<br>");

        $mail->line('<b>ПОДРОБНОСТИ ПРОЕКТА</b> <br>');
        $mail->line('<b>Дата:</b> ' . $this->project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') . '<br>');
        $mail->line('<b>Время:</b> ' . $this->project->date_start->timezone(config('project.city_time_zone'))->format('H:i') . '<br>');
        $mail->line('<b>Место:</b> ' . $this->project->place_cached . '<br>');
        $mail->line('<b>Регион:</b> ' . $this->project->region_code . '<br>');

        $mail->line('<br>Обязательно уточните информацию у координатора!!!');

        return $mail;
    }
}
