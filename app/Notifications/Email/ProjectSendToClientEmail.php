<?php

namespace App\Notifications\Email;


use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\ProjectSendToClient;


class ProjectSendToClientEmail extends ProjectSendToClient
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->email) != '') {
            $return[] = 'mail';
        }

        return $return;
    }

    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->subject('Сдача проекта геометрии')
            ->greeting('Добрый день!')
            ->line(nl2br($this->msg));

        return $mail;
    }

    public function tags()
    {
        return ['notifications', 'email', 'project:'.$this->project->id];
    }
}
