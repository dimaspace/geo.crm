<?php

namespace App\Notifications;


use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class ProjectSendToClient extends Notification
{

    protected $msg = '', $stiker = false;
    public $project, $region = false;

    /**
     * Create a new notification instance.
     *
     * @param array $projects12
     * @return void
     */
    public function __construct($project)
    {

        $this->project = $project;
        $this->region = $project->region_code;

            $this->msg .= "Ссылка на файлы с вашего проекта.\n";
            $this->msg .= "==========\nИнформация о вашем проекте:\n";

            $this->msg .= "Начало: ";
            $this->msg .= $project->date_start ? $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.Y H:i e') : NULL;

            $this->msg .= "\n";
            $this->msg .= "Конец: ";
            $this->msg .= $project->date_end ? $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : NULL;

            $this->msg .= "\n";
            $this->msg .= "Место: ";
            $this->msg .= $project->place_cached;

            if(trim($project->geo_link)){
                $this->msg .= "\n";
                $this->msg .= "Публикация на геометрии:\n ";
                $this->msg .= $project->geo_link;
            }

            $this->msg .= "\n";
            $this->msg .= "Ссылка на файлы:\n ";
            $this->msg .= $project->download_link;

            $this->msg .= "\n==========\n";
            $this->msg .= "Данное уведомление сформированно автоматически.\n ";
            $this->msg .= "Автоуведомления работают в тестовом режиме, если у вас есть какие то пожелания по работе этой системы, то сообщите нам и мы рассмотрим ваши предложения.\n ";
            $this->msg .= "\n==========\n";


    }
}
