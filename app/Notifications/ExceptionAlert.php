<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class ExceptionAlert extends Notification implements ShouldQueue
{
    use Queueable;

    private $source;
    private $msg;
    private $exception;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($source, $msg = '',  $exception = false)
    {
        $this->source = $source;
        $this->msg = $msg;
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            $return[] = TelegramChannel::class;
        }

        return $return;
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Ошибка в системе GEO CRM</b>' . "\n";
            $msg .= 'Источник проблемы: ' . $this->source . "\n";
            if($this->msg){
                $msg .= $this->msg . "\n";
            }
            if($this->exception){
                $msg .= '[' . $this->exception->getCode() . '] ' .$this->exception->getMessage() . "\n";
            }

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }
}
