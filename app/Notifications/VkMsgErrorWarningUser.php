<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\User;

class VkMsgErrorWarningUser extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user, $region_code, $group_link;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($region_code = NULL)
    {
        $regions = config('project.regions');
        $def_region = $regions[0];
        $this->region_code = $region_code ?? $def_region;

        $this->group_link = config('project.vk_groups.' . $this->region_code);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            //$return[] = TelegramChannel::class;
        }
        if(in_array('email', $notify_channels)){
            //$return[] = 'mail';
        }

        return $return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
                    ->subject('Мы не может доставить вам сообщение Вконтакте')
                    ->greeting('Приветствуем!')
                    ->line('Вконтакте отключил возможность доставлять автоматические сообщения от имени пользователя.');
        $mail->line('Теперь мы можем отправлять уведомления только от имени сообщества.');
        $mail->line('Чтобы, как и раньше своевременно и автоматически получать от нас снятый материал, уведомления кто назначен исполнителем на Ваш заказ и т.д., просим Вас написать сообщение "Привет" в нашу группу (' .$this->group_link. '). Приносим извинения за неудобства, таковы новые технические правила и особенности "В Контакте".');

        return $mail;
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Мы не может доставить вам сообщение Вконтакте!</b>'."\n";
            $msg = 'Вконтакте отключил возможность доставлять автоматические сообщения от имени пользователя.'."\n";
            $msg = 'Теперь мы можем отправлять уведомления только от имени сообщества.'."\n";
            $msg = 'Чтобы, как и раньше своевременно и автоматически получать от нас снятый материал, уведомления кто назначен исполнителем на Ваш заказ и т.д., просим Вас написать сообщение "Привет" в нашу группу (' .$this->group_link. '). Приносим извинения за неудобства, таковы новые технические правила и особенности "В Контакте".'."\n";
            $msg .= $this->group_link;

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }
}
