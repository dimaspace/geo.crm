<?php

namespace App\Notifications\Telegram;

use App\Notifications\ProjectDeadLineToWorker;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;



class ProjectDeadLineToWorkerTelegram extends ProjectDeadLineToWorker
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            $return[] = TelegramChannel::class;
        }

        return $return;
    }
}
