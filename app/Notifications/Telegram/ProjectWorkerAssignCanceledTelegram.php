<?php

namespace App\Notifications\Telegram;

use App\Notifications\ProjectWorkerAssignCanceled;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;



class ProjectWorkerAssignCanceledTelegram extends ProjectWorkerAssignCanceled
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            $return[] = TelegramChannel::class;
        }

        return $return;
    }

    public function toTelegram($notifiable)
    {
            $msg = 'Hasta la vista, meatbag!'."\n";
            $msg .= '<b>Твоё назначение на проект № ' . $this->project->id . ' отменено!</b>'."\n\n";
            $msg .= '<b>ПОДРОБНОСТИ ПРОЕКТА</b>'."\n";
            $msg .= '<b>Дата:</b> ' . $this->project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') ."\n";
            $msg .= '<b>Время:</b> ' . $this->project->date_start->timezone(config('project.city_time_zone'))->format('H:i') ."\n";
            $msg .= '<b>Место:</b> ' . $this->project->place_cached . "\n";
            $msg .= '<b>Регион:</b> ' . $this->project->region_code. "\n";
            $msg .= 'Обязательно уточни информацию у координатора!!!' . "\n\n";
            $msg .= 'Выше нос, кусок мяса! Мы замутим свою съёмку с блекджеком и шлюхами!';

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

            sleep(1);

        return $TMsg;

    }
}
