<?php

namespace App\Notifications;

use Illuminate\Support\Collection;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\Channels\VkChannel;


abstract class ProjectCreateToClient extends Notification
{

    protected $project, $msg = '', $stiker = false;
    public $region = false;
    /**
     * Create a new notification instance.
     *
     * @param array $project
     * @return void
     */
    public function __construct($project)
    {
        $this->project = $project;
        $this->region = $project->region_code;

        $this->msg .= "Автоматическое уведомление.\n";
        $this->msg .= "==========\nВаша заявка занесена в систему распределения съемок, теперь вы можете можете быть уверены, что она не потерялась в большом количестве чатов и других переписок.\n";
        $this->msg .= "==========\nИнформация о вашем проекте:\n";

        $this->msg .= "Начало: ";
        $this->msg .= $project->date_start ? $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.Y H:i e') : NULL;

        $this->msg .= "\n";
        $this->msg .= "Конец: ";
        $this->msg .= $project->date_end ? $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : NULL;

        $this->msg .= "\n";
        $this->msg .= "Место: ";
        $this->msg .= $project->place_cached;

        $this->msg .= "\n";
        $this->msg .= "Тип: ";
        $this->msg .= config('project.category')[$project->category_id];

        $this->msg .= "\n";
        $this->msg .= "Описание: ";
        $this->msg .= $project->desc_public;

        $this->msg .= "\n";
        $this->msg .= "Бюджет: ";
        $this->msg .= $project->amount;

        $this->msg .= "\n";
        $this->msg .= "Вид оплаты: ";
        $this->msg .= config('project.payment_type_b')[$project->payment_type_id_b];

        if($project->ssmanager){
            $this->msg .= "\n";
            $this->msg .= "Назначен менеджер съёмки: ";
            $this->msg .= $project->ssmanager->user->name;
        }

        $this->msg .= "\n==========\n";
        $this->msg .= "Автоуведомления работают в тестовом режиме, если у вас есть какие то пожелания по работе этой системы, то сообщите нам и мы рассмотрим ваши предложения.\n ";
        $this->msg .= "\n==========\n";


    }
}
