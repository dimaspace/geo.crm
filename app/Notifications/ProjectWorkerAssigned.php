<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\Project;

class ProjectWorkerAssigned extends Notification implements ShouldQueue
{
    use Queueable;

    protected $project;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            $return[] = TelegramChannel::class;
        }
        if(in_array('email', $notify_channels)){
            $return[] = 'mail';
        }

        return $return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
                    ->subject('Вы назначены исполнителем')
                    ->greeting('Приветствуем!')
                    ->line('Вы назначены исполнителем на проект № ' . $this->project->id . '' . "<br>");

        $mail->line('Подробности о проекте вы можете узнать на своей панели в системе GeoCRM');

        return $mail;
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Слава роботам!</b>'."\n";
            $msg .= 'Ты назначен исполнителем на проект № ' . $this->project->id . '' . "\n";
            $msg .= 'Подробности о проекте можешь узнать на своей панели в системе GeoCRM';

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }
}
