<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class PasswordReset extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = ['mail'];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
           // $return[] = TelegramChannel::class;
        }

        return $return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Вы получили это письмо так как запросили восстановление пароля к своему аккаунту.')
            ->action('Изменить пароль', url('password/reset', $this->token))
            ->line('Если вы не запрашивали изменение пароля, то проигнорируйте это письмо.');
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Запрос восстановления пароля!</b>'."\n";
            $msg .= $notifiable->name;;
            $msg .= ', Вы запросили восстановление пароля к своему аккаунту!'."\n";
            $msg .= '<b>Ссылка для изменения пароля: </b>' . url('password/reset', $this->token) ."\n\n";
            $msg .= 'Если вы не запрашивали изменение пароля, то проигнорируйте это письмо.'."\n";

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }
}
