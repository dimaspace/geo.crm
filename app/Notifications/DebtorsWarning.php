<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\Project;
use App\User;

class DebtorsWarning extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            $return[] = TelegramChannel::class;
        }
        if(in_array('email', $notify_channels)){
            $return[] = 'mail';
        }

        return $return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
                    ->subject('Уведомление о долге')
                    ->greeting('Приветствуем!')
                    ->line('Вы привысили порог блокировки, в ближайшее время вас перестанут назначать на съёмки');


        $mail->line('Срочно пополните свой счёт!');

        return $mail;
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Уведомление о долге!</b>'."\n";
            $msg .= $notifiable->name;;
            $msg .= ', вы привысили порог блокировки!'."\n";
            $msg .= 'В ближайшее время вас перестанут назначать на съёмки!'."\n";
            $msg .= 'Срочно пополните свой счёт в системе GeoCRM!'."\n";
            $msg .= '<b>Ваш баланс:</b> '. $notifiable->balance() ."\n";
            $msg .= '<b>Порог блокировки:</b> -'. $notifiable->threshold ."\n";

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }
}
