<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

use NotificationChannels\Telegram\TelegramMessage;

use \App\Project;
use Spatie\Period\Period;
use Spatie\Period\Precision;


class ProjectDeadLineToWorker extends Notification implements ShouldQueue
{
    use Queueable;


    protected $project, $stiker, $msg;
    public $region = false;
    /**
     * Create a new notification instance.
     *
     * @param Project $project
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->region = $project->region_code;
        $this->stiker = false;

        $interval = date_diff($project->date_deadline, Carbon::now());

        $this->msg = 'Обращаем Ваше внимание, что вы не сдали материал по проекту # ' . $project->id .
         ' ' . $project->place_cached . ' от ' . $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.Y');

        $this->msg .=  "\n\n" . 'Просрочка Дедлайна на ' . $this->getIntervalForHumans($interval) . "\n\n";

        if(trim($project->geo_link) === '') {
            $this->msg .= '- Нет репортажа на сайте' . "\n";
        }

        if(trim($project->download_link) === '') {
            $this->msg .= '- Нет архива на материал в GeoCrm' . "\n\n";
        }

        $this->msg .= 'Обращаем Ваше внимание, что редакцией могут быть применены штрафные санкции за просрочку сдачи материала.' . "\n";

    }

    public function toTelegram($notifiable)
    {
        $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($this->msg)->options([
            'parse_mode' => 'HTML'
        ]);

        sleep(1);

        return $TMsg;
    }

    public function toVk($notifiable)
    {
        return ['stiker' => $this->stiker, 'text' => $this->msg];
    }

    protected function getIntervalForHumans($interval){
        $result = '';
        if($interval->y > 0){
            $result .= $interval->y .'г. ';
        }
        if($interval->m > 0){
            $result .= $interval->m .'м. ';
        }
        if($interval->d > 0){
            $result .= $interval->d .'д. ';
        }
        if($interval->h > 0){
            $result .= $interval->h .'ч. ';
        }
        if($interval->i > 0){
            $result .= $interval->i .'мин. ';
        }
        return $result;
    }
}
