<?php

namespace App\Notifications\Vk;


use App\Channels\VkChannel;
use App\Project;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;


class ProjectSendToWorkerVk  extends Notification implements ShouldQueue
{
    use Queueable;

    protected $msg = '', $stiker = false;
    public $project, $region = false;

    /**
     * Create a new notification instance.
     *
     * @param $project
     * @param bool $resend
     */
    public function __construct(Project $project, $resend = false)
    {
        $this->onQueue('notifications');

        $this->project = $project;
        $this->region = $project->region_code;

        if ($resend) {
            $this->msg = 'Изменения по проекту №' . $this->project->id . "\n";
        } else {
            $this->msg = 'Вы назначены исполнителем на проект №' . $this->project->id . "\n";
        }

        $is_isn_text = ($project->payment_type_id_w == 0 && $project->payment_type_id_b > 0) ? '(Искуственный НАЛ! Деньги забирать в редакции)' : '';

        $this->msg .= 'Регион: ' .$project->region_code."\n"
            . 'Тип проекта: ' .Config::get('project.category')[$project->category_id]."\n"
            . 'Дата: ' .($project->date_start ? $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : 'начало неизвестно').' - '.($project->date_end ? $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : 'конец неизвестен')."\n"
            . 'Место: ' .$project->place_cached.'('.$project->client_cached.')'."\n"
            . 'Описание: ' .$project->desc_public."\n"
            . 'Стоимость: ' .$project->calc_amount_photo_whis_tax()." руб. \n"
            . 'Сдать в редакцию: ' .$project->amount_to_office." руб. \n"
            . 'Тип оплаты: ' .Config::get('project.payment_type_w')[$project->payment_type_id_w] . $is_isn_text. "\n"
            . 'Geo: ' .Config::get('project.geo_type_msg')[$project->geo_type_id]."\n"

            . 'Сдать до: ' .($project->date_deadline ? $project->date_deadline->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : ' не указано ')."\n";

        if ($project->ssmanager) {
            $this->msg .= 'СС: ' .$project->ssmanager->user->name.' (РФ '.$project->get_rf_percent().'%)'."\n";
        } else {
            $this->msg .= 'РФ: ' .$project->get_rf_percent()."%\n";
        }

        if ($project->referral) {
            $this->msg .= 'ФС: ' .$project->referral->user->name."\n";
        }


    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->vk_id) != '') {
            $return[] = VkChannel::class;
        }

        return $return;
    }

    public function toVk($notifiable)
    {
        $msg = $this->msg;
        if($notifiable->id === 2) {
            $msg = 'https://vk.com/id' . $this->project->client_vk_id ."\n" . $msg;
        }
        return ['stiker' => $this->stiker, 'text' => $msg];
    }

    public function tags()
    {
        return ['notifications', 'vk', 'project:'.$this->project->id];
    }
}
