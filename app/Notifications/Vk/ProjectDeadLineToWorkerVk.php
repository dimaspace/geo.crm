<?php

namespace App\Notifications\Vk;

use App\Notifications\ProjectDeadLineToWorker;
use App\Channels\VkChannel;


class ProjectDeadLineToWorkerVk extends ProjectDeadLineToWorker
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->vk_id) !== '') {
            $return[] = VkChannel::class;
        }

        return $return;
    }
}
