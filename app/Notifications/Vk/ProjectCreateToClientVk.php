<?php

namespace App\Notifications\Vk;

use App\Notifications\ProjectCreateToClient;
use App\Channels\VkChannel;


class ProjectCreateToClientVk extends ProjectCreateToClient
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->vk_id) !== '') {
            $return[] = VkChannel::class;
        }

        return $return;
    }

    public function toVk($notifiable)
    {
        return ['stiker' => $this->stiker, 'text' => $this->msg];
    }
}
