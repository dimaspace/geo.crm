<?php

namespace App\Notifications\Vk;

use App\Notifications\ProjectWorkerAssignCanceled;

use App\Channels\VkChannel;


class ProjectWorkerAssignCanceledVk extends ProjectWorkerAssignCanceled
{
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if($notifiable->vk_id) {
            $return[] = VkChannel::class;
        }

        return $return;
    }
    public function toVk($notifiable)
    {
        $msg = 'Ваше назначение исполнителем на проект № ' . $this->project->id . '' . " отменено!\n\n";
        $msg .= 'ПОДРОБНОСТИ ПРОЕКТА' . "\n";
        $msg .= 'Дата: ' . $this->project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') ."\n";;
        $msg .= 'Время: ' . $this->project->date_start->timezone(config('project.city_time_zone'))->format('H:i') ."\n";
        $msg .= 'Место: ' . $this->project->place_cached . "\n";
        $msg .= 'Регион: ' . $this->project->region_code. "\n";
        $msg .= 'Обязательно уточните информацию у координатора!!!' . "\n\n";

        return ['stiker' => false, 'text' => $msg];
    }
}
