<?php

namespace App\Notifications\Vk;

use App\Channels\VkChannel;
use App\Social\VkTools;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class VkMsgDeny extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;
    public $region, $group_id, $group_name, $group_screen_name, $group_link, $group_dialog_link;

    /**
     * Create a new notification instance.
     *
     * @param null $region_code
     * @param null $group_id
     * @param null $group_name
     * @param null $group_screen_name
     */
    public function __construct($region_code = NULL, $group_id = NULL, $group_name = NULL, $group_screen_name = NULL)
    {
        $this->onQueue('notifications');

        if($region_code){
            $this->region = $region_code;
        }
        if($group_id){
            $this->group_id = $group_id;
            $this->group_dialog_link = VkTools::getGroupDialogLinkById($group_id);
        }
        if($group_name){
            $this->group_name = $group_name;
        }
        if($group_screen_name){
            $this->group_screen_name = $group_screen_name;
            $this->group_link = VkTools::getGroupLinkByScreenName($group_screen_name);
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {

        $return = [];
        if(trim($notifiable->vk_id) !== '') {
            $return[] = VkChannel::class;
        }

        return $return;
    }

    public function toVk($notifiable)
    {
        $msg = 'По данному вопросу, мы должны были Вам написать от @' . $this->group_screen_name . ' ('. $this->group_name .') , но с ней у Вас нет активных диалогов.'
            . ' Просим Вас начать диалог с ней ' . "\n"
        . $this->group_dialog_link;

        return ['stiker' => false, 'text' => $msg];
    }
}
