<?php

namespace App\Notifications\Vk;


use App\Notifications\ProjectSendToClient;
use App\Channels\VkChannel;


class ProjectSendToClientVk  extends ProjectSendToClient
{

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $return = [];
        if(trim($notifiable->vk_id) != '') {
            $return[] = VkChannel::class;
        }

        return $return;
    }

    public function toVk($notifiable)
    {
        $msg = $this->msg;
        return ['stiker' => $this->stiker, 'text' => $msg];
    }

    public function tags()
    {
        return ['notifications', 'vk', 'project:'.$this->project->id];
    }
}
