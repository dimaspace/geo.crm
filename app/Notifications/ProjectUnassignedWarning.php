<?php

namespace App\Notifications;

use Illuminate\Support\Collection;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

use App\Channels\VkChannel;


class ProjectUnassignedWarning extends Notification implements ShouldQueue
{
    use Queueable;

    protected $projects12, $msg = '', $stiker = false;
    /**
     * Create a new notification instance.
     *
     * @param array $projects12
     * @return void
     */
    public function __construct(Collection $projects12)
    {
        $this->projects12 = $projects12;

        if($this->projects12->count() > 0){
            //$this->stiker = 2472;
            $this->msg .= "ВНИМАНИЕ! Найдены проекты которым не назначены исполнители за 12 часов до начала\n";
            $this->msg .= "=======================\n";
            foreach($this->projects12 as $project){
                $this->msg .= "Начало: ";
                $this->msg .= $project->date_start ? $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.Y H:i e') : NULL;

                $this->msg .= "\n";
                $this->msg .= "Конец: ";
                $this->msg .= $project->date_end ? $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : NULL;

                $this->msg .= "\n";
                $this->msg .= "Место: ";
                $this->msg .= $project->place_cached;

                $this->msg .= "\n=\n";
            }
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(trim($this->msg) != ''){
            $return[] = VkChannel::class;
            if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
                $return[] = TelegramChannel::class;
            }
        }


        return $return;
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Мешки с костями просирают проект!</b>'."\n";
            $msg .= $this->msg;

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

            sleep(1);

        return $TMsg;

    }
    public function toVk($notifiable)
    {
        return ['stiker' => $this->stiker, 'text' => $this->msg];
    }
}
