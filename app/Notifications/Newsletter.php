<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Newsletter extends Notification  implements ShouldQueue
{
    use Queueable;

    private $email_msg;
    private $telegram_msg;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email_msg, $telegram_msg)
    {
        $this->email_msg = $email_msg;
        $this->telegram_msg = $telegram_msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $return = [];

        if($this->email_msg){
            $return[] = 'mail';
        }

        if($notifiable->telegram_chat_id && $this->telegram_msg){
            //$return[] = 'telegram';
        }

        return $return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject('Рассылка от администрации GeoRegions')->view(
        'emails.newsletter', ['text' => $this->email_msg]
        );
    }

    public function toTelegram($notifiable)
    {
            $msg = '<b>Рассылка от администрации</b>'."\n";
            $msg .= $this->telegram_msg;

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
