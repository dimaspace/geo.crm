<?php

namespace App\Notifications;

use App\Channels\VkChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Auth;

use App\User;

class VkMsgErrorWarningCord extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user, $region_code, $group_link, $msg;
    /**
     * Create a new notification instance.
     *
     * @param  mixed  $user
     * @param  mixed  $msg_text
     * @param  mixed  $region_code
     * @return void
     */
    public function __construct($user = NULL, $msg_text = NULL, $region_code = NULL)
    {
        $this->user = $user ?? Auth::user();
        $regions = config('project.regions');
        $def_region = $regions[0];
        $this->region_code = $region_code ?? $def_region;

        $this->group_link = config('project.vk_groups.' . $this->region_code);

        $this->msg = $this->user->name . ' не дал разрешение на получение сообщений от сообщества вконтакте!';
        if($msg_text){
            $this->msg .= "\nТекст неполученного сообщения: \n" . $msg_text;
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notify_channels = explode(',', $notifiable->notify_channels);
        $return = [];

        if(in_array('telegram', $notify_channels) && $notifiable->telegram_chat_id){
            //$return[] = TelegramChannel::class;
        }
        if(in_array('email', $notify_channels)){
            //$return[] = 'mail';
        }
        if(trim($notifiable->vk_id) != '') {
            //$return[] = VkChannel::class;
        }

        return $return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
                    ->subject('Сообщение не доставлено через Вконтакте')
                    ->greeting('Приветствуем!')
                    ->line($this->msg);
        return $mail;
    }

    public function toTelegram($notifiable)
    {
            $msg = nl2br($this->msg);

            $TMsg = TelegramMessage::create()->to($notifiable->telegram_chat_id)->content($msg)->options([
                    'parse_mode' => 'HTML'
                ]);

        return $TMsg;

    }

    public function toVk($notifiable)
    {
        return ['stiker' => false, 'text' => $this->msg];
    }
}
