<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RestClients\Moedelo\RestMoedeloKontragent;
use Cache;

class MoeDeloKontragent extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;

    static function getKontragentNameById($id){
        return Cache::tags(['md_kontragent_names'])->rememberForever('md_kontragent_name_'.$id, function () use($id) {
            return RestMoedeloKontragent::call('get_kontragent_by_id', [ 'id' => $id])->result->Name;
        });

    }
}
