<?php

namespace App;

use Config;
use Auth;
use DB;
use Cache;

//use Spiritix\LadaCache\Database\Model;
use Illuminate\Database\Eloquent\Model;
/**
 * Class BalanceTransaction
 * @package CawaKharkov\LaravelBalance\Models
 */
class BalanceTransaction extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'hash',
        'type',
        'accepted',
        'user_id'
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'    =>  'integer',
        'value' => 'float'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    const CONST_TYPE_WORKER = 100;
    const CONST_TYPE_TAX = 200;
    const CONST_TYPE_ED_FEE = 300;
    const CONST_TYPE_FS = 400;
    const CONST_TYPE_SS = 500;


    const CONST_TYPE_BONUS = 2000;
    const CONST_TYPE_PENALTY = 2100;
    const CONST_TYPE_CORRECT = 2200;
    const CONST_TYPE_OTHER = 10000;
    /*const CONST_TYPE_ = 600;*/
    /**
     * Transactions labels
     * @var array
     */
    private $typeLabels = [
        self::CONST_TYPE_WORKER => 'Оплата исполнителю',
        self::CONST_TYPE_TAX => 'Налоги',
        self::CONST_TYPE_ED_FEE => 'РедСбор',
        self::CONST_TYPE_FS => 'ФС(Реферрал)',
        self::CONST_TYPE_SS => 'Менеджер СС',
        self::CONST_TYPE_BONUS => 'Бонус',
        self::CONST_TYPE_PENALTY => 'Штраф',
        self::CONST_TYPE_CORRECT => 'Коррекция',
        self::CONST_TYPE_OTHER => 'Другое'
    ];

    public function user()
    {
       return $this->belongsTo('App\User');
    }

    public function cross()
    {
       return $this->HasOne('App\BalanceTransaction', 'cross_t_id');
    }


    static function getUserIdByTransactionId($id){
        return Cache::tags(['transactions_user_id'])->rememberForever('transactions_user_id_'.$id, function () use($id) {
            return BalanceTransaction::findOrNew($id, ['user_id'])->user_id;
        });

    }

    public function cash_flow()
    {
       return $this->belongsTo('App\CashFlow', 'cash_flow_id');
    }

    public function scopeNotAccepted($query)
    {
        return $query->where('accepted', 0);
    }

    public function scopeAccepted($query)
    {
        return $query->where('accepted', 1);
    }

    public function scopeByTypeCode($query, $code)
    {
        return $query->where('type', self::_getTypeIdByCode($code));
    }

    public function descCompiled(){
        return self::descCompile($this);
    }

    static function descCompile($transaction){
        $desc = $transaction->desc;

        $tag ='{*USER_T_CROSS*}';
        if(strpos($desc, $tag) !== false){
            if($transaction->cross){
                $cross_user = $transaction->cross->user;
                $str = '<b>' . $cross_user->name . '</b>';
                $desc =str_replace($tag, $str, $desc );
            }
        }

        $tag ='{*VALUE*}';
        if(strpos($desc, $tag) !== false){
            $desc =str_replace($tag, abs($transaction->value), $desc );
        }

        $tag ='{*CASH_FLOW_ID*}';
        if(strpos($desc, $tag) !== false){
            $desc =str_replace($tag, abs($transaction->cross->cash_flow_id), $desc );
        }

        if($transaction->cross && $transaction->cross->user){
            $cross_user = $transaction->cross->user;
            if($cross_user->vk_id){
                $desc .= '<h5>Контакты для связи</h5>';
                $desc .=  'Страница Вконтакте: <a href="http://vk.com/id' . $cross_user->vk_id . '" target="_new">&nbsp;http://vk.com/id' . $cross_user->vk_id . '</a>';
            }
        }

        return $desc;
    }

    public function accept(){
        $this->accepted = 1;
        $this->update();
    }

    /**
     * Get type label
     * @return mixed
     */
    public function getType()
    {
        return Config::get('transaction.types')[array_flip(Config::get('transaction.type_codes'))[$this->type]];
    }
    public function getTypeCode()
    {
        return array_flip(Config::get('transaction.type_codes'))[$this->type];
    }
    public function getTypeIcon()
    {
        return Config::get('transaction.type_icons')[array_flip(Config::get('transaction.type_codes'))[$this->type]];
    }
    public function getTypeColor()
    {
        return Config::get('transaction.type_colors')[array_flip(Config::get('transaction.type_codes'))[$this->type]];
    }
    public function getCrTypeCode()
    {
        return array_flip(Config::get('transaction.cr_types'))[$this->cr_type];
    }
    public function getTypes()
    {
        return Config::get('transaction.types');
    }
    static function _getTypes()
    {
        return Config::get('transaction.types');
    }
    static function _getTypeIdByCode($code)
    {
        return Config::get('transaction.type_codes')[$code];
    }
    static function _getCrTypeIdByCode($code)
    {
        return Config::get('transaction.cr_types')[$code];
    }
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    static function make_payment($value, $from, $to, $project_id = NULL, $type = 'OTHER', $cr_type = 'TRIGGER', $desc = '', $accepted = 1, $cash_flow_id = 0){
        DB::transaction(function () use($value, $from, $to, $project_id, $type, $cr_type, $desc, $accepted, $cash_flow_id) {
            $transaction_1 = new BalanceTransaction();
            $transaction_2 = new BalanceTransaction();
            $user1 = User::findOrNew((int) $to);
            $user2 = User::findOrNew((int) $from);

            $transaction_1->value = $value;
            $transaction_1->project_id = $project_id;
            $transaction_1->type = self::_getTypeIdByCode($type);
            $transaction_1->cr_type = self::_getCrTypeIdByCode($cr_type);
            $transaction_1->desc = is_array($desc) ? $desc[0] : $desc;
            $transaction_1->accepted = $accepted;
            $transaction_1->user_id = (int) $to;
            $transaction_1->creator_id = Auth::user() ? Auth::user()->id : 0;
            $transaction_1->last_balance = $user1->balance();
            $transaction_1->now_balance = $user1->balance() + $value;
            $transaction_1->cash_flow_id = is_array($cash_flow_id) ? $cash_flow_id[0] : $cash_flow_id;
            $transaction_1->save();

            $transaction_2->value = -$value;
            $transaction_2->project_id = $project_id;
            $transaction_2->type = self::_getTypeIdByCode($type);
            $transaction_2->cr_type = self::_getCrTypeIdByCode($cr_type);
            $transaction_2->desc =  is_array($desc) ? $desc[1] : $desc;
            $transaction_2->accepted = $accepted;
            $transaction_2->user_id = (int) $from;
            $transaction_2->creator_id =  Auth::user() ? Auth::user()->id : 0;
            $transaction_2->last_balance = $user2->balance();
            $transaction_2->now_balance = $user2->balance() - $value;
            $transaction_2->cross_t_id = $transaction_1->id;
            $transaction_2->cash_flow_id = is_array($cash_flow_id) ? $cash_flow_id[1] : $cash_flow_id;
            $transaction_2->save();

            $transaction_1->cross_t_id = $transaction_2->id;
            $transaction_1->save();

        });
    }
}