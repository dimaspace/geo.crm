<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MoeDeloSaleBill;
use App\RestClients\Moedelo\RestMoedeloSaleBill;
use Carbon\Carbon;
use Storage;

class MoeDeloLoad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moedelo:load {--date=} {--log=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных из сервиса МоёДело';

    var $log = false;
    var $log_path = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->option('date');
        $this->log = (int) $this->option('log');
        if($this->log){
            $this->log_path = '/public/moedelo_update_logs/' . $this->log . '.txt';
            $this->write_log(1);
        }

        if($date != null){
            $date = Carbon::createFromFormat('d.m.Y', $date, config('project.city_time_zone'))->startOfDay();
        }else{
            $date = Carbon::now(config('project.city_time_zone'))->startOfDay()->subMonths(2);
        }

        $date_formated = $date->format('Y-m-d\TH:i:s' . ' ');

        echo "Loading bills after {$date_formated}\n";
        $bill_result = RestMoedeloSaleBill::call('get_bills', [ 'afterDate' => $date_formated]);
        $bills = $bill_result->result->ResourceList;
        unset($bill_result);
        $bills_count = is_array($bills) ? count($bills) : 0;

        $progress = 1;
        $this->write_log($progress);

        $unpaid_bills = MoeDeloSaleBill::where('status_id', '<>', 6)->get()->keyBy('id');

        $progress_step = $bills_count ? 70/$bills_count : 70;

        foreach($bills as $bill){

            echo "Loading bill #{$bill->Id} ";
            $bill_data = RestMoedeloSaleBill::call('get_bill_by_id', [ 'id' => $bill->Id]);
            if($bill_data->code !== 404){
                $bill = $bill_data->result;
                $progress = $progress + $progress_step;
                $this->write_log($progress);

                $local_bill = MoeDeloSaleBill::find($bill->Id);
                if($local_bill){
                    echo " [Update] \n";
                    $local_bill->doc_create_date = $bill->Context->CreateDate ? Carbon::createFromFormat(Carbon::ATOM, $bill->Context->CreateDate) : null;
                    $local_bill->doc_modify_date = $bill->Context->ModifyDate ? Carbon::createFromFormat(Carbon::ATOM, $bill->Context->ModifyDate) : null;
                    $local_bill->doc_modify_user = $bill->Context->ModifyUser;
                    if($local_bill->is_force_closed === 0){
                        $local_bill->status_id = (int) $bill->Status;
                    }
                    $local_bill->is_covered = (int) $bill->IsCovered ? 1 : 0;
                    $local_bill->sum = (float) $bill->Sum;
                    $local_bill->paid_sum = (float) $bill->PaidSum;

                    $unpaid_bills->forget($bill->Id);
                }else{
                    echo " [Add] \n";
                    $local_bill = new MoeDeloSaleBill();
                    $local_bill->id = (int) $bill->Id;
                    $local_bill->number = (int) $bill->Number;
                    $local_bill->doc_date = $bill->DocDate ? Carbon::createFromFormat('Y-m-d', $bill->DocDate) : null;
                    $local_bill->doc_dd_date = $bill->DeadLine ? Carbon::createFromFormat('Y-m-d', $bill->DeadLine) : null;
                    $local_bill->doc_create_date = $bill->Context->CreateDate ? Carbon::createFromFormat(Carbon::ATOM, $bill->Context->CreateDate) : null;
                    $local_bill->doc_modify_date = $bill->Context->ModifyDate ? Carbon::createFromFormat(Carbon::ATOM, $bill->Context->ModifyDate) : null;
                    $local_bill->doc_modify_user = $bill->Context->ModifyUser;
                    $local_bill->online = $bill->Online;
                    $local_bill->type_id = (int) $bill->Type;
                    $local_bill->status_id = (int) $bill->Status;
                    $local_bill->kontragent_id = (int) $bill->KontragentId;
                    $local_bill->project_id = (int) $bill->ProjectId;
                    $local_bill->info = $bill->AdditionalInfo;
                    $local_bill->nds_id = (int) $bill->NdsPositionType;
                    $local_bill->is_covered = (int) $bill->IsCovered ? 1 : 0;
                    $local_bill->sum = (float) $bill->Sum;
                    $local_bill->paid_sum = (float) $bill->PaidSum;
                }
                $local_bill->save();
                unset($bill);
            }else{
                echo " [KILL BILL] \n";
            }
            unset($bill_data);
            sleep(2);
        }

        // Check unpaid
        $progress = 71;
        $this->write_log($progress);

        $unpaid_bills_count = $unpaid_bills->count();
        $progress_step = $unpaid_bills_count ? 29/$unpaid_bills_count : 29;

        echo "\nChecking old unpaid bills\n";
        foreach($unpaid_bills as $bill_id => $bill){

            echo "Loading bill #{$bill_id} \n";
            $bill_data = RestMoedeloSaleBill::call('get_bill_by_id', [ 'id' => $bill_id]);
            if($bill_data->code !== 404) {
                $bill = $bill_data->result;

                $progress = $progress + $progress_step;
                $this->write_log($progress);

                $local_bill = MoeDeloSaleBill::find($bill_id);

                $local_bill->doc_create_date = $bill->Context->CreateDate ? Carbon::createFromFormat(Carbon::ATOM, $bill->Context->CreateDate) : null;
                $local_bill->doc_modify_date = $bill->Context->ModifyDate ? Carbon::createFromFormat(Carbon::ATOM, $bill->Context->ModifyDate) : null;
                $local_bill->doc_modify_user = $bill->Context->ModifyUser;
                if($local_bill->is_force_closed === 0) {
                    $local_bill->status_id = (int) $bill->Status;
                }
                $local_bill->is_covered = (int) $bill->IsCovered ? 1 : 0;
                $local_bill->sum = (float) $bill->Sum;
                $local_bill->paid_sum = (float) $bill->PaidSum;

                $local_bill->save();
                unset($bill);
            }else{
                echo "KILL BILL!!! #{$bill_id} \n";
                $local_bill = MoeDeloSaleBill::find($bill_id);
                if($local_bill){
                    $local_bill->delete();
                }
            }
            unset($bill_data);
            sleep(2);
        }

        $this->write_log(101);
    }

    protected function write_log($progress){
        if($this->log){
            Storage::disk('local')->put($this->log_path, $progress);
        }
    }
}
