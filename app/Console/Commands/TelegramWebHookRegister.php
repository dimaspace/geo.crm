<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class TelegramWebHookRegister extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tbot:webhook {--info} {--host=} {--port=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Добавление вебхука телеграм-бота';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $token  =  env('TELEGRAM_BOT_TOKEN');

        if($this->option('info')){
            $url = 'https://api.telegram.org/bot' . $token . '/getWebhookInfo';
            $this->_requestGet($url);
        }else{
            $url = 'https://api.telegram.org/bot' . $token . '/setWebhook';
            $web_hook_url =
                // host
                ($this->option('host')
                    ? 'https://' . $this->option('host')
                    : env('APP_URL')
                )
                // port
                . ($this->option('port')
                    ? ':' . $this->option('port')
                    : ''
                )
                // query
                . '/bot';

            $this->_requestSet($url, $web_hook_url);
        }

    }

    /**
     * @param string $url
     * @param string $web_hook_url
     */
    protected function _requestSet(string $url, string $web_hook_url): void
    {
        $client = new Client();
        try {
            $this->info("\nRegistering url " . $web_hook_url . "  ...\n\n");

            $response = $client->post($url, [
                'form_params' => [
                    'url' => $web_hook_url
                ]
            ]);

            $body = $response->getBody();
            $body_json = json_decode($body);

            if(isset($body_json->ok) && $body_json->ok == true){
                $this->info($body_json->description);
            }else{
                $this->error('ERROR!');
                $this->error($body_json->description);
            }

        } catch (ClientException $e) {
            $error =  "Error!\n\n";
            $error .= "Code: ".$e->getcode()."\n\n";
            $error .= "Reason: ".$e->getMessage()."\n";
            $this->error($error);
        }
    }

    /**
     * @param string $url
     */
    protected function _requestGet(string $url): void
    {
        $client = new Client();
        try {
            $response = $client->get($url);

            $body = $response->getBody();
            $body_json = json_decode($body);

            if(isset($body_json->ok) && $body_json->ok == true){
                dd($body_json->result);
            }

        } catch (ClientException $e) {
            echo "Error!\n\n";
            echo "Code: ".$e->getcode()."\n\n";
            echo "Reason: ".$e->getMessage()."\n";
        }
    }
}
