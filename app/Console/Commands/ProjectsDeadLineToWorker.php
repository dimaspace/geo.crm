<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Project;
use Notification;
use App\Notifications\Vk\ProjectDeadLineToWorkerVk;
use App\Notifications\Telegram\ProjectDeadLineToWorkerTelegram;

class ProjectsDeadLineToWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:dl_worker_send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Project::DeadlineFailed()->each(function ($project) {

            if($project->worker_id > 0) {
                Notification::send($project->worker(), new ProjectDeadLineToWorkerVk($project));
                Notification::send($project->worker(), new ProjectDeadLineToWorkerTelegram($project));
            }

        });
    }
}
