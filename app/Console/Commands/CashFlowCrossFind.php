<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CashFlow;

class CashFlowCrossFind extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cashflow:crossfind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $check = true;
        while($check){
            //print "\nmain loop\n";
            $check = CashFlow::crossFind('many');
        }
    }
}
