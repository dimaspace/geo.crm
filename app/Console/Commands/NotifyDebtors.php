<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Notification;
use App\Notifications\DebtorsWarning;

class NotifyDebtors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:debtors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::all()->each(function ($user) {
            if($user->isDebt()){
                Notification::send(User::findOrFail($user->id), new DebtorsWarning($user));
            }
        });
    }
}
