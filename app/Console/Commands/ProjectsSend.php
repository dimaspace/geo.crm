<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Client;
use App\Project;
use Notification;
use App\Notifications\Vk\ProjectSendToClientVk;
use App\Notifications\Email\ProjectSendToClientEmail;

class ProjectsSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:client_send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Project::Deadline2FiredForClient()->each(function ($project) {
            $client = new Client();
            $client->vk_id = trim($project->client_vk_id) ? $project->client_vk_id : '';
            $client->email = trim($project->client_email) ? $project->client_email : '';

            Notification::send($client, new ProjectSendToClientVk($project));
            Notification::send($client, new ProjectSendToClientEmail($project));

            //$project->client_notified = 1;
            //$project->pass_type_id = 2;
            //$project->update();

            //$project->close();
        });
    }
}
