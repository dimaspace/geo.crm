<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Project;
use Ramsey\Uuid\Uuid;

class ProjectsCreateUuid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:create_uuid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создание uuid для старых проектов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         Project::withUuid('')->chunk(300, function ($projects){
            $projects->each(function($project) {
                $project->uuid = (string) Uuid::uuid4();
                if (Project::withUuid($project->uuid)->count() > 0) {
                    exit('Попытка получить неуникальный UUID для проекта #'.$project->id.': '.$project->uuid);
                } else {
                    print '#'.$project->id . " add uuid\n";
                    $project->save();
                }
            });
        });

    }
}
