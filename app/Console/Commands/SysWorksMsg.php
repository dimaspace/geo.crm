<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Notification;
use App\Notifications\SysWorks;

class SysWorksMsg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:sysWorks {--a|ask} {--t|time=} {--title=} {--msg=} {--msg2=} {--color=} {--icon=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $title = 'Системное уведомление';
        $msg = 'Через <b>{time}</b> минут будет запущено обновление системы GeoCrm на сервере! В процессе обновления система может работать нестабильно. Рекомендуем сохранить все изменения до того, как начнётся процесс обновления.';
        $msg2 = 'На сервере GeoCrm начались технические работы. Настоятельно не рекомендуем работать в это время в системе.';
        $icon = 'fa-exclamation-triangle';

        $is_ask = $this->option('ask');
        $title = $this->option('title') ?? $title;
        $msg = $this->option('msg') ?? $msg;
        $msg2 = $this->option('msg2') ?? $msg2;
        $time = $this->option('time') ?? 5;
        $color = $this->option('color') ?? 'deep-orange';
        $icon = $this->option('icon') ?? $icon;

        if((boolean)$is_ask === true){
            $title = $this->ask('Заголовок сообщения', false) ?? $title;
            $msg = $this->ask('Сообщение', false) ?? $msg;
            $msg2 = $this->ask('Второе сообщение', false) ?? $msg2;
            $time = (int) $this->ask('Сколько минут выполнять ожидание') ?? $time;
            $color = $this->choice('Цвет сообщения', ['deep-orange', 'red', 'teal'],0);
            $icon = $this->ask('Какую использовать иконку', false) ?? $icon;
        }

        if($icon){
            $icon = 'fa ' . $icon;
        }

        User::all()->each(function ($user) use ($title, $msg, $msg2, $time, $color, $icon){
            Notification::send(User::findOrFail($user->id), new SysWorks($msg, $title, $time, $color, $msg2, $icon));
        });
    }
}
