<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Carbon\Carbon;
use App\Role;
use App\Project;

use Notification;
use App\Notifications\ProjectUnassignedWarning;
use App\Notifications\ProjectUnassignedNum;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\CashFlowCrossFind'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:clean')->daily()->at('05:00')->timezone(config('project.city_time_zone'));
        $schedule->command('backup:run')->daily()->at('06:00')->timezone(config('project.city_time_zone'));

        $schedule->call(function () {
            $cfg_worker_unassigned_status = config('project.worker_unassigned_status');
            $projects12 = Project::whereIn('status_id', $cfg_worker_unassigned_status)->where('date_start_real', '<=', Carbon::now()->addHours(12))->get();

            $boses = Role::whereName('boss')->first()->users()->get();
            foreach($boses as $boss) {
                Notification::send($boss, new ProjectUnassignedWarning($projects12));
                sleep(5);
            }
        })->hourly();

        $schedule->call(function () {
            $cfg_worker_unassigned_status = config('project.worker_unassigned_status');
            $projects24 = Project::whereIn('status_id', $cfg_worker_unassigned_status)->where('date_start_real', '<=', Carbon::now()->addHours(24));
            $projects72 = Project::whereIn('status_id', $cfg_worker_unassigned_status)->where('date_start_real', '<=', Carbon::now()->addHours(72));

            if($projects24->count() > 0 || $projects72->count() > 0 ) {
                $boses = Role::whereName('boss')->first()->users()->get();
                foreach ($boses as $boss) {
                    Notification::send($boss, new ProjectUnassignedNum($projects24, $projects72));
                    sleep(5);
                }
            }
        })->twiceDaily(8, 20)->timezone(config('project.city_time_zone'));

        $schedule->command('projects:client_send')
            ->everyThirtyMinutes()
            ->between('0:01', '23:59')->timezone(config('project.city_time_zone'));

        $schedule->command('cashflow:rebuild')
            ->daily()->at('07:00')->timezone(config('project.city_time_zone'));

        // Horizon snapshot
        $schedule->command('horizon:snapshot')->everyFiveMinutes();

        // напоминание исполнителям о просрочке дедлайна
        $schedule->command('projects:dl_worker_send')->cron('0 */2 * * *');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
        $this->load(__DIR__.'/Commands');
    }
}
