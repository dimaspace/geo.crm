<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;

class ProjectReferral extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;
    protected $fillable = ['user_id', 'royalty'];

    protected $with = ['user'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
