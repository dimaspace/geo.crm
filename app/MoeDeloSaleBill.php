<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoeDeloSaleBill extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;

    protected $dates = [
        'created_at',
        'updated_at',
        'doc_date',
        'doc_dd_date',
        'doc_create_date',
        'doc_modify_date'
    ];

}
