<?php

namespace App\Channels;

use dimaspace\VKAPI\VKAPIException;
use Illuminate\Notifications\Notification;
use \Exception;
use VKAPI;
use Bugsnag;
use App\Social\VkTools;
use App\Events\Notification\Vk\VkDenied;


class VkChannel
{

    /**
     * Отправить данное уведомление.
     *
     * @param mixed $notifiable
     * @param Notification $notification
     * @return mixed
     * @throws Exception
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toVk($notifiable);
        $msg_text = $message['text'] ?? '';
        $msg_stiker = $message['stiker'] ?? false;

        $region_code = (isset($notification->region) && $notification->region) ? $notification->region : self::get_def_region();

        VKAPI::setAccessToken(config()->get('project.vk_tokens')[$region_code]);

        $check_allow = VkTools::checkMsgAllowByRegion($notifiable->vk_id, $region_code);
        if ($check_allow === false) {
            $allowed_region_code = self::find_allowed($notifiable->vk_id);
            VKAPI::setAccessToken(VkTools::getTokenByRegionCode($allowed_region_code));
            event(new VkDenied($notifiable->vk_id, $region_code, $allowed_region_code));
        }

        if ($notifiable->vk_id > 0 && trim($msg_text) !== '') {
            if ($msg_stiker !== false) {
                try {
                    return VKAPI::call('messages.send', ['peer_id' => $notifiable->vk_id, 'sticker_id' => $msg_stiker, 'v' => '5.38', 'dont_parse_links' => 1]);
                } catch (VKAPIException $e) {

                }
                sleep(1);
            }

            try {
                return VKAPI::call('messages.send', ['peer_id' => $notifiable->vk_id, 'message' => $msg_text, 'v' => '5.38', 'dont_parse_links' => 1]);
            } catch (VKAPIException $e) {
                Bugsnag::notifyException($e);
            }
        }
        return [];
    }

    public static function get_def_region()
    {
        $regions = config('project.regions');
        return $regions[0];
    }

    public static function find_allowed($user_id)
    {
        $group_tokens = config()->get('project.vk_tokens');
        $allowed = [];
        foreach ($group_tokens as $region_code => $token) {
            VKAPI::setAccessToken($token);
            if (VkTools::checkMsgAllowByRegion($user_id, $region_code)) {
                $allowed[] = $region_code;
            }
        }
        if (count($allowed) < 1) {
            // TODO Уведомлять о недоступности отправки сообщений пользователю
            throw new Exception('Нет доступа ни в одну группу');
        }
        return $allowed[0];
    }

}