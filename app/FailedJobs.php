<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FailedJobs extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;

    protected $dates = [
        'failed_at'
    ];

    protected $table = 'failed_jobs';

}
