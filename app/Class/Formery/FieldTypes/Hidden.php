<?php

namespace App\Formery\FieldTypes;

use Formery\Field;

class Hidden extends Field
{
    protected $input_type = 'hidden';
}