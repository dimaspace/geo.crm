<?php
namespace App\Tools;
class Hasher
{

    /**
     * Generate a random hash.
     *
     * @param integer $length
     *
     * @return string
     */
    public static function generate($length = 6): string
    {
        $characters = str_repeat('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', $length);
        return substr(str_shuffle($characters), 0, (int)$length);
    }
}