<?php

namespace App\Tools;

use App\BalanceTransaction;
use App\CashFlow;

class TFinance
{
    static function rebuildCashFlows(){
        $cf_carried = BalanceTransaction::select('cash_flow_id')
            ->where('cr_type', 300)
            ->where('cash_flow_id', '>', 0)->groupBy('cash_flow_id')
            ->get();

        $cash_flows = CashFlow::all();

        foreach($cash_flows as $cash_flow){
            $carried = $cash_flow->carried;
            $real_carried = $cash_flow->transactions()->sum('value');
            $real_carried_abs = abs($real_carried);

            if($carried != $real_carried_abs){
                var_dump($carried - $real_carried_abs);
                echo '#' . $cash_flow->id . ' FIXED carried ' . $carried . ' -> ' . $real_carried_abs . "\n";
                $cash_flow->carried = $real_carried_abs;
                $cash_flow->update();
            }
        }
    }
}