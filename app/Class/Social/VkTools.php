<?php

namespace App\Social;

use dimaspace\VKAPI\Facades\VKAPI;
use dimaspace\VKAPI\VKAPIException;
use Cache;
use Carbon\Carbon;

class VkTools
{
    public static function getUserId($idOrSlug){
        try {
            $result = VKAPI::call('users.get', ['user_ids' => $idOrSlug]);
            $user_data = $result[0] ?? false;
            $return = false;
            if($user_data && isset($user_data['id'])){
                $return = $user_data['id'];
            }
            return $return;
        } catch(VKAPIException $e) {
            return false;
        }
    }

    public static function getUserName($idOrSlug){
        $cache_lifetime = Carbon::now()->addDays(10);
        return Cache::tags(['vk_user_names'])->remember('vk_user_name_'.$idOrSlug, $cache_lifetime, static function () use($idOrSlug) {
            try {
                $result = VKAPI::call('users.get', [ 'user_ids' => $idOrSlug, 'fields' => 'first_name,last_name' ]);
                $result = $result[0] ?? [];

                $user_name = (isset($result['first_name'], $result['last_name']))
                    ? $result['first_name'].' '.$result['last_name'] : 'Имя не получено';
                return $user_name;
            } catch (VKAPIException $e) {
                return false;
            }
        });
    }

    public static function getUserInfo($idOrSlug){
        try {
            $result = VKAPI::call('users.get', ['user_ids' => $idOrSlug, 'fields' => 'photo_50']);
            return $result[0] ?? false;
        } catch(VKAPIException $e) {
            return false;
        }
    }

    public static function getUserIdByLink($link){
        $idOrSlug = false;
        if(preg_match('~^([a-zA-Z0-9_.]+)$~', $link, $matches)){
            $idOrSlug = $matches[1] ?? false;
        }elseif(preg_match('~/([a-zA-Z0-9_.]+)$~', $link, $matches)){
            $idOrSlug = $matches[1] ?? false;
        }
        if($idOrSlug){
            return self::getUserId($idOrSlug);
        }

        return false;

    }

    /**
     * @param string $group_id
     *
     * @return string
     */
    public static function getGroupNameById($group_id): string
    {
        $cache_lifetime = Carbon::now()->addDays(10);
        return Cache::tags(['vk_group_names'])->remember('vk_group_name_'.$group_id, $cache_lifetime, static function () use($group_id) {
            $result = VKAPI::call('groups.getById', ['group_id' => $group_id, 'fields' => 'name']);
            $result = $result[0] ?? [];

            return $result['name'] ?? 'Имя не получено';
        });
    }

    /**
     * @param string $group_id
     *
     * @return string|bool
     */
    public static function getGroupScreenName($group_id){
        $cache_lifetime = Carbon::now()->addDays(10);
        return Cache::tags(['vk_group_links'])->remember('vk_group_link_'.$group_id, $cache_lifetime, static function () use($group_id) {
            $result = VKAPI::call('groups.getById', ['group_id' => $group_id, 'fields' => 'screen_name']);
            $result = $result[0] ?? [];

            return $result['screen_name'] ?? false;
        });
    }

    /**
     * @param $screen_name
     * @return string|bool
     */
    public static function getGroupLinkByScreenName($screen_name){
        return 'http://vk.com/' . $screen_name;
    }

    /**
     * @param $group_id
     * @return string|bool
     */
    public static function getGroupDialogLinkById($group_id){
        return 'https://vk.com/im?sel=-' . $group_id;
    }

    /**
     * @param string $region_code
     *
     * @return integer
     */
    public static function getGroupIdByRegion($region_code): int
    {
        $groups_id = config('project.vk_groups_id');
        return $groups_id[$region_code] ?? 0;
    }


    /**
     * @param integer $vk_user_id
     * @param string $region_code
     *
     * @return bool
     */
    public static function checkMsgAllowByRegion($vk_user_id, $region_code): bool
    {
        $vk_user_id = (int) $vk_user_id;
        $vk_group_id = self::getGroupIdByRegion($region_code);

        self::setTokenByRegionCode($region_code);

        if($vk_group_id > 0){
            $result = VKAPI::call('messages.isMessagesFromGroupAllowed', [
                'group_id' => $vk_group_id,
                'user_id' => $vk_user_id
            ]);
        }else{
            return false;
        }

        return isset($result['is_allowed']) && $result['is_allowed'] === 1;
    }



    /**
     * @param string $region_code
     *
     * @return bool|string
     */
    public static function setToken($token)
    {
        VKAPI::setAccessToken($token);
    }



    /**
     * @param string $region_code
     *
     * @return bool|string
     */
    public static function setTokenByRegionCode($region_code)
    {
        self::setToken(self::getTokenByRegionCode($region_code));
    }

    /**
     * @param string $region_code
     *
     * @return bool|string
     */
    public static function getTokenByRegionCode($region_code)
    {
        $tokens = config()->get('project.vk_tokens');
        return $tokens[$region_code] ?? false;
    }
}