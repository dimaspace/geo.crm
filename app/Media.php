<?php

namespace App;

use Spatie\MediaLibrary\Models\Media as BaseMedia;
use Overtrue\LaravelFollow\Traits\CanBeVoted;
use Cache;

class Media extends BaseMedia
{
    use \Spatie\Tags\HasTags;
    use CanBeVoted;


    protected function getCacheKeys(){
        $cache_keys = [];

        $cache_keys['approved_status'] = 'media'. $this->id. '_approved_status';
        $cache_keys['quorum_status'] = 'media'. $this->id. '_quorum_status';
        $cache_keys['voters_count'] = 'media'. $this->id. '_voters_count';
        $cache_keys['up_voters_count'] = 'media'. $this->id. '_up_voters_count';
        $cache_keys['my_vote'] = 'media'. $this->id. 'user_' .auth()->user(). '_vote';
        $cache_keys['all_approved'] = 'medias_all_approved';
        $cache_keys['tags_used'] = 'tags_used';
        return $cache_keys;
    }

    public function killCache(){
        $cache_keys = collect($this->getCacheKeys());
        $cache_keys->each(function($item){
            Cache::forget($item);
        });

    }

    public function scopeCollection($query, $collection_name = null){
        if($collection_name){
            return $query->where('collection_name', $collection_name);
        }else{
            return $query;
        }
    }

    public function getIsApprovedAttribute(){
        $cache_key =  $this->getCacheKeys()['approved_status'];
            $result = Cache::remember($cache_key, 60*12, function(){
            $up_voters = $this->upvoters()->count();
            $voters = $this->voters()->count();
            if($voters >= config('portfolio.min_vote', 5) && $up_voters >= $voters - $up_voters ){
                return true;
            }else{
                return false;
            }
        });

        return $result;
    }

    public function scopeApproved($query)
    {
        $cache_key =  $this->getCacheKeys()['all_approved'];
        $approved_media_ids = Cache::remember($cache_key, 60 * 12, function () {
            return Media::get()->filter(function ($media) {
                return $media->is_approved;
            })->pluck('id')->toArray();
        });

        return $query->whereIn('id', $approved_media_ids);
    }

    public function getIsQuorumAttribute(){
        $cache_key =  $this->getCacheKeys()['quorum_status'];
        $result = Cache::remember($cache_key, 60*12, function(){
            return ($this->voters()->count() >= config('portfolio.min_vote', 5));
        });

        return $result;
    }

    public function getVotersCountAttribute(){
        $cache_key =  $this->getCacheKeys()['voters_count'];
        $result = Cache::remember($cache_key, 60*12, function(){
            return $this->voters()->count();
        });

        return $result;
    }

    public function getUpVotersCountAttribute(){
        $cache_key =  $this->getCacheKeys()['up_voters_count'];
        $result = Cache::remember($cache_key, 60*12, function(){
            return $this->upvoters()->count();
        });

        return $result;
    }

    public function getMyVoteAttribute(){
        $cache_key =  $this->getCacheKeys()['my_vote'];
        $result = Cache::remember($cache_key, 60*12, function(){
            $u = auth()->user();
            return $this->isVotedBy($u) ? ($this->isUpvotedBy($u) ? 1 : -1) : 0;
        });

        return $result;
    }
}
