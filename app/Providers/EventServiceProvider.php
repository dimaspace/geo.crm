<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Mail\Events\MessageSending;
use App\Listeners\MessageSendingCustomHeader;
use App\Listeners\MonitoringAuthenticated;
use Illuminate\Auth\Events\Authenticated;
use App\Events\Notification\Vk\VkDenied;
use App\Listeners\Vk\VkDeniedUserNotification;
use App\Listeners\Vk\VkDeniedAdminNotification;
use App\Listeners\Notification\NotificationEventSubscriber;
use App\Listeners\Project\ProjectNotificationEventSubscriber;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        MessageSending::class => [
            MessageSendingCustomHeader::class
        ],
        Authenticated::class => [
            MonitoringAuthenticated::class,
        ],
        VkDenied::class => [
            VkDeniedUserNotification::class,
            VkDeniedAdminNotification::class,
        ]
    ];

    protected $subscribe = [
        NotificationEventSubscriber::class,
        ProjectNotificationEventSubscriber::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
