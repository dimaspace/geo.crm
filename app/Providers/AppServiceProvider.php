<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\BalanceTransaction;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        BalanceTransaction::saved(function ($model) {
            Cache::tags('user_balances')->flush();
          return true;
        });

        \App\User::saved(function ($model) {
            perm_flush_cache();
          return true;
        });
        \App\Permission::saved(function ($model) {
            perm_flush_cache();;
          return true;
        });
        \App\Role::saved(function ($model) {
            perm_flush_cache();
          return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }
}
