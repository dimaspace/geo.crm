<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('mustache', function ($expression) { //user_notification_block
            return  $this->include_mustache($expression);
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    /**
     * @param $expression
     *
     * @return string
     */
    protected function include_mustache($expression): string
    {
        $expression = trim(substr($expression, 1, -1));
        if (preg_match('/^[a-zA-Z0-9_.]+$/', $expression)) {
            $mustache_path = str_replace('.', '/', $expression);
            $mustache_name = str_replace('.', '-', $expression);
            $mustache_full_path = '/mustache/' . $mustache_path . '.mustache';
            $result = '<?php $__env->startPush("mustache"); ?>';
            $result .= "<script id='{$mustache_name}' type='text/x-handlebars-template'>";
            $result .= "<?php include resource_path('{$mustache_full_path}') ?>";
            $result .= '</script>';
            $result .= "<script type='text/javascript'>";
            $result .= 'if(!window.mustache_tpl){ window.mustache_tpl = []; }';
            $result .= "$(function () { GeoCrm.mustache.addTpl('".$mustache_name."'); })";
            $result .= '</script>';
            $result .= '<?php $__env->stopPush(); ?>';

            return $result;
        }

        return '';
    }
}
