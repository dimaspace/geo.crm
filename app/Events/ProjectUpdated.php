<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Project;

class ProjectUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $project;
    public $changes;
    protected $ignoring_fields = ['updated_at'];
    /**
     * Create a new event instance.
     * @param Project $project
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->changes = $this->filter_changes($this->project->getDirty());
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    protected function filter_changes($changes){
        return collect($changes)->filter(function ($value, $key) {
            if(in_array($key, $this->ignoring_fields)){
                return false;
            }else{
                return true;
            }
        });
    }
}
