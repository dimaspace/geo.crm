<?php

namespace App\Events\Notification\Vk;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Social\VkTools;

class VkDenied
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_id, $region_code, $group_id, $group_name, $group_screen_name;

    /**
     * VkDenied constructor.
     * @param $user_id
     * @param $region_code
     * @param $allowed_region_code
     */
    public function __construct($user_id, $region_code, $allowed_region_code)
    {
        $this->user_id = $user_id;
        $this->region_code = $allowed_region_code;
        $this->group_id = VkTools::getGroupIdByRegion($region_code);
        $this->group_name = VkTools::getGroupNameById($this->group_id);
        $this->group_screen_name = VkTools::getGroupScreenName($this->group_id);
    }
}
