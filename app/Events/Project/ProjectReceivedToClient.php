<?php

namespace App\Events\Project;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class ProjectReceivedToClient
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $project, $response;

    /**
     * ProjectReceivedToWorker constructor.
     * @param $project
     * @param $response
     */
    public function __construct($project, $response)
    {
        $this->project = $project;
        $this->response = $response;
    }

}
