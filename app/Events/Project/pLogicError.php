<?php

namespace App\Events\Project;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Project;

class pLogicError
{
    use InteractsWithSockets, SerializesModels;

    public $project;
    public $error_code;
    public $error_msg;


    /**
     * Create a new event instance.
     *
     * @param Project $project
     * @param         $error_code
     * @param         $error_msg
     */
    public function __construct(Project $project, $error_code = '', $error_msg = '')
    {
        $this->project = $project;
        $this->project->load('cords')->load('extra')->load('extra.user');

        $this->error_code = $error_code;
        $this->error_msg = $error_msg;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
