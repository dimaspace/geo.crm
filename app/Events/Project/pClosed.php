<?php

namespace App\Events\Project;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Notification;
use App\Notifications\ProjectClosed;
use Auth;
use App\Project;
use App\Role;

class pClosed
{
    use InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        /*$users = Role::whereName('cord')->first()->users()->get();

        $worker = $project->worker();
        $users->push($worker);
        $users = $users->unique(function ($item) {
            return $item->id;
        });

        foreach($users as $user){
            Notification::send($user, new ProjectClosed($project));
        }*/
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
