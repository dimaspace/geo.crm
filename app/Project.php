<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;
use DB;
use Config;
use Entrust;
use Auth;
use App\Events\Project\pLogicError;
use App\Events\Project\pStatusAutoChanged;
use App\Events\Project\pClosed;
use App\ProjectCords;
use Carbon\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Events\ProjectUpdated;
use JamesMills\Uuid\HasUuidTrait;


class Project extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;
    use LogsActivity;
    use HasUuidTrait;

    protected $fillable = ['status_id', 'desc_public', 'desc_hidden', 'category_id', 'region_code', 'client_cached',
        'place_cached', 'date_start', 'date_start_real', 'date_end', 'date_deadline', 'deadline2', 'payment_type_id_w',
        'payment_type_id_b', 'amount', 'amount_with_tax', 'amount_per_hour', 'geo_type_id', 'smm_type_id',
        'cord_prin_id', 'cord_rasp_id', 'cord_control_id', 'pass_type_id', 'visibility_id', 'visibility_list',
        'worker_id', 'time_unknown', 'geo_link', 'geo_id', 'download_link', 'client_vk_id', 'client_email',
        'ss_mode', 'ss_base', 'amount_to_office'

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date_start',
        'date_end',
        'date_deadline',
        'date_start_real',
        'upload_at',
        'moderated_at'
    ];

    protected static $logName = 'project';
    protected static $logAttributes = ['*', 'cords'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;

    protected $dispatchesEvents = [
        'updated' => ProjectUpdated::class,
    ];

    public function scopeDeadlineFailed($query)
    {
        return $query
            ->where('date_deadline', '<', Carbon::now())
            ->where('date_start_real', '>', Carbon::create(2018, 8, 1, 0, 0, 0, config('project.city_time_zone')))
            ->whereNotIn('status_id', Config::get('project.fail_status'))
            ->whereIn('category_id', Config::get('project.category_need_links'))
            ->where(function ($query) {
                $query->orWhere('download_link', NULL)
                  ->orWhere('geo_type_id', 0);

            });

    }


    public function scopeDeadline2Failed($query)
    {
        return $query
            ->where('date_start_real', '>', Carbon::create(2018, 8, 1, 0, 0, 0, config('project.city_time_zone')))
            ->whereNotNull('upload_at')
            ->where('pass_type_id', 0)
            ->whereNotIn('status_id', Config::get('project.fail_status'))
            ->where(function ($query) {
                $query->orWhereRaw(DB::raw('(DATE_ADD(`upload_at`, INTERVAL `deadline2` hour) < now() AND `upload_at` <= `date_deadline`)'))
                    ->orWhereRaw(DB::raw('(DATE_ADD(`date_deadline`, INTERVAL `deadline2` hour) < now() AND `upload_at` > `date_deadline`)'))
                    ->orWhereRaw(DB::raw('(DATE_ADD(`date_deadline`, INTERVAL `deadline2` hour) < now() AND `upload_at` IS NULL)'));
            });
    }


    public function scopeDeadline2FailedForCord($query)
    {
        return $query
            ->orwhere(function ($query) {
                $query->DeadlineFailed()
                    ->WhereRaw(DB::raw('(DATE_ADD(`date_deadline`, INTERVAL `deadline2` hour) < now() AND `upload_at` IS NULL)'));
            })
        ->orwhere(function ($query) {
                $query->Deadline2Failed();
            });
    }


    public function scopeDeadline2FiredForClient($query)
    {
        return $query
            ->Deadline2Failed()
            ->where('client_notified', 0)
            ->where(function ($query) {
                $query->orWhere('client_vk_id', '>', 0)
                    ->orWhereRaw('LENGTH(client_email) > 0');
            });
    }

    function cords(){
        return $this->hasMany('App\ProjectCords');
    }

    function getCordsByType($type_code){
        return $this->cords->where('type_code', $type_code);
    }

    function getCordsNumByType($type_code){
        return $this->cords->where('type_code', $type_code)->count();
    }

    function extra(){
        return $this->hasOne('App\ProjectExtra');
    }

    function ssmanager(){
        return $this->extra()->with('user')->where('type_extras', 'ss');
    }

    function referral(){
        return $this->hasOne('App\ProjectReferral');
    }

    function desire(){
        return $this->hasMany('App\ProjectDesire');
    }

    function desireAuth(){
        return $this->hasOne('App\ProjectDesire')->where('user_id', Auth::user()->id);
    }

    function desireByUserId($user_id){
        return $this->hasOne('App\ProjectDesire')->where('user_id', $user_id);
    }

    function worker(){
        if($this->worker_id){
            return User::find($this->worker_id);
        }else{
            return false;
        }
    }

    public function transactions(){
        return $this->hasMany(BalanceTransaction::class);
    }

    function calc_amount_photo(){
        $result = $this->amount - $this->calc_tax() - $this->calc_ed_fee() - $this->amount_to_office;
        if($this->ss_mode == 0){
            $result = $result - $this->calc_ss_fee();
        }

        return $result;
    }

    function calc_amount_photo_whis_tax(){
        return $this->amount - $this->calc_tax();
    }

    function get_rf_percent(){
        return $this->calc_ed_fee(true) + $this->calc_ss_fee(true);
    }

    function calc_ed_fee($in_percent = false){
        if($this->worker()){
            if($this->payment_type_id_w == 1) {
                if($in_percent){
                    return $this->worker()->ed_fee_noncash;
                }else{
                    return ($this->amount - $this->calc_tax() - $this->amount_to_office) * $this->worker()->ed_fee_noncash / 100;
                }
            }else{
                if($in_percent){
                    return $this->worker()->ed_fee_cash;
                }else{
                    return ($this->amount - $this->calc_tax() - $this->amount_to_office) * $this->worker()->ed_fee_cash / 100;
                }
            }
        }else{
            return 0;
        }
    }

    function calc_ss_fee($in_percent = false){
        if($this->ssmanager){
            if($in_percent){
                return $this->ssmanager->royalty;
            }else{
                if($this->ss_base == 0){
                    return ($this->amount - $this->calc_tax()) * $this->ssmanager->royalty / 100;
                }else{
                    return ($this->calc_ed_fee() + $this->amount_to_office) * $this->ssmanager->royalty / 100;
                }
            }
        }else{
            return 0;
        }
    }

    function is_referral(){
        if($this->referral){
            return true;
        }else{
            return false;
        }
    }

    function is_referral_himself(){
        if($this->referral){
            if(isset($this->worker()->id)){
                return $this->referral->user_id == $this->worker()->id ? true : false;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function calc_referral($in_percent = false){
        if($this->referral){
            if($in_percent){
                return $this->referral->royalty;
            }else {
                return ($this->amount - $this->calc_tax()) * $this->referral->royalty / 100;
            }
        }else{
            return 0;
        }
    }

    function calc_tax(){
        if($this->payment_type_id_b == 1){
            $tax_rate = Config::get('project.tax_rate');
            $tax_amount = $this->amount * $tax_rate;
            return $tax_amount;
        }else{
            return 0;
        }
    }

    function calc_cord_royalty($cord_type){
        $cord_types = config('project.cord_type.' . $cord_type);
        $cord_royalty_config = collect(
            config('project.cord_royalty')
        );
        $pay_type_id = $this->payment_type_id_w;
        $project_amount = $this->amount;

        $params = false;
        foreach($cord_royalty_config->keys()->sort()->reverse() as $amount){
            if($project_amount >= $amount){
                break;
            }
            $params = $cord_royalty_config[$amount];
        }
        $royalty = 0;
        if(isset($params[$cord_type]) && isset($params[$cord_type][$pay_type_id])){
            $royalty = $params[$cord_type][$pay_type_id];
        }
        return $royalty;
    }

    function calc_all_cords_royalty(){
        $total_royalty = 0;
        if(isset($this->cords) && $this->cords){
            foreach($this->cords as $cord){
                $royalty = $this->calc_cord_royalty($cord->type_code);
                $cord_num = $this->getCordsNumByType($cord->type_code);

                if($cord_num > 0){
                    $royalty = round($royalty/$cord_num, 2);
                }
                $total_royalty += $royalty;
            }
        }

        return $total_royalty;
    }

    public function close(){
        return self::makeClose($this);
    }

    public function check_complete(){
        self::CheckComplete($this);
    }

    static function makeClose(Project $project){
       $is_complete = Project::CheckComplete($project);

       if($is_complete && $project->status_id != Config::get('project.closed_status')){
           $project->status_id = Config::get('project.closed_status');
           $project->save();

           event(new pClosed($project));


           /* Вознаграждение координаторов*/
           if(isset($project->cords) && $project->cords){
               foreach($project->cords as $cord){
                   $royalty = $project->calc_cord_royalty($cord->type_code);
                   $cord_num = $project->getCordsNumByType($cord->type_code);

                   $desc = 'Тип: ' .' '. ProjectCords::getTypeNameByCode($cord->type_code);

                   if($cord_num > 0){
                       $royalty = round($royalty/$cord_num, 2);
                       BalanceTransaction::make_payment($royalty, Config::get('transaction.billing_users.regions')[$project->region_code], $cord->user_id, $project->id, 'CORD_ROYALTY', 'TRIGGER', $desc, 1);
                   }
               }
           }


           self::makePayout($project);
       }

       return $is_complete;
    }

    static function CheckComplete(Project $project){

        $result = true;

        if(trim($project->download_link) ===''){
            $result = false;
        }

        if($project->geo_type_id === 0){
            if(trim($project->geo_link) ===''){
                $result = false;
            }else{
                $project->geo_type_id = 2;
                $project->save();
            }
        }

        if(trim($project->geo_link) !==''){
            $project->geo_type_id = 2;
            $project->save();
        }

        if($project->pass_type_id === 0){
            $result = false;
        }

        if($project->pass_type_id === 2){
            if($project->status_id === Config::get('project.worker_assigned_status')){
                // Перевод проекта в статус Сдача проекта
                $project->status_id = Config::get('project.passing_status');
                $project->save();
            }else{
                event(new pLogicError($project, 'PASSED_WITHOUT_WORKER', 'Проект сдан без назначенного исполнителя'));
            }
        }

        if($project->worker_id == false){
            $result = false;
        }

        if(in_array($project->status_id, Config::get('project.fail_status'))) {
            $result = false;
        }

        return $result;
    }

    public function Payout(){
        return self::makePayout($this);
    }

    static function makePayout(Project $project, $cr_type = 'TRIGGER'){

        DB::transaction(function () use($project, $cr_type){
            if($project->status_id == Config::get('project.closed_status')){
                if($project->payment_type_id_w == 0){
                    if($project->po_at_once == 0){
                        //WORKER
                        //BalanceTransaction::make_payment($project->calc_amount_photo(), $project->worker_id, Config::get('project.billing_users.regions')[$project->region_code], 'WORKER', $cr_type, $desc = '', $accepted = 1);
                        //TAX
                        if($project->calc_tax()){
                            //BalanceTransaction::make_payment($project->calc_tax(), $project->worker_id, Config::get('project.billing_users.regions')[$project->region_code], $project->id, 'TAX', $cr_type, $desc = '', $accepted = 1);
                        }
                        //ED_FEE
                        if($project->calc_ed_fee()){
                            BalanceTransaction::make_payment($project->calc_ed_fee(), $project->worker_id, Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'ED_FEE', $cr_type, '', 1);
                        }
                        //OFFICE_FEE
                        if($project->amount_to_office > 0){
                            BalanceTransaction::make_payment($project->amount_to_office, $project->worker_id, Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'OFFICE_FEE', $cr_type, '', 1);
                        }

                        if($project->payment_type_id_b == 0){
                            //SS
                            if($project->calc_ss_fee()){
                                BalanceTransaction::make_payment($project->calc_ss_fee(), $project->worker_id, $project->ssmanager->user->id, $project->id, 'SS', $cr_type, '', 1);
                            }

                            //FS
                            if($project->calc_referral()){
                                BalanceTransaction::make_payment($project->calc_referral(), Config::get('transaction.billing_users.regions')[$project->region_code], $project->referral->user->id, $project->id, 'FS', $cr_type, '', 1);
                            }
                        }else{
                            //CLIENT_PAY
                            BalanceTransaction::make_payment($project->amount, Config::get('transaction.billing_users.client'), Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'CLIENT_PAY', $cr_type, '', 1);

                            if($project->ss_mode == 0){
                                //SS
                                if($project->calc_ss_fee()){
                                    BalanceTransaction::make_payment($project->calc_ss_fee(), $project->worker_id, Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'SS', $cr_type, 'На оплату менеджеру СС', 1);
                                }
                            }
                        }

                        $project->po_at_once = 1;
                    }

                    if($project->po_later_on == 0 && $project->payment_type_id_b > 0 &&  $project->pay_status_id == 1){
                        //SS
                        if($project->calc_ss_fee()){
                            BalanceTransaction::make_payment($project->calc_ss_fee(), Config::get('transaction.billing_users.regions')[$project->region_code], $project->ssmanager->user->id, $project->id, 'SS', $cr_type, '', 1);
                        }
                        //FS
                        if($project->calc_referral()){
                            BalanceTransaction::make_payment($project->calc_referral(), Config::get('transaction.billing_users.regions')[$project->region_code], $project->referral->user->id, $project->id, 'FS', $cr_type, '', 1);
                        }

                        //CLIENT RECHARGE
                        BalanceTransaction::make_payment($project->amount, Config::get('transaction.billing_users.inkassator'), Config::get('transaction.billing_users.client'), $project->id, 'COLLECT_RETENTION', $cr_type, 'Деньги получены на Р/C', 1);
                        //TAX
                        if($project->calc_tax()){
                            BalanceTransaction::make_payment($project->calc_tax(), Config::get('transaction.billing_users.regions')[$project->region_code], Config::get('transaction.billing_users.tax'), $project->id, 'TAX', $cr_type, 'Налоги', 1);
                        }


                        $project->po_later_on = 1;
                    }

                    $project->save();

                }else{
                    if($project->po_at_once == 0){
                        //CLIENT_PAY
                        BalanceTransaction::make_payment($project->amount, Config::get('transaction.billing_users.client'), Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'CLIENT_PAY', $cr_type, '', 1);
                        //WORKER
                        BalanceTransaction::make_payment($project->calc_amount_photo_whis_tax(), Config::get('transaction.billing_users.regions')[$project->region_code], $project->worker_id, $project->id, 'WORKER', $cr_type, '', 1);
                        //TAX
                        /*if($project->calc_tax()){
                            BalanceTransaction::make_payment($project->calc_tax(), Config::get('transaction.billing_users.regions')[$project->region_code], Config::get('transaction.billing_users.tax'), $project->id, 'TAX', $cr_type, '', 1);
                        }*/
                        //ED_FEE
                        if($project->calc_ed_fee()){
                            BalanceTransaction::make_payment($project->calc_ed_fee(), $project->worker_id, Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'ED_FEE', $cr_type, '', 1);
                        }
                        //OFFICE_FEE
                        if($project->amount_to_office > 0){
                            BalanceTransaction::make_payment($project->amount_to_office, $project->worker_id, Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'OFFICE_FEE', $cr_type, '', 1);
                        }

                        if($project->ss_mode == 0){
                            if($project->calc_ss_fee()){
                                BalanceTransaction::make_payment($project->calc_ss_fee(), $project->worker_id, Config::get('transaction.billing_users.regions')[$project->region_code], $project->id, 'SS', $cr_type, 'На оплату менеджеру СС', 1);
                            }
                        }

                        $project->po_at_once = 1;
                    }
                    if($project->po_later_on == 0 && $project->pay_status_id == 1){
                        //CLIENT_PAY accept
                        /* Перевод сразу приходит акцептированным
                        $client_pays =BalanceTransaction::where('project_id', $project->id)->where('type', BalanceTransaction::_getTypeIdByCode('CLIENT_PAY'))->where('accepted', 0)->get();
                        foreach($client_pays as $client_pay){
                            $client_pay->accepted = 1;
                            $client_pay->save();
                        }*/
                        //CLIENT RECHARGE
                        BalanceTransaction::make_payment($project->amount, Config::get('transaction.billing_users.inkassator'), Config::get('transaction.billing_users.client'), $project->id, 'COLLECT_RETENTION', $cr_type, 'Деньги получены на Р/C', 1);
                        //TAX
                        if($project->calc_tax()){
                            BalanceTransaction::make_payment($project->calc_tax(), Config::get('transaction.billing_users.regions')[$project->region_code], Config::get('transaction.billing_users.tax'), $project->id, 'TAX', $cr_type, 'Налоги', 1);
                        }
                        //SS
                        if($project->calc_ss_fee()){
                            BalanceTransaction::make_payment($project->calc_ss_fee(), Config::get('transaction.billing_users.regions')[$project->region_code], $project->ssmanager->user->id, $project->id, 'SS', $cr_type, '', 1);
                        }
                        //FS
                        if($project->calc_referral()){
                            BalanceTransaction::make_payment($project->calc_referral(), Config::get('transaction.billing_users.regions')[$project->region_code], $project->referral->user->id, $project->id, 'FS', $cr_type, '', 1);
                        }

                        $project->po_later_on = 1;
                    }

                    $project->save();

                }
            }

        }, 5);
    }


}
