<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  mixed $site
     * @return mixed
     */
    public function handle($request, Closure $next, $site = 'default')
    {
        if($site == 'portfolio') {
            Menu::make('PortfolioMainMenu', function ($menu) {

                $menu->add('Главная', ['url' => route('extraPortfolioMainPage')])->nickname('home');

                $menu->add('Фотографы', ['url' => false, 'subnav' => true])->nickname('photers');
                $photers = User::whereHas('media', function ($query) {
                    $query->where('collection_name', 'portfolio')->has('tags', '>', 0);
                })->orderBy('is_staff', 'desc')->get();
                foreach($photers as $photer){
                    $name = $photer->is_staff ? $photer->name : 'Внештатный фотограф #' . $photer->id;
                    $menu->item('photers')->add($name,
                        ['url' => route('extraPortfolioUserPage', ['user' => $photer->id])]
                    );
                }

            });
        }

        return $next($request);
    }
}
