<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'logout',
        'login',
        'bot.php',
        'bottest.php',
        'bot'
    ];

    public function handle($request, Closure $next)
    {
        try {
            return parent::handle($request, $next);
        } catch (TokenMismatchException $e) {
            if ($request->wantsJson()) {
                return response()->json(['error' => trans('errors.token.mismatch')], 418);
            }
            abort(418, trans('errors.token.mismatch'));
        }
    }

}
