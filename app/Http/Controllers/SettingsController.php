<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\UserInfo;
use App\Project;
use Config;


class SettingsController extends Controller
{

    public function externalServices(Request $request){
        if(!Entrust::can('sys_settings')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете изменять системные настройки CRM!' ]);
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'НАСТРОЙКИ ВНЕШНИХ СЕРВИСОВ';

        return view('settings/ext_services', $data);

    }

}
