<?php

namespace App\Http\Controllers;

use App\Events\Notification\Vk\VkDenied;
use App\Notifications\Email\ProjectCreateToClientEmail;
use App\Notifications\Vk\ProjectCreateToClientVk;
use App\Notifications\Vk\ProjectSendToWorkerVk;
use Illuminate\Http\Request;
use Auth;
use App\Project;
use App\ProjectCords;
use App\ProjectExtra;
use App\ProjectReferral;
use App\Region;
use App\User;
use App\Client;
use Config;
use Carbon\Carbon;
use VKAPI;
use dimaspace\VKAPI\VKAPIException;
use Entrust;
use Notification;
use App\Notifications\ProjectWorkerAssigned;
use App\Notifications\Vk\ProjectWorkerAssignCanceledVk;
use App\Notifications\Telegram\ProjectWorkerAssignCanceledTelegram;
use App\Notifications\Email\ProjectWorkerAssignCanceledEmail;

use NotificationChannels\Telegram\Exceptions\CouldNotSendNotification;
use App\Social\VkTools;


class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('pageForClient');
    }

    /**
     * Получение проекта
     *
     * @return \Illuminate\Http\Response
     */
    public function getOne(Request $request)
    {
        $project_id = $request->input('project_id');

        $project = Project::findOrFail($project_id);

        $project->date_start = $project->date_start ? $project->date_start->timezone(config('project.city_time_zone')) : NULL;
        $project->date_start_real = $project->date_start_real ? $project->date_start_real->timezone(config('project.city_time_zone')) : NULL;
        $project->date_end = $project->date_end ? $project->date_end->timezone(config('project.city_time_zone')) : NULL;
        $project->date_deadline = $project->date_deadline ? $project->date_deadline->timezone(config('project.city_time_zone')) : NULL;

        $project_desire_by_user_id = [];
        foreach($project->desire()->orderBy('rating', 'desc')->get() as $desire){
            $project_desire_by_user_id[$desire->user_id] = $desire->rating;
        }

        $workers = User::whereHas('roles', function ($query) {
            $query->where('name', 'photo');
        })->whereHidden(0)->get();

        $workers_weight_by_id = [];
        foreach($workers as $worker){
            $desire_weight = isset($project_desire_by_user_id[$worker->id]) ? $project_desire_by_user_id[$worker->id] : 0;
            $workers_weight_by_id[$worker->id] = $worker->getWeight() + $desire_weight * 10000;
        }

        $cords = [];
        foreach($project->cords as $cord){
            $cord_type = $cord->type_code;
            if(!isset($cords[$cord_type])){
                $cords[$cord_type] = [];
            }
            $cords[$cord_type][] = $cord->user_id;
        }

        return response()->json([
            'success' => true,
            'project'  => $project,
            'cords' => $cords,
            'ssmanager' => $project->ssmanager,
            'referral'  => $project->referral,
            'worker_desire' => $project_desire_by_user_id,
            'worker_weight' => $workers_weight_by_id
        ]);
    }

    /**
     * Получение проекта
     *
     * @return \Illuminate\Http\Response
     */
    public function calcAmount(Request $request)
    {
        $project_id = $request->input('project_id');

        $params = $request->all();

        $project = new Project($params);
        if(trim($project->amount) == '' || !is_numeric($project->amount)){
            $project->amount = 0;
        }
        if(trim($project->amount_to_office) == '' || !is_numeric($project->amount_to_office)){
            $project->amount_to_office = 0;
        }

        if($params['ssmanager_id']){
            $project_ss = [];
            $project_ss['user_id'] = $params['ssmanager_id'];
            $project_ss['royalty'] = (float) str_replace(',', '.', $params['ssmanager_royalty']);
            $project_ss['type_extras'] = 'ss';

            $ss = new ProjectExtra($project_ss);
            $project->ssmanager = $ss;
        }
       if(isset($params['referral_id'])){
            $project_referral = [];
            $project_referral['user_id'] = $params['referral_id'];
            $project_referral['royalty'] = (float) str_replace(',', '.', $params['referral_royalty']);
            $referral = new ProjectReferral($project_referral);
            $project->referral = $referral;
        }

        $result = [
            'amount' => $project->amount,
            'amount_to_office' => $project->amount_to_office,
            'amount_photo' => number_format(
                $project->is_referral_himself() ? $project->calc_amount_photo() + $project->calc_referral() : $project->calc_amount_photo()
                , 2, '.', ' '),
            'tax' =>  number_format($project->calc_tax(), 2, '.', ' '),
            'ref' => number_format($project->calc_referral(), 2, '.', ' '),
            'ss_fee' => number_format($project->calc_ss_fee(), 2, '.', ' '),
            'ed_fee' => number_format($project->calc_ed_fee(), 2, '.', ' '),
            'rf' => $project->get_rf_percent()
        ];


        return response()->json(['result' =>$result]);
    }

    /**
     * Создание проекта
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if(Entrust::can('add_projects')) {

            $data = collect($request->all());

            $data->forget('_token');
            if(isset($data['project_client_vk_id'])){
                $data['project_client_vk_id'] = (int) $data['project_client_vk_id'];
            }

            if ( ! trim($data['project_id'])) {
                $data->forget('project_id');
            }

            if ($data['project_worker_id'] && in_array($data['project_status_id'],
                    Config::get('project.worker_unassigned_status'))) {
                $data['project_status_id'] = Config::get('project.worker_assigned_status');
            }
            if ($data['project_worker_id'] == 0 && $data['project_status_id'] == Config::get('project.worker_assigned_status')) {
                $data['project_status_id'] = Config::get('project.worker_cancel_status');
            }

            $project_cord_array = [];
            if (isset($data['project_cord'])) {
                foreach ($data['project_cord'] as $cord_type => $cord_data) {
                    if ($cord_data != null && count($cord_data)) {
                        foreach ($cord_data as $user_id) {
                            $project_cord_array[] = new ProjectCords([
                                'type_code' => $cord_type,
                                'user_id'   => $user_id
                            ]);
                        }
                    }
                }
                $data->forget('project_cord');
            }

            $project_ss = false;
            if (isset($data['project_ssmanager_id']) && $data['project_ssmanager_id']) {
                $project_ss = [];
                $project_ss['user_id'] = $data['project_ssmanager_id'];
                $project_ss['royalty'] = (float) str_replace(',', '.', $data['project_ssmanager_royalty']);
                $project_ss['type_extras'] = 'ss';
            }
            $data->forget('project_ssmanager_id');
            $data->forget('project_ssmanager_royalty');

            $project_referral = false;
            if (isset($data['project_referral_id']) && $data['project_referral_id']) {
                $project_referral = [];
                $project_referral['user_id'] = $data['project_referral_id'];
                $project_referral['royalty'] = (float) str_replace(',', '.', $data['project_referral_royalty']);

            }
            $data->forget('project_referral_id');
            $data->forget('project_referral_royalty');

            $data = $this->process_dates($data);

            $geo_id = 0;
            $data_save = $data->mapWithKeys(function ($item, $key) use (&$geo_id, $request) {
                if ( ! is_array($item) && trim($item) === '') {
                    $item = null;
                }

                $key = str_replace("project_", "", $key);

                if ($key == 'status_geo') {
                    $key = 'geo_type_id';
                }

                /*if($key == 'date_start'){
                    if($item){
                        $item = Carbon::createFromFormat('d.m.Y H:i', $item . ' ' . $time_start);
                    }else{
                        $item = NULL;
                    }
                }*/

                if ($key == 'date_deadline') {
                    if ($item) {
                        $item = Carbon::createFromFormat('d.m.Y H:i', $item, config('project.city_time_zone'))->setTimezone('UTC');
                    } else {
                        $item = null;
                    }
                }

                if ($key == 'deadline2') {
                    preg_match('/[0-9]+/', $item, $match);
                    $item = isset($match[0]) ? $match[0] : 0;
                }

                if ($key == 'visibility_list') {
                    $item = implode(',', (array) $item);
                }

                if ($key == 'geo_link') {
                    if ($item) {
                        switch ($request->input('project_category_id')) {
                            case 1 :
                                preg_match('/(?=\/events\/).+\/([0-9]+)\/pictures\/[0-9]+$/', $item, $geo_link);
                                if (isset($geo_link[1])) {
                                    $geo_id = $geo_link[1];
                                } else {
                                    preg_match('/(?=\/events\/).+\/([0-9]+)$/', $item, $geo_link);
                                    if (isset($geo_link[1])) {
                                        $geo_id = $geo_link[1];
                                    } else {
                                        $geo_id = 0;
                                    }
                                }
                                break;

                            case 2 :
                                preg_match('/(?=\/video\/).+\/([0-9]+)$/', $item, $geo_link);
                                if (isset($geo_link[1])) {
                                    $geo_id = $geo_link[1];
                                } else {
                                    $geo_id = 0;
                                }
                                break;

                            default :
                                $geo_id = 0;
                        }
                    }
                }

                return [ $key => $item ];
            });

            $data_save->put('geo_id', $geo_id);

            $project = new Project($data_save->toArray());
            $project->creator_id = Auth::user()->id;
            $project->save();

            $project->cords()->saveMany($project_cord_array);

            if ($project_ss) {
                $project->extra()->save(new ProjectExtra($project_ss));
            }

            if ($project_referral) {
                $project->referral()->save(new ProjectReferral($project_referral));
            }


            if(trim($project->client_vk_id) || trim($project->client_email)){
                $client = new Client();
                $client->vk_id = trim($project->client_vk_id) ? $project->client_vk_id : '';
                $client->email = trim($project->client_email) ? $project->client_email : '';
                try {
                    Notification::send($client, new ProjectCreateToClientVk($project));
                    Notification::send($client, new ProjectCreateToClientEmail($project));
                } catch(CouldNotSendNotification $e) {}
            }

            return back()->with('success', 'Проект создан!');
        }else{
            return back()->with('error', 'У вас нет прав для создания проекта!');
        }
    }


    /**
     * Редактирование проекта
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(Entrust::can('manage_projects')) {

            $data = collect($request->all());

            $project = Project::findOrFail($data['project_id']);

            $old_status_id = $project->status_id;

            $data->forget('_token');
            if(isset($data['project_client_vk_id'])){
                $data['project_client_vk_id'] = (int) $data['project_client_vk_id'];
            }

            if ( ! trim($data['project_id'])) {
                $data->forget('project_id');
            }

            if ($data['project_worker_id'] && in_array($data['project_status_id'],
                    Config::get('project.worker_unassigned_status'))) {
                $data['project_status_id'] = Config::get('project.worker_assigned_status');
                try {
                    Notification::send(User::findOrFail($data['project_worker_id']), new ProjectWorkerAssigned($project));
                } catch(CouldNotSendNotification $e) {}
            }
            if ($data['project_worker_id'] == 0 && $data['project_status_id'] == Config::get('project.worker_assigned_status')) {
                $data['project_status_id'] = Config::get('project.worker_cancel_status');
            }

            // Проверка изменился ли исполнитель
            $clear_vk_msg_id = false;
            if ($data['project_worker_id'] && $project->worker() && $project->worker()->id !== (int) $data['project_worker_id']){
                $clear_vk_msg_id = true;
                try {
                    Notification::send(User::findOrFail($project->worker()->id),
                        new ProjectWorkerAssignCanceledVk($project));
                    Notification::send(User::findOrFail($project->worker()->id),
                        new ProjectWorkerAssignCanceledTelegram($project));
                    Notification::send(User::findOrFail($project->worker()->id),
                        new ProjectWorkerAssignCanceledEmail($project));
                } catch(CouldNotSendNotification $e) {}
            }

            $data->forget('order_staff_only');
            $data->forget('ssmanager');

            $project_cord_array = [];
            if (isset($data['project_cord'])) {
                foreach ($data['project_cord'] as $cord_type => $cord_data) {
                    if ($cord_data != null && count($cord_data)) {
                        foreach ($cord_data as $user_id) {
                            $project_cord_array[] = new ProjectCords([
                                'type_code' => $cord_type,
                                'user_id'   => $user_id
                            ]);
                        }
                    }
                }
                $data->forget('project_cord');
            }

            $project_ss = false;
            if (isset($data['project_ssmanager_id']) && $data['project_ssmanager_id']) {
                $project_ss = [];
                $project_ss['user_id'] = $data['project_ssmanager_id'];
                $project_ss['royalty'] = (float) str_replace(',', '.', $data['project_ssmanager_royalty']);
                $project_ss['type_extras'] = 'ss';

            }
            $data->forget('project_ssmanager_id');
            $data->forget('project_ssmanager_royalty');

            $project_referral = false;
            if (isset($data['project_referral_id']) && $data['project_referral_id']) {
                $project_referral = [];
                $project_referral['user_id'] = $data['project_referral_id'];
                $project_referral['royalty'] = (float) str_replace(',', '.', $data['project_referral_royalty']);

            }
            $data->forget('project_referral_id');
            $data->forget('project_referral_royalty');

            $data = $this->process_dates($data);

            $geo_id = 0;
            $data_save = $data->mapWithKeys(function ($item, $key) use (&$geo_id, $request) {
                if ( ! is_array($item) && trim($item) === '') {
                    $item = null;
                }

                $key = str_replace("project_", "", $key);

                if ($key == 'status_geo') {
                    $key = 'geo_type_id';
                }

                if ($key == 'date_deadline') {
                    if ($item) {
                        $item = Carbon::createFromFormat('d.m.Y H:i', $item, config('project.city_time_zone'))->setTimezone('UTC');
                    } else {
                        $item = null;
                    }
                }

                if ($key == 'deadline2') {
                    preg_match('/[0-9]+/', $item, $match);
                    $item = isset($match[0]) ? $match[0] : 0;
                }

                if ($key == 'visibility_list') {
                    $item = implode(',', (array) $item);
                }

                if ($key == 'geo_link') {
                    if ($item) {
                        switch($request->input('project_category_id')){
                            case 1 :
                                preg_match('/(?=\/events\/).+\/([0-9]+)\/pictures\/[0-9]+$/', $item, $geo_link);
                                if (isset($geo_link[1])) {
                                    $geo_id = $geo_link[1];
                                } else {
                                    preg_match('/(?=\/events\/).+\/([0-9]+)$/', $item, $geo_link);
                                    if (isset($geo_link[1])) {
                                        $geo_id = $geo_link[1];
                                    } else {
                                        $geo_id = 0;
                                    }
                                }
                            break;

                            case 2 :
                                preg_match('/(?=\/video\/).+\/([0-9]+)$/', $item, $geo_link);
                                if (isset($geo_link[1])) {
                                    $geo_id = $geo_link[1];
                                }else{
                                    $geo_id = 0;
                                }
                                break;

                            default :
                                $geo_id = 0;
                        }
                    }
                }

                return [ $key => $item ];
            });

            $data_save->put('geo_id', $geo_id);
            if($geo_id > 0){
                $data_save->put('geo_type_id', 2);
            }

            $project->update($data_save->toArray());


            // Сброс vk msg id
            if($clear_vk_msg_id){
                $project->vk_msg_id = 0;
                $project->update();
            }

            $project->cords()->delete();
            $project->cords()->saveMany($project_cord_array);

            $project->extra()->delete();
            if ($project_ss) {
                $project->extra()->save(new ProjectExtra($project_ss));
            }

            $project->referral()->delete();
            if ($project_referral) {
                $project->referral()->save(new ProjectReferral($project_referral));
            }

            if($old_status_id != $project->status_id && $project->status_id == Config::get('project.closed_status')){
                Project::makePayout($project);
            }

            $project->close();

            return back()->with('success', 'Проект изменён!');

        }else{
            return back()->with('error', 'У вас нет прав для управления проекта!');
        }
    }

    /**
     * Проверка привязки профиля к VK
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserVK(Request $request)
    {
        $user_id = $request->input('user_id');

        $user = User::find($user_id);
        if($user){
            if($user->vk_id){
                return response()->json([
                    'result' => $user->vk_id
                ]);
            }else{
                return response()->json([
                    'result' => false
                ]);
            }
        }else{
            return response()->json([
                'error' => 'Пользователь не найден'
            ]);
        }

    }

    /**
     * Получение данных юзера VK из ссылки профиля юзера
     *
     * @return \Illuminate\Http\Response
     */
    public function getVkUserIdByLink(Request $request)
    {
        $link = trim($request->input('link'));
        $region_code = trim($request->input('region_code'));

        $uid = VkTools::getUserIdByLink($link);

        $user_info = $uid ? VkTools::getUserInfo($uid) : false;

        if($user_info !== false){
            VkTools::setTokenByRegionCode($region_code);
            $user_info['allow'] = VkTools::checkMsgAllowByRegion($uid, $region_code);

            if($user_info['allow'] === false){
                $user_info['group_id'] = VkTools::getGroupIdByRegion($region_code);
                $user_info['group_name'] = VkTools::getGroupNameById($user_info['group_id']);
                $user_info['group_screen_name'] = VkTools::getGroupScreenName($user_info['group_id']);
                $user_info['group_dialog_link'] = VkTools::getGroupDialogLinkById($user_info['group_id']);
            }
        }

        return response()->json([
            'user_info' => $user_info
        ]);
    }

    /**
     * Редактирование проекта
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendProjectToVK(Request $request)
    {
        if(Entrust::can('manage_projects')) {

            $project_id = $request->input('project_id');
            $user_id = $request->input('user_id');

            $project = Project::findOrFail($project_id);

            if ( ! $user_id) {
                $user_id = $project->worker()->id;
            }

            $user = User::findOrFail($user_id);
            $vk_id = $user->vk_id;
            if($vk_id === 0){
                return response()->json([
                    'error' => 'У пользователя не указан VK ID'
                ]);
            }

            try {
                $check_allow = VkTools::checkMsgAllowByRegion($vk_id, $project->region_code);
                if ($check_allow === false) {
                    throw new VKAPIException(901,  'message not allowed',  'message not allowed');
                }

                Notification::send($user, new ProjectSendToWorkerVk($project, $request->input('resend', false)));
            } catch (VKAPIException $e) {

                if($e->code === 15){
                    return response()->json([
                        'error' => 'Нет доступа по API<br> Возможно в настройках указан неверный token'
                    ]);
                }

                $vk_group_id =  VkTools::getGroupIdByRegion($project->region_code);
                $group_dialog_link = VkTools::getGroupDialogLinkById($vk_group_id);
                $group_name = VkTools::getGroupNameById($vk_group_id);
                $group_screen_name = VkTools::getGroupScreenName($vk_group_id);

                $msg = 'Приветствую, чтоб мы могли тебе автоматически отправлять сообщения о предстоящийх съёмках напиши сообщение группе @' . $group_screen_name
                . '(' . $group_name . ")\n" . $group_dialog_link;

                $error_msg = '<b>У группы нет доступа на отправку сообщения пользователю</b><br>';
                $error_msg .= 'Перешлите этот текст вручную и скажите <a target="_new" href="https://vk.com/im?sel=' .$vk_id. '">фотографу</a>, чтоб написал группе';
                $error_msg .= '<button type="button" class="btn btn-xs bg-teal pull-left vk_msg_copy_button"><i class="material-icons" title="Скопировать текст">file_copy</i></button>';
                $error_msg .= '<textarea id="vk_msg_copy" style="width: 100%; height: 200px;">' . $msg . '</textarea>';

                return response()->json([
                    'error' => $error_msg
                ]);
        }


            return response()->json([
                'success' => 'Сообщение отправлено',
            ]);

        }else{
            return response()->json([
                'success' => 'Вы не можете отправить это уведомление'
            ]);
        }
    }


    /**
     * @param $data
     *
     * @return mixed
     */
    protected function process_dates($data)
    {
        if (isset($data['project_time_unknown'])) {
            $data['project_time_unknown'] = 1;
            $date_start_real = NULL;
            $date_end = NULL;

            if (isset($data['project_date_start']) && $data['project_date_start']) {
                $date_start = Carbon::createFromFormat('d.m.Y H:i', $data['project_date_start'].' 00:00', config('project.city_time_zone'))->setTimezone('UTC');
            } else {
                $date_start = NULL;
            }
        } else {
            if (isset($data['project_date_start']) && $data['project_date_start']) {
                $date_start = Carbon::createFromFormat('d.m.Y H:i', $data['project_date_start'].' 00:00', config('project.city_time_zone'))->setTimezone('UTC');
                //$date_end = $date_start;
            } else {
                $date_start = $date_end = NULL;
            }

            if (isset($data['project_real_start']) && $data['project_real_start'] && isset($data['project_real_end']) && $data['project_real_end']) {

                $date_start_real = Carbon::createFromFormat('d.m.Y H:i', $data['project_real_start'], config('project.city_time_zone'))->setTimezone('UTC');
                $date_end = Carbon::createFromFormat('d.m.Y H:i', $data['project_real_end'], config('project.city_time_zone'))->setTimezone('UTC');

                if (isset($data['project_time_start']) && $data['project_time_start']) {
                    $date_start = Carbon::createFromFormat('d.m.Y H:i', $data['project_date_start'].' ' . $data['project_time_start'], config('project.city_time_zone'))->setTimezone('UTC');
                }

            } else {
                $date_start_real = $date_end = NULL;
            }
        }
        $data['project_date_start'] = $date_start;
        $data->put('project_date_end', $date_end);
        $data->put('project_date_start_real', $date_start_real);

        return $data;
    }

    /**
     * Вставка ссылкм
     *
     * @return \Illuminate\Http\Response
     */
    public function addLink(Request $request)
    {
        $project_id = $request->input('project_id');

        $project = Project::findOrFail($project_id);

        if(Auth::user()->id == $project->worker_id){
            if($request->input('type_link') == 'geo'){
                if($link = $request->input('link')){

                    switch($project->category_id){
                        case 1 :
                            preg_match('/\/reportage\/([0-9]+)-(?=.+)/', $link, $geo_link);
                            if (isset($geo_link[1])) {
                                $geo_id = $geo_link[1];
                            } else {
                                $geo_id = 0;

                            }
                        break;

                        case 2 :
                            preg_match('/(?=\/tv\/).+\/([0-9]+)\//', $link, $geo_link);
                            if (isset($geo_link[1])) {
                                $geo_id = $geo_link[1];
                            }else{
                                $geo_id = 0;
                            }
                            break;

                        default :
                            $geo_id = 0;
                    }

                    if($geo_id === 0){
                        return back()->with('error', 'Cсылка на GEO некорректна!');
                    }else{
                        if(trim($project->geo_link) === ''){
                            $project->geo_link = $request->input('link');
                            $project->geo_id = $geo_id;
                            $project->geo_type_id = 2;
                        }else{
                            return back()->with('error', 'Cсылка на GEO уже отправлялась!');
                        }
                    }
                }else{
                    return back()->with('error', 'Вы отправили пустую ссылку!');
                }
            }else{
                if(trim($project->download_link) === ''){
                    $project->download_link = $request->input('link');
                }else{
                    return back()->with('error', 'Cсылка на материал уже отправлялась!');
                }
            }

            $project->upload_at = Carbon::now();

            $project->save();

            if($project->close()){
                return back()->with('success', 'Проект завершён');
            }else{
                return back()->with('success', 'Ссылка сохранена');
            }



        }else{
            return back()->with('error', 'У вас нет доступа на публикацию ссылки на эту съёмку!');
        }

    }

    /**
     * Пометка проекта оплаченым
     *
     * @return \Illuminate\Http\Response
     */
    public function makePaid(Request $request)
    {
        $project_id = $request->input('project_id');

        $project = Project::findOrFail($project_id);

        if(Auth::user()->can('manage_payments')){


            $project->pay_status_id = 1;

            $project->save();

            $project->Payout();

            return back()->with('success', 'Проект помечен оплаченным');

        }else{
            return back()->with('error', 'У вас нет доступа на публикацию ссылки на эту съёмку!');
        }

    }

    public function pageForClient($uuid)
    {
        try {
            $project =  Project::findByUuidOrFail($uuid);
        return view('token_pages.project_for_client', [
            'project' => $project
        ]);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            abort(404);
        }
    }
}
