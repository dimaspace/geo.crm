<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Region;

class RegionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRegionsJson(Request $request){

        $regions = Region::select(['id', 'title', 'code', 'geo_id']);

        $is_all = (integer) $request->input('all', 0);
        if($is_all !== 1){
            $regions = $regions->whereIn('code', config('project.regions'))->orderBy('title', 'desc');
        }

        return response()->json($regions->get());
    }
}
