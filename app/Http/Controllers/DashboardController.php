<?php

namespace App\Http\Controllers;

use App\Busy;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
//use Request;
use Entrust;
use URL;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Region;
use App\Project;
use App\User;
use App\CashFlow;
use App\BalanceTransaction;
use Auth;
use Config;
use DB;

class DashboardController extends Controller
{
    private $cfg_worker_assigned_status, $cfg_worker_cancel_status, $cfg_worker_unassigned_status, $cfg_active_status;

    public function __construct(){
        $this->cfg_worker_assigned_status = [Config::get('project.worker_assigned_status')];
        $this->cfg_worker_cancel_status = [Config::get('project.worker_cancel_status')];
        $this->cfg_worker_unassigned_status = Config::get('project.worker_unassigned_status');

        $this->cfg_active_status = array_merge(
            $this->cfg_worker_assigned_status,
            $this->cfg_worker_cancel_status,
            $this->cfg_worker_unassigned_status
        );
    }

    /*
     * Главная страница
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = [];

        if(Entrust::can('manage_projects_list')) {
            $data = $this->cordDash($data, $request);
            $data = $this->cordFormProject($data);
        }


        if(Entrust::can('view_work_market')) {
            $data = $this->photoDashMarket($data, $request);
            $data = $this->photoNowProjects($data, $request);
        }

        if(Entrust::can('view_balance_stats')) {
            //$data = $this->profileBalance($data, Auth::user()->id);
            $data['user'] = User::findOrFail(Auth::user()->id);
        }

        $data['cf_transactions_not_accepted'] = BalanceTransaction::with('cash_flow', 'cross', 'cross.cash_flow', 'cross.user')
            ->where('user_id', Auth::user()->id)->NotAccepted()->ByTypeCode('CASH_FLOW')->get();

        $data['auth_user'] = Auth::user();

        return view('dash/index', $data);
    }


    /**
     * @param $data
     *
     * @return mixed
     */
    protected function cordDash($data, $request)
    {


        $data['cfg_active_status'] = $this->cfg_active_status;

        $data['info_boxes'] = [];
        $data['info_boxes']['projects_in_work'] =Project::whereIn('status_id', $this->cfg_active_status)->count();
        $data['info_boxes']['unassigned'] =Project::whereIn('status_id', $this->cfg_worker_unassigned_status)->count();
        $data['info_boxes']['unassigned24'] =Project::whereIn('status_id', $this->cfg_worker_unassigned_status)->where('date_start_real', '<=', Carbon::now()->addHours(24))->count();
        $data['info_boxes']['unassigned96'] =Project::whereIn('status_id', $this->cfg_worker_unassigned_status)->where('date_start_real', '<=', Carbon::now()->addHours(96))->count();
        $data['info_boxes']['new'] =Project::where('status_id', 0)->count();
        $data['info_boxes']['all'] =Project::count();
        $data['info_boxes']['unknow_date'] =Project::where('date_start', NULL)->count();
        $data['info_boxes']['deadline_fail'] =Project::DeadlineFailed()->count();
        $data['info_boxes']['deadline2_fail'] =Project::Deadline2FailedForCord()->count();
        $data['info_boxes']['deadline2_fail_client'] =Project::Deadline2FiredForClient()->count();

        $data['projects_filter_alternate'] = $request->input('projects_filter_alternate');

        $data['projects_filter'] = [];


        $data = $this->getProjectsByFilter($data, $request);

        $filters = $request->only('projects_filter', 'projects_filter_alternate');
        $data['filters_link'] = http_build_query($filters);

        $data['projects_list_mode'] = $request->input('projects_list_mode', 'table');

        $data['project_regions'] = Region::whereIn('code', Config::get('project.regions'))->get()->keyBy('code');

        return $data;
    }


    /**
     * @param $data
     *
     * @return mixed
     */
    protected function cordFormProject($data)
    {
// Получение списка фотографов в регионе
        $workers_grouped = collect();
        $regions = Region::whereIn('code', Config::get('project.regions'))->orderBy('title', 'desc')->get();
        $data['regions_by_code'] = [];
        foreach ($regions as $region) {
            $data['regions_by_code'][$region->code] = $region->title;
            $workers = $region->users()->whereHas('roles', function ($query) {
                $query->where('name', 'photo');
            })->whereHidden(0)->orderBy('is_staff', 'desc')->get();
            $workers_grouped[$region->code] = $workers;
        }

        $workers = User::whereHas('roles', function ($query) {
            $query->where('name', 'photo');
        })->doesntHave('regions')->get();

        $workers_grouped[0] = $workers;

        $data['workers_grouped'] = $workers_grouped;
        $data['regions_by_code'][0] = 'Без региона';

        return $data;
    }

    protected function photoDashMarket($data, $request)
    {
        $project_list = Project::with(['ssmanager', 'referral', 'desireAuth'])->whereIn('status_id', Config::get('project.worker_unassigned_status'));

        $auth_user = Auth::user();
        $auth_user_regions = Auth::user()->regions()->get();

        $data['photo_work_market_project_list'] = [];
        foreach($project_list->get() as $project){
            $visiblity_check =  false;
            switch($project->visibility_id){
                case 0 :
                    break;

                case 1:
                    if($auth_user_regions->contains('code', $project->region_code)){
                        $visiblity_check = true;
                    }
                    break;

                case 2 :
                    if($auth_user_regions->contains('code', $project->region_code) && $auth_user->is_staff){
                        $visiblity_check = true;
                    }
                    break;
                case 3 :
                    $visibility_list = explode(',', $project->visibility_list);
                    if($auth_user_regions->contains('code', $project->region_code) && $auth_user->is_staff && !in_array($auth_user->id, $visibility_list) ){
                        $visiblity_check = true;
                    }
                    break;
                case 4 :
                    $visibility_list = explode(',', $project->visibility_list);
                    if(in_array($auth_user->id, $visibility_list) ){
                        $visiblity_check = true;
                    }
                    break;
            }
            if($visiblity_check){
                $data['photo_work_market_project_list'][] = $project;
            }
        }




        return $data;
    }

    protected function photoNowProjects($data, $request)
    {
        $project_list = Project::with(['ssmanager', 'referral'])->whereIn('status_id', [Config::get('project.worker_assigned_status'), Config::get('project.worker_request_status'), Config::get('project.passing_status')])->where('worker_id', Auth::user()->id)->get();

        $data['photo_now_project_list'] = $project_list;

        return $data;
    }

    public function profileBalance($data, $user_id){

        $data['user'] = User::findOrFail($user_id);

        $data['PAGE_TITLE'] = \Illuminate\Support\Str::upper($data['user']->name);

        $transactions_data = $data['user']->transactions()->orderBy('created_at')->get();

        $data['transactions_by_project'] = $transactions_data->groupBy('project_id');

        $data['transactions'] = [];

        $transactions_project = [];
        $transactions_single = [];
        foreach($transactions_data as $transaction){
            if( $transaction->project){
                $transactions_project[] = $transaction;
            }else{
                $transactions_single[] = $transaction;
            }
        }
        $transactions_project = collect($transactions_project)->unique('project_id');

        /*$data['transactions'] = collect(
            array_merge($transactions_project, $transactions_single)
        );*/
        $data['transactions'] = $transactions_project->merge($transactions_single)->sortByDesc('created_at');


        return $data;
    }


    /**
     * @param $data
     * @param $request
     * @param $filter
     * @param $carbon_sub_10
     * @param $carbon_add_10
     * @param $filter_in
     * @param $cfg_worker_unassigned_status
     * @param $cfg_active_status
     *
     * @return mixed
     */
    protected function getProjectsByFilter($data, $request, $for_calendar = false){

        $carbon_sub_10 =Carbon::now()->subDays(10)->startOfDay();
        $carbon_add_10 = Carbon::now()->addDays(10)->endOfDay();

        $filter_in = [];
        $filter = [];

        $filter_alternate = $request->input('projects_filter_alternate');
        if ($request->input('projects_filter') && !$filter_alternate) {
            $filter_request = $request->input('projects_filter');

            if (isset($filter_request['start'])) {
                if($for_calendar){
                    /*$filter[] = [
                        'date_start',
                        '>=',
                        Carbon::createFromFormat('Y-m-d', $filter_request['start'],
                            config('project.city_time_zone'))->startOfDay()->subDay()->setTimezone('UTC')
                    ];*/
                }else{
                    $filter[] = [
                        'date_start',
                        '>=',
                        Carbon::createFromFormat('Y-m-d', $filter_request['start'],
                            config('project.city_time_zone'))->startOfDay()->setTimezone('UTC')
                    ];
                }
                $data['projects_filter']['start'] = $filter_request['start'];
            } else {
                $data['projects_filter']['start'] = $carbon_sub_10->format('Y-m-d');
            }
            if (isset($filter_request['end'])) {
                $filter[] = [
                    'date_start',
                    '<=',
                    Carbon::createFromFormat('Y-m-d', $filter_request['end'],
                        config('project.city_time_zone'))->endOfDay()->setTimezone('UTC')
                ];
                $data['projects_filter']['end'] = $filter_request['end'];
            } else {
                $data['projects_filter']['end'] = $carbon_add_10->format('Y-m-d');
            }

            if (isset($filter_request['regions'])) {
                $filter_in[] = [ 'region_code', $filter_request['regions'] ];
                $data['projects_filter']['regions'] = $filter_request['regions'];
            } else {
                $data['projects_filter']['regions'] = Config::get('project.regions');
            }

            if (isset($filter_request['statuses'])) {
                $filter_in[] = [ 'status_id', $filter_request['statuses'] ];
                $data['projects_filter']['statuses'] = $filter_request['statuses'];
            } else {
                $data['projects_filter']['statuses'] = [];
            }

            if (isset($filter_request['workers'])) {
                if (count($filter_request['workers'])) {
                    $filter_request['workers'][] = 0;
                    $filter_in[] = [ 'worker_id', $filter_request['workers'] ];
                }
                $data['projects_filter']['workers'] = $filter_request['workers'];
            } else {
                $data['projects_filter']['workers'] = [];
            }

            if (isset($filter_request['staff'])) {
                $data['projects_filter']['staff'] = $filter_request['staff'];
            } else {
                $data['projects_filter']['staff'] = [];
            }

        } else {
            if ($filter_alternate) {
                switch ($filter_alternate) {
                    case 'all':
                        break;
                    case 'new':
                        $filter[] = [ 'status_id', '=', 0 ];
                        break;
                    case 'unassigned':
                        $filter_in[] = [ 'status_id', $this->cfg_worker_unassigned_status ];
                        break;
                    case 'unassigned24':
                        $filter_in[] = [ 'status_id', $this->cfg_worker_unassigned_status ];

                        $filter[] = [ 'date_start_real', '<=', Carbon::now()->addHours(24) ];
                        break;
                    case 'unassigned96':
                        $filter_in[] = [ 'status_id', $this->cfg_worker_unassigned_status ];

                        $filter[] = [ 'date_start_real', '<=', Carbon::now()->addHours(96) ];
                        break;
                    case 'projects_in_work':
                        $filter[] = [ 'status_id', $this->cfg_active_status ];
                        break;
                    case 'unknow_date':
                        $filter[] = [ 'date_start', null ];
                        break;
                    /*case 'deadline_fail':
                        $filter[] = ['date_deadline', '<', Carbon::now()];
                        $filter[] = ['geo_type_id', '=', 0];
                        $filter[] = ['download_link', '=', NULL];
                        $filter_in[] = ['status_id', $cfg_active_status];
                    break;*/
                }

                $data['projects_filter']['start'] = $carbon_sub_10->format('Y-m-d');
                $data['projects_filter']['end'] = $carbon_add_10->format('Y-m-d');
                $data['projects_filter']['regions'] = Config::get('project.regions');
            } else {
                $filter[] = [ 'date_start', '>', $carbon_sub_10 ];
                $filter[] = [ 'date_end', '<', $carbon_add_10 ];

                $data['projects_filter']['start'] = $carbon_sub_10->format('Y-m-d');
                $data['projects_filter']['end'] = $carbon_add_10->format('Y-m-d');
                $data['projects_filter']['regions'] = Config::get('project.regions');
            }
        }

        if ($filter_alternate === 'deadline_fail' || $filter_alternate === 'deadline2_fail' || $filter_alternate === 'deadline2_fail_client') {
            switch($filter_alternate){
                case('deadline_fail') :
                    $projects = Project::with(['ssmanager', 'referral'])->DeadlineFailed();
                    break;
                case('deadline2_fail') :
                    $projects = Project::with(['ssmanager', 'referral'])->Deadline2FailedForCord();
                    break;
                case('deadline2_fail_client') :
                    $projects = Project::with(['ssmanager', 'referral'])->Deadline2FiredForClient();
                    break;
            }
        } else {
            $projects = Project::with(['ssmanager', 'referral'])->where(function ($query) use ($filter, $filter_in) {
                $query->where($filter);
                foreach ($filter_in as $filter_one_in) {
                    $query->whereIn($filter_one_in[0], $filter_one_in[1]);
                }
            });
        }

        if($for_calendar && isset($filter_request['start'], $filter_request['end'])) {
            $start = Carbon::createFromFormat('Y-m-d', $filter_request['start'],
                config('project.city_time_zone'))->startOfDay()->subDay()->setTimezone('UTC');

            $end = Carbon::createFromFormat('Y-m-d', $filter_request['end'],
                config('project.city_time_zone'))->endOfDay()->setTimezone('UTC');

            $period = CarbonPeriod::create($start, $end);

            $projects = $projects->overlaps($period, ['date_start_real', 'date_end']);
        }

        $data['projects'] = $projects->with('cords', 'extra', 'referral', 'desire')->get();

        return $data;
    }


    /**
     * Получение отфильтрованных проектов
     *
     * @return \Illuminate\Http\Response
     */
    public function getFilteredProjects(Request $request)
    {
        $result = [];
        $result = $this->getProjectsByFilter($result, $request, true);

        foreach($result['projects'] as &$project){
            $project['startDate'] = $project->date_start_real ? $project->date_start_real->toIso8601ZuluString() : false;
            $project['endDate'] = $project->date_end ? $project->date_end->toIso8601ZuluString() : false;

            $project['text'] = $project->place_cached . '[' . $project->client_cached . '] ';
            $project['text'] .= ($project->amount ? $project->amount : 'х/з') . ' р. ';
            $project['text'] .= ($project->payment_type_id_w == 1 ? 'бн' : 'н');
            $project['text'] .= '(' . ($project->payment_type_id_b > 0 ? 'бн' : 'н') . ')';

            $project['is_busy'] = false;
        }

        foreach(self::getUserBusy() as $busy){
            $result['projects'][] = [
                'startDate' => $busy->started_at->toIso8601ZuluString(),
                'endDate' => $busy->ended_at->toIso8601ZuluString(),
                'text' => $busy->comment,
                'worker_id' => $busy->user_id,
                'is_busy' => true,
                'busy' => $busy->busy
            ];
        }

        return response()->json($result['projects']);

    }


    protected static function getUserBusy(){

        $filter_request = request()->input('projects_filter');
        $start = isset($filter_request['start']) ? Carbon::createFromFormat('Y-m-d', $filter_request['start'],
            config('project.city_time_zone'))->startOfDay()->setTimezone('UTC') : false;
        $end = isset($filter_request['end']) ? Carbon::createFromFormat('Y-m-d', $filter_request['end'],
            config('project.city_time_zone'))->endOfDay()->setTimezone('UTC') : false;

        $events = [];

        if($start && $end){
            $period = CarbonPeriod::create($start, $end);
            $events = Busy::overlaps($period)->get();

            if(isset($filter_request['workers'])){
                Busy::whereIn('user_id', $filter_request['workers']);
            }
        }

        return $events;
    }

}
