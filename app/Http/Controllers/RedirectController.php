<?php

namespace App\Http\Controllers;

use App\ShortUrl;
use Illuminate\Routing\Controller;
use Cache;

class RedirectController extends Controller
{
    /**
     * Redirect to url by its code.
     *
     * @param string $code
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect($code)
    {
        $url = Cache::rememberForever("url.$code", function () use ($code) {
            return ShortUrl::whereCode($code)->first();
        });

        if ($url !== null) {
            return redirect()->away($url->url, 301);
        }

        abort(404);
    }
}