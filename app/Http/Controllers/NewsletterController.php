<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\Role;
use Notification;
use \App\Notifications\Newsletter;

class NewsletterController extends Controller
{

    /*
     * Создание массовой рассылки
     *
     * @return \Illuminate\Http\Response
     */
    public function Add(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'СОЗДАТЬ РАССЫЛКУ';

        return view('newsletter/add', $data);
    }

    public function Send(Request $request)
    {

        $roles = $request->input('roles');
        $email_msg = $request->input('email_msg');
        $telegram_msg = $request->input('telegram_msg');

        $newsletter_regions = $request->input('newsletter_regions');
        $region_filter_on = $request->input('region_filter_on');

        $users = new Collection();
        foreach($roles as $role){
            $role_user = Role::whereName($role)->first()->users()->get();
            $users = $users->merge($role_user);
        }
        $users = $users->unique('id');

        if($region_filter_on){
            $users = $users->filter(function ($user, $key) use ($newsletter_regions){

                if($user->regions->whereIn('id', $newsletter_regions)->count()){
                    return true;
                }else{
                    return false;
                }
            });
        }

        foreach($users as $user){
            Notification::send($user, new Newsletter($email_msg, $telegram_msg));
        }

        return redirect('/newsletter/add')->with('success', 'Отправка рассылки запущена');

    }
}
