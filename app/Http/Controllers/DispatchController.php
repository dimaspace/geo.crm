<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Project;
use App\ProjectCords;
use App\ProjectExtra;
use App\ProjectReferral;
use App\Region;
use App\User;
use Cache;
use Carbon\Carbon;
use VKAPI;
use Entrust;
use Notification;
use App\Notifications\ProjectWorkerAssigned;
use Formery\FormBuilder;
use Excel;
use File;
use App\Jobs\VkMsgSend;
use App\FailedJobs;
use App\FailedDispatch;
use Artisan;


class DispatchController extends Controller
{
    protected $scripts = [];
    protected $msgs_ok_num = 0;
    protected $msgs_fail_num = 0;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Создание и импорт рассылки
     *
     * @return \Illuminate\Http\Response
     */
    public function MassVkExcelImport(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'СОЗДАТЬ РАССЫЛКУ И ЗАГРУЗИТЬ ЭКСЕЛЬ';

        $action = '/dispatch/massvk';

        $form = new FormBuilder($action, 'POST', ['class'=>'form-horizontal']);
        $form->allowUploads();
        $form->addField('excel', 'Excel файл',  \Formery\FieldTypes\File::class);

        $form->addField('attachment', 'Прикрепить файл',  \Formery\FieldTypes\Text::class, ['placeholder'=>'photo[число]_[число] или video[число]_[число]']);
        $form->addField('datetime', 'Дата отправки',  \Formery\FieldTypes\Text::class, ['class'=>'datetimepicker']);

        $data['form'] = $form;

        return view('dispatch/add', $data);
    }

    public function failedMsg(Request $request)
    {
        $data = [];

        $failed = FailedDispatch::whereHide(0)->get();
        $data['failed'] = $failed;

        return view('dispatch/failed', $data);
    }

    public function MassVkSend(Request $request)
    {
        if (Entrust::can('dispatch')) {

            if($request->file('excel') != NULL && $request->file('excel')->isValid() && File::exists($request->excel->path())){

                Excel::selectSheetsByIndex([0,1])->load($request->excel->path(), function ($reader) use($request) {
                    $sheets = $reader->get();

                    $this->loadScripts($sheets[1]);

                    $time_delay = 1;
                    $datetime = $request->input('datetime');
                    foreach($sheets[0] as $row){
                        $row->script = (int) $row->script;
                        $row->vkid = (int) $row->vkid;
                        if($row->script > 0 &&  $row->vkid != null && $row->vkid > 0){
                            $msg = $this->renderScript($row->script, $row);
                            if($msg){
                                $vk_id = $row->vkid;
                                $params = [ 'user_id' => $vk_id, 'message' => $msg ];
                                $attachment = $request->input('attachment');
                                if($attachment){
                                    $params['attachment'] = $attachment;
                                }

                                $time =  $time_delay;

                                if(trim($datetime) !== '' && preg_match('#^[0-9]{2}.[0-9]{2}.[0-9]{4} [0-9]{2}:[0-9]{2}$#', $datetime)){
                                    $datetime_carbon = Carbon::createFromFormat('d.m.Y H:i', $datetime, config('project.city_time_zone'));
                                    $time = $datetime_carbon->addSeconds($time);
                                    //dd($time);
                                }

                                $group_ids = config('project.vk_groups_id');
                                if(isset($row->region) && $row->region && trim($row->region) != '' && isset($group_ids[$row->region])){
                                    $params['group_id'] = $group_ids[$row->region];
                                    $params['region_code'] = $row->region;
                                }else{
                                    $region_codes = config('project.regions');
                                    $region_code = array_shift($region_codes);
                                    $params['group_id'] = $group_ids[$region_code];
                                    $params['region_code'] = $region_code;
                                }
                                dispatch((new VkMsgSend($params))->delay($time));

                                $this->msgs_ok_num++;
                            }else{
                                $this->msgs_fail_num++;
                            }
                        }else{

                                $this->msgs_fail_num++;
                        }
                        $time_delay += rand(50,120);
                    }
                });
            }

            return redirect('/dispatch/massvk')->with(
                'success',
                "Сообщения отправлены<br>Отправлено: " . $this->msgs_ok_num . "<br>Ошибка: " . $this->msgs_fail_num
            );

        } else {
            return redirect('/dispatch/massvk')->with(
                'error',
                'Вы не можете отправить эти сообщения'
            );
        }
    }

    public function jobsFailed(Request $request)
    {
        if(!Entrust::can('dispatch')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }
        $data = [];

        return view('dispatch/failed_jobs', $data);
    }

    public function jobsFailedJson(Request $request)
    {
        if(!Entrust::can('dispatch')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        $failed_jobs = FailedJobs::get();

        $data = [];

        foreach($failed_jobs as $failed_job){

            $payload = json_decode($failed_job->payload);

            $command = '';
            $vk_user = false;
            $vk_msg = '';
            $vk_user_id = '';
            $vk_user_name = '';
            if(isset($payload->data) && isset($payload->data->command)){
                $command_obj = unserialize($payload->data->command);
                if($command_obj instanceof VkMsgSend){
                    $vk_msg = $command_obj->msg['message'];
                    $vk_user_id = $command_obj->msg['user_id'];
                    $vk_user = $this->getVkUserById($vk_user_id);

                    $vk_user_name = isset($vk_user['first_name']) ? $vk_user['first_name'] : '';
                    $vk_user_name .= isset($vk_user['nickname']) ? ' '.$vk_user['nickname'] : '';
                    $vk_user_name .= isset($vk_user['last_name']) ? ' '.$vk_user['last_name'] : '';
                    //dd($vk_user);

                }else{
                    $command = $payload->data->command;
                }
            }



            $data[] = [
                "id" => $failed_job->id,
                "connection" => $failed_job->connection,
                "queue" => $failed_job->queue,
                "payload" => $payload,
                "exception" => $failed_job->exception,
                "failed_at" => $failed_job->failed_at->format('Y/m/d H:i:s'),
                "failed_day" => $failed_job->failed_at->format('d.m.Y'),

                "p_attempts" => $payload->attempts,
                "p_data_command" => $command,

                "vk_msg" => $vk_msg,
                "vk_user" => $vk_user,
                'vk_user_id' => $vk_user_id,
                'vk_user_name' => $vk_user_name
            ];
        }

        return response()->json($data);
    }

    public function jobsFailedDelete(Request $request)
    {
        if ( ! Entrust::can('dispatch_manage')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете смотреть управлять задачами!' ]);
            exit();
        }

        $id = (int) $request->id;

        $result = Artisan::call('queue:forget', ['id' => $id]);

        return response()->json([
            'success' => 'Задача удалена из списка проваленных'
        ]);

    }

    public function jobsFailedRetry(Request $request)
    {
        if ( ! Entrust::can('dispatch_manage')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете смотреть управлять задачами!' ]);
            exit();
        }

        $id = (int) $request->id;

        $result = Artisan::call('queue:retry', ['id' => $id]);

        return response()->json([
            'success' => 'Задача повторно добавлена в очередь на исполнение'
        ]);

    }

    protected function loadScripts($scriptSheet){
        foreach($scriptSheet as $script){
            if($script->answer !== null && trim($script->answer) !== '' && $script->script !== null && trim($script->script) !== ''){
                $script->script = (int) $script->script;
                if($script->script > 0){
                    $this->scripts[$script->script] = $script->answer;
                }
            }
        }
    }

    protected function renderScript($scriptNumber, $vars){
        if(isset($this->scripts[$scriptNumber])){
            $script = $this->scripts[$scriptNumber];
            $vars->each(function ($item, $key) use(&$script) {
                $script = str_ireplace('['.$key.']', $item, $script);
            });
            $script = str_replace("\r", '', $script);

            return $script;
        }else{
            return false;
        }
    }

    protected function getVkUserById($id){
        return Cache::tags(['vk_users'])->rememberForever('vk_user_'.$id, function () use($id) {
            $result = VKAPI::call('users.get', ['user_ids' => $id, 'fields' => 'nickname' ]);
            sleep(1);
            return $result[0];
        });

    }

}