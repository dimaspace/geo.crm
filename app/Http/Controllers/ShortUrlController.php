<?php

namespace App\Http\Controllers;

use App\ShortUrl as Url;
use Illuminate\Routing\Controller;
use App\Http\Requests\ShortUrlRequest;
use App\Http\Responses\ShortUrlResponse;
use App\Tools\Hasher;

class ShortUrlController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:short_url');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urls = Url::orderBy('created_at', 'desc')->paginate(20);

        return view('short_url.index', compact('urls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('short_url.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ShortUrlRequest $request
     *
     * @return ShortUrlResponse
     */
    public function store(ShortUrlRequest $request)
    {
        $url = Url::create([
            'url'  => $request->get('url'),
            'code' => $request->get('code') ? str_slug($request->get('code')) : Hasher::generate(),
        ]);

        return new ShortUrlResponse($url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url = Url::findOrFail($id);

        return view('short_url.edit', compact('url'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ShortUrlRequest  $request
     * @param  int  $id
     *
     * @return ShortUrlResponse
     */
    public function update(ShortUrlRequest $request, $id)
    {
        $url = Url::findOrFail($id);

        \Cache::forget("url.{$url['code']}");

        $url->update($request->all());

        return new ShortUrlResponse($url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $url = Url::findOrFail($id);

        \Cache::forget("url.{$url['code']}");

        $url->delete();

        if (request()->wantsJson()) {
            return response([], 204);
        }

        return back()
            ->with('short_url', true);
    }
}