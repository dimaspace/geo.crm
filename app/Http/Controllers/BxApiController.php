<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class BxApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function findContacts(Request $request)
    {
        $response = Curl::to('https://geometria.bitrix24.ru/rest/3/hb89cv8rbv5x27vb/crm.contact.list')
        ->withData( array( 'filter[%LAST_NAME]' => $request->input('q') ) ) //, 'select[ID]' => 1 , 'select[TITLE]' => 1
        ->asJson()
        ->get();

        return json_encode($response->result);
    }

    public function findCompany(Request $request)
    {
        $response = Curl::to('https://geometria.bitrix24.ru/rest/3/hb89cv8rbv5x27vb/crm.company.list')
        ->withData( array( 'filter[%TITLE]' => $request->input('q') ) )
        ->asJson()
        ->get();

        return json_encode($response->result);

    }

    public function getCompany(Request $request)
    {
        $response = Curl::to('https://geometria.bitrix24.ru/rest/3/hb89cv8rbv5x27vb/crm.company.get')
        ->withData( array( 'id' => $request->input('id') ) )
        ->asJson()
        ->get();

        return json_encode($response->result);

    }
}
