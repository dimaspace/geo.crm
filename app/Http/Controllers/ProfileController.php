<?php

namespace App\Http\Controllers;

use App\BalanceTransaction;
use App\Busy;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\UserInfo;
use App\UserPrice;
use App\CashFlow;
use DNS2D;
use Artisan;
use DB;
use Spatie\Tags\Tag;

class ProfileController extends Controller
{

    /*
     * Редактирование профиля
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'ВАШ ПРОФИЛЬ';

        if(Auth::user() && trim(Auth::user()->name == '')){
            return view('profile/register', $data);
        }else{
            return view('profile/index', $data);
        }
    }

    /*
     * Сохранение профиля
     *
     * @return \Illuminate\Http\Response
     */
    public function saveProfile(Request $request)
    {
        $data = [];

        $user = Auth::user();
        $user_info = $user->info;

        if(!$user_info){
            $user_info = new UserInfo();
        }
        $user_info->phone = $request->input('phone');
        $user_info->geo_login = $request->input('geo_login');
        $user_info->about = $request->input('about');

        $user->info()->save($user_info);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->vk_id = $request->input('vk_id');
        $user->save();

        if($request->input('register')){
            return redirect('/')->with('success', 'Добро пожаловать в систему!');
        }else{
            return redirect('/profile')->with('success', 'Профиль сохранён!');
        }
    }

    /*
     * Сохранение прайса
     *
     * @return \Illuminate\Http\Response
     */
    public function savePrice(Request $request)
    {
        $data = [];

        $tag_prices = $request->input('tag_price', []);

        UserPrice::where(['user_id' => Auth::user()->id])->delete();
        foreach($request->input('tags', []) as $tag_id => $value){
            if(isset($tag_prices[$tag_id])){
                $tag_price = new UserPrice;
                $tag = Tag::findOrFail($tag_id);
                $tag_price->user()->associate(Auth::user());
                $tag_price->tag()->associate($tag);
                $tag_price->price = (int) str_replace(' ', '', $tag_prices[$tag_id]);

                $tag_price->save();
            }
        }
            return redirect('/profile?active_tab=price')->with('success', 'Цены сохранёны!');
    }

    /*
     * Редактирование настроек уведомлений
     *
     * @return \Illuminate\Http\Response
     */
    public function Notify(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'НАСТРОЙКА УВЕДОМЛЕНИЙ';

        $user = Auth::user();

        $data['channels'] = explode(',', $user->notify_channels);

        if($user->telegram_chat_id ==0 && $user->telegram_find_code == ''){
            $find_code = $this->genTelegramFindCode();
            // Проверка на уникальность
            while(User::where('telegram_find_code', $find_code)->first()){
                $find_code = $this->genTelegramFindCode();
            }
            $user->telegram_find_code = $find_code;
            $user->save();
        }

        $data['telegram_user_status'] = true;
        if($user->telegram_chat_id == 0){
            $data['telegram_user_status'] = false;
        }

        $data['telegram_user_code'] = $user->telegram_find_code;
        $data['telegram_user_auth_link'] = 'https://telegram.me/'. config('notify.telegram_bot_name') .'?start=' . $user->telegram_find_code;
        $data['telegram_user_auth_link_qr_code'] = '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG($data['telegram_user_auth_link'], "QRCODE", 10, 10) . '" alt="barcode" class="m-b-15"  />';


        return view('profile/notify', $data);
    }
    /*
     * Сохранение профиля
     *
     * @return \Illuminate\Http\Response
     */
    public function saveNotify(Request $request)
    {

        if($request->input('channels', false)){
            $channels = $request->input('channels');
            if($channels != null && count($channels) > 0){

                $user = Auth::user();
                $user->notify_channels = implode(',', array_keys($channels));
                $user->save();

                return redirect('/profile/notify')->with('success', 'Настройки уведомлений сохранены!');
            }
        }else{
            return redirect('/profile/notify')->with('error', 'Надо выбрать хотябы один канал уведомлений!');
        }

        //
    }

    private function genTelegramFindCode(){
        return rand(111,999) . '-' . rand(111,999) . '-' . rand(111,999) . '-' . rand(111,999);
    }


    /*
     * Просмотр баланса
     *
     * @return \Illuminate\Http\Response
     */
    public function balance(Request $request)
    {
        if(!\Zizaco\Entrust\EntrustFacade::can('view_balance_stats')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете просматривать свой баланс!' ]);
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'ИСТОРИЯ БАЛАНСА';

        $data = $this->getProfileBalance($data, Auth::user()->id);

        return view('profile/balance', $data);
    }

    public function Busy(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'ПРОФИЛЬ';

        $user = Auth::user();
        return view('profile/busy', $data);
    }

    public function loadBusy(Request $request)
    {
        $user = Auth::user();

        $start = $request->input('start', false);
        $end = $request->input('end', false);

        $events = Busy::where('user_id', $user->id);

        if($start && $end){
            $start = Carbon::createFromTimestamp($start);
            $end = Carbon::createFromTimestamp($end);
            $period = CarbonPeriod::create($start, $end);
            $events = $events->overlaps($period);
        }

        $events = $events->get();

        $busy_code = config('project.busy_code');
        $busy_text = config('project.busy_text');

        $data = $events->map(function($event) use($busy_code, $busy_text){
            $data = [
                'id' => $event->id,
                'text' => $busy_text[$event->busy] ?? 'статус неизвестен',
                'startDate' => $event->started_at->toIso8601ZuluString(),
                'endDate' => $event->ended_at->toIso8601ZuluString(),
                'comment' => nl2br($event->comment),
                'busy' => $busy_code[$event->busy] ?? 'none'
            ];

            return $data;
        });

        return response()->json($data);
    }

    public function saveBusy(Request $request)
    {

        $from = Carbon::createFromTimestamp($request->input('from'));
        $to = Carbon::createFromTimestamp($request->input('to'));

        $status = (int)$request->input('status');
        $comment = $request->input('comment');

        Busy::create([
            'user_id' => auth()->user()->id,
            'started_at' => $from,
            'ended_at' => $to,
            'busy' => $status,
            'comment' => $comment
        ])->save();


        return response()->json([]);
    }

    /*
     * Создание заявки на ввод/вывод средств
     *
     * @return \Illuminate\Http\Response
     */
    public function AddCashFlow(Request $request)
    {
        if(isset($request->amount)){
            $balance = Auth::user()->balance();
            if(!in_array($request->type, [0,1])){
                return back()->with('error', 'Некорректные параметры запроса!');
            }
            if($request->amount > 0){
                $amount = (float) abs($request->amount);

                if(($request->type == CashFlow::type_by_code('TAKE') && $balance >= $amount) || $request->type == CashFlow::type_by_code('RECHARGE')){
                    $cash_flow = new CashFlow();

                    $cash_flow->user_id = Auth::user()->id;
                    $cash_flow->amount = $amount;
                    $cash_flow->carried = 0;
                    $cash_flow->comment = $request->comment;
                    $cash_flow->type = $request->type;

                    $cash_flow->save();

                    //CashFlow::crossFind('many');
                    Artisan::call('cashflow:crossfind');

                    $cash_flow = CashFlow::find($cash_flow->id);
                    $cash_flow_result = false;
                    if($cash_flow->carried > 0){
                        $cash_flow_result = true;
                    }

                    return back()->with('success', 'Заявка создана')->with('cash_flow_result', $cash_flow_result);

                }else{
                    return back()->with('error', 'У вас нет такой суммы на балансе!');
                }
            }else{
                    return back()->with('error', 'Сумма должна быть больше нуля!');
            }
        }else{
            return back()->with('error', 'Неверно указана сумма заявки!');
        }
    }

    public function CashFlowTransactionAccept(Request $request){
        $transaction_id = $request->transaction_id;
        $transaction = BalanceTransaction::findOrFail($transaction_id);

        if($transaction->user_id == Auth::user()->id){
            DB::transaction(function () use($transaction) {
                if($cross_transaction = $transaction->cross){
                    $transaction->accept();
                    $cross_transaction->accept();
                }

            });

            return back()->with('success', 'Получение денег подтверждено! Баланс изменён');

        }else{
            return back()->with('error', 'Вы не можете подтверждать чужие заявки!');
        }
    }

    protected function getProfileBalance($data, $user_id){

        $data['user'] = User::findOrFail($user_id);

        $data['PAGE_TITLE'] = \Illuminate\Support\Str::upper($data['user']->name);

        $transactions_data = $data['user']->transactions()->orderBy('created_at')->get();

        $data['transactions_by_project'] = $transactions_data->groupBy('project_id');

        $data['transactions'] = [];

        $transactions_project = [];
        $transactions_single = [];
        foreach($transactions_data as $transaction){
            if( $transaction->project){
                $transactions_project[] = $transaction;
            }else{
                $transactions_single[] = $transaction;
            }
        }
        $transactions_project = collect($transactions_project)->unique('project_id');

        /*$data['transactions'] = collect(
            array_merge($transactions_project, $transactions_single)
        );*/
        $data['transactions'] = $transactions_project->merge($transactions_single)->sortByDesc('created_at');


        return $data;
    }
}

