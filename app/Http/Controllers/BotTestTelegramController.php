<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class BotTestTelegramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


private function file_get_contents_curl($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $data = curl_exec($ch);

    print curl_error($ch );

    curl_close($ch);

    return $data;
}


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bot_token = config('services.telegram-bot-api.token');

        $content = file_get_contents("php://input");
        $update = json_decode($content, TRUE);

        if(isset($update["message"])){
            $message = $update["message"];
        }else{
            $message = '';
        }

        //$data = print_r($update, true);
        //$result = file_put_contents('/var/www/kcm82/test.txt', $data);

        $MSG = '';

        $text = '';
        if(isset($message['text'])){
            $text = $message['text'];
        }
        $chat_id = 0;
        if(isset($message['chat']) && isset($message['chat']['id'])){
            $chat_id = $message['chat']['id'];
            $user = User::where('telegram_chat_id', $chat_id)->first();
        }

        if(preg_match("/(?=\/start ){0,1}([0-9]{3}-[0-9]{3}-[0-9]{3}-[0-9]{3})/", $text, $match)) {
            if (isset($match[1]) && $match[1]) {
                $find_code = $match[1];
                $user = User::where('telegram_find_code', $find_code)->first();
                if ($user) {

                    $user->telegram_chat_id = $chat_id;
                    $user->telegram_find_code = '';

                    $notify_channels = $user->notify_channels;
                    $notify_channels = explode(',', $notify_channels);
                    if ( ! in_array('telegram', $notify_channels)) {
                        $notify_channels[] = 'telegram';
                        $user->notify_channels = implode(',', $notify_channels);
                    }

                    $user->save();

                    $this->file_get_contents_curl('https://api.telegram.org/bot'.$bot_token.'/sendSticker?chat_id='.$user->telegram_chat_id.'&sticker=CAADBAADHwADmDVxAsVMpnbj30pPAg');

                    $MSG = 'Слава роботам!';
                    $this->file_get_contents_curl('https://api.telegram.org/bot'.$bot_token.'/sendMessage?chat_id='.$user->telegram_chat_id.'&parse_mode=HTML&text='.$MSG);

                    $MSG = 'Привет, <b>'.$user->name."</b>. Я опознал и запомнил тебя! Канал уведомлений через телеграм активирован, ты всегда можешь отключить его в настройках.";
                    $this->file_get_contents_curl('https://api.telegram.org/bot'.$bot_token.'/sendMessage?chat_id='.$user->telegram_chat_id.'&parse_mode=HTML&text='.$MSG);
                } else {
                    $this->file_get_contents_curl('https://api.telegram.org/bot'.$bot_token.'/sendSticker?chat_id='.$user->telegram_chat_id.'&sticker=CAADBAADEwADmDVxAp3k1xTFyNcyAg');

                    $MSG = 'Человек, ключ '.$find_code.' неверный! Попробуй ещё раз!';
                    $this->file_get_contents_curl('https://api.telegram.org/bot'.$bot_token.'/sendMessage?chat_id='.$user->telegram_chat_id.'&text='.$MSG);
                }
            } else {

                $MSG = 'НЕ УДАЛОСЬ РАСПОЗНАТЬ КОД АВТОРИЗАЦИИ!';
                $this->file_get_contents_curl('https://api.telegram.org/bot'.$bot_token.'/sendMessage?chat_id=70829226&text='.$MSG);

            }

        }elseif(preg_match("/(?=\/id){1}/", $text, $match)){
            $MSG = 'ID ' . $chat_id;
            //$MSG .= print_r($update, true);
            $this->file_get_contents_curl('https://api.telegram.org/bot' . $bot_token . '/sendMessage?chat_id=' .  '246242179' . '&text=' . $MSG);
        }else{

            $this->file_get_contents_curl('https://api.telegram.org/bot' . $bot_token . '/sendSticker?chat_id=' . $user->telegram_chat_id . '&sticker=CAADBAADGwADmDVxApFTHg4W1PB8Ag');


            $MSG = 'Поцелуй мой блестящий металлический зад, человек. Я не понял, что ты мне сказал!';

            //file_put_contents('/var/www/kcm82/test.txt', print_r($update, true));

            $this->file_get_contents_curl('https://api.telegram.org/bot' . $bot_token . '/sendMessage?chat_id=' . $user->telegram_chat_id . '&text=' . $MSG);
        }


    }
}
