<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\UserInfo;
use App\Project;
use App\BalanceTransaction;
use Excel;
use Carbon\Carbon;


class ExcelExportController extends Controller
{

    /*
     * Редактирование профиля
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'ЭКСПОРТ В EXCEL';

        return view('excel/export/index', $data);
    }

    public function projects(Request $request){
        if(!Entrust::can('excel')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете экспортировать проекты!' ]);
            exit();
        }

        $data = [];

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        if(($date_start && $date_end) || (!$date_start && !$date_end) ){

            $projects = new Project;
            if($date_start){
                $date_start_carbon = Carbon::createFromFormat('Y-m-d', $date_start, config('project.city_time_zone'))->startOfDay()->setTimezone('UTC');
                $date_end_carbon = Carbon::createFromFormat('Y-m-d', $date_end, config('project.city_time_zone'))->endOfDay()->setTimezone('UTC');
                $projects = $projects->where('date_start', '>=', $date_start_carbon)
                                     ->where('date_start', '<=', $date_end_carbon);
            }

            $data['projects'] = $projects->get();

            $file_name = 'export_projects';
            Excel::create($file_name, function ($excel) use ($data) {

                $excel->sheet('list', function ($sheet) use ($data) {

                    $sheet->setOrientation('landscape');
                    $sheet->loadView('excel.tpl.projects', $data);

                    $sheet->freezeFirstRow();

                });

            })->export('xls');

        }else{
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы неверно указали дату начала и конца' ]);
        }

    }

    public function transactions(Request $request){
        if(!Entrust::can('excel')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете экспортировать проекты!' ]);
            exit();
        }

        $data = [];

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        if(($date_start && $date_end) || (!$date_start && !$date_end) ){

            $transactions = new BalanceTransaction;
            if($date_start){
                $date_start_carbon = Carbon::createFromFormat('Y-m-d', $date_start, config('project.city_time_zone'))->startOfDay()->setTimezone('UTC');
                $date_end_carbon = Carbon::createFromFormat('Y-m-d', $date_end, config('project.city_time_zone'))->endOfDay()->setTimezone('UTC');
                $transactions = $transactions->where('created_at', '>=', $date_start_carbon)
                                     ->where('created_at', '<=', $date_end_carbon);
            }

            $data['transactions'] = $transactions->get();

            $file_name = 'export_transactions';
            Excel::create($file_name, function ($excel) use ($data) {

                $excel->sheet('list', function ($sheet) use ($data) {

                    $sheet->setOrientation('landscape');
                    $sheet->loadView('excel.tpl.transactions', $data);

                    $sheet->freezeFirstRow();

                });

            })->export('xls');

        }else{
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы неверно указали дату начала и конца' ]);
        }

    }

}
