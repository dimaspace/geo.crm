<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserInfo;
use App\Region;
use App\BalanceTransaction;
use Formery\FormBuilder;
use Config;
use App\Formery\FieldTypes\Hidden;
use Auth;
use App\Role;
use Validator;
use Illuminate\Validation\Rule;
use Entrust;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Список фотографов.
     *
     * @return \Illuminate\Http\Response
     */
    public function photo(Request $request)
    {

        $regions = Region::whereIn('code', Config::get('project.regions'))->orderBy('title', 'desc')->get();
        $regions_options = [];
        //$regions_options[] = ['label' => 'без региона', 'value' => 0];
        foreach($regions as $region){
            $regions_options[] = ['label' => $region->title, 'value' => $region->code];
        }

        $yes_no_options = [
            ['label' => 'Нет', 'value' => 0],
            ['label' => 'Да', 'value' => 1]
        ];

        if($request->edit){
            $action = '/users/photo/edit';
        }else{
            $action = '/users/photo/add';
        }
        $form_edit = new FormBuilder($action, 'POST', ['class'=>'form-horizontal']);

        $form_edit->addField('user_id', '',  Hidden::class);
        $form_edit->addField('form_type', '',  Hidden::class)->value('photo');
        $form_edit->addField('name', 'Имя', \Formery\FieldTypes\Text::class);
        $form_edit->addField('threshold', 'Порог блокировки', \Formery\FieldTypes\Text::class)->initial(0)->value(0);
        $form_edit->addField('email', 'E-mail', \Formery\FieldTypes\Text::class);
        $form_edit->addField('region', 'Регионы', \Formery\FieldTypes\Select::class, ['multiple'=>'multiple'])->addRules('required')->options($regions_options);
        $form_edit->addField('primary_region', 'Главный регион', \Formery\FieldTypes\Select::class)->addRules('required')->options($regions_options);
        $form_edit->addField('is_staff', 'Штатный?', \Formery\FieldTypes\Select::class)->addRules('required')->options($yes_no_options);
        $form_edit->addField('hidden', 'Скрыть из списков?', \Formery\FieldTypes\Select::class)->addRules('required')->options($yes_no_options);
        $form_edit->addField('ed_fee_cash', 'Редсбор нал', \Formery\FieldTypes\Text::class);
        $form_edit->addField('ed_fee_noncash', 'Редсбор безнал', \Formery\FieldTypes\Text::class);
        $form_edit->addField('vk_id', 'VK ID', \Formery\FieldTypes\Text::class);

        $form_edit->addField('phone', 'Телефон', \Formery\FieldTypes\Text::class);
        $form_edit->addField('skype', 'Скайп', \Formery\FieldTypes\Text::class);
        $form_edit->addField('instagramm', 'Instagramm', \Formery\FieldTypes\Text::class);
        $form_edit->addField('in_g', 'В G сообществe?', \Formery\FieldTypes\Select::class)->addRules('required')->options($yes_no_options);
        $form_edit->addField('bank_card', 'Номер карты для оплаты', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');

        $form_edit->addField('camera_body', 'Камеры', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');
        $form_edit->addField('camera_glasses', 'Стёкла', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');
        $form_edit->addField('camera_accessory', 'Аксесуары(вспышки и тд)', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');

        $form_edit->addField('size', 'Размер одежды(футболка)', \Formery\FieldTypes\Text::class);

        return view('users.photo', ['form_edit' => $form_edit]);
    }

    /**
     * Список всех юзеров.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {

        $regions = Region::whereIn('code', Config::get('project.regions'))->orderBy('title', 'desc')->get();
        $regions_options = [];
        //$regions_options[] = ['label' => 'без региона', 'value' => 0];
        foreach($regions as $region){
            $regions_options[] = ['label' => $region->title, 'value' => $region->code];
        }

        $yes_no_options = [
            ['label' => 'Нет', 'value' => 0],
            ['label' => 'Да', 'value' => 1]
        ];

        if($request->edit){
            $action = '/users/photo/edit';
        }else{
            $action = '/users/photo/add';
        }

        $form_edit = new FormBuilder($action, 'POST', ['class'=>'form-horizontal']);

        $form_edit->addField('user_id', '',  Hidden::class);
        $form_edit->addField('form_type', '',  Hidden::class)->value('all');
        $form_edit->addField('name', 'Имя', \Formery\FieldTypes\Text::class);
        $form_edit->addField('threshold', 'Порог блокировки', \Formery\FieldTypes\Text::class)->initial(0)->value(0);
        $form_edit->addField('email', 'E-mail', \Formery\FieldTypes\Text::class);
        $form_edit->addField('region', 'Регионы', \Formery\FieldTypes\Select::class, ['multiple'=>'multiple'])->addRules('required')->options($regions_options);
        $form_edit->addField('primary_region', 'Главный регион', \Formery\FieldTypes\Select::class)->addRules('required')->options($regions_options);
        $form_edit->addField('is_staff', 'Штатный?', \Formery\FieldTypes\Select::class)->addRules('required')->options($yes_no_options);
        $form_edit->addField('hidden', 'Скрыть из списков?', \Formery\FieldTypes\Select::class)->addRules('required')->options($yes_no_options);
        $form_edit->addField('ed_fee_cash', 'Редсбор нал', \Formery\FieldTypes\Text::class);
        $form_edit->addField('ed_fee_noncash', 'Редсбор безнал', \Formery\FieldTypes\Text::class);
        $form_edit->addField('vk_id', 'VK ID', \Formery\FieldTypes\Text::class);

        $form_edit->addField('phone', 'Телефон', \Formery\FieldTypes\Text::class);
        $form_edit->addField('skype', 'Скайп', \Formery\FieldTypes\Text::class);
        $form_edit->addField('instagramm', 'Instagramm', \Formery\FieldTypes\Text::class);
        $form_edit->addField('in_g', 'В G сообществe?', \Formery\FieldTypes\Select::class)->addRules('required')->options($yes_no_options);
        $form_edit->addField('bank_card', 'Номер карты для оплаты', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');

        $form_edit->addField('camera_body', 'Камеры', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');
        $form_edit->addField('camera_glasses', 'Стёкла', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');
        $form_edit->addField('camera_accessory', 'Аксесуары(вспышки и тд)', \Formery\FieldTypes\Text::class)->setViewFile('vendor.form.fields.textarea');

        $form_edit->addField('size', 'Размер одежды(футболка)', \Formery\FieldTypes\Text::class);

        return view('users.all', ['form_edit' => $form_edit]);
    }

    public function photoData(Request $request)
    {

    }

    /**
     * Создание фотографа.
     *
     * @return \Illuminate\Http\Response
     */
    public function photoAdd(Request $request)
    {

        $data = $request->all();

        $messages = [
          'primary_region_code.in_array' => 'Главного региона нет в списке регионов пользователя',
        ];

        $password = bcrypt(str_random(40));

        $data_save = [
            'email' => $data['email'],
            'name' => $data['name'],
            'is_staff' => $data['is_staff'],
            'ed_fee_cash' => $data['ed_fee_cash'] ? $data['ed_fee_cash'] : NULL,
            'ed_fee_noncash' => $data['ed_fee_noncash'] ? $data['ed_fee_noncash'] : NULL,
            'vk_id' => $data['vk_id'] ? $data['vk_id'] : 0,
            'hidden' => $data['hidden'] ? $data['hidden'] : 0,
            'password' => $password,
            'password_confirmation' => $password,
            'threshold' => $data['threshold'] ? $data['threshold'] : 0,
            'region' => isset($data['region']) ? $data['region'] : [],
            'primary_region_code' => $data['primary_region'] ? $data['primary_region'] : ''
        ];

        $validator = Validator::make($data_save, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'threshold' => 'numeric|min:0',
            'vk_id' => 'numeric|min:0',
            'primary_region_code' => 'required|in_array:region.*'
        ], $messages);

        if ($validator->fails()) {
          return redirect('/users/photo')
                      ->withErrors($validator)
                      ->withInput();
        }


        $user = User::create($data_save);

        if(isset($data['region']) && $data['region'] != null && count($data['region']) > 0){
            $regions = Region::whereIn('code', $data['region'])->get();
            $user->regions()->saveMany($regions);
        }

        $user_info = $user->info;
        if (!$user_info) {
            $user_info = new UserInfo();
        }

        $user_info->phone = $data['phone'];
        $user_info->skype = $data['skype'];
        $user_info->instagramm = $data['instagramm'];
        $user_info->in_g = $data['in_g'] ? 1 : 0;
        $user_info->bank_card = $data['bank_card'];
        $user_info->size = $data['size'];


        $user_info->camera_body = $data['camera_body'];
        $user_info->camera_glasses = $data['camera_glasses'];
        $user_info->camera_accessory = $data['camera_accessory'];


        $user->info()->save($user_info);

        $user->attachRole(Role::where('name', 'photo')->first());


        if($data['form_type'] == 'all'){
            $redirect = '/users/all';
        }else{
            $redirect = '/users/photo';
        }

        return redirect($redirect)->with('success', 'Фотограф создан!');

    }
    /**
     * Редактирование фотографа.
     *
     * @return \Illuminate\Http\Response
     */
    public function photoEdit(Request $request)
    {
        $user_id = $request->input('user_id');
        $user = User::findOrFail($user_id);

        $data = $request->all();

        $messages = [
          'primary_region_code.in_array' => 'Главного региона нет в списке регионов пользователя',
        ];

        $data_save = [
            'email' => $data['email'],
            'name' => $data['name'],
            'is_staff' => $data['is_staff'],
            'ed_fee_cash' => $data['ed_fee_cash'] ? $data['ed_fee_cash'] : NULL,
            'ed_fee_noncash' => $data['ed_fee_noncash'] ? $data['ed_fee_noncash'] : NULL,
            'vk_id' => $data['vk_id'] ? $data['vk_id'] : 0,
            'hidden' => $data['hidden'] ? $data['hidden'] : 0,
            'threshold' => $data['threshold'] ? $data['threshold'] : 0,
            'region' => isset($data['region']) ? $data['region'] : [],
            'primary_region_code' => $data['primary_region'] ? $data['primary_region'] : ''
        ];

        $validator = Validator::make($data_save, [
            'name' => 'required|max:255',
            'email' => [
                'required', 'email', 'max:255',
                Rule::unique('users')->ignore($user_id)
            ],
            'threshold' => 'numeric|min:0',
            'vk_id' => 'numeric|min:0',
            'primary_region_code' => 'required|in_array:region.*'
        ], $messages);

        if ($validator->fails()) {
          return redirect('/users/photo?edit=1')
                      ->withErrors($validator)
                      ->withInput();
        }

        $user->regions()->detach();

        if(isset($data['region']) && $data['region'] != null && count($data['region']) > 0){
            $regions = Region::whereIn('code', $data['region'])->get();
            $user->regions()->saveMany($regions);
        }

        $user->update($data_save);

        $user_info = $user->info;
        if (!$user_info) {
            $user_info = new UserInfo();
        }

        $user_info->phone = $data['phone'];
        $user_info->skype = $data['skype'];
        $user_info->instagramm = $data['instagramm'];
        $user_info->in_g = $data['in_g'] ? 1 : 0;
        $user_info->bank_card = $data['bank_card'];
        $user_info->size = $data['size'];


        $user_info->camera_body = $data['camera_body'];
        $user_info->camera_glasses = $data['camera_glasses'];
        $user_info->camera_accessory = $data['camera_accessory'];


        $user->info()->save($user_info);


        if($data['form_type'] == 'all'){
            $redirect = '/users/all';
        }else{
            $redirect = '/users/photo';
        }

        return redirect($redirect)->with('success', 'Фотограф изменён!');

    }

    /**
     * Получение данных о фотографе.
     *
     * @return \Illuminate\Http\Response
     */
    public function photoGetOne(Request $request)
    {
        $user_id = $request->input('user_id');
        $user = User::findOrFail($user_id);

        $info = [];
        if($user->info){
            $info = $user->info;
        }

        $region_codes = [];
        if($user->regions){
            foreach($user->regions as $user_region)
            $region_codes[] = $user_region->code;
        }

        return response()->json([
            'user' => $user,
            'info' => $info,
            'region_codes' => $region_codes
        ]);
    }

    public function profile($user_id){
        $data = [];

        $data['user'] = User::findOrFail($user_id);

        $data['PAGE_TITLE'] = \Illuminate\Support\Str::upper($data['user']->name);

        $transactions_data = $data['user']->transactions()->orderBy('created_at')->get();

        $data['transactions_by_project'] = $transactions_data->groupBy('project_id');

        $data['transactions'] = [];

        $transactions_project = [];
        $transactions_single = [];
        foreach($transactions_data as $transaction){
            if( $transaction->project){
                $transactions_project[] = $transaction;
            }else{
                $transactions_single[] = $transaction;
            }
        }
        $transactions_project = collect($transactions_project)->unique('project_id');

        /*$data['transactions'] = collect(
            array_merge($transactions_project, $transactions_single)
        );*/
        $data['transactions'] = $transactions_project->merge($transactions_single)->sortByDesc('created_at');

        return view('users.user_profile', $data);
    }

    public function addTransaction(Request $request){
        $data = $request->all();

        if(Auth::user()->can('manual_transactions')){

            if((int) $data['to'] == (int) $data['from']){
                return redirect('/users/profile/' . $data['user_id'])->with('error', 'Пользователи должны быть разными!');
            }

            if($data['value'] <= 0){
                return redirect('/users/profile/' . $data['user_id'])->with('error', 'Сумма должна быть больше нуля');
            }

            $data['value'] = str_replace(',', '.', $data['value']);

            $transaction_1 = new BalanceTransaction();

            $user1 = $transaction_1->last_balance = User::findOrNew((int) $data['to']);
            $transaction_1->value = $data['value'];
            $transaction_1->type = BalanceTransaction::_getTypeIdByCode($data['type']);
            $transaction_1->cr_type = Config::get('transaction.cr_types')['MANUAL'];
            $transaction_1->desc = $data['comment'];
            $transaction_1->accepted = 1;
            $transaction_1->user_id = (int) $data['to'];
            $transaction_1->creator_id = Auth::user()->id;
            $transaction_1->last_balance = $user1->balance();
            $transaction_1->now_balance = $user1->balance() + $data['value'];
            $transaction_1->save();

            $transaction_2 = new BalanceTransaction();

            $user2 = $transaction_2->last_balance = User::findOrNew((int) $data['from']);
            $transaction_2->value = - $data['value'];
            $transaction_2->type = BalanceTransaction::_getTypeIdByCode($data['type']);
            $transaction_2->cr_type = Config::get('transaction.cr_types')['MANUAL'];
            $transaction_2->desc = $data['comment'];
            $transaction_2->accepted = 1;
            $transaction_2->user_id = (int) $data['from'];
            $transaction_2->creator_id = Auth::user()->id;
            $transaction_2->last_balance = $user2->balance();
            $transaction_2->now_balance = $user2->balance() - $data['value'];
            $transaction_2->save();


            return redirect('/users/profile/' . $data['user_id'])->with('success', 'Транзакции проведены!');
        }else{
            return redirect('/users/profile/' . $data['user_id'])->with('error', 'У вас нет права создавать ручные транзакции!');
        }
    }

    public function login(Request $request){
        if(!Entrust::can('free_login')) {
            return response()->json(['error' => 'Вы не так круты, чтоб логиниться под других юзеров :)']);
            exit();
        }

        $user_id = (integer) $request->id;
        if($user_id === 1){
            return redirect('/')->with('error', 'Под админа логиниться нельзя');
            exit();
        }

        $user = User::find($user_id);

        if(!$user){
            return redirect('/')->with('error', 'Такого пользователя нет в системе');
            exit();
        }

        Auth::loginUsingId($user->id);

        return redirect('/')->with('success', 'Вы успешно залогнены под пользователя ' . $user->name);
    }

    public function getUsersJson(Request $request){

        $filter_in = [];
        $filter = [];

        $filter_request = $request->input('projects_filter');

        if (isset($filter_request['regions'])) {
            $filter_in[] = [ 'region.code', $filter_request['regions'] ];
        }

        if (isset($filter_request['workers'])) {
            if (count($filter_request['workers'])) {
                $filter_in[] = [ 'worker_id', $filter_request['workers'] ];
            }
        }

        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'photo');
        })->where('hidden', 0)
        ->where(function ($query) use ($filter_request) {

            if (isset($filter_request['regions'])) {
                $query->whereHas('regions', function ($query) use ($filter_request) {
                    $query->whereIn('code', $filter_request['regions']);
                });
            }
            if (isset($filter_request['workers'])) {
                if (count($filter_request['workers'])) {
                    $query->whereIn('id', $filter_request['workers']);
                }
            }
            if (isset($filter_request['staff'])) {
                if (count($filter_request['staff'])) {
                    $query->whereIn('is_staff', $filter_request['staff']);
                }
            }

        })->orderByDesc('primary_region_code')->get();

        $result = [];
        $result[] = [
            'id' => 0,
            'name' => 'не распределено',
            'is_staff' => 0,
            'vk_id' => 0,
            'threshold' => 0,
            'balance' => 0,
            'is_debt' => 0,
            'region_ids' => [],
            'region_codes' => [],
            'primary_region' =>  ''
        ];


        foreach($users as $user){
            $result[] = [
                'id' => $user->id,
                'name' => $user->name,
                'is_staff' => $user->is_staff,
                'vk_id' => $user->vk_id,
                'threshold' => $user->threshold,
                'balance' => number_format($user->balanceCached(), 0, ',', ' '),
                'is_debt' => $user->isDebt(),
                'region_ids' => $user->getRegionsIds(),
                'region_codes' => $user->getRegionsCodes(),
                'primary_region' => $user->primary_region_code
            ];
        }

        return response()->json($result);
    }
}
