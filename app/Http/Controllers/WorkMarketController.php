<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Project;
use App\ProjectDesire;
use Formery\FormBuilder;
use Config;
use App\Formery\FieldTypes\Hidden;

class WorkMarketController extends Controller
{

    public function desireAdd(Request $request)
    {

        $project_id = (int) $request->input('project_id');
        $project = Project::findOrFail($project_id);

        $desire_rating = (int) $request->input('desire_rating');

        $project->desireAuth()->delete();

        $saved = false;

        if($desire_rating > 0){
            if($desire_rating > 5){
                $desire_rating = 5;
            }

            $project->desire()->save(new ProjectDesire([
                'user_id' => Auth::user()->id,
                'rating' => $desire_rating
            ]));

            $saved = true;
        }

        return response()->json([
            'saved' => $saved
        ]);
    }


}
