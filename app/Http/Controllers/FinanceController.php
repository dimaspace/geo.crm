<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\UserInfo;
use App\CashFlow;
use App\MoeDeloSaleBill;
use Config;
use Carbon\Carbon;
use App\MoeDeloKontragent;
use Artisan;
use League\Flysystem\FileNotFoundException;
use Storage;


class FinanceController extends Controller
{

    public function cashFlows(Request $request){
        if(!Entrust::can('excel')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете смотреть статистику по финансам!' ]);
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'СПИСОК НЕВЫПОЛНЕННЫХ ВЗАИМОЗАЧЁТОВ';

        return view('finance/cash_flows', $data);

    }

    public function cashFlowsJson(Request $request){
        if(!Entrust::can('excel')) {
            return response()->redirectTo('/excel/export')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        $cash_flows = CashFlow::NotCarried()->get();

        $data = [];

        foreach($cash_flows as $cash_flow){



            $data[] = [
                "id" => $cash_flow->id,
                "user_id" => $cash_flow->user_id,
                "user_name" => $cash_flow->user->name,
                "amount" => $cash_flow->amount,
                "carried" => $cash_flow->carried,
                "left" => $cash_flow->amount - $cash_flow->carried,
                "comment" => $cash_flow->comment,
                "type" => $cash_flow->type,
                "created_at" => $cash_flow->created_at->format('Y/m/d H:i:s'),
                "updated_at" => $cash_flow->updated_at->format('Y/m/d H:i:s'),
            ];
        }

        return response()->json($data);

    }

    public function buhProjects(Request $request){
        if(!Entrust::can('view_booker')) {
            abort(401, 'Вы не имеете прав для доступа в этот раздел!');
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'ПАНЕЛЬ БУХГАЛТЕРА';

        $unpaid_bills = MoeDeloSaleBill::where('status_id', '<>', 6);
        $data['unpaid_bills_num'] = $unpaid_bills->count();

        $deadline_exp_unpaid_bills = $unpaid_bills->where('doc_dd_date', '<=', Carbon::now());
        $data['deadline_exp_unpaid_bills_num'] = $deadline_exp_unpaid_bills->count();

        return view('finance/buh_projects', $data);

    }

    public function billsJson(Request $request){
        if(!Entrust::can('view_booker')) {
            abort(401, 'Вы не имеете прав для доступа в этот раздел!');
            return response()->json(['error' => 'нет доступа!']);
            exit();
        }

        $unpaid_bills = MoeDeloSaleBill::where('status_id', '<>', 6)->get();

        $data = [];

        foreach($unpaid_bills as $bill){

            $dl_exp = 0;
            if($bill->doc_dd_date && $bill->doc_dd_date < Carbon::now()){
                $dl_exp = 1;
            }

            $data[] = [
                "id" => $bill->id,
                "doc_date" => $bill->doc_date ? $bill->doc_date->format('Y/m/d H:i:s') : false,
                "doc_dd_date" => $bill->doc_dd_date ? $bill->doc_dd_date->format('Y/m/d H:i:s') : false,
                "doc_create_date" => $bill->doc_create_date ? $bill->doc_create_date->format('Y/m/d H:i:s') : false,
                "doc_modify_date" => $bill->doc_modify_date ? $bill->doc_modify_date->format('Y/m/d H:i:s') : false,
                "number" => $bill->number,
                "online" => $bill->online,
                "type_id" => $bill->type_id,
                "status_id" => $bill->status_id,
                "info" => $bill->info,
                "nds_id" => $bill->nds_id,
                "is_covered" => $bill->is_covered,
                "sum" => $bill->sum,
                "paid_sum" => $bill->paid_sum,
                "dl_exp" => $dl_exp,
                "kontragent_id" => $bill->kontragent_id,
                "kontragent_name" => MoeDeloKontragent::getKontragentNameById($bill->kontragent_id)
            ];
        }

        return response()->json($data);

    }

    public function billsLoadFromApi(Request $request){
        if(!Entrust::can('view_booker')) {
            abort(401, 'Вы не имеете прав для доступа в этот раздел!');
            return response()->json(['error' => 'нет доступа!']);
            exit();
        }

        $log = (int) $request->input('log');

        $log_path = '/public/moedelo_update_logs/' . $log . '.txt';
        Storage::disk('local')->put($log_path, 0);

        Artisan::queue('moedelo:load', [
            '--log' => $log
        ])->onQueue('unlimed');

        $data = ['success' => true];

        return response()->json($data);

    }

    public function billsLoadFromApiProgress(Request $request){
        if(!Entrust::can('view_booker')) {
            abort(401, 'Вы не имеете прав для доступа в этот раздел!');
            return response()->json(['error' => 'нет доступа!']);
            exit();
        }

        $log = (int) $request->input('log');
        $log_path = '/public/moedelo_update_logs/' . $log . '.txt';
        try{
            if(Storage::disk('local')->has($log_path)){
                $data = ['progress' => Storage::disk('local')->get($log_path)];
            }else{
                throw new FileNotFoundException($log_path);
            }

        }
        catch(FileNotFoundException $e){
            $data = ['progress' => false];
        }


        return response()->json($data);
    }

    public function billsGetDataForInfoboxesJson(Request $request){
        if(!Entrust::can('view_booker')) {
            abort(401, 'Вы не имеете прав для доступа в этот раздел!');
            return response()->json(['error' => 'нет доступа!']);
            exit();
        }

        $unpaid_bills = MoeDeloSaleBill::where('status_id', '<>', 6);

        $data = [
            'unpaid_bills' => $unpaid_bills->count(),
            'deadline_bills' => $unpaid_bills->where('doc_dd_date', '<=', Carbon::now())->count()
        ];

        return response()->json($data);
    }

    public function billsForceClose(Request $request){
        if(!Entrust::can('view_booker')) {
            abort(401, 'Вы не имеете прав для доступа в этот раздел!');
            return response()->json(['error' => 'нет доступа!']);
            exit();
        }

        $id = (int) $request->input('id');

        $bill = MoeDeloSaleBill::findOrFail($id);
        $bill->status_id = 6;
        $bill->is_force_closed = 1;
        $bill->save();

        $data = ['result' => 'OK'];

        return response()->json($data);
    }

}
