<?php

namespace App\Http\Controllers\Portfolio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Media;
use Spatie\Tags\Tag;
use Cache;

class HomeController extends Controller
{
    protected $all_tags_link_active = false;
    protected $all_users_link_active = false;

    public function index() {
        $filter = request()->input('filter', []);

        $users = User::whereHas('media', function ($query) {
            $query->where('collection_name', 'portfolio')->has('tags', '>', 0);
        })->orderBy('is_staff', 'desc')->get()->map(function ($user) use ($filter) {
            $user->selected = false;

            $slug = (string)$user->id;
            if (isset($filter['mans'])) {
                $check = array_search($slug, $filter['mans'], true);
                if ($check !== false) {
                    $user->selected = true;
                    unset($filter['mans'][$check]);
                } else {
                    array_push($filter['mans'], $slug);
                }
            } else {
                $this->all_users_link_active = true;
                $filter['mans'] = [$slug];
            }

            $user->link = route('extraPortfolioMainPage', ['filter' => $filter]);
            return $user;
        });
        $all_users_filter = $filter;
        unset($all_users_filter['mans']);

        $items = Media::Collection('portfolio');
        $items->when(isset($filter['mans']), function ($query) use($filter){
            $man_ids = collect(($filter['mans']))->map(function($man){
                return (integer)$man;
            })->filter(function($man){
                return $man > 0;
            });
            return $query->whereIn('model_id', $man_ids);
        });
        $items->when(isset($filter['tags']), function ($query) use($filter){
            /*$tags = [];
            foreach ($filter['tags'] as $tag_slug){
                $tags[] = Tag::where("id", $tag_slug)->first()->name;
            }*/
            return $query->whereHas('tags', function ($query) use($filter) {
                $query->whereIn('id', $filter['tags']);
            });
        });
        $items = $items->with('tags')->has('tags', '>', 0)->approved()->simplePaginate(20);

        $tags = Cache::remember('tags_used', 60 * 12, function () {
            return Tag::WithType('photo')->get()->filter(function ($tag) {
                return Media::whereHas('tags', function ($query) use ($tag) {
                        $query->whereId($tag->id);
                    })->count() > 0;
            });
        });

        $tags = $tags->map(function ($tag) use($filter){
            $tag->selected = false;

            $slug = $tag->id;
            if(isset($filter['tags'])){
                $check = array_search($slug, $filter['tags'], false);
                if($check !== false){
                    $tag->selected = true;
                    unset($filter['tags'][$check]);
                }else{
                    array_push($filter['tags'], $slug);
                }
            }else{
                $this->all_tags_link_active = true;
                $filter['tags'] = [$slug];
            }

            $tag->link = route('extraPortfolioMainPage', ['filter' => $filter]);
            return $tag;
        });
        $all_tags_filter = $filter;
        unset($all_tags_filter['tags']);

        return view('extra_sites.portfolio.main', [
            'items' => $items,
            'tags' => $tags,
            'users' => $users,
            'filter' => $filter,
            'all_tags_link' => route('extraPortfolioMainPage', ['filter' => $all_tags_filter]),
            'all_tags_link_active' => $this->all_tags_link_active,
            'all_users_link' => route('extraPortfolioMainPage', ['filter' => $all_users_filter]),
            'all_users_link_active' => $this->all_users_link_active
        ]);
    }
}
