<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\UserInfo;
use App\Project;
use Config;


class StatsController extends Controller
{

    public function projects(Request $request){
        if(!Entrust::can('all_projects_stats')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'СТАТИСТИКА ПРОЕКТОВ';

        return view('stats/projects', $data);

    }

    public function projectsSelfPhoto(Request $request){
        if(!Entrust::can('self_photo_projects_stats')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'СТАТИСТИКА ВАШИХ ПРОЕКТОВ';

        return view('stats/projects_self_photo', $data);

    }

    public function projectsJson(Request $request){
        $can_all = Entrust::can('all_projects_stats');
        $can_self_photo = Entrust::can('self_photo_projects_stats');
        if(!$can_all && !($request->input('self_photo') == true && $can_self_photo)) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        if($request->input('self_photo') != true && $can_all){
            $projects = Project::where('worker_id', '>', 0)->where('date_start_real', '!=', NULL)->whereNotIn('status_id', Config::get('project.fail_status'))->get();
        }else{
            $projects = Project::where('worker_id', Auth::user()->id)->where('date_start_real', '!=', NULL)->whereNotIn('status_id', Config::get('project.fail_status'))->get();
        }

        $data = [];

        foreach($projects as $project){

            $amount = $project->amount;
            $amount_photo = $project->calc_amount_photo();

            $ss_sum = 0;
            if($project->ssmanager){
                $ss_sum = $amount;
            }
            $referral_sum = 0;
            if($project->referral){
                $referral_sum = $amount;
            }

            $rf = $project->calc_ed_fee();

            $data[] = [
                "id" => $project->id,
                "status_id" => $project->status_id,
                "date" => $project->date_start_real->timezone(config('project.city_time_zone'))->format('Y/m/d'),
                "date_time" => $project->date_start_real->timezone(config('project.city_time_zone'))->format('Y/m/d H:i:s'),
                "worker" => $project->worker()->name,
                "ss_name" => $project->ssmanager ? $project->ssmanager->user->name : 'без СС',
                "rf" => $rf - $project->calc_referral(),
                "rf_referral" => $project->referral ? $rf - $project->calc_referral(): 0,
                "rf_geo" => $project->referral ? 0 : $rf,
                "amount" => $project->amount,
                "amount_photo" => $amount_photo,
                "amount_minus_tax" => $project->amount - $project->calc_tax(),

                "amount_ss" => $project->calc_ss_fee(),
                //"total_sum" => $ss_sum,
                "is_ss" => $project->ssmanager ? 'Да' : 'Нет',

                "amount_referral" => $project->calc_referral(),

                "amount_office" => $project->calc_ed_fee() - $project->calc_referral() - $project->calc_all_cords_royalty(),
                "amount_cords" => $project->calc_all_cords_royalty(),

                //"total_referral" => $referral_sum,
                "is_referral" => $project->referral ? 'Да' : 'Нет',
                "is_referral_hs" => $project->is_referral_himself() ? 'Да' : 'Нет',

                "amount_hs" => $project->is_referral_himself() ? $amount : 0,
                "amount_geo" => $project->is_referral_himself() ? 0 : $amount,

                "project_num" => 1,
                "project_num_hs" => $project->is_referral_himself() ? 1 : 0,
                "project_num_geo" => $project->is_referral_himself() ? 0 : 1,

                "pay_b" => Config::get('project.payment_type_b')[$project->payment_type_id_b] . '[Б]',
                "pay_bb" => Config::get('project.payment_type_b')[$project->payment_type_id_b],
                "pay_w" => Config::get('project.payment_type_w')[$project->payment_type_id_w],

                "region" => $project->region_code,

                "place" => $project->place_cached ? $project->place_cached : '[БЕЗ МЕСТА]',
                "client" => $project->client_cached ? $project->client_cached : '[БЕЗ КЛИЕНТА]'
            ];
        }

/*        $data = [

  [
    "id" => 640,
    "region" => "Africa",
    "country" => "EGY",
    "city" => "Cairo",
    "amount" => 500,
    "date" => "2012/05/26"
],
            [
    "id" => 641,
    "region" => "South America",
    "country" => "ARG",
    "city" => "Buenos Aires",
    "amount" => 780,
    "date" => "2015/05/07"
    ]
];*/

        return response()->json($data);

    }

    public function projectsBuhJson(Request $request){
        if(!Entrust::can('all_projects_stats')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        $projects = Project::where('worker_id', '>', 0)->where('date_start_real', '!=', NULL)->whereNotIn('status_id', Config::get('project.fail_status'))->get();

        $data = [];

        foreach($projects as $project){

            $amount = $project->amount;
            $amount_photo = $project->calc_amount_photo();

            $ss_sum = 0;
            if($project->ssmanager){
                $ss_sum = $amount;
            }
            $referral_sum = 0;
            if($project->referral){
                $referral_sum = $amount;
            }

            $rf = $project->calc_ed_fee();

            $data[] = [
                "id" => $project->id,
                "status_id" => $project->status_id,
                "date" => $project->date_start_real->timezone(config('project.city_time_zone'))->format('Y/m/d'),
                "date_time" => $project->date_start_real->timezone(config('project.city_time_zone'))->format('Y/m/d H:i:s'),
                "worker" => $project->worker()->name,
                "rf" => $rf,
                "rf_referral" => $project->referral ? $rf: 0,
                "rf_geo" => $project->referral ? 0 : $rf,
                "amount" => $project->amount,
                "amount_photo" => $amount_photo,

                "amount_ss" => $project->calc_ss_fee(),
                //"total_sum" => $ss_sum,
                "is_ss" => $project->ssmanager ? 'Да' : 'Нет',

                "amount_referral" => $project->calc_referral(),

                "amount_office" => $project->calc_ed_fee() - $project->calc_referral() - $project->calc_all_cords_royalty(),
                "amount_cords" => $project->calc_all_cords_royalty(),

                //"total_referral" => $referral_sum,
                "is_referral" => $project->referral ? 'Да' : 'Нет',
                "is_referral_hs" => $project->is_referral_himself() ? 'Да' : 'Нет',

                "amount_hs" => $project->is_referral_himself() ? $amount : 0,
                "amount_geo" => $project->is_referral_himself() ? 0 : $amount,

                "project_num" => 1,
                "project_num_hs" => $project->is_referral_himself() ? 1 : 0,
                "project_num_geo" => $project->is_referral_himself() ? 0 : 1,

                "pay_b" => Config::get('project.payment_type_b')[$project->payment_type_id_b] . '[Б]',
                "pay_bb" => Config::get('project.payment_type_b')[$project->payment_type_id_b],
                "pay_w" => Config::get('project.payment_type_w')[$project->payment_type_id_w],

                "region" => $project->region_code,

                "place" => $project->place_cached ? $project->place_cached : '[БЕЗ МЕСТА]',
                "client" => $project->client_cached ? $project->client_cached : '[БЕЗ КЛИЕНТА]'
            ];
        }

/*        $data = [

  [
    "id" => 640,
    "region" => "Africa",
    "country" => "EGY",
    "city" => "Cairo",
    "amount" => 500,
    "date" => "2012/05/26"
],
            [
    "id" => 641,
    "region" => "South America",
    "country" => "ARG",
    "city" => "Buenos Aires",
    "amount" => 780,
    "date" => "2015/05/07"
    ]
];*/

        return response()->json($data);

    }

}
