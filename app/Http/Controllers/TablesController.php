<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
use App\UserInfo;
use App\Project;
use Config;


class TablesController extends Controller
{

    public function projectsSelfPhoto(Request $request){
        if(!Entrust::can('self_photo_projects_stats')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не можете смотреть статистику по проектам!' ]);
            exit();
        }

        $data = [];

        $data['PAGE_TITLE'] = 'СПИСОК ВАШИХ ПРОЕКТОВ';

        return view('tables/projects_self_photo', $data);

    }


    public function projectStatusesJson(Request $request){

        $data = [];

        foreach(config('project.status') as $id => $status){
            $data[] = [
                'id' => $id,
                'title' => $status
            ];
        }

        return response()->json($data);

    }

}
