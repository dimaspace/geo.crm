<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use Spatie\Activitylog\Models\Activity;
use App\Transformers\ActivityLogs;
use Fractal;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use Entrust;

class LogProjects extends Controller
{

    public function view(Request $request)
    {
        if(!Entrust::can('logs_projects')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не имеете доступа в этот раздел!' ]);
            exit();
        }
        $data = [];

        $data['PAGE_TITLE'] = 'ЛОГИ ПРОЕКТОВ';

        return view('logs.projects', $data);

    }

    public function getAllJson(Request $request)
    {
        if(!Entrust::can('logs_projects')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не имеете доступа в этот раздел!' ]);
            exit();
        }

        $activity = Activity::inLog('project');

        $by_project_id = (int) $request->input('by_project_id', 0);
        if($by_project_id){
            $activity = $activity->where('subject_id', $by_project_id);
        }
        $paginator = $activity->paginate($request->input('take', 10));

        $activity = $paginator->getCollection();

        $activity = Fractal::create()
            ->collection($activity, new ActivityLogs())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($activity);
    }


    public function viewProject(Request $request, $project_id)
    {
        if(!Entrust::can('logs_projects')) {
            return response()->redirectTo('/')->with([ 'error' => 'Вы не имеете доступа в этот раздел!' ]);
            exit();
        }
        $data = [];

        $data['PAGE_TITLE'] = 'ЛОГИ ПРОЕКТА #' . $project_id;
        $data['project_id'] = $project_id;

        return view('logs.project', $data);
    }
}
