<?php

namespace App\Http\Controllers;

use Cache;
use App\Transformers\PortfolioPhoto;
use App\Transformers\UserSimple;
use Spatie\Fractalistic\ArraySerializer;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Http\Request;
use Fractal;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Tags\Tag;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Media;
use App\User;
use Spatie\Image\Image;
use Entrust;

class PortfolioController extends Controller
{
    /*
     * Главная страница портфолио
     *
     * @return \Illuminate\Http\Response
     */
    public function photoIndex(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'ФОТО-ПОРТФОЛИО';
        $data['moderate_mode'] = false;
        $data['voter_mode'] = false;
        $data['edit_mode'] = true;

        return view('portfolio/photo_index', $data);
    }

    /*
     * Модерация портфолио цензорами
     *
     * @return \Illuminate\Http\Response
     */
    public function censorIndex(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'МОДЕРАЦИЯ ПОРТФОЛИО';
        $data['moderate_mode'] = Entrust::can('portfolio_moderate');
        $data['voter_mode'] = Entrust::can('portfolio_vote');
        $data['edit_mode'] = Entrust::can('portfolio_edit_all');

        return view('portfolio/photo_index', $data);
    }

    /*
     * Главная страница портфолио
     *
     * @return \Illuminate\Http\Response
     */
    public function photoAdd(Request $request)
    {
        $data = [];

        $data['PAGE_TITLE'] = 'ДОБАВИТЬ ФОТО В ПОРТФОЛИО';

        $data['upload_by_another_user'] = Entrust::can('portfolio_upload_by_another_user');

        return view('portfolio/photo_add', $data);
    }

    public function photoRemove(Request $request){
        $data = [];

        $id = $request->input('id');
        $media = Media::findOrFail($id);

        if(Auth::user()->id !== $media->model->id && Entrust::can('portfolio_edit_all') == false){
            $data['error'] = 'Вы не можете удалить чужой объект';
        }else{
            $media->delete();
            $data['success'] = 'Удаление успешно!';
        }

        return response()->json($data);
    }

    public function photoSelfEdit($id, Request $request){
        $data = [];

        $media = Media::findOrFail($id);
        if(Auth::user()->id !== $media->model->id && Entrust::can('portfolio_edit_all') == false){
            $data['success'] = false;
            $data['error'] = 'Вы не можете править чужой объект';
        }else{
            $tags = $request->input('tags', []);
            $tags = Tag::find($tags);
            //dd($tags);
            $media->syncTags($tags);
            $data['success'] = 'Объект отредактирован!';
        }

        return response()->json($data);
    }

    public function photoEdit($id, Request $request){
        $data = [];

        $data['success'] = false;
        $media = Media::findOrFail($id);

        if($request->__isset('tags')) {
            if (Auth::user()->id !== $media->model->id && Entrust::can('portfolio_edit_all') == false) {
                $data['error'] = 'Вы не можете править чужой объект!';
            } else {
                $tags = $request->input('tags', []);
                $tags = Tag::find($tags);
                $media->syncTags($tags);
                $data['success'] = 'Объект отредактирован!';
            }
        }

        if($request->__isset('vote')){
            if(Entrust::can('portfolio_vote') == false){
                $data['error'] = 'Вы не можете голосовать!';
            }else{
                $vote = (int) $request->input('vote', 0);
                $voter = Auth::user();
                if($vote > 0){
                    $voter->upvote($media);
                }elseif($vote < 0){
                    $voter->downvote($media);
                }else{
                    $voter->cancelVote($media);
                }
                $data['success'] = 'Объект отредактирован!';
            }
        }

        $media->killCache();
        return response()->json($data);
    }

    public function Vote($id, Request $request){
        $data = [];

        $media = Media::findOrFail($id);

        if(Entrust::can('portfolio_vote') == false){
            $data['success'] = false;
            $data['error'] = 'Вы не можете голосовать';
        }else{
            $vote = (int) $request->input('vote', 0);
            $voter = Auth::user();
            if($vote > 0){
                $voter->upvote($media);
            }elseif($vote < 0){
                $voter->downvote($media);
            }else{
                $voter->cancelVote($media);
            }
            $media->killCache();
            $data['success'] = 'Голос принят';
        }

        return response()->json($data);
    }

    public function photoAddSubmit(Request $request){
        $user_id = (int) $request->input('user', 0);
        if($user_id > 0){
            $user = User::findOrFail($user_id);
        }else{
            $user = Auth::user();
        }

        $tags = $request->input('tags', []);

        $messages = [
            'photos.*.required' => 'Вы не загрузили ни одного фото!',
            'photos.*.max' => 'Фото не должно весить больше :max Kb!',
            'photos.*.mimes' => 'Разрешено загружать только фото c разрешением JPEG!',
            'photos.*.mimetypes' => 'Разрешено загружать только фото в формате JPEG!',
            'photos.*.dimensions' => 'Недопустимый размер!(требуется ширина не меньше :min_width)',
        ];
        $validator = Validator::make($request->all(), [
            //'photos.*' => 'required|max:10240|mimetypes:image/*|mimes:jpeg|dimensions:min_width=1920'
            'photos.*' => ['required', 'max:10240', 'mimetypes:image/*', 'mimes:jpeg',
                function ($attribute, $value, $fail) {
                    try{
                        $image =  Image::load($value);
                        $width = $image->getWidth();
                        $height = $image->getHeight();
                        if($width < 1920  &&  $height < 1920){
                            $fail('Размер фото по длинной стороне должен быть не меньше 1920');
                        }
                    }catch(\Intervention\Image\Exception\NotReadableException $e){
                        $fail('Недопустимый тип файла');
                    }
                }
            ]
            
        ], $messages)->validate();

        $user->addAllMediaFromRequest()->each(function ($fileAdder) use($tags){
            $fileAdder->preservingOriginal()->usingFileName('photo.jpeg');
            $media = $fileAdder->toMediaCollection('portfolio');
            $media->syncTagsWithType($tags, 'photo');
        });

        return redirect()
            ->route('portfolioPhotoIndex')
            ->with('success', 'Портфолио успешно пополнено, перед публикацией требуется одобрение цензоров!');
    }

    public function getTags($type)
    {
        $data = [];

        $used_tags = Tag::getWithType($type);
        $used_tags_flat = $used_tags->pluck('name');

        $config_tags_flat = collect(config('portfolio.tags.' . $type));

        $unused_tags_flat =$config_tags_flat->diff($used_tags_flat);
        $unused_tags = $unused_tags_flat->map(function ($item, $key) {
            $tag =Tag::findOrCreate($item, 'photo');
            return $tag;
        });

        $tags = $unused_tags->merge($used_tags)->sortBy('name')->values();

        return response()->json($tags);
    }

    public function getSelfItems($type, Request $request)
    {
        $items = Auth::user()->getMedia('portfolio')->sortByDesc('id');
        $items = Fractal::collection($items)->transformWith(new PortfolioPhoto())->toArray();

        return response()->json($items);
    }

    public function getSelfItem($id, Request $request)
    {
        $item = Auth::user()->getMedia('portfolio')->where('id', $id)->first();
        if($item && $item->count() > 0){
            //$items = Fractal::dataArray($item)->transformWith(new PortfolioPhoto())->toArray();
            $items = fractal($item, new PortfolioPhoto(), new ArraySerializer())->toArray();
            return response()->json($items);
        }else{
            throw (new ModelNotFoundException)->setModel('App\Media', $id);
        }
    }

    public function getItems($type, Request $request)
    {
        $filter = $request->input('filter', []);

        $items = Media::with(['tags', 'voters', 'upvoters', 'model'])->Collection('portfolio');
        if(isset($filter['users']) && (int) $filter['users'] > 0){
            $items = $items->where('model_id', (int) $filter['users']);
        }
        if(isset($filter['tags'])){
            $items = $items->withAnyTags([$filter['tags']], $type);
        }

        //$items = $items->where();

        $items = $items->orderBy('id', 'desc')->simplePaginate(40);


        if(isset($filter['status'])){
            $status = (int) $filter['status'];
            if($status > 0){
                $items = $items->where('is_approved', true);
            }elseif($status < 0){
                $items = $items->where('is_approved', false)->where('is_quorum', true);
            }else{
                $items = $items->where('is_quorum', false);
            }

        }
        if(isset($filter['vote'])){
            $vote = (int) $filter['vote'];
            if($vote > 0){
                $items = $items->filter(function ($item) {
                    return $item->isUpvotedBy(Auth::user());
                });
            }elseif($vote < 0){
                $items = $items->filter(function ($item) {
                    return $item->isDownvotedBy(Auth::user());
                });
            }else{
                $items = $items->filter(function ($item) {
                    return !$item->isVotedBy(Auth::user());
                });
            }

        }

        $portfolio_transformer = new PortfolioPhoto();
        $portfolio_transformer->setDefaultIncludes(['user']);
        $items = Fractal::collection($items)->transformWith($portfolio_transformer)->toArray();

        return response()->json($items);
    }

    public function getItem($id, Request $request)
    {
        $item = Media::where('id', $id)->collection('portfolio')->first();
        if($item && $item->count() > 0){
            if(Entrust::can('portfolio_edit_all') == false && Entrust::can('portfolio_vote') == false){
                return response()->json(['error' => 'Вы не можете править чужой объект']);
            }else{
                //$items = Fractal::dataArray($item)->transformWith(new PortfolioPhoto())->toArray();
                $items = fractal($item, new PortfolioPhoto(), new ArraySerializer())->toArray();
                return response()->json($items);
            }
        }else{
            throw (new ModelNotFoundException)->setModel('App\Media', $id);
        }
    }

    public function getUsers($type, Request $request)
    {
        $users = User::whereHas('media', function ($query) {
            $query->where('collection_name', 'portfolio');
        })->get();
        $user_transformer = new UserSimple();
        $users = Fractal::collection($users)->transformWith($user_transformer)->toArray();

        return response()->json($users);
    }

    public function getPhotographers(Request $request)
    {
        $users = User::getByRole('photo', true);
        if($request->input('search')){
            $users->where('name', 'like', '%' . $request->input('search') . '%');
        }
        $user_transformer = new UserSimple();
        $users = Fractal::collection($users->get())->transformWith($user_transformer)->toArray();

        return response()->json($users);
    }

    public function notFoundFile()
    {
        return redirect('/images/404.gif');
    }
}

