<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Media;
use Entrust;

class PortfolioPhoto extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     * @param Media $media
     *
     * @return array
     */
    public function transform(Media $media){
        $result = [
            'id'        => $media->id,
            'name'      => $media->name,
            'mime'      => $media->mime_type,
            'size'      => $media->size,
            'full_url'  => $media->getUrl(),
            'url'       => $media->getUrl('main'),
            'thumb_url' => $media->getUrl('thumb_250'),
            'title'     => $media->getCustomProperty('title', ''),
            'tags'      => $media->tags()->get(),
            'tags_names'=> $media->tags()->get()->pluck('name'),
            'tags_ids'  => $media->tags()->get()->pluck('id'),
            'tags_list' => $media->tags()->get()->implode('name', ', '),
            'order'     => $media->order_column,
            'approved'  => $media->is_approved,
            'votes'     => $media->voters_count,
            'up_votes'  => $media->up_voters_count,
            'quorum'    => $media->is_quorum,
            'my_vote'   => $media->my_vote
        ];

        return $result;
    }

    public function includeUser(Media $media){
         $user = $media->model;

         return $this->primitive($user, new UserSimple);
    }

}
