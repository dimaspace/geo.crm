<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Spatie\Activitylog\Models\Activity;

class ActivityLogs extends TransformerAbstract
{

    protected $trans = [
        'geo_link' => 'Ссылка на гео',
        'geo_id' => 'ID отчёта на гео'
    ];


    /**
     * A Fractal transformer.
     * @param Activity $activity
     *
     * @return array
     */
    public function transform(Activity $activity)
    {

        $subject = new $activity->subject_type();
        $causer = new $activity->causer_type();

        $changes = $activity->changes();
        $changed_fields = $this->get_changed_fields($changes);

        return [
            'id' => $activity->id,
            'subject' => $subject::find($activity->subject_id),
            'causer' => $causer::find($activity->causer_id),
            'changes' => $changes,
            'changed_fields' => $changed_fields,
            'changed_fields_format' => $this->changed_fields_format($changed_fields),
            'changes_table' => $this->get_changes_table($changes),
            'date' => $activity->created_at->timezone(config('project.city_time_zone'))->format('Y/m/d'),
            'datetime' => $activity->created_at->timezone(config('project.city_time_zone'))->format('Y/m/d H:i:s'),
            'subject_type' => $this->subject_type_format($activity->subject_type),
            'description' => $activity->description,
            'description_format' => $this->description_format($activity->description),
            'icon' => $this->get_icon($activity->description),
        ];
    }

    protected function get_changed_fields($changes){
        if(count($changes) > 0 && isset($changes['attributes'])){
            return array_keys($changes['attributes']);
        }else{
            return [];
        }
    }

    protected function get_changes_table($changes){
        $result = [];
        if($changes && isset($changes['attributes'])){
            foreach($changes['attributes'] as $attribute => $change){
                $old = isset($changes['old'][$attribute]) ? $changes['old'][$attribute] : '';
                $attribute_trans = isset($this->trans[$attribute]) ? $this->trans[$attribute] : $attribute;
                $result[] = [
                    'field' => $attribute_trans,
                    'old' => $old,
                    'new' => $change
                ];
            }
        }
        return $result;
    }


    protected function changed_fields_format($changed_fields){
        return str_replace(array_keys($this->trans), array_values($this->trans), $changed_fields);
    }

    protected function subject_type_format($value){
        switch ($value){
            case 'App\Project' :
                return 'Проект';
                break;
            default :
                return $value;
        }
    }

    protected function description_format($value){
        switch ($value){
            case 'updated' :
                return 'Обновлён';
                break;
            case 'created' :
                return 'Создан';
                break;
            default :
                return $value;
        }
    }

    protected function get_icon($value){
        switch ($value){
            case 'updated' :
                return 'update';
                break;
            case 'created' :
                return 'add_box';
                break;
            default :
                return 'device_unknown';
        }
    }
}
