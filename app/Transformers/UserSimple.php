<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserSimple extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     * @param User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        $result = [
            'id'       => $user->id,
            'name'     => $user->name,
            'email'    => $user->email,
            'vk_id'    => $user->vk_id,
            'region'   => $user->rimary_region_code,
            'is_staff' => $user->is_staff,
        ];

        return $result;
    }

}
