<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spiritix\LadaCache\Database\Model;

class ProjectExtra extends Model
{
    use \Spiritix\LadaCache\Database\LadaCacheTrait;
    protected $fillable = ['user_id', 'royalty', 'type_extras'];

    protected $with = ['user'];


    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
