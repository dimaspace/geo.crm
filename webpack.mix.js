const mix = require('laravel-mix');
mix.webpackConfig({ devtool: "inline-source-map" });

mix.sass('resources/assets/scss/style.scss', 'public/css/style.css',
    {
        outputStyle: 'compressed'
    })
    .version().sourceMaps();

mix.minify('public/css/style.css');
mix.minify('public/plugins/DevExpress/css/dx.common.css');
mix.minify('public/plugins/DevExpress/css/dx.light.css');
mix.minify('public/js/admin.js');
mix.minify('public/js/forms.js');
mix.minify('public/plugins/DevExpress/js/dx.all.js');
mix.minify('public/plugins/DevExpress/js/localization/dx.messages.ru.js');
mix.minify('public/js/logs/projects.js');
mix.minify('public/js/logs/project.js');
mix.minify('public/js/profile/my_calendar_fullcalendar.js');


mix.js('resources/assets/js/echo.init.js', 'public/js/echo.init.js').version();
mix.js([
    'resources/assets/js/echo.sockets/users.online.js',
    'resources/assets/js/echo.sockets/app.users.id.js'
], 'public/js/echo.sockets.js').version();

mix.copy('node_modules/mustache/mustache.min.js', 'public/js/mustache.min.js').version();

mix.version([
    'public/css/style.css',
    'public/plugins/DevExpress/css/dx.common.css',
    'public/plugins/DevExpress/css/dx.light.css',
    'public/js/admin.js',
    'public/js/forms.js',
    'public/js/form.project.js',
    'public/js/form.project.calendar.js',
    'public/js/demo.js',
    'public/plugins/DevExpress/js/jszip.min.js',
    'public/plugins/DevExpress/js/dx.all.js',
    'public/plugins/DevExpress/dx.custom.global.options.js',
    'public/plugins/DevExpress/js/localization/dx.messages.ru.js',
    'public/plugins/DevExpress/js/cldr.js',
    'public/plugins/DevExpress/js/cldr/event.js',
    'public/plugins/DevExpress/js/cldr/supplemental.js',
    'public/plugins/DevExpress/js/globalize.js',
    'public/plugins/DevExpress/js/globalize/message.js',
    'public/plugins/DevExpress/js/globalize/number.js',
    'public/plugins/DevExpress/js/globalize/currency.js',
    'public/plugins/DevExpress/js/globalize/date.js',
    'public/js/logs/projects.js',
    'public/js/profile/portfolio.js',
    'public/js/logs/project.js',
    'public/js/profile/my_calendar_fullcalendar.js'
]);

mix.ts('resources/assets/ts/GeoCrm/GeoCrm.ts', 'public/js/geocrm.js').version().sourceMaps();

// Сайт портфолио

mix.copyDirectory(
    'resources/assets/portfolio/dogma/source/images',
    'public/portfolio/images');
mix.copyDirectory(
    'resources/assets/portfolio/dogma/source/fonts',
    'public/portfolio/fonts');

mix.sass('resources/assets/portfolio/dogma/source/scss/style.scss', 'public/portfolio/css/style.css',
    {
        outputStyle: 'compressed'
    })
    .version().sourceMaps();

mix.styles([
    'resources/assets/portfolio/dogma/source/css/font-awesome.min.css',
    'resources/assets/portfolio/dogma/source/css/reset.css',
    'resources/assets/portfolio/dogma/source/css/dangerous.swiper.css',
    'resources/assets/portfolio/dogma/source/css/plugins.css',
    'node_modules/ion-checkradio/css/ion.checkRadio.css',
    'node_modules/ion-checkradio/css/ion.checkRadio.html5.css',
    'public/portfolio/css/style.css'
], 'public/portfolio/css/all.css').version();
mix.minify('public/portfolio/css/all.css').version();

mix.scripts([
    'resources/assets/portfolio/dogma/source/js/jquery.min.js',
    'resources/assets/portfolio/dogma/source/js/plugins.js',
    'public/plugins/lazyload/jquery.lazyload.js',
    'node_modules/masonry-layout/distmasonry.pkgd.min.js',
    'node_modules/imagesloaded/imagesloaded.pkgd.min.js',
    'node_modules/jquery.nicescroll/dist/jquery.nicescroll.min.js',
    'node_modules/jquery-bridget/jquery-bridget.js',
    'node_modules/infinite-scroll/dist/infinite-scroll.pkgd.min.js',
    'resources/assets/portfolio/dogma/source/js/core.js',
    'resources/assets/portfolio/dogma/source/js/filter.js',
    'resources/assets/portfolio/dogma/source/js/scripts.js'
], 'public/portfolio/js/all.js');