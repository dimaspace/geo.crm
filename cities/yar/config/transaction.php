<?php
return [
    'types' => [
        'WORKER' => 'Оплата исполнителю',
        'TAX' => 'Налоги',
        'ED_FEE' => 'РедСбор',
        'OFFICE_FEE' => 'Сдать в офис',
        'FS' => 'ФС(Реферрал)',
        'SS' => 'Менеджер СС',
        'CORD_ROYALTY' => 'Вознаграждение координатора',
        'CLIENT_PAY' => 'Оплата от клиента',
        'BONUS' => 'Бонус',
        'PENALTY' => 'Штраф',
        'CORRECT' => 'Коррекция',
        'REFILL_MANUAL' => 'Ручное пополнение',
        'CASH_FLOW' => 'Взаимозачёт',
        'COLLECT_RETENTION' => 'Удержание с инкасатора',
        'COLLECT_REFUND' => 'Возмещение инкасатору',
        'ZP' => 'Зарплата',
        'OUTLAY' => 'Расходы',
        'OTHER' => 'Другое'
    ],

    'type_icons' => [
        'WORKER' => 'account_box',
        'TAX' => 'account_balance',
        'ED_FEE' => '',
        'OFFICE_FEE' => '',
        'CORD_ROYALTY' => 'contac_mail',
        'FS' => '',
        'SS' => '',
        'CLIENT_PAY' => '',
        'BONUS' => '',
        'PENALTY' => '',
        'CORRECT' => 'pan_tool',
        'REFILL_MANUAL' => 'account_box',
        'CASH_FLOW' => 'transfer_within_a_station',
        'COLLECT_RETENTION' => 'format_textdirection_l_to_r',
        'COLLECT_REFUND' => 'format_textdirection_r_to_l',
        'ZP' => '',
        'OUTLAY' => '',
        'OTHER' => ''
    ],

    'type_codes' => [

        'WORKER' => 100,
        'TAX' => 200,
        'ED_FEE' => 300,
        'OFFICE_FEE' => 350,
        'FS' => 400,
        'SS' => 500,
        'CORD_ROYALTY' => 600,

        'CLIENT_PAY' => 2000,

        'BONUS' => 5000,
        'PENALTY' => 5100,
        'CORRECT' => 5200,
        'REFILL_MANUAL' => 5300,
        'CASH_FLOW' => 6000,
        'ZP' => 7000,
        'OUTLAY' => 7500,
        'COLLECT_RETENTION' => 8000,
        'COLLECT_REFUND' => 8500,
        'OTHER' => 10000
    ],

    'type_colors' => [
        'WORKER' => '#568d67',
        'TAX' => '#e1481f',
        'ED_FEE' => '#e11fb6',
        'OFFICE_FEE' => '#e11fb6',
        'CORD_ROYALTY' => '#9d4906',
        'FS' => '#8303b3',
        'SS' => '#8e692e',
        'CLIENT_PAY' => '#1e3dc2',
        'BONUS' => '#597bf9',
        'PENALTY' => '#e19a1f',
        'CORRECT' => '#57a095',
        'REFILL_MANUAL' => '#132464',
        'CASH_FLOW' => '#bb1658',
        'ZP' => '#bb1658',
        'OUTLAY' => '#bb1658',
        'COLLECT_RETENTION' => '#21597d',
        'COLLECT_REFUND' => '#185879',
        'OTHER' => '#3d4042'
    ],

    'cr_types' => [
        'MANUAL' => 100,
        'AUTO' => 200,
        'TRIGGER' => 300
    ],

    'billing_users' => [
        'regions' => [
            'yar' => 76,
            'rb' => 77,
            'ivanovo' => 78,
            'kostroma' => 79,
            'vladimir' => 80
        ],
        'client' => 81,
        'tax' => 82,
        'boss' => 115,
        'inkassator' => 115
    ],

    'cash_flow_types' => [
        0 => 'RECHARGE',
        1 => 'TAKE'
    ]
];