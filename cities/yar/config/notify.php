<?php

return [
    'telegram_bot_name' => env('TELEGRAM_BOT_NAME'),
    'telegram_bot_show_stiker_id' => env('TELEGRAM_BOT_SHOW_STIKER_ID')
];