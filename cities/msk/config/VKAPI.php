<?php
/**
 * File: VKAPI.php
 * Created by bafoed.
 * This file is part of VKTM project.
 * Do not modify if you do not know what to do.
 * 2016.
 */

return [
    'access_token' => env('VKAPI_TOKEN', ''),
    'version' => '5.25',
    'api_url' => 'https://api.vk.com/method/%s?%s',
    'moderate_chat_id' => env('VKAPI_MODERATE_CHAT_ID', 2000000045)
];