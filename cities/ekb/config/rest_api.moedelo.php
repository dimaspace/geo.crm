<?php

return [
    'api_key' => env('MYDEAL_API_KEY', ''),
    'api_url' => 'https://restapi.moedelo.org/accounting/api/'
];