<?php
return [
    "layout" => "entrust-gui::app",
    "route-prefix" => "entrust",
    "pagination" => [
        "users" => 150,
        "roles" => 50,
        "permissions" => 50,
    ],
    "middleware" => ['web', 'entrust-gui.admin'],
    "unauthorized-url" => '/login',
    "middleware-role" => 'admin',
    "confirmable" => false,
    "users" => [
      'fieldSearchable' => [],
    ],
];
