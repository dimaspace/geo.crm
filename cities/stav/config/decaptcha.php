<?php
return [

   /*
   | Any request to the API must be passed the API key 
   */
   'key'    => env('DECAPTCHA_KEY', 'dde612fa034165e60461cabc208080a9'),

   /*
   | Service which will load the captcha
   */
   'domain' => env('DECAPTCHA_DOMAIN', 'rucaptcha.com'),

   /*
   | The folder inside storage folder that the script should save images got by reference
   */
   'tmp'    => env('DECAPTCHA_TMP', 'captcha'),
];