$(function () {

    $('#add_photoman').on('hide.bs.modal', function(event) {

    });

    $('#add_photoman').on('show.bs.modal', function(event) {
      var $rt = $(event.relatedTarget);
      var mode = $rt.data('mode');

      if(mode == 'new'){
            new_photoman_form();
      }
      if(mode == 'edit'){
          var id =  $rt.data('id')
          edit_photoman_form(id);
      }
    })

    $('#users_datatable tbody tr').on('dblclick', function(event){
        var $rt = $(event.currentTarget);

        $('#add_photoman').modal('show');
        var id =  $rt.data('id')
        edit_photoman_form(id);
    });

    var table = $('#users_datatable').DataTable({
        fixedHeader: true,
        paging: false,
        dom: 'lfrtp'
    });

    function new_photoman_form(){
        $('.page-loader-wrapper').fadeIn();

        var $fp = $('#add_photoman form');
        var $sb = $('#add_photoman button.submit')

        $fp.trigger('reset');
        $fp.find('select').selectpicker('refresh');
        $sb.text('СОЗДАТЬ');
        $fp.attr('action', '/users/photo/add');

        //$('#project_date_start').bootstrapMaterialDatePicker('destroy');


        $('.page-loader-wrapper').fadeOut();
    }

    function edit_photoman_form(user_id){

        /* Получаем данные о проекте */
        $.ajax({
            url: '/users/photo/get/',
            type: "GET",
            cache: false,
            data: {
                user_id: user_id
            },
            beforeSend: function () {
                $('.page-loader-wrapper').fadeIn();
            },
            success: function (data) {
                var $fp = $('#add_photoman form');
                var $sb = $('#add_photoman button.submit')

                $fp.trigger('reset');
                $fp.find('select').selectpicker('refresh');
                $sb.text('РЕДАКТИРОВАТЬ');
                $fp.attr('action', '/users/photo/edit');

                var user = data.user;
                var info = data.info;
                var region_codes = data.region_codes;

                $.each(user, function(key, val){
                    if(key == 'info') {
                        $.each(info, function (key, val) {
                            $fp.find('#' + key).val(val);

                        });
                    }else if(key == 'regions'){
                        $fp.find('#region').val(region_codes);
                    }else if(key == 'id'){
                        $fp.find('#user_id').val(val);
                    }else{
                        $fp.find('#' + key).val(val);
                    }
                });

                $fp.find('select').selectpicker('refresh');

                $('.page-loader-wrapper').fadeOut();
            }
        });

    }

});