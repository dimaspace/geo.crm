$(function () {



DevExpress.localization.locale(navigator.language || navigator.browserLanguage);

DevExpress.config({ defaultCurrency: 'RUB' });

DevExpress.ui.dxDataGrid.defaultOptions({
    options: {
        rowAlternationEnabled: true,
        showBorders: false
    }
});

var store = new DevExpress.data.CustomStore({
    load: function(loadOptions) {
        var d = $.Deferred(), args = {};

        args.skip = loadOptions.skip || 0;
        args.take = loadOptions.take || 12;
        args.page = args.skip / args.take + 1;
        args.by_project_id = window.by_project_id;

        $.getJSON("/logs/projects/json", args).done(function (result) {
            d.resolve(result.data,{
                totalCount: result.meta.pagination.total,
                count: result.meta.pagination.count
            });
        });

        return d.promise();
    }
});

$(function(){

var dataGrid = $("#logsTable").dxDataGrid({
        dataSource: {
            store:store
        },
        stateStoring: {
            enabled: false
        },
        showColumnLines: true,
        showRowLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        sorting: {
            mode: "multiple"
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Найти..."
        },
        export: {
            enabled: false
        },
        grouping: {
            autoExpandAll: false,
            contextMenuEnabled: true
        },
        columnChooser: {
            enabled: true,
            mode:  'select'
        },
        scrolling: {
            mode: "standard"
        },
        groupPanel: {
            visible: true
        },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },

        summary: {
            groupItems: [{
                summaryType: "count",
                displayFormat: "Редактирований: {0}"
            }]
        },

        editing: {
            allowUpdating: false,
        },

        columnFixing: {
            enabled: true
        },
        remoteOperations: {
            paging: true,
            filtering: false,
            sorting: false,
            grouping: false,
            summary: false,
            calculation: false
        },
        paging: {
            pageSize: 20
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20, 40],
            showInfo: true
        },
        masterDetail: {
            enabled: true,
            template: function(container, options) {
                var currentData = options.data;

                $("<div>")
                    .addClass("master-detail-caption")
                    .text("Список изменений в объекте #" + currentData.subject.id + ". Юзер: " + currentData.causer.name + "(#" + currentData.causer.id + ")")
                    .appendTo(container);

                $("<div>")
                    .dxDataGrid({
                        columnAutoWidth: true,
                        showBorders: true,
                        columns: [{
                                dataField: "field",
                                caption: "Поле",
                                dataType: "string"
                            },{
                                dataField: "old",
                                caption: "Было",
                                dataType: "string"
                            },{
                                dataField: "new",
                                caption: "Стало",
                                dataType: "string"
                        }],
                        dataSource: currentData.changes_table
                    }).appendTo(container);
            }
        },
        columns: [
            {
                caption: "",
                dataField: "icon",
                allowGrouping: false,
                allowEditing: false,
                allowFiltering: false,
                allowHeaderFiltering: false,
                allowSearch: false,
                allowHiding: false,
                width: 50,
                alignment: 'center',
                cellTemplate: function(container, options) {
                    var icon = $("<i />")
                        .attr('title', options.data.description_format)
                        .html(options.data.icon)
                        .addClass('material-icons');
                    container.html(icon);
                }
            },  {
                caption: "ID",
                dataField: "id",
                allowGrouping: false,
                allowEditing: false,
                allowHeaderFiltering: false,
                width: 100
            }/*,  {
                caption: "ID Объекта",
                dataField: "subject.id",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: true,
                width: 100
            }*/,  {
                caption: "Действие",
                dataField: "description_format",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: true
            },  {
                caption: "Дата",
                dataField: "date",
                dataType: 'date',
                allowGrouping: true,
                allowEditing: false,
                width: 100
            },  {
                caption: "Время",
                dataField: "datetime",
                dataType: 'datetime',
                allowGrouping: false,
                allowEditing: false,
                width: 100,
                format:  'longTime'
            },  {
                caption: "Юзер",
                dataField: "causer.name",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: false
            },  {
                caption: "Затронутые поля",
                dataField: "changed_fields_format",
                allowGrouping: false,
                allowEditing: false,
                width: '50%'
            }

        ]
    }).dxDataGrid("instance");

    $("#autoExpand").dxCheckBox({
        value: false,
        text: "Раскрыть все группы",
        onValueChanged: function(data) {
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
    });


});


});