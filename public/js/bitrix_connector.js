
function get_clients(){


}

function client_bx_formatter(row) {
			var icons = '';
			if($.inArray(242, row.UF_CRM_1466447423) !== -1){
				icons += "📷" + ' ';
			}
			if($.inArray(244, row.UF_CRM_1466447423) !== -1){
				icons += "🎥" + ' ';
			}
			if($.inArray(248, row.UF_CRM_1466447423) !== -1){
				icons += "💰" + ' ';
			}
			if($.inArray(250, row.UF_CRM_1466447423) !== -1){
				icons += "🍸" + ' ';
			}
			if($.inArray(252, row.UF_CRM_1466447423) !== -1){
				icons += "🍴" + ' ';
			}
			if($.inArray(246, row.UF_CRM_1466447423) !== -1){
				icons += "👔" + ' ';
			}
			if($.inArray(254, row.UF_CRM_1466447423) !== -1){
				icons += "👑" + ' ';
			}

            return icons + row.NAME + ' ' + row.SECOND_NAME + ' ' + row.LAST_NAME;
        }

function client_bx_onselect(record){
    //$(this).combo('setText', record.ID);
	$('#new_order_client_bitrix_selected').removeClass('hidden').find('span.result').text(record.NAME + ' ' + record.SECOND_NAME + ' ' + record.LAST_NAME);
	$('#new_order_client_bitrix_id').val(record.ID);
	$('#new_order_client_bitrix_name').val(record.NAME + ' ' + record.SECOND_NAME + ' ' + record.LAST_NAME);

	var company_info = ''
	if(check_cl2co() || check_cl2p()){
		company_info = get_company_by_id(record.COMPANY_ID).company;
	}

	if(check_cl2co() && company_info){
		$('#new_order_company_bitrix_id').val(record.COMPANY_ID);
		$('#new_order_company_bitrix_name').val(company_info.TITLE);
		$('#new_order_company_bitrix_combobox').combobox('setText', company_info.TITLE);
	}
	if(check_cl2p() && company_info){
		$('#new_order_place_bitrix_id').val(record.COMPANY_ID);
		$('#new_order_place_bitrix_name').val(company_info.TITLE);
		$('#new_order_place_bitrix_combobox').combobox('setText', company_info.TITLE);
	}

}
function client_bx_onunselect(record){ 
    //$(this).combo('setText', record.ID);
	$('#new_order_client_bitrix_selected').addClass('hidden').find('span.result').text('');
	$('#new_order_client_bitrix_id').val(0);
	$('#new_order_client_bitrix_name').val('');
}

var company_selected_id = false;
function company_get_selected_id(){
	return company_selected_id;
}
function company_bx_onselect(record){
    //$(this).combo('setText', record.ID);
console.info('company select');
	if (typeof record !== "undefined") {
		console.info('company =)');
		$('#new_order_company_bitrix_selected').removeClass('hidden').find('span.result').text(record.TITLE);
		$('#new_order_company_bitrix_id').val(record.ID);
		$('#new_order_company_bitrix_name').val(record.TITLE);

		company_selected_id = record.ID;


		if (check_cl2co()) {
			$('#new_order_client_bitrix_combobox').combobox('reload').combobox('setValue', 0).combobox('setText', '');

		}

		if (check_p2oc()) {
			$('#new_order_place_bitrix_id').val(record.ID);
			$('#new_order_place_bitrix_name').val(record.TITLE);
			$('#new_order_place_bitrix_selected').removeClass('hidden').find('span.result').text(record.TITLE);


			if ($('#new_order_place_bitrix_combobox').combobox('getValue') != record.ID) {
				var url = $('#new_order_place_bitrix_combobox').combo('options').url;
				$('#new_order_place_bitrix_combobox').combobox('reload', url + '?q=' + encodeURI(record.TITLE)).combobox('select', record.ID);
			}
		}
	}
}

function company_bx_onunselect(record){
    //$(this).combo('setText', record.ID);
	$('#new_order_company_bitrix_id').val(0);
	$('#new_order_company_bitrix_name').val('');
	$('#new_order_company_bitrix_selected').addClass('hidden').find('span.result').text('');
}
function place_bx_onselect(record){

	if (typeof record !== "undefined") {
		console.info('place =)');
		$('#new_order_place_bitrix_selected').removeClass('hidden').find('span.result').text(record.TITLE);
		$('#new_order_place_bitrix_id').val(record.ID);
		$('#new_order_place_bitrix_name').val(record.TITLE);

		company_selected_id = record.ID;


		if (check_cl2co()) {
			$('#new_order_place_bitrix_combobox').combobox('reload').combobox('setValue', 0).combobox('setText', '');
		}

		if (check_p2oc()) {
			$('#new_order_company_bitrix_id').val(record.ID);
			$('#new_order_company_bitrix_name').val(record.TITLE);
			$('#new_order_company_bitrix_selected').removeClass('hidden').find('span.result').text(record.TITLE);

			if ($('#new_order_company_bitrix_combobox').combobox('getValue') != record.ID) {
				var url = $('#new_order_company_bitrix_combobox').combo('options').url;
				$('#new_order_company_bitrix_combobox').combobox('reload', url + '?q=' + encodeURI(record.TITLE)).combobox('select', record.ID);
			}
		}
	}
}
function place_bx_onunselect(record){
    //$(this).combo('setText', record.ID);
	$('#new_order_place_bitrix_selected').addClass('hidden').find('span.result').text('');
	$('#new_order_place_bitrix_id').val(0);
	$('#new_order_place_bitrix_name').val('');

}

function get_company_by_id(id){
	if(!id){
		return {company: false};
	}
	var request;
	$.ajax({
		async: false,
		url: "/bx24api/get_company_by_id",
		data: {
			id: id
		},
		type: 'get',
		success: function(data){
			request = data;
		}
	});
	return request;
}
function get_client_by_id(id){
	var request;
	$.ajax({
		async: false,
		url: "/bx24api/get_client_by_id",
		data: {
			id: id
		},
		type: 'get',
		success: function(data){
			request = data;
		}
	});
	return request;
}


/* Проверка активироанности связи места/заведения/клиента */
function check_cl2p() {
	if($('#client_link_place').prop('checked')){
		return true;
	}else{
		return false;
	}
}
function check_cl2co() {
	if($('#client_link_company').prop('checked')){
		return true;
	}else{
		return false;
	}
}
function check_p2oc() {
	if($('#place_link_company').prop('checked')){
		return true;
	}else{
		return false;
	}
}
/* -- Проверка активироанности связи места/заведения/клиента */
function bx_check_client_id(){
	var client_id = $('#new_order_client_bitrix_id').val();
	if(check_cl2co() && client_id){
		return client_id;
	}else{
		return 0;
	}
}

$(function () { // document ready

	$('#client_link_place, #client_link_company, #place_link_company').on('change', function () {

		if (check_cl2co() && check_cl2p()) {
			if (!check_p2oc()) {
				$('#place_link_company').prop('checked', true).parent().addClass('active');
			}
		}

	});
});