$(function () {

   DevExpress.localization.locale(navigator.language || navigator.browserLanguage);
   DevExpress.config({ defaultCurrency: 'RUB' });
   DevExpress.ui.dxOverlay.baseZIndex(25000);

    var fix_scroll = function (e) {
        console.log(e.component);
        var sv = e.component._popup.content().find(".dx-scrollview").dxScrollView("instance");
        sv.option('useNative', true);
    }

    var show_ajax_error = function(ajax_data) {
        var error = "Непредвиденная ошибка";
        if (ajax_data.error) {
            error = ajax_data.error;
        }
        swal("Ошибка!", error, "error");
    }

    var reload_items = function () {
        //$loadPanel.show();
        photo_portfolio_items_source.reload().done(function(result) {
            //$loadPanel.hide();
        });
    }

    function cbox_mute(mute){
        if(mute){
            $('#cboxOverlay').addClass('muted');
            $('#colorbox').addClass('muted');
        }else{
            $('#cboxOverlay').removeClass('muted');
            $('#colorbox').removeClass('muted');
        }
    }

    var vote_changed = false;
    var portfolio_make_vote = function (id, vote) {
        //cbox_mute(true);
        var approve_group = $('#colorbox .colorbox-vote').dxButtonGroup('instance');
        $('#cboxContent .moderate-function').addClass('loading');
        $.post({
            url: '/moderate/portfolio/vote/' + id,
            beforeSend: function () {
            },
            cache: false,
            dataType: 'json',
            data: {
                vote: vote
            },
            success: function (data) {
                if (data.success) {
                    vote_changed = true;
                    $.colorbox.element().data('my-vote', vote);

                    approve_group.option('selectedItemKeys', ['btn_' + vote]);
                    $('#cboxContent .moderate-function').removeClass('loading');
                    DevExpress.ui.notify({
                        message: 'Голос принят',
                        width: 200,
                        position: 'center center'
                    }, 'success', 500);
                    $.colorbox.next();

                } else {
                    var error_text = 'Неизвестная ошибка';
                    if (data.error) {
                        error_text = data.error;
                    }
                    approve_group.option('selectedItemKeys', []);
                    DevExpress.ui.notify({message: error_text, position: 'center center'}, 'error', 4000);
                }
            },
            error: function (error, status) {
                approve_group.option('selectedItemKeys', []);
                $('#cboxContent .moderate-function').removeClass('loading');
                swal("Ошибка!", "Код статуса ошибки: " + error.status, "error");
            }
        });
        /*swal({
            title: 'Подтверждение',
            text: "Вы уверены, что хотите " + ((vote > 0) ? 'утвердить' : 'отклонить') + "?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да, голосую!',
            cancelButtonText: 'Отмена',
        }).then(function (result) {
            //cbox_mute(false);
            if (result.value) {

            }else{
                $('#cboxContent .moderate-function').removeClass('loading');
                approve_group.option('selectedItemKeys', []);
                cbox_mute(false);
            }
        });*/
    }

    var photo_portfolio_tags = new DevExpress.data.CustomStore({
        loadMode: "raw",
        key: "id",
        load: function() {
            return $.getJSON("/profile/portfolio/tags/by_type_photo");
        },
        onLoading: function (loadOptions) {
            if($loadPanel){
                $loadPanel.show();
            }
        },
        onLoaded: function (result) {
            if($loadPanel) {
                $loadPanel.hide();
            }
        }
    });
    var photo_portfolio_tags_source= new DevExpress.data.DataSource({
        store: photo_portfolio_tags,
        sort: ['name']
    });
    /*var photo_portfolio_items = new DevExpress.data.CustomStore({
        loadMode: "raw",
        load: function() {
            return $.getJSON("/profile/portfolio/tags/by_type_photo");
        }
    });*/
    var item_filters = {
        vote: 0,
        status: null,
        users: null,
        tags: null
    }
    var photo_portfolio_items = new DevExpress.data.CustomStore({
        key: "id",
        load: function (loadOptions) {
            var d = $.Deferred(), filter = {};
            if(item_filters.vote !== null){
                filter.vote = item_filters.vote;
            }
            if(item_filters.status !== null){
                filter.status = item_filters.status;
            }
            if(item_filters.users !== null){
                filter.users = item_filters.users;
            }
            if(item_filters.tags !== null){
                filter.tags = item_filters.tags;
            }
            var url = "/profile/portfolio/by_type_photo/json";
            if(window.portfolio_moderate){
                url = "/moderate/portfolio/by_type_photo/json";
            }
            $.getJSON(url, {filter: filter}).done(function (result) {
                d.resolve(result.data);
            });

            return d.promise();
        }
    });
    var photo_portfolio_items_source= new DevExpress.data.DataSource({
        store: photo_portfolio_items
    });

    var photo_portfolio_users = new DevExpress.data.CustomStore({
        key: "id",
        load: function (loadOptions) {
            var d = $.Deferred(), args = {};
            $.getJSON("/moderate/portfolio/get_users_with_portfolio/by_type_photo/json", args).done(function (result) {
                d.resolve(result.data);
            });

            return d.promise();
        }
    });
    var photo_portfolio_users_source= new DevExpress.data.DataSource({
        store: photo_portfolio_users
    });

    var photo_portfolio_all_photographers = new DevExpress.data.CustomStore({
        key: "id",
        useDefaultSearch: false,
        load: function (loadOptions) {
            var d = $.Deferred(), args = {};
            if(loadOptions.searchValue){
                args.search = loadOptions.searchValue;
            }
            $.getJSON("/moderate/portfolio/get_photographers/json", args).done(function (result) {
                d.resolve(result.data);
            });

            return d.promise();
        }
    });
    var photo_portfolio_all_photographers_source= new DevExpress.data.DataSource({
        store: photo_portfolio_all_photographers
    });

    var photo_portfolio_vote_store = [{
        value: 0,
        title: 'Не голосовал',
        color: 'silver',
        icon: 'dx-icon-clock'
    }, {
        value: 1,
        title: 'Подтверждаю',
        color: 'green',
        icon: 'dx-icon-todo'
    }, {
        value: -1,
        title: 'Отклоняю',
        color: 'red',
        icon: 'dx-icon-clear'
    }];

    var photo_portfolio_status_store = [{
        value: 0,
        title: 'Идёт голосование',
        color: 'silver',
        icon: 'dx-icon-clock'
    }, {
        value: 1,
        title: 'Опубликовано',
        color: 'green',
        icon: 'dx-icon-todo'
    }, {
        value: -1,
        title: 'Отклонено',
        color: 'red',
        icon: 'dx-icon-clear'
    }];

    var photo_portfolio_vote_item_tpl = function (itemData, itemIndex, element) {
        element.append(
            $("<span>").addClass('dx-icon ' + itemData.icon).css('color', itemData.color),
            $("<span>").text(' ' + itemData.title).css('color', itemData.color)
        );
    };

    var photo_portfolio_vote_drop_tpl = function(itemData){
        if (itemData){
            console.log(itemData);
            return $("<span>").addClass('dx-icon ' + itemData.icon).css('color', itemData.color)
        }else{
            return "dropDownButton";
        }
    };

    var photo_add_data = {
        //title : '',
        tags : [],
        photos : []
    };
    $("#form_photo_add").dxForm({
        colCount: 2,
        formData: photo_add_data,
        labelLocation: "top",
        showValidationSummary: true,
        showOptionalMark: true,
        stylingMode: 'underlined',
        /*elementAttr: {
            action: "/profile/portfolio/add",
            method: "put"
        },*/
        items: [/*{
                dataField: "title",
                label: {
                    text: "Заголовок"
                },
                helpText: 'При массвовой загрузке лучше заполнить потом',
                editorOptions: {
                    showClearButton: true,
                    placeholder: "Короткое описание фото",
                    maxLength: 255
                }
            },*/
            {
                dataField: "tags",
                editorType: "dxTagBox",
                label: {
                    text: "Тэги"
                },
               // helpText: 'Если тэг не найден среди ранее указаных, то после ввода тега нажмите "Enter"',
                editorOptions: {
                    dataSource: photo_portfolio_tags,
                    displayExpr: 'name.ru',
                    searchEnabled: true,
                    acceptCustomValue: false,
                    showClearButton: true,
                    placeholder: "Репортаж, свадебное фото, пейзаж и тому подобное",
                    name: 'tags[]'
                },
                /*validationRules: [{
                    type: "required",
                    message: "Укажите минимум один тэг"
                }]*/
            },
            {
                dataField: "photos",
                editorType: "dxFileUploader",
                label: {
                    text: "Фотографии"
                },
                editorOptions: {
                    uploadMode: 'useForm',
                    allowedFileExtensions: [".jpg", ".jpeg"],
                    maxFileSize: 1024*1024*10,
                    multiple: true,
                    name: 'photos[]'
                    /*onValueChanged: function (e) {
                        var files = e.value;
                        if (files.length > 0) {
                            $("#selected-files .selected-item").remove();
                            $.each(files, function (i, file) {
                                var $selectedItem = $("<div />").addClass("selected-item");
                                console.log(file);
                                $selectedItem.append(
                                    $("<span />").html("Name: " + file.name + "<br/>"),
                                    $("<span />").html("Size " + file.size + " bytes" + "<br/>"),
                                    $("<span />").html("Type " + file.type + "<br/>"),
                                    $("<span />").html("Last Modified Date: " + file.lastModifiedDate)
                                );
                                $selectedItem.appendTo($("#selected-files"));
                            });
                            $("#selected-files").show();
                        } else
                            $("#selected-files").hide();
                    }*/
                },
                validationRules: [{
                    type: "required",
                    message: "Требуется минимум одно фото"
                }]
        },
        {
            dataField: "user",
            visible: window.portfolio_upload_by_another_user,
            editorType: "dxSelectBox",
            label: {
                text: "Автор"
            },
            editorOptions: {
                width: 'auto',
                dataSource: photo_portfolio_all_photographers,
                searchEnabled: true,
                showDropDownButton: true,
                showClearButton: true,
                displayExpr: 'name',
                valueExpr: 'id',
                placeholder: 'От своего имени',
                searchExpr: 'name',
                onContentReady: fix_scroll,
                //itemTemplate: photo_portfolio_vote_item_tpl,
                onValueChanged: function (args) {
                    //console.log(args);
                    item_filters.users = args.value;
                    reload_items();
                }
            }
        },
        {
            itemType: "button",
            horizontalAlignment: "left",
            verticalAlignment: "bottom",
            buttonOptions: {
                text: "Сохранить в портфолио",
                type: "success",
                useSubmitBehavior: true
            }
        }]
    });

    var $popupEditorContainer = $("<div />").addClass("popup_item").appendTo($("#portfolio_photo_popup"));
    var $popupEditor = $popupEditorContainer.dxPopup({
        title: 'Редактирование',
        shading: true,
        closeOnBackButton: true,
        closeOnOutsideClick: true,
        width: '50%',
        height: 'auto',
        toolbarItems: [{
            widget: "dxButton",
            location: "after",
            toolbar: 'bottom',
            options: {
                text: "Сохранить",
                onClick: function (e) {
                    var send_data = {};

                    if (window.portfolio_edit_tags) {
                        var $tags = $("#editorTags").dxTagBox("instance");
                        send_data.tags = $tags.option('value');
                    }

                    if (window.portfolio_vote) {
                        var $vote_editor = $("#editorVote").dxSelectBox("instance");
                        send_data.vote = $vote_editor.option('value');
                    }

                   $.post({
                       url: edit_url_prefix + $('#editorId').val(),
                       beforeSend: function () {
                           $loadPanel.show();

                       },
                       cache: false,
                       dataType: 'json',
                       data: send_data,
                       success: function (data) {
                           $loadPanel.hide();
                           reload_items();
                           $popupEditor.hide();
                           if(data.success){
                               var text = "Фото отредактировано";
                               if(data.error){
                                   text += ", но не полность!<br>";
                                   text += "<b class='col-red'>Оганичения доступа: " + data.error + "</b>";
                               }
                               swal("Ура!", text, "success");

                           }else{
                               var error = "Непредвиденная ошибка";
                               if(data.error){
                                   error = data.error;
                               }
                               swal("Ошибка!", error, "error");

                           }

                       }
                   })
                }
            }
        }]
    }).dxPopup("instance");

    var $loadPanel = $("#portfolio_photo_loader").dxLoadPanel({
        shading: true,
        shadingColor: "rgba(0,0,0,0.4)",
    }).dxLoadPanel("instance");

    var $photo_tile = $("#portfolio_photo_tile").dxTileView({
        dataSource: photo_portfolio_items_source,
        height: '100%',
        baseItemHeight: 250,
        baseItemWidth: 250,
        itemMargin: 10,
        direction: 'vertical',
        focusStateEnabled: false,
        hoverStateEnabled: false,
        noDataText: 'Подходящих объектов не найдено',
        itemTemplate: function (itemData, itemIndex, itemElement) {
            var tile = "";
            //tile += "<div class='title'>" + (itemData.title ? itemData.title : '[без названия]') + "</div>";
            var vote_class = itemData.quorum ? (itemData.approved ? 'approved' : 'not_approved') : 'not_quorum';
            tile += "<div class='vote " + vote_class + "'>" + itemData.up_votes + " из " + itemData.votes + "</div>";
            if(window.portfolio_moderate){
                tile += "<div class='author'>" + itemData.user.name + "</div>";

                var my_vote_class = 'fa ';
                if(itemData.my_vote < 0 ){
                    my_vote_class += 'fa-ban';
                }
                if(itemData.my_vote > 0 ){
                    my_vote_class += 'fa-check';
                }
                tile += "<div class='my_vote " + my_vote_class + "'></div>";
            }
            //tile += "<div class='tags'>" + itemData.tags_list + "</div>";
            var tags_list = '';
            if(itemData.tags_names.length > 0){
                tags_list =  '<span>' + itemData.tags_names.join ( '</span><span>' ) + '</span>';
            }else{
                tags_list = '<span class="no_tags">ТЭГИ НЕ УКАЗАНЫ!</span>';
            }

            tile += "<div class='tags'>" + tags_list + "</div>";
            tile += "<div class=\"image\" style=\"background-image: url('" + itemData.thumb_url + "')\"></div>";
            tile += "<a title='Увеличить' class='zoom' data-my-vote ='" + itemData.my_vote + "' href='" + itemData.url + "'></a>";
            tile += "<div title='Редактировать' class='edit'></div>";
            tile += "<div title='Удалить' class='remove'></div>";
            var $tile = itemElement.append(tile);

            var $item_zoom = $tile.find('.zoom');
            var $item_edit = $tile.find('.edit');
            var $item_remove = $tile.find('.remove');
            $($item_zoom).colorbox({
                rel: 'portfolio',
                open: false,
                title: itemData.tags_list,
                onClosed: function(){
                    if(vote_changed){
                        //$loadPanel.show();
                        var reload_promise = photo_portfolio_items_source.reload();
                        reload_promise.done(function (result) {
                            vote_changed = false;
                            //$loadPanel.hide();
                        });
                    }
                },
                onComplete: function(){

                    if(window.portfolio_vote /* && DevExpress.devices.current().phone == false*/){
                        $('#cboxContent .moderate-function').remove(); // Чистим кнопки модерации с прошлого раза
                        var  selectedItemKeys = [];
                        var my_vote = $.colorbox.element().data('my-vote');
                        if(my_vote != 0) {
                            selectedItemKeys = ['btn_' + my_vote];
                        }
                        var $approve = $('<span />').addClass('colorbox-vote')
                            .dxButtonGroup({
                                items: [
                                    {
                                        icon: "todo",
                                        alignment: "left",
                                        type: 'success',
                                        val: 'btn_1',
                                        focusStateEnabled: false,
                                        onClick: function(e){
                                            portfolio_make_vote(itemData.id, 1);

                                            return false;
                                        }
                                    },{
                                        icon: "clear",
                                        alignment: "center",
                                        type: 'danger',
                                        val: 'btn_-1',
                                        focusStateEnabled: false,
                                        onClick: function(e){
                                            portfolio_make_vote(itemData.id, -1, e.element);
                                            return false;
                                        }
                                    }
                                ],
                                keyExpr: "val",
                                stylingMode: "outlined",
                                selectedItemKeys: selectedItemKeys,
                                selectionMode: "multiple",
                                focusStateEnabled: false,
                                onSelectionChanged: function (e) {
                                    //e.component.option('selectedItems', []);
                                    //e.component.option('selectionMode', 'single');

                                }
                        });

                        var $cbox_current = $('#cboxCurrent');
                        var $cbox_content = $('#cboxContent');
                        var $moderate_box = $('<div>').addClass('moderate-function');
                        var $load_indicator = $('<div>').addClass('load_indicator');

                        $load_indicator.append(
                            $('<i />').addClass('fa fa-refresh fa-spin')
                        );
                        $load_indicator.append('<span>сохранение</span>');

                        $approve.prependTo($moderate_box);
                        $load_indicator.prependTo($moderate_box);
                        $moderate_box.appendTo($cbox_content);
                    }
                }
            });

            window.edit_url_prefix = "/profile/portfolio/";
            if(window.portfolio_moderate) {
                window.edit_url_prefix = "/moderate/portfolio/";
            }

            $item_edit.on('click', function(){
                $.get({
                    url: edit_url_prefix + itemData.id,
                    beforeSend: function () {
                        $loadPanel.show();
                    },
                    cache: false,
                    dataType: 'json',
                    error: function (error, status) {
                        $loadPanel.hide();
                        swal("Ошибка!", "Код статуса ошибки: " + error.status, "error");
                    },
                    success: function (load_data) {
                        $loadPanel.hide();
                        if (load_data.error) {
                            show_ajax_error(load_data);
                        }else{
                            $popupEditor.option('contentTemplate', function () {
                                var popup_content = $("<div />")

                                var portfolio_id = $("<input type='hidden' id='editorId' />").val(load_data.id);
                                popup_content.append(portfolio_id);

                                if(window.portfolio_edit_tags) {
                                    var tags = $("<div id='editorTags' />").dxTagBox({
                                        dataSource: photo_portfolio_tags_source,
                                        searchEnabled: true,
                                        acceptCustomValue: true,
                                        showDropDownButton: true,
                                        showClearButton: true,
                                        displayExpr: 'name.ru',
                                        valueExpr: 'id',
                                        value: load_data.tags_ids,
                                        multiline: false,
                                        onContentReady: fix_scroll,
                                        onInitialized: function () {
                                        },
                                        onOpened: function () {
                                            $loadPanel.hide();
                                        }
                                    });
                                    popup_content.append(
                                        $("<h4 />").text("Тэги"), tags
                                    );
                                }

                                if(window.portfolio_vote) {
                                    var vote = $("<div id='editorVote' />").dxSelectBox({
                                        dataSource: photo_portfolio_vote_store,
                                        searchEnabled: false,
                                        showDropDownButton: true,
                                        showClearButton: false,
                                        displayExpr: 'title',
                                        valueExpr: 'value',
                                        value: load_data.my_vote,
                                        onContentReady: fix_scroll,
                                        itemTemplate: photo_portfolio_vote_item_tpl
                                    });
                                    popup_content.append(
                                        $("<h4 />").text("Ваш голос"), vote
                                    );

                                }

                                return popup_content;
                            });
                            $popupEditor.show();
                        }
                    }
                });
            });

            $item_remove.on('click', function(){
                swal({
                    title: 'Подтверждение',
                    text: "Вы уверены, что хотите безвозвратно удалить объект #" + itemData.id + " из портфолио?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, удалить!',
                    cancelButtonText: 'Отмена',
                }).then(function (result){
                    if (result.value){
                        $.get({
                            url: '/profile/portfolio/remove',
                            beforeSend: function () {
                                $loadPanel.show();
                            },
                            cache: false,
                            dataType: 'json',
                            data: {
                              id: itemData.id
                            },
                            success: function (data) {
                                $loadPanel.hide();
                                if (data.success) {
                                    var reload_promise = photo_portfolio_items_source.reload();
                                    reload_promise.done(function(result){
                                        swal("Ура!", "Объект успешно удалён", "success");
                                    });
                                } else {
                                    show_ajax_error(data);
                                }
                            },
                            error: function(error, status){
                                $loadPanel.hide();
                                swal("Ошибка!", "Код статуса ошибки: " + error.status, "error");
                            }
                        });
                    }
                });
            });




        },
        onInitialized: function (e) {
        },
        onItemRendered: function (e) {
        },
        onItemClick: function(event){
        }
    }).dxTileView('instance');

    var portfolio_list_toolbar_items = [];
    if(window.portfolio_moderate){
        //Filter tag
        portfolio_list_toolbar_items.push({
            location: 'after',
            widget: 'dxSelectBox',
            locateInMenu: 'auto',
            options: {
                width: 'auto',
                dataSource: photo_portfolio_tags,
                valueExpr: "name.ru",
                displayExpr: "name.ru",
                placeholder: 'Тэги',
                showClearButton: true,
                searchEnabled: true,
                //value: productTypes[0].id,
                onValueChanged: function (args) {
                    //console.log(args);
                    item_filters.tags = args.value;
                    reload_items();
                }
            }
        });
        //Filter vote
        portfolio_list_toolbar_items.push({
            location: 'before',
            widget: 'dxSelectBox',
            locateInMenu: 'auto',
            options: {
                width: 'auto',
                dataSource: photo_portfolio_vote_store,
                searchEnabled: false,
                showDropDownButton: true,
                showClearButton: true,
                displayExpr: 'title',
                valueExpr: 'value',
                placeholder: 'Голосование',
                value: item_filters.vote,
                onContentReady: fix_scroll,
                itemTemplate: photo_portfolio_vote_item_tpl,
                //dropDownButtonTemplate: photo_portfolio_vote_drop_tpl,
                onValueChanged: function (args) {
                    //console.log(args);
                    item_filters.vote = args.value;
                    reload_items();
                }
            }
        });
        //Filter status
        portfolio_list_toolbar_items.push({
            location: 'before',
            widget: 'dxSelectBox',
            locateInMenu: 'auto',
            options: {
                width: 'auto',
                dataSource: photo_portfolio_status_store,
                searchEnabled: false,
                showDropDownButton: true,
                showClearButton: true,
                displayExpr: 'title',
                valueExpr: 'value',
                placeholder: 'Статус публикации',
                onContentReady: fix_scroll,
                itemTemplate: photo_portfolio_vote_item_tpl,
                //dropDownButtonTemplate: photo_portfolio_vote_drop_tpl,
                onValueChanged: function (args) {
                    //console.log(args);
                    item_filters.status = args.value;
                    console.log(args.value);
                    reload_items();
                }
            }
        });
        //Filter user
        portfolio_list_toolbar_items.push({
            location: 'after',
            widget: 'dxSelectBox',
            locateInMenu: 'auto',
            options: {
                width: 'auto',
                dataSource: photo_portfolio_users_source,
                searchEnabled: false,
                showDropDownButton: true,
                showClearButton: true,
                displayExpr: 'name',
                valueExpr: 'id',
                placeholder: 'Автор',
                onContentReady: fix_scroll,
                //itemTemplate: photo_portfolio_vote_item_tpl,
                onValueChanged: function (args) {
                    //console.log(args);
                    item_filters.users = args.value;
                    reload_items();
                }
            }
        });
    }else{
        portfolio_list_toolbar_items.push({
            location: 'before',
            widget: 'dxButton',
            locateInMenu: 'auto',
            options: {
                icon: "add",
                type: "success",
                text: "Добавить фото",
                onClick: function () {
                    document.location = '/profile/portfolio/add';
                }
            }
        });
    }

    $('#portfolio_photo_toolbar').dxToolbar({
        items: portfolio_list_toolbar_items
    });

});