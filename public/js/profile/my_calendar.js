$(function () {

    function init() {

        //var date_ = scheduler.date.date_to_str("%d/%m/%Y");

        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
        scheduler.config.api_date = "%Y-%m-%d %H:%i";
        scheduler.config.time_step = 1;
        scheduler.config.multi_day = false;
        scheduler.locale.labels.section_busy = "Занятость";
        scheduler.locale.labels.section_comment = "Комментарий";
        scheduler.config.first_hour = 0;
        scheduler.config.last_hour = 24;
        scheduler.config.limit_time_select = true;
        scheduler.config.details_on_dblclick = true;
        scheduler.config.details_on_create = true;
        //scheduler.config.container_autoresize = true;
        scheduler.config.dblclick_create = true;
        scheduler.config.full_day = false;
        scheduler.config.icons_select = [
            "icon_details",
            "icon_delete"
        ];
        scheduler.config.mark_now = true;
        scheduler.config.server_utc = true;
        scheduler.config.show_loading = true;

        var dragged_event_id;
        var dragged_event_mode;
        scheduler.attachEvent("onBeforeDrag", function (id, mode, e) {
            dragged_event_id = id;
            dragged_event_mode = mode;
            return true;
        });

        scheduler.attachEvent("onBeforeLightbox", function (eventId) {
            correct_event(eventId);
            return true;
        });
        scheduler.attachEvent("onDragEnd", function () {
            if(dragged_event_mode == "move" || dragged_event_mode == "resize"){
                correct_event(dragged_event_id, true);
            }
            return true;
        });
        scheduler.attachEvent("onEventChanged", function (eventId){
            save_one_event(scheduler.getEvent(eventId));
        });
        scheduler.attachEvent("onEventAdded", function (eventId){
            save_one_event(scheduler.getEvent(eventId));
        });


        scheduler.templates.event_class = function (start, end, event) {
            var css = "";

            if (event.busy) // if event has subject property then special class should be assigned
                css += "event_" + event.busy;

            if (event.id == scheduler.getState().select_id) {
                css += " selected";
            }
            return css; // default return

            /*
                Note that it is possible to create more complex checks
                events with the same properties could have different CSS classes depending on the current view:

                var mode = scheduler.getState().mode;
                if(mode == "day"){
                    // custom logic here
                }
                else {
                    // custom logic here
                }
            */
        };

        scheduler.config.collision_limit = 1;
        scheduler.config.check_limits = true;
        scheduler.attachEvent("onEventLoading", function (ev) {
            return scheduler.checkCollision(ev);
        });

        var busy = [
            {key: 'none', label: 'Не знаю'},
            {key: 'free3', label: 'Снимаю'},
            {key: 'busy3', label: 'Не снимаю'}
        ];

        var busy_text = {
            none: 'Не знаю',
            free3: 'Снимаю',
            busy3: 'Не снимаю'
        };

        scheduler.config.lightbox.sections = [
            {name:"comment", height:143, map_to:"comment", type:"textarea" , focus:false, default_value: '' },
            {
                name: "busy",
                height: 40,
                type: "radio",
                vertical: false,
                options: busy,
                map_to: "busy",
                default_value: 'none'
            },
            /*{name:"time", height:72, type:"time", map_to:"auto" }*/
        ];

        scheduler.templates.event_text = function (start, end, ev) {


            return (busy_text[ev.busy] ? '<b>' + busy_text[ev.busy] + '</b>' : '') + (ev.comment ? "<br>" + ev.comment : '');
        };

        scheduler.init('my_calendar', new Date(), "week");

        scheduler.setLoadMode("week");
        scheduler.load("/profile/busy/load/", 'json');

        /*scheduler.parse([
            {id: 1, start_date: "2017-04-18 09:00", end_date: "2017-04-18 12:00", text: "English lesson", busy: 'free3', is_old: true},
            {id: 2, start_date: "2017-04-20 10:00", end_date: "2017-04-20 16:00", text: "Math exam", busy: 'busy3', is_old: true},
            {id: 3, start_date: "2017-04-21 10:00", end_date: "2017-04-21 14:00", text: "Science lesson", busy: 'free3', is_old: true},
            {id: 4, start_date: "2017-04-23 16:00", end_date: "2017-04-23 17:00", text: "English lesson", busy: 'none', is_old: true},
            {id: 5, start_date: "2017-04-22 09:00", end_date: "2017-04-22 17:00", text: "Usual event", busy: 'busy3', is_old: true}
        ], "json");*/


    }

    window.show_minical = function () {
        if (scheduler.isCalendarVisible())
            scheduler.destroyCalendar();
        else
            scheduler.renderCalendar({
                position: "dhx_minical_icon",
                date: scheduler._date,
                navigation: true,
                handler: function (date, calendar) {
                    scheduler.setCurrentView(date);
                    scheduler.destroyCalendar()
                }
            });
    }

    window.correct_event = function (eventId, isSave) {
        var event = scheduler.getEvent(eventId);
        event.text = '';

        if (event.end_date.getHours() == 23 && event.end_date.getMinutes() > 30) {
            event.end_date.setMinutes(59);
        }
        if (event.end_date.getHours() == 0 && event.end_date.getMinutes() == 0) {
            event.end_date = scheduler.date.add(event.end_date, -1, 'minute');
        }

        scheduler.setEvent(eventId, event);
        scheduler.updateEvent(eventId);

        if(isSave) {
            setTimeout(function () {
                save_one_event(event);
            }, 1000);
        }

    }

    window.save_busy_calendar = function() {
        var events = scheduler.getEvents();
        var state = scheduler.getState();

        var events_data = [];
        $.each(events, function(i, e){
            events_data.push({
                start_date: moment(e.start_date).format('YY-MM-DD HH:mm Z'),
                end_date: moment(e.start_date).format('YY-MM-DD HH:mm Z'),
                comment: e.comment,
                busy: e.busy
            });

        });

        $.ajax({
            type: "POST",
            url: '/profile/busy/save',
            cache: false,
            data: {
                events: events_data,
                from: moment(state.min_date).format('YY-MM-DD HH:mm Z'),
                to: moment(state.max_date).format('YY-MM-DD HH:mm Z')
            },
            success: function (data) {


            },
            error: function (data) {

            }
        });




    }

    window.save_one_event = function(event) {
        console.log(event);
    }


    init();


});