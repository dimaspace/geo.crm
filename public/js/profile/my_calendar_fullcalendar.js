$(function () {

    jQuery_3_3_1('#my_calendar').fullCalendar({
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        locale: 'ru',
        timezone: window.cfg_time_zone,
        now: '2018-04-07',
        editable: true,
        //aspectRatio: 1.8,
        height: 'auto',
        scrollTime: '00:00',
        header: {
            left: 'today prev,next',
            center: 'title',
            right: 'timelineThreeDays,agendaWeek,month'
        },
        defaultView: 'timelineThreeDays',
        views: {
            timelineThreeDays: {
                type: 'timeline',
                duration: {days: 3}
            }
        },
        resources: [
            {
                id: '0',
                title: 'в'
            }
        ],
        eventSources: [
                {
                    url: '/profile/busy/json',
                }
        ]

    });

    var add_range_form = $("#add_range_form").dxForm({
        formData: [],
        readOnly: false,
        showColonAfterLabel: true,
        labelLocation: "top",
        minColWidth: 300,
        colCount: 1
    });
    var add_range_form_inst = add_range_form.dxForm("instance")

    $("#busy").dxSelectBox({
        displayExpr: "Name",
        dataSource: [
            {
                id: 1,
                text: 'text'
            },{
                id: 2,
                text: 'text2'
            }
        ],
        value: 1,
        onValueChanged: function(data) {
           form.option("formData", data.value);
        }
    });

    window.add_range = function(){
        var popup = null,
        popupOptions = {
            width: 300,
            height: 250,
            contentTemplate: function() {
                return add_range_form;
            },
            showTitle: true,
            title: "Добавить диапазон",
            visible: false,
            dragEnabled: true,
            closeOnOutsideClick: true
        };

        if(popup) {
            $(".popup").remove();
        }

        var $popupContainer = $("#add_range_popup");
        popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
        popup.show();
    }

});