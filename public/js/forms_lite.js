$(document).ready(function() {

// CSRF для всех ajax запросов
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': Laravel.csrfToken
        },
        dataType: 'json'
    })

// Страница быстрого подтверждения транзакции
    $("#booker_unapproved_quick_transaction a.btn").click(function(event){
        var text = $(this).text();
        var url = $(this).attr('href');
        var transaction_id = $(this).data('transaction');
        var user_id = $(this).data('user');
        event.preventDefault();

        swal({
            title: "Подтверждение",
            text: "Вы уверены, что хотите " + text + " платёж?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, уверен!",
            cancelButtonText: "Отмена",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        transaction_id: transaction_id,
                        user_id: user_id
                    },
                    success: function (data) {
                        if (!data.error) {
                            swal("Успех!", data.success, "success");
                        } else {
                            swal("Ошибка!", data.error, "warning");
                        }

                    },
                    error: function (data) {
                        if (data.status = 422) {
                            var error_text = '';
                            $.each(data.responseJSON, function (i, e) {
                                error_text += e + "\n";
                            });
                            swal("Ошибка!", error_text, "error");
                        } else {
                            swal("Ошибка!", data.status, "error");
                        }
                    }
                });
        });

    });

});