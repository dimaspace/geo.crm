$(function () {

var table_npl = $('#now_project_list_table').DataTable({
    fixedHeader: true,
    paging: false,
    scrollX: true,
    dom: 'lfrtp',


    colReorder: true,

    stateSave: true,
    "stateDuration": 60 * 60 * 24 * 365 * 3,

    language: {
        "url": "/plugins/jquery-datatable/ru.lang"
    }
});

$('#now_project_list_table tbody').on('click', 'tr', function () {
    if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
    }
    else {
        table_npl.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});

var table_pwmpl = $('#photo_work_market_project_list_table').DataTable({
    fixedHeader: true,
    paging: false,
    scrollX: true,
    dom: 'lfrtp',


    colReorder: true,

    stateSave: true,
    "stateDuration": 60 * 60 * 24 * 365 * 3,

    language: {
        "url": "/plugins/jquery-datatable/ru.lang"
    }
});

$('#photo_work_market_project_list_table tbody').on('click', 'tr', function () {
    if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
    }
    else {
        table_pwmpl.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});


 $('.market-vote .rating').change(function(){
     $this = $(this);

     var rating = $this.val();
     var project_id = $this.data('projectId');

        $.ajax({
            url: '/work_market/add_desire',
            type: "POST",
            cache: false,
            data: {
                desire_rating: rating,
                project_id : project_id
            },
            beforeSend: function () {

            },
            success: function (data) {
                if(data.saved){
                    showNotification('bg-green', 'Пожелание сохранено', 2000, 'bottom', 'right', 'animated fadeInUp');
                }else{
                    showNotification('bg-black', 'Пожелание отменено', 2000, 'bottom', 'right', 'animated fadeInUp');
                }
            }

        });

 });



});