$(function () {

    var $project_date_range = $('#project-date-range');
    var $project_date_range_start = $('#project-date-range-start');
    var $project_date_range_end= $('#project-date-range-end');

    project_range_function = function (start, end) {

        $project_date_range.find('span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));

        // noinspection NonAsciiCharacters
        $project_date_range.daterangepicker({
            "startDate": start,
            "endDate": end,
            "ranges": {
                'Неделя назад': [moment().startOf('day').subtract(7, 'days'), moment().endOf('day')],
                'Месяц назад': [moment().startOf('day').subtract(1, 'month'), moment().endOf('day')],
                'Этот год': [moment().startOf('year'), moment().endOf('year')],
                'Прошлый год': [moment().subtract(1, 'year').startOf('year'), moment().startOf('year').subtract(1, 'minute')]

            },
            "locale": {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Вручную",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            }
        }, function (start, end) {
            $project_date_range.find('span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
            $project_date_range_start.val(start.format('YYYY-MM-DD'));
            $project_date_range_end.val(end.format('YYYY-MM-DD'));

        });
    };

    var start = moment().startOf('day');
    var end = moment().endOf('day');
    project_range_function(start, end);


    var $transactions_date_range = $('#transactions-date-range');
    var $transactions_date_range_start = $('#transactions-date-range-start');
    var $transactions_date_range_end= $('#transactions-date-range-end');

    transactions_range_function = function (start, end) {

        $transactions_date_range.find('span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));

        // noinspection NonAsciiCharacters
        $transactions_date_range.daterangepicker({
            "startDate": start,
            "endDate": end,
            "ranges": {
                'Неделя назад': [moment().startOf('day').subtract(7, 'days'), moment().endOf('day')],
                'Месяц назад': [moment().startOf('day').subtract(1, 'month'), moment().endOf('day')],
                'Этот год': [moment().startOf('year'), moment().endOf('year')],
                'Прошлый год': [moment().subtract(1, 'year').startOf('year'), moment().startOf('year').subtract(1, 'minute')]

            },
            "locale": {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Вручную",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            }
        }, function (start, end) {
            $transactions_date_range.find('span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
            $transactions_date_range_start.val(start.format('YYYY-MM-DD'));
            $transactions_date_range_end.val(end.format('YYYY-MM-DD'));

        });
    };

    transactions_range_function(start, end);

});