// CSRF для всех ajax запросов
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': Laravel.csrfToken
    },
    dataType: 'json'
})

// Уведомление о появлении уведомлений через телеграм
var tnd_cookie_name = 'telegram_notify_dash';

if(!Cookies.get(tnd_cookie_name)){
    $('#telegram_notify_dash_modal').modal('show');
}

$('#telegram_notify_dash_modal').on('hidden.bs.modal', function (e) {
    if(!Cookies.get(tnd_cookie_name)) {
        Cookies.set(tnd_cookie_name, 'OK', {expires: 3});
    }
});
$('#telegram_notify_dash_modal .hide_permanent').on('click', function (e) {
    Cookies.set(tnd_cookie_name, 'OK');
    $('#telegram_notify_dash_modal').modal('hide');
});

//
function vk_auth_callback(data){
  if(data.uid){
      $('#vk_id').val(data.uid);
      swal("Ваш ID Вконтакте #" + data.uid + "!", "Не забудьте сохранить изменения!", "success");
      $('#vk_auth').html('<div class="form-line col-grey"><i class="material-icons col-teal font-20">done</i> ' + data.uid + '</div>').css('height', 'auto');
  }else{
      swal("Ошибка", "Не удалось получить ваш ID Вконтакте", "error");
  }
}

// Даш координатора

/*
var mygrid=dhtmlXGridFromTable("cord_dash_table");
//mygrid.enableAutoWidth(true);
//mygrid.enableAutoHeight(true);
//mygrid.setSizes();
mygrid.attachHeader("#combo_filter, #combo_filter, #numeric_filter, #text_filter, #text_filter, #text_filter, #text_filter, #text_filter, #text_filter, #text_filter, #text_filter, #text_filter, #numeric_filter, #numeric_filter, #combo_filter, #combo_filter, , , , #combo_filter, #combo_filter, #combo_filter");
mygrid.enableHeaderMenu();
mygrid.enableSmartRendering(true);

*   mygrid = new dhtmlXGridObject('gridbox');

    //the path to images required by grid
    mygrid.setImagePath("/js/dhtmlx/grid/imgs/");
    mygrid.setHeader("Регион", "Статус", "Дата", "Место", "Время", "Заказчик", "Описание", "SMM", "Налог", "Вид оплаты", "Фотограф", "Вид оплаты", "Исполнитель", "Вид оплаты бух.", "Оплата/час", "Стоимость", "Сдача материала", "Скачать", "Скрытое примечание", "Прин. Корд", "Расп. Корд.", "Контр. Корд", "ФС %", "Реферал", "СС %", "Менеджер");//the headers of columns
    mygrid.setInitWidths("100,250,150,100,100,100,250,150,100,100,100,250,150,100,100,100,250,150,100,100,100,250,150,100,100,100");          //the widths of columns
    mygrid.setColAlign("right,left,left,left,left,right,left,left,left,left,right,left,left,left,left,right,left,left,left,left,right,left,left,left,left,left");       //the alignment of columns
    mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");                //the types of columns
    mygrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");          //the sorting types
    mygrid.init();      //finishes initialization and renders the grid on the page

data={
    rows:[
        { id:1, data: ["Ярославль (76)", "отмена заявки заранее", "01.10.2017", "Гэтсби", "Уточняется.Заявка предварительная", "Аня Разумова", "Заявка-предложение.Коняева в стопе", "", "0", "нал. на месте", "Не назначен", "нал. на месте", "", "0", "Надо отправить", "Не выложено, а надо", "", "", "", "Анна Тыртова", "", "", "", "", "", ""]},
    ]
};
mygrid.parse(data,"json");*/ //takes the name and format of the data source

// Правка баланса
function showBalanceEdit(region_id) {
    swal({
        title: "Изменение баланса",
        text: "ПИЗДЕЦ КАК ОСТОРОЖНО ДЕЛАТЬ!!!",
        type: "input",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Сохранить!",
        cancelButtonText: "Отмена",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        animation: "slide-from-top",
        inputPlaceholder: $('.info-box.balance .content .number span').text()
    }, function (inputValue) {
        if(inputValue !== false){
            $.ajax({
                type: "POST",
                url: '/region/balance-edit',
                data: {
                    balance: inputValue,
                    region_id: region_id
                },
                success: function (data) {
                    if(!data.error){
                        $('.info-box.balance .content .number span').text(inputValue);
                        if(inputValue < 0){
                            $('.info-box.balance .icon').removeClass('bg-teal').addClass('bg-red');
                        }else{
                            $('.info-box.balance .icon').addClass('bg-teal').removeClass('bg-red');
                        }
                        swal("Успех!", "Баланс изменён.", "success");
                    }else{
                        swal("Ошибка!", data.error, "warning");
                    }

                },
                error: function (data) {
                    if(data.status = 422){
                        var error_text = '';
                        $.each(data.responseJSON, function(i, e){
                            console.info(i);
                            console.info(e);
                            error_text +=  e + "\n";
                        });
                        swal("Ошибка!", error_text, "error");
                    }else{
                        swal("Ошибка!", data.status, "error");
                    }
                }
            });

        }
    });
}

$(function () {

// Приглашение главы региона
    $('#form_invite_boss').validate({
        rules: {
            'email': {
                required: true,
                email: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error').addClass('success');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

// Заполнение рекурентных платежей
    $('#form_fill_recurrent_payment').validate({
        rules: {
            'balance': {
                required: true,
                float: true
            },
            'reportage_rate': {
                required: true,
                float: true
            },
            'population': {
                required: true,
                number: true
            },
            'ths_rate': {
                required: true,
                float: true
            },
            'k1': {
                required: true,
                float: true
            },
            'k2': {
                required: true,
                float: true
            },
            'threshold': {
                required: true,
                float: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error').addClass('success');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    $("#fill_recurrent_payment_month_range").ionRangeSlider({
        type: "double",
        min: 1,
        max: 12,
        grid: true,
        hide_min_max: true,
        grid_num: 12,
        force_edges: true,
        prettify: function (num) {
            var m = moment(num, "MM").locale("ru");
            return m.format("MMMM");
        }
    });

// Рекуррентные платежи за текущий год
    $("#year_recurrent_payment .form-control.copy-fields + button.btn").click(function(){
        var month = $(this).data('month');
        var field = $(this).data('field');
        var value = $(this).parent().find('.form-control.copy-fields').val();

        swal({
            title: "Вы уверены?",
            text: "Данные из текущего месяца будет перенесены на все последующие!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, уверен!",
            cancelButtonText: "Отмена",
            closeOnConfirm: true
        }, function () {
                for(var i = month + 1; i < 13; i++){
                    $('#' + field + '_' + i).val(value);
                }
        });

    });

    $('#year_recurrent_payment').find('input[type="text"]').keyup(function(event){
        var month = $(this).parent().parent().data('month');
        var row = $('tr#row_month_' + month);

        var ths_rate = row.find('#ths_rate_' + month).val();
        var population = row.find('#population_' + month).val();
        var k1 = row.find('#k1_' + month).val();
        var k2 = row.find('#k2_' + month).val();
        var reportage_rate = row.find('#reportage_rate_' + month).val();
        var reportage_num = row.find('#reportage_num_' + month).val();

        if(/^-?\d+(\.\d+)?$/.test(ths_rate) && /^-?\d+(\.\d+)?$/.test(population) && /^-?\d+(\.\d+)?$/.test(k1) && /^-?\d+(\.\d+)?$/.test(k2) && /^-?\d+(\.\d+)?$/.test(reportage_rate) && /^-?\d+(\.\d+)?$/.test(reportage_num)){
             var calc = (ths_rate * population) * (k1 * k2) + (reportage_rate * reportage_num);
             row.find('.action-field span').html(calc);
             row.find('.action-field p').removeClass('hidden');
        }else{
            row.find('.action-field p').addClass('hidden');
        }


    });

    $('#year_recurrent_payment').find('button[type="submit"].per_month').click(function(event){

        var month = $(this).val();
        var data = $('#row_month_' + month + ' input');

        var validate_result = true;

        data.each(function(i, el){
            $this = $(this);
            var validate_type = $this.data('validate');
            var value = $this.val();
            var validate = true;

            if(validate_type == 'number' && !(/^-?\d+$/.test(value))){
                validate = false;
            }
            if(validate_type == 'float' && !(/^-?\d*(\.\d+)?$/.test(value))){
                validate = false;
            }

            if(validate){
                $this.removeClass('bg-red');
            }else{
                $this.addClass('bg-red');
                validate_result = false;
            }
        });

        if(validate_result){

        }else{
            swal("Ошибка валидации!", "Найдены незаполненные или неверные поля!", "error");
            event.preventDefault();
            return false;
        }

        return true;
    });

// Форма оплаты
    $('#make_payment').validate({
        rules: {
            'amount': {
                required: true,
                float: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error').addClass('success');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    $('#send_payment').click(function(){
        $('#make_payment').submit();
    });

    autosize($('#make_payment textarea#info'));

    Dropzone.options.myAwesomeDropzone = {
        paramName: 'file',
        maxFilesize: 2,
        method: 'post',
        uploadMultiple: false,
        maxFiles: true,
        acceptedFiles: 'image/*',
        dictInvalidFileType: 'Запрещено загружать файлы этого типа',
        dictFileTooBig: 'Максимальный разрешённый размер файла - {{maxFilesize}} Mb. А вы пытаетесь загрузить - {{filesize}} Mb',
        dictMaxFilesExceeded: 'Можно загрузить только 1 файл!',
        dictResponseError: 'Ошибка',
        dictRemoveFile: 'Удалить',
        //previewTemplate: '<div id="preview-template" style="display: none;"></div>',
        accept: function (file, done) {
            done();
        },
        init: function () {
            var dz = this;
            this.on('addedfile', function (file) {
                $.each(dz.files, function(i, _file){
                    if(file.name != _file.name || file.size != _file.size){
                        dz.removeFile(_file);
                    }
                });
                file.previewElement.addEventListener("click", function() {
                    dz.removeFile(file);
                    if(dz.files.length == 0){
                        $('#attached_file').val('');
                    }
                });

            });
            this.on('success', function (file, data) {
                $('#attached_file').val(data.file_name);
            });
        }
    };

    $('.lightgallery').lightGallery({
        thumbnail: true,
        selector: 'a'
    });

// Панель бухгалтера
    $("#booker_unapproved_transaction_table a.btn").click(function(event){
        var text = $(this).text();
        var url = $(this).attr('href');
        var transaction_id = $(this).data('transaction');
        var row = $(this).parent().parent();
        event.preventDefault();

        swal({
            title: "Подтверждение",
            text: "Вы уверены, что хотите " + text + " платёж?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, уверен!",
            cancelButtonText: "Отмена",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        transaction_id: transaction_id
                    },
                    success: function (data) {
                        if (!data.error) {
                            swal("Успех!", data.success, "success");
                            row.hide();
                            $('#booker_all_transaction_table .transaction_row_' + data.transaction_id + ' .status').html(data.status_icon_html);
                            $('.info-box.unapproved_transaction_count .number').html(data.unapproved_count);
                            $('.info-box.all_regions_debt .number').html(data.all_regions_debt);
                            $('.info-box.debt_regions_num .number').html(data.debt_regions_num);
                            $('.info-box.money_in_month .number').html(data.money_in_month);

                        } else {
                            swal("Ошибка!", data.error, "warning");
                        }

                    },
                    error: function (data) {
                        if (data.status = 422) {
                            var error_text = '';
                            $.each(data.responseJSON, function (i, e) {
                                error_text += e + "\n";
                            });
                            swal("Ошибка!", error_text, "error");
                        } else {
                            swal("Ошибка!", data.status, "error");
                        }
                    }
                });
        });

    });


//Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'DD.MM.YYYY HH:mm',
        clearButton: true,
        weekStart: 1
    });

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'DD.MM.YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: true,
        date: false
    });

// Экспорт в эксель транзакций

    $('#transactions_excel_date_end').bootstrapMaterialDatePicker({
        format : 'DD.MM.YYYY',
        weekStart: 1,
        time: false
    });
    $('#transactions_excel_date_start').bootstrapMaterialDatePicker({
        format : 'DD.MM.YYYY',
        weekStart: 1,
        time: false
    }).on('change', function (e, date) {
        $('#transactions_excel_date_end').bootstrapMaterialDatePicker('setMinDate', date);
    });

// Импорт из экселя репортажей

    Dropzone.options.excelImportReportDropzone = {
        paramName: 'file',
        maxFilesize: 2,
        method: 'post',
        uploadMultiple: false,
        maxFiles: true,
        acceptedFiles: '.xls,.xlsx',
        dictInvalidFileType: 'Запрещено загружать файлы этого типа',
        dictFileTooBig: 'Максимальный разрешённый размер файла - {{maxFilesize}} Mb. А вы пытаетесь загрузить - {{filesize}} Mb',
        dictMaxFilesExceeded: 'Можно загрузить только 1 файл!',
        dictResponseError: 'Ошибка',
        dictRemoveFile: 'Удалить',
        //previewTemplate: '<div id="preview-template" style="display: none;"></div>',
        accept: function (file, done) {
            done();
        },
        init: function () {
            var dz = this;
            this.on('addedfile', function (file) {
                $.each(dz.files, function(i, _file){
                    if(file.name != _file.name || file.size != _file.size){
                        dz.removeFile(_file);
                    }
                });
                file.previewElement.addEventListener("click", function() {
                    dz.removeFile(file);
                    if(dz.files.length == 0){
                        $('#attached_file').val('');
                    }
                });

            });
            this.on('success', function (file, data) {
                $.ajax({
                  url: '/import/reports/save',
                  type: "POST",
                  data: {
                      month: $('#excel-import-report-month').val(),
                      file_name: data.file_name

                  },
                  success: function(data){
                    var type = 'success';
                    var text = 'Сохранение данных произошло без ошибок';
                    var confirmButtonColor ='#86CCEB';
                    var  showCancelButton = false;
                    var confirmButtonText = 'OK';
                    var closeOnConfirm = true;
                    var confirm_function = false;
                    if(data.error){
                        type = "warning";
                        confirmButtonColor ='#DD6B55';
                        confirmButtonText = 'Обработать игнорируя ошибки!';
                        text = "При обработке обнаружены ошибки:<br><span class='font-12 col-red'>" + data.error + "</span>";
                        showCancelButton = true;
                        closeOnConfirm = false;
                        confirm_function = function () {
                            $.ajax({
                                url: '/import/reports/save?approve_error=1',
                                type: "POST",
                                data: {
                                    month: $('#excel-import-report-month').val(),
                                    file_name: data.file_name

                                },
                                success: function (data) {
                                    swal.enableButtons();
                                    swal({
                                        title: "Операция завершена!",
                                        text: "<div>" + data.log + "</div>",
                                        type: "success",
                                        html: true,
                                    });
                                }
                            });
                        }
                    }
                    swal({
                        title: "Файл успешно загружен",
                        text: text,
                        type: type,
                        showCancelButton: showCancelButton,
                        confirmButtonColor: confirmButtonColor,
                        confirmButtonText: confirmButtonText,
                        cancelButtonText: "Отмена",
                        closeOnConfirm: closeOnConfirm,
                        showLoaderOnConfirm: true,
                        html: true,
                    }, confirm_function);
                  }
                });




            });
        }
    };

// Новостная рассылка
    $('#newsletter_regions').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<h4>Не отправлять</h4><input type='text' class='search-input' autocomplete='off' placeholder=' фильтр'>",
        selectionHeader: "<h4>Отправлять</h4><input type='text' class='search-input' autocomplete='off' placeholder=' фильтр'>",
        afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on('keydown', function(e){
          if (e.which === 40){
            that.$selectableUl.focus();
            return false;
          }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on('keydown', function(e){
          if (e.which == 40){
            that.$selectionUl.focus();
            return false;
          }
        });
        },
        afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
        },
        afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
        }

    });

    $('#region_filter_on').on('change', function(e){
        if($(this).prop('checked')){
            $('#newsletter_regions_aria').removeClass('hidden')
        }else{
            $('#newsletter_regions_aria').addClass('hidden')
        }
    });


//TinyMCE
tinymce.init({
    selector: "textarea.tinymce",
    language_url : '/plugins/tinymce/langs/ru.js',
    menubar: 'edit view format table tools',
    theme: "modern",
    height: 300,
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'preview | forecolor backcolor',
    image_advtab: true
});
tinymce.suffix = ".min";
tinyMCE.baseURL = '/plugins/tinymce';

// Настройки валидатора

    $.validator.addMethod('float',
        function (val, el, args) {
            if (val.match(/^\d*(\.\d+)?$/)) {
                return true;
            }
            else {
                return false;
            }

        },
        "Нужно указанть целое или дробное число! Например 2.2");

    $.extend( $.validator.messages, {
        required: "Это поле необходимо заполнить.",
        remote: "Пожалуйста, введите правильное значение.",
        email: "Пожалуйста, введите корректный адрес электронной почты.",
        url: "Пожалуйста, введите корректный URL.",
        date: "Пожалуйста, введите корректную дату.",
        dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
        number: "Пожалуйста, введите число.",
        digits: "Пожалуйста, вводите только цифры.",
        creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
        equalTo: "Пожалуйста, введите такое же значение ещё раз.",
        extension: "Пожалуйста, выберите файл с правильным расширением.",
        maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
        minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
        rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
        range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
        max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
        min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
    } );

   $('.user_recharge_money').on('click', function (e) {
        var $form = $('#user_recharge_money_form');
        recharge_money_wizard($form);
    });

    function recharge_money_wizard(form) {
        swal.setDefaults({
            input: 'text',
            confirmButtonText: 'Далее &rarr;',
            cancelButtonText: 'Отмена',
            showCancelButton: true,
            progressSteps: ['1', '2']
        });

        var steps = [
            {
                title: 'Пополнение счёта',
                text: 'Какую сумму внести на счёт?'
            }/*,
            {
                title: 'Пополнение счёта',
                text: 'При желании можете оставить комментарий',
                input: 'textarea'
            }*/
        ];

        swal.queue(steps).then(function (result) {
            swal.resetDefaults();
            if (result.value) {
                var amount = parseFloat(result.value[0]);
                if (!amount || amount < 0) {
                    swal({
                        title: 'Неверная сумма пополнения!',
                        type: 'error',
                        confirmButtonText: 'OK!'
                    });
                } else {
                    form.find('[name="amount"]').val(amount);
                    //form.find('[name="comment"]').val(result.value[1]);
                    form.find('[name="comment"]').val('');
                    form.submit();
                }
            }
        })
    }

   $('.user_take_money').on('click', function (e) {
        var $form = $('#user_take_money_form');
        take_money_wizard($form);
    });

    function take_money_wizard(form) {
        swal.setDefaults({
            input: 'text',
            confirmButtonText: 'Далее &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2']
        });

        var steps = [
            {
                title: 'Вывод денег',
                text: 'Какую сумму вывести со счёта?'
            },
            {
                title: 'Вывод денег',
                text: 'При желании можете оставить комментарий',
                input: 'textarea'
            }
        ];

        swal.queue(steps).then(function (result) {
            swal.resetDefaults();
            if (result.value) {
                var amount = parseFloat(result.value[0]);
                if (!amount || amount < 0) {
                    swal({
                        title: 'Неверная сумма пополнения!',
                        type: 'error',
                        confirmButtonText: 'OK!'
                    });
                } else {
                    form.find('[name="amount"]').val(amount);
                    form.find('[name="comment"]').val(result.value[1]);
                    form.submit();
                }
            }
        })
    }

});


function showPromptMessage(msg, id, type) {
    swal({
        title: "",
        text: msg,
        input: "url",
        showCancelButton: true,
        //closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "укажите ссылку",
        cancelButtonText: "Отмена",

    }).then( function (link) {
        if (link.value == undefined) return false;
        if (link.value  === "") {
            swal.showInputError("Вы не указали ссылку"); return false
        }
        document.location.href = "/worker/add_link/?project_id="+ encodeURIComponent(id) +"&type_link="+ encodeURIComponent(type) +"&link=" + encodeURIComponent(link.value);
    });
}