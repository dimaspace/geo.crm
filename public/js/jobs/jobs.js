$(function () {



DevExpress.localization.locale(navigator.language || navigator.browserLanguage);

DevExpress.config({ defaultCurrency: 'RUB' });

DevExpress.ui.dxDataGrid.defaultOptions({
    options: {
        rowAlternationEnabled: true,
        showBorders: false
    }
});

var store = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/jobs/failed/json");
    }
});

$(function(){

var dataGrid = $("#gridContainer").dxDataGrid({
        dataSource: {
            store:store
        },
        stateStoring: {
            enabled: true
        },
        showColumnLines: true,
        showRowLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        sorting: {
            mode: "multiple"
        },
        selection: {
            mode: "multiple"
        },
        export: {
            enabled: true,
            fileName: "fail",
            allowExportSelectedData: true
        },
        grouping: {
            autoExpandAll: false,
            contextMenuEnabled: true
        },
        columnChooser: {
            enabled: true,
            mode:  'select'
        },
        scrolling: {
            mode: "virtual"
        },
        groupPanel: {
            visible: true
        },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        masterDetail: {
            enabled: true,
            template: function (container, options) {
                var currentEmployeeData = options.data;

                $("<div>").addClass('p-b-10')
                    .append(
                        $("<b>").text('Получатель: ')
                    )
                    .append(
                        $("<a>")
                            .attr("target", "_new")
                            .attr("href", "https://vk.com/id" + currentEmployeeData.vk_user_id)
                            .text(currentEmployeeData.vk_user_name)
                    )
                    .append(
                        $("<a>")
                            .attr("target", "_new")
                            .attr("href", "https://vk.com/im?sel=" + currentEmployeeData.vk_user_id)
                            .html('<i class="material-icons col-teal">message</i>')
                            .addClass('m-l-5')
                    )
                    .appendTo(container);

                $("<div>")
                    .dxTabPanel({
                        itemTemplate:  function (itemData, itemIndex, element) {
                            element.append(
                                $("<textarea rows='20'>").addClass("pd-15 form-control").html(itemData.text)
                            )
                        },
                        items: [{
                            title: 'Сообщение',
                            text: currentEmployeeData.vk_msg
                        }, {
                            title: 'Стек Ошибки',
                            text: currentEmployeeData.exception
                        }
                        ],
                        selectedIndex: 0,
                        loop: false,
                        animationEnabled: true,
                        swipeEnabled: true,
                        onSelectionChanged: function (e) {
                            $(".selected-index")
                                .text(e.component.option("selectedIndex") + 1);
                        }
                }).appendTo(container);

            }
        },
        summary: {
            groupItems: [{
                summaryType: "count",
                displayFormat: "Задач: {0}"
            }]
        },
        columns: [
             {
                caption: "ID",
                dataField: "id",
                allowGrouping: false,
                width: 70
            },  {
                caption: "Дата",
                dataField: "failed_day",
                allowGrouping: true,
                groupIndex: 0
            },  {
                caption: "Дата провала",
                dataField: "failed_at",
                allowGrouping: true,
                dataType: 'datetime',
                format: 'shortDateShortTime'
            }, {
                caption: "connection",
                dataField: "connection",
                allowGrouping: true,
            }, {
                caption: "queue",
                dataField: "queue",
                allowGrouping: true,
            },  {
                caption: "Комманда",
                dataField: "p_data_command",
                allowGrouping: true,
            },  {
                caption: "exception",
                dataField: "exception",
                allowGrouping: true,
            },  {
                caption: "Попыток",
                dataField: "p_attempts",
                allowGrouping: true,
            },  {
                caption: "VK ID",
                dataField: "vk_user_id",
                allowGrouping: true,
            },  {
                caption: "VK ИМЯ",
                dataField: "vk_user_name",
                allowGrouping: true,
            },  {
                caption: "VK Сообщение",
                dataField: "vk_msg",
                allowGrouping: true,
            }, {
                alignment: 'center',
                cellTemplate: function(element, info) {
                     element.append(
                         $("<button title='Отменить'><i class='material-icons'>clear</i></button>")
                             .addClass("btn bg-red m-r-5")
                             .data('loading-text', "<i class='material-icons'>access_time</i>")
                            .click(function(){
                                var $button = $(this);
                                $.ajax('/jobs/failed/delete/' + info.data.id,
                                    {
                                        beforeSend: function(){
                                            $button.button('loading');

                                        },
                                        success: function(data){
                                            $button.button('reset');
                                            dataGrid.refresh();
                                            swal("Ура!", data.success, "success");
                                        }
                                    });
                            })
                         ).append(
                         $("<button title='Повторить'><i class='material-icons'>refresh</i></button>")
                             .addClass("btn bg-teal")
                            .data('loading-text', "<i class='material-icons'>access_time</i>")
                            .click(function(){
                                var $button = $(this);
                                $.ajax('/jobs/failed/retry/' + info.data.id,
                                    {
                                        beforeSend: function(){
                                            $button.button('loading');

                                        },
                                        success: function(data){
                                            $button.button('reset');
                                            dataGrid.refresh();
                                            swal("", data.success, "info");
                                        }
                                    });
                            })
                         );
                }
            }
        ]
    }).dxDataGrid("instance");

    $("#autoExpand").dxCheckBox({
        value: false,
        text: "Раскрыть все группы",
        onValueChanged: function(data) {
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
    });

});








});