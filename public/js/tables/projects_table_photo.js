$(function () {




$.when(
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-generic.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-gregorian.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/numbers.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/currencies.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/dateFields.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/likelySubtags.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/timeData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/weekData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/currencyData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/calendarData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/numberingSystems.json")
).then(function () {
    //The following code converts the got results into an array
    return [].slice.apply( arguments, [0] ).map(function( result ) {
        return result[ 0 ];
    });
}).then(
    Globalize.load //loads data held in each array item to Globalize
).then(function(){

DevExpress.config({ defaultCurrency: 'RUB' });
Globalize.locale(navigator.language || navigator.browserLanguage);



var formatterRUB = Globalize.currencyFormatter( "RUB" );




var store = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/stats/projects/json?self_photo=true");
    }
});

var store_lookup_statuses = new DevExpress.data.CustomStore({
    key: "id",
    loadMode: "raw",
    load: function() {
        return $.getJSON("/tables/lookups/statuses.json");
    }
});

$(function(){


    var projectsTablePhoto = $("#projectsTablePhoto").dxDataGrid({
        rowAlternationEnabled: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        columnAutoWidth: true,
        paging: {
            pageSize: 20,
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            showNavigationButtons: true
        },
        searchPanel: {
            visible: true,
            highlightCaseSensitive: true
        },
        groupPanel: {
            visible: true
        },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true,
            allowSearch: true
        },
        summary: {
            groupItems: [{
                summaryType: "count",
                displayFormat: "Проектов: {0}"
            }]
        },
        stateStoring: {
            enabled: true
        },
        columnFixing: {
            enabled: true
        },
        columns: [
            {
                dataField: "id",
                caption: "#ID",
            },{
                dataField: "status_id",
                caption: "Статус",
                lookup: {
                    dataSource: {
                        store: store_lookup_statuses,
                    },
                    valueExpr: 'id',
                    displayExpr: 'title',
                },
                editorOptions: {
                    searchEnabled: true,
                    searchExpr: ["id", "title"],
                    searchPlaceholder: 'Искать'
                }
            },{
                dataField: "date_time",
                caption: "Дата/Время",
                dataType: "datetime"
            },{
                dataField: "region",
                caption: "Регион",
            },{
                dataField: "place",
                caption: "Место",
            },{
                dataField: "amount_minus_tax",
                caption: "Цена проекта",
                dataType: "number",
                format: function (value) {
                    return formatterRUB(value);
                },
            },{
                dataField: "amount_photo",
                caption: "Г-р Фотографа",
                dataType: "number",
                format: function (value) {
                    return formatterRUB(value);
                },
            },{
                dataField: "rf",
                caption: "РФ",
                dataType: "number",
                format: function (value) {
                    return formatterRUB(value);
                },
            },{
                caption: "ФС(Реферал)",
                columns: [
                    {
                        caption: "Свой ФС?",
                        dataField: "is_referral_hs",
                        dataType: "string",
                    }, {
                        caption: "ФС съёмка?",
                        dataField: "is_referral",
                        dataType: "string",
                    }, {
                        caption: "Г-р ФС",
                        dataField: "amount_referral",
                        dataType: "number",
                        format: function (value) {
                            return formatterRUB(value);
                        },
                        area: "data",
                        areaIndex: 90
                    }
                ]

            },{
                caption: "СС",
                columns: [
                    {
                        caption: "CС съёмка?",
                        dataField: "is_ss",
                        dataType: "string",
                    }, {
                        caption: "CC",
                        dataField: "ss_name",
                    }, {
                        dataField: "amount_ss",
                        dataType: "number",
                        caption: "Г-р CC",
                        format: function (value) {
                            return formatterRUB(value);
                        },
                    }
                ]

            },{
                dataField: "pay_w",
                caption: "Опл",
                allowGrouping: true
            }
        ],
        summary: {
            totalItems: [{
                column: "id",
                summaryType: "count",
                showInColumn: "date_time"
            }, {
                column: "amount_minus_tax",
                summaryType: "sum",
                customizeText: function (data) {
                    return formatterRUB(data.value);
                },
            }, {
                column: "amount_photo",
                summaryType: "sum",
                customizeText: function (data) {
                    return formatterRUB(data.value);
                },
            }, {
                column: "rf",
                summaryType: "sum",
                customizeText: function (data) {
                    return formatterRUB(data.value);
                },
            }, {
                column: "amount_referral",
                summaryType: "sum",
                customizeText: function (data) {
                    return formatterRUB(data.value);
                },
            }, {
                column: "amount_ss",
                summaryType: "sum",
                customizeText: function (data) {
                    return formatterRUB(data.value);
                },
            }]
        },
        dataSource: {
            store: store
        }
    }).dxDataGrid("instance");


});


})





});