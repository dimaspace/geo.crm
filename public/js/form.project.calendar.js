$(function() {

    DevExpress.config({
        defaultCurrency: "RUB",
        forceIsoDateParsing: true,
    });

    //***
    // Режим календаря в списке проектов
    //***

    // Датасурс проектов
    var storeCalendarProjects = new DevExpress.data.CustomStore({
        loadMode: "raw",
        load: function() {
            $('#projects_filter_form #projects_list_mode').val('calendar');

            var query = "/projects/filtered/json?";
            var filter_query = $('#projects_filter_form').serialize();
            query = query + filter_query;

            return $.getJSON(query);
        },
        insert: function(){

        }

    });
    window.dataSourceCalendarProjects = new DevExpress.data.DataSource({
        store: storeCalendarProjects,
        //filter: ["startDateTimeZone", window.cfg_time_zone]
        pageSize: 10000,
        paginate: false
    });

    // Датасурс исполнителей
    var storeCalendarWorkers = new DevExpress.data.CustomStore({
        loadMode: "raw",
        load: function() {
            var query = "/projects/workers/json?";
            var filter_query = $('#projects_filter_form').serialize();
            query = query + filter_query;

            return $.getJSON(query);
        }
    });
    var dataSourceCalendarWorkers = new DevExpress.data.DataSource({
        store: storeCalendarWorkers,
    });

    // Датасурс регионов
    var storeCalendarRegions = new DevExpress.data.CustomStore({
        loadMode: "raw",
        load: function() {
            return $.getJSON("/regions/json");
        }
    });
    var dataSourceCalendarRegions = new DevExpress.data.DataSource({
        store: storeCalendarRegions,
    });

    var filter_start = $('#projects_filter_form input[name="projects_filter[start]"]').val();
    var filter_end = $('#projects_filter_form input[name="projects_filter[end]"]').val();

    //alert(filter_start);
    var moment_filter_start = moment(filter_start + ' ' + window.cfg_time_zone_momentjs , "YYYY-MM-DD ZZ");
    var moment_filter_end = moment(filter_end, "YYYY-MM-DD").add(1, 'd').add(6, 'h');
    //alert(moment_filter_start);
    var duration_filter = moment.duration(moment_filter_end.diff(moment_filter_start));

    var calendarIntervalCount = duration_filter.asDays();
    //alert(calendarIntervalCount);
    var user_calendar_row_index_data = [];
    window.projects_calendar = $("#projects_calendar").dxScheduler({
        //dataSource: "/projects/filtered/json", //dataSourceCalendarProjects,
        dataSource: dataSourceCalendarProjects,
        views: [
            { name: "1 час", type: "timelineDay",  cellDuration: "60",
                intervalCount: calendarIntervalCount,
                startDate: moment_filter_start._d,
                timeCellTemplate: function(itemData){
                    moment_date = moment(itemData.date);
                    return moment_date.format("D, HH:mm");
                },
                maxAppointmentsPerCell: 2
            },

            { name: "3 часа", type: "timelineDay", cellDuration: "180",
                intervalCount: calendarIntervalCount,
                startDate: moment_filter_start._d,
                timeCellTemplate: function(itemData){
                    moment_date = moment(itemData.date);
                    return moment_date.format("D, HH:mm");
                },
                maxAppointmentsPerCell: 2,
            },
            { name: "6 часов", type: "timelineDay",  cellDuration: "360",
                intervalCount: calendarIntervalCount,
                startDate: moment_filter_start._d,
                timeCellTemplate: function(itemData){
                    moment_date = moment(itemData.date);
                    return moment_date.format("D, HH:mm");
                },
                maxAppointmentsPerCell: 2
            },

        ],
        editing: {
            allowAdding: false,
            allowUpdating: false,
            allowDeleting: false,
            allowDragging: false
        },
        currentView: "3 часа",
        currentDate: moment_filter_start,
        /*height: function () {
            return 32 * 25;
        },*/
        timeZone: window.cfg_time_zone,
        cellDuration: 60,
        firstDayOfWeek: 1,
        groups: ["worker_id"],
        resources: [
            {
                fieldExpr: "region_code",
                displayExpr: "title",
                //allowMultiple: true,
                valueExpr: 'code',
                label: 'reg',
               // groups: ["worker_id"],
                //valueExpr: "worker_id",
                dataSource: "/regions/json" //dataSourceCalendarRegions
            },            {
                fieldExpr: "worker_id",
                //groups: ["worker_id"],
                //valueExpr: "worker_id",
                dataSource: dataSourceCalendarWorkers,
                valueExpr: 'id',
                displayExpr: "name",
            }
        ],
        resourceCellTemplate: function (cellData, index, element) {
            //console.info(cellData);
            user_calendar_row_index_data[cellData.id] = index;
            var is_staff = $("<span>").addClass('staff');
            if(cellData.data.is_staff == true){
                is_staff.append($('<i class="material-icons col-teal">check_circle</i>'));
            }

            var $name = $("<span>").addClass('name').text(cellData.text);
            if(cellData.data.is_debt == true){
                $name.addClass('col-red');
            }

            //console.log(element);

            var balance = cellData.data.balance;
            var threshold = cellData.data.threshold;
            var region = cellData.data.primary_region;

            var $balance = $("<span class='balance'></span>").text(balance);
            var $region = $("<span class='region'></span>").text(region).addClass('region_' + region);

            return $("<span class='group-header-users'></span>").append([is_staff, $region, $name, $balance]);
        },
        dropDownAppointmentTemplate: function (data, index, element) {


            var markup = $("<div class='appointment-content'>" +
                           "<div class='appointment-text'>" +
                            data.text +
                        "</div>" +

                        "</div>");


            return markup;
        },
        /*appointmentTooltipTemplate: function (data, element) {
            element.append("<i>" + data.text + "(" + data.year + ")</i>");
            //element.append("<p><img style='height: 80px' src='" + data.img + "' /></p>");
        },*/
        /*elementAttr: {
            startDate: "date_start_real",
            endDate: "date_end"
            //class: "class-name"
        },*/
        appointmentTooltipTemplate: function (data, container) {
            setTimeout(function(){
                container.parent().css('zIndex', 5);
                container.parent().parent().css('zIndex', 4);
            }, 3000);

            if(data.is_busy === false) {

                var desire = '<div class="desire">';

                if(data.desire !== null){
                    data.desire.forEach(function(desire_data){
                        var user_name = getWorkerNameById(desire_data.user_id);
                        desire_data.rating;

                        desire += user_name + ' <span class="desire_'+ desire_data.rating +'"></span><br>';
                    });
                }
                desire += '</div>';

                var moment_start = moment(data.startDate);
                var moment_end = moment(data.endDate);
                var markup = $("<div class='project-calendar-tooltip'>" +
                    "<div>" +
                    //'ID: ' + data.id + "<br>" +
                    'Регион: ' + project_regions[data.region_code].title + "<br>" +
                    'Тип: ' + project_categories[data.category_id] + "<br>" +
                    'Время: ' + moment_start.format('DD.MM.YYYY') + ' ' + moment_start.format('HH:mm') + '-' + moment_end.format('HH:mm') + "<br>" +
                    'Место: ' + data.place_cached + "&nbsp;" +
                    'Клиент: ' + data.client_cached + "<br>" +
                    'Бюджет: ' + data.amount + "<br>" +
                    'Опл. ф.:' + payment_types_w[data.payment_type_id_w] + "&nbsp;" +
                    'Опл. б.: ' + payment_types_b[data.payment_type_id_b] + "<br>" +
                    (data.desc_hidden ? 'Скр. опс.: ' + data.desc_hidden + "<br>" : '') +
                    (data.desc_public ? data.desc_public : '[нет]') + "<br>" +
                    desire +
                    "<br></div>" +
                    "<div class='vk_send ' data-project-id='" + data.id + "'></div>&nbsp;" +
                    "<div class='edit'></div>" +
                    "</div>");
                markup.find(".edit").dxButton({
                    text: "Править",
                    type: "default",
                    onClick: function () {
                        $('#add_project').modal('show');
                        window.edit_project_form(data.id);
                    }
                });
                if(data.worker_id > 0 && (data.vk_msg_id === 0) || data.vk_msg_id === null){
                    markup.find(".vk_send").dxButton({
                        icon: "fa fa-vk",
                        hint: 'Отправить информация через вконтакт',
                        type: "default",
                        onClick: vkSend
                    });
                }
            }else{
                var markup = $('<div>').text(data.text);
            }
            return markup;
        },

        appointmentTemplate: function(data, index, container) {
            var row_height  = $('.dx-scheduler-group-row:first').height();
            var parent_container = container.parent();
            if(data.is_busy === false){
                container.addClass('region_bg_' + data.region_code);
                if(data.vk_msg_id === 0 && data.worker_id > 0){
                    container.addClass('vk_not_send');
                }
                parent_container.addClass('dx-scheduler-appointment-project');

                var pay_type_w = data.payment_type_id_w == 1 ? 'бн' : 'н';
                var pay_type_b = data.payment_type_id_b > 0 ? 'бн' : 'н';

                return $("<div class='project'>" +

                    "<div>" + data.place_cached + "</div>" +
                    "<div>" + (data.amount ? data.amount : 'х/з') + " р. " + pay_type_w + '(' + pay_type_b + ")</div>" +

                    "</div>");
            }else{
                parent_container.addClass('dx-scheduler-appointment-busy').addClass('busy_' + data.busy);
                parent_container.hide();
                setTimeout(function(){
                    parent_container.css('height', row_height);
                    parent_container.show();
                    var matrix = parent_container.css('transform').match(/matrix\((\d+, ?\d+, ?\d+, ?\d+,  ?[0-9.]+, )?\d+/)[1]
                    var row_index = user_calendar_row_index_data[data.worker_id];
                    parent_container.css('transform', 'matrix('+matrix + row_index * row_height+')');
                }, 2000);

                return $("<div class='busy'>" +

                    "<div>" + data.text + "</div>" +

                    "</div>");
            }
        },

        onAppointmentDblClick: function(e) {
            if(e.appointmentData.is_busy === false) {
                e.cancel = true;
                $('#add_project').modal('show');
                window.edit_project_form(e.appointmentData.id);
            }
        },
        /*onAppointmentClick: function(e) {
            if(e.appointmentData.is_busy === false) {
                e.cancel = true;
                $('#add_project').modal('show');
                window.edit_project_form(e.appointmentData.id);
            }
        },*/
        //startDateExpr: "date_start_real2",
        //endDateExpr: "date_end",
        //textExpr: 'id',
        //dateSerializationFormat: 'yyyy-MM-dd HH:mm:ss',
        //startDateTimeZoneExpr: 'startDateTimeZone',
        //endDateTimeZoneExpr: 'endDateTimeZone'
        crossScrollingEnabled: true,
        //editing: false,
        useDropDownViewSwitcher: true

    }).dxScheduler("instance");

    $("#calendar-options-layout").dxSelectBox({
        items: ["15", "30", "60", "180", "720", "1440"],
        width: 180,
        value: "15",
        onValueChanged: function(data) {
            alert(data.value);
            window.projects_calendar.option("cellDuration", data.value);
            //window.projects_calendar.repaint();
        }
    });

    function getWorkerNameById(id) {
        var user_name = '??';
        dataSourceCalendarWorkers.items().forEach(function(user){
            if(user.id === id){
                user_name = user.name;
            }
        });
        return user_name;
    }

});