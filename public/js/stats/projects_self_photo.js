$(function () {

$.when(
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-generic.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-gregorian.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/numbers.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/currencies.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/dateFields.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/likelySubtags.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/timeData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/weekData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/currencyData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/calendarData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/numberingSystems.json")
).then(function () {
    //The following code converts the got results into an array
    return [].slice.apply( arguments, [0] ).map(function( result ) {
        return result[ 0 ];
    });
}).then(
    Globalize.load //loads data held in each array item to Globalize
).then(function(){

DevExpress.config({ defaultCurrency: 'RUB' });
Globalize.locale(navigator.language || navigator.browserLanguage);



var formatterRUB = Globalize.currencyFormatter( "RUB" );




var store = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/stats/projects/json?self_photo=true");
    }
});

$(function(){

    var drillDownDataSource = {};

    var pivotGridChart = $("#projectsChart").dxChart({
        commonSeriesSettings: {
            type: "spline"
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (args) {
                var valueText = (args.seriesName.indexOf("Total") != -1) ?
                        Globalize.formatCurrency(args.originalValue,
                            "RUB", { maximumFractionDigits: 0 }) :
                        args.originalValue;

                return {
                    html: args.originalArgument + ": " + args.seriesName + "<div class='currency'>"
                        + valueText + "</div>"
                };
            }
        },
        size: {
            height: 320
        },
        adaptiveLayout: {
            width: 450
        },
        onLegendClick: function (e) {
            var series = e.target;
            if (series.isVisible()) {
                series.hide();
            } else {
                series.show();
            }
        }
    }).dxChart("instance");

    /*$("#projectsChartrangeSelector").dxRangeSelector({
        size: {
            height: 120
        },
        margin: {
            left: 10
        },
        scale: {
            minorTickCount:1
        },
        dataSource: store,
        chart: {
            series: series,
            palette: "Harmony Light"
        },
        behavior: {
            callValueChanged: "onMoving"
        },
        onValueChanged: function (e) {
            var zoomedChart = $("#projectsChart").dxChart("instance");
            zoomedChart.zoomArgument(e.value[0], e.value[1]);
        }
    });*/

    var pivotGridInstance = $("#projectsTable").dxPivotGrid({

        dataFieldArea: 'row',
        allowSortingBySummary: true,
        allowSorting: true,
        allowFiltering: true,
        allowExpandAll: true,
        //height: 440,
        showBorders: true,
        export: {
            enabled: true,
            fileName: 'projects'
        },
        fieldChooser: {
            enabled: true,
            allowSearch: true,
            layout: 1,
            width: 800
        },
        fieldPanel: {
            visible: true
        },
        /*scrolling: {
            mode: "virtual"
        },*/
        headerFilter: {
            allowSearch: true
        },
        stateStoring: {
            enabled: true
        },
        dataSource: {
            fields: [{
                dataField: "id",
                visible: false
            },{
                dataField: "worker",
                visible: false
            },{
                dataField: "pay_bb",
                visible: false
            },{
                dataField: "date_time",
                visible: false
            },{
                caption: "Регион",
                width: 120,
                dataField: "region",
                area: "row"
            }, {
                caption: "Опл. Б.",
                width: 120,
                dataField: "pay_b",
                visible: false
                //area: "filter"
            }, {
                caption: "Опл",
                width: 120,
                dataField: "pay_w",
                //area: "filter"
            }, {
                caption: "РФ",
                dataField: "rf",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                displayFolder: 'РФ',
                areaIndex: 10
            }, {
                caption: "РФ ($ ФС)",
                dataField: "rf_referral",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                displayFolder: 'РФ',
                areaIndex: 20
            }, {
                caption: "РФ ($ GEO)",
                dataField: "rf_geo",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                displayFolder: 'РФ',
                areaIndex: 15
            }, {
                caption: "Гонорар Ф",
                dataField: "amount_photo",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                areaIndex: 0
            }, {
                caption: "Гонорар К",
                dataField: "amount_cords",
                visible: false
            }, {
                caption: "Заработано",
                dataField: "amount_office",
                visible: false
            }, {
                caption: "Цена проекта",
                dataField: "amount_minus_tax",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                areaIndex: 100
            }, {
                dataField: "amount",
                visible: false
            }, {
                caption: "Место",
                dataField: "place",
                dataType: "string",
                summaryType: "sum",
                area: "filter"
            }, {
                caption: "Клиент",
                dataField: "client",
                dataType: "string",
                summaryType: "sum",
            }, {
                caption: "Г-р CC",
                dataField: "amount_ss",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                //area: "filter",

            }, {
                caption: "CC",
                dataField: "ss_name",
                //area: "filter",
            }, {
                caption: "Г-р ФС",
                dataField: "amount_referral",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                areaIndex: 90
            }, {
                caption: "Съёмок",
                dataField: "project_num",
                dataType: "number",
                summaryType: "sum",
                area: "data",
                areaIndex: 2
            }, {
                caption: "От GEO(шт)",
                dataField: "project_num_geo",
                dataType: "number",
                summaryType: "sum",
                area: "data",
                areaIndex: 30
            }, {
                caption: "Свои(шт)",
                dataField: "project_num_hs",
                dataType: "number",
                summaryType: "sum",
                area: "data",
                areaIndex: 35
            }, {
                caption: "От GEO($)",
                dataField: "amount_geo",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                areaIndex: 40
            }, {
                caption: "Свои($)",
                dataField: "amount_hs",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                areaIndex: 45
            }, {
                caption: "Своя съёмка?",
                dataField: "is_referral_hs",
                dataType: "string",
                displayFolder: 'Признак'
            }, {
                caption: "ФС съёмка?",
                dataField: "is_referral",
                dataType: "string",
                displayFolder: 'Признак'
            }, {
                caption: "CС съёмка?",
                dataField: "is_ss",
                dataType: "string",
                displayFolder: 'Признак'
            }, {
                groupName: "date",
                caption: "Месяц",
                groupInterval: "month",
                visible: true
            }, {
                groupName: "date",
                caption: "Квартал",
                groupInterval: "quarter",
                visible: false
            }, {
                groupName: "date",
                caption: "Год",
                groupInterval: "year",
                expanded: true
            }, {
                dataField: "date",
                dataType: "date",
                caption: "Дата",
                area: "column"
            }, {
                caption: "Выбор месяцев",
                area: 'filter',
                dataField: 'date',
                groupInterval: 'month',
                dataType: 'date',
                filterType: 'include',
            }],
            store: store
        },
        onCellClick: function(e) {
            if(e.area == "data") {
                var pivotGridDataSource = e.component.getDataSource(),
                    rowPathLength = e.cell.rowPath.length,
                    rowPathName = e.cell.rowPath[rowPathLength - 1],
                    popupTitle = (rowPathName ? rowPathName : "Итого") + " Детализация";

                drillDownDataSource = pivotGridDataSource.createDrillDownDataSource(e.cell);
                projectsPopup.option("title", popupTitle);
                projectsPopup.show();
            }
        }
    }).dxPivotGrid("instance");



    var projectsPopup = $("#projectsPopup").dxPopup({
        //width: "auto",
        //height: "auto",
        dragEnabled: true,
        resizeEnabled: true,

        contentTemplate: function(contentElement) {
            $("<div />")
                .addClass("drill-down")
                .dxDataGrid({
                    rowAlternationEnabled: true,
                    paging: {
                        pageSize: 5,
                    },
                    groupPanel: {
                        visible: true
                    },
                    filterRow: {
                        visible: true,
                        applyFilter: "auto"
                    },
                    headerFilter: { visible: true },
                    summary: {
                        groupItems: [{
                            summaryType: "count",
                            displayFormat: "Проектов: {0}"
                        }]
                    },
                    columns: [
                        {
                            dataField: "id",
                            caption: "#ID",
                        },                        {
                            dataField: "date_time",
                            caption: "Дата/Время",
                        },{
                            dataField: "region",
                            caption: "Регион",
                        },{
                            dataField: "place",
                            caption: "Место",
                        },{
                            dataField: "amount_minus_tax",
                            caption: "Цена проекта",
                            dataType: "number",
                            format: function (value) {
                                return formatterRUB(value);
                            },
                        },{
                            dataField: "amount_photo",
                            caption: "Г-р Фотографа",
                            dataType: "number",
                            format: function (value) {
                                return formatterRUB(value);
                            },
                        },{
                            dataField: "rf",
                            caption: "РФ",
                            dataType: "number",
                            format: function (value) {
                                return formatterRUB(value);
                            },
                        }, {
                            caption: "Свой ФС?",
                            dataField: "is_referral_hs",
                            dataType: "string",
                        }, {
                            caption: "ФС съёмка?",
                            dataField: "is_referral",
                            dataType: "string",
                        }, {
                            caption: "Г-р ФС",
                            dataField: "amount_referral",
                            dataType: "number",
                            format: function (value) {
                                return formatterRUB(value);
                            },
                            area: "data",
                            areaIndex: 90
                        }, {
                            caption: "CС съёмка?",
                            dataField: "is_ss",
                            dataType: "string",
                        }, {
                            caption: "CC",
                            dataField: "ss_name",
                        }, {
                            dataField: "amount_ss",
                            dataType: "number",
                            caption: "Г-р CC",
                            format: function (value) {
                                return formatterRUB(value);
                            },
                        },{
                            dataField: "pay_w",
                            caption: "Опл",
                            allowGrouping: true
                        }
                    ],
                    summary: {
                        totalItems: [{
                            column: "id",
                            summaryType: "count",
                            showInColumn: "date_time"
                        }, {
                            column: "amount_minus_tax",
                            summaryType: "sum",
                            customizeText: function (data) {
                                return formatterRUB(data.value);
                            },
                        }, {
                            column: "amount_photo",
                            summaryType: "sum",
                            customizeText: function (data) {
                                return formatterRUB(data.value);
                            },
                        }, {
                            column: "rf",
                            summaryType: "sum",
                            customizeText: function (data) {
                                return formatterRUB(data.value);
                            },
                        }, {
                            column: "amount_referral",
                            summaryType: "sum",
                            customizeText: function (data) {
                                return formatterRUB(data.value);
                            },
                        }, {
                            column: "amount_ss",
                            summaryType: "sum",
                            customizeText: function (data) {
                                return formatterRUB(data.value);
                            },
                        }]
                    }
                })
                .appendTo(contentElement);
        },
        onShowing: function() {
            $(".drill-down")
                .dxDataGrid("instance")
                .option("dataSource", drillDownDataSource);
        },
        onShown: function() {
            $(".drill-down")
                .dxDataGrid("instance")
                .updateDimensions();
        }
    }).dxPopup("instance");

   pivotGridInstance.bindChart(pivotGridChart, {
        dataFieldsDisplayMode: "splitPanes",
        alternateDataFields: false
    });


});


})



});