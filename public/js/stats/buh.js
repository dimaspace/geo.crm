$(function () {



DevExpress.localization.locale(navigator.language || navigator.browserLanguage);

DevExpress.config({ defaultCurrency: 'RUB' });

DevExpress.ui.dxDataGrid.defaultOptions({
    options: {
        rowAlternationEnabled: true,
        showBorders: false
    }
});

var store = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/stats/projects/json");
    }
});
var storeBills = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/finance/buh/bills/json");
    }
});

$(function(){

    var logName = 0;
    var progress_timer = 0;
    var progress_bar;
    var popup;

var dataGrid = $("#gridContainer").dxDataGrid({
        dataSource: {
            store:store
        },
        stateStoring: {
            enabled: true
        },
        showColumnLines: true,
        showRowLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        sorting: {
            mode: "multiple"
        },
        selection: {
            mode: "multiple"
        },
        export: {
            enabled: true,
            fileName: "fail",
            allowExportSelectedData: true
        },
        grouping: {
            autoExpandAll: false,
            contextMenuEnabled: true
        },
        columnChooser: {
            enabled: true,
            mode:  'select'
        },
        scrolling: {
            mode: "virtual"
        },
        groupPanel: {
            visible: true
        },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },

        summary: {
            groupItems: [{
                summaryType: "count",
                displayFormat: "Задач: {0}"
            }]
        },

        editing: {
            mode: 'cell',
            allowUpdating: true,
            texts: {
                confirmDeleteMessage: null
            }
        },

        onEditorPreparing: function(e) {
            if (e.dataField == "invoice") {
                e.editorOptions.onValueChanged = function (args) {
                    // Implement your logic here

                    e.setValue(args.value); // Updates the cell value
                    //console.log(args);
                }
            }
        },

        columns: [
             {
                caption: "НОМЕР СЧЁТА",
                dataField: "invoice",
                allowGrouping: false,
                allowUpdating: true,
                validationRules: [
                    {
                        type: "numeric",
                        message: "Только числа!"
                    },{
                        type: "custom",
                        validationCallback: function(params){
                            if(params.value == 4){
                                params.rule.isValid = true;
                                params.rule.message = "OK";
                                params.validator.validate();
                            }
                        }
                    }
                ]
            },{
                caption: "ID",
                dataField: "id",
                allowGrouping: false,
                allowEditing: false,
                width: 70
            },  {
                caption: "Дата и время",
                dataField: "date_time",
                dataType: 'datetime',
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Заказчик",
                dataField: "client",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Регион",
                dataField: "region",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Место",
                dataField: "place",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Прин. корд.",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Расп. корд.",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Контрол. корд.",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Вид оплаты(Б)",
                dataField: "pay_bb",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Вид оплаты(Ф)",
                dataField: "pay_w",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Вид проекта",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Статус",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Описание",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Описание(скрытое)",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Цензор",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "SMM",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Исполнитель",
                dataField: "worker",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Оплата в час",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Стоимость",
                dataField: "amount",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "СС",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "СС %",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "ФС",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "ФС %",
                dataField: "",
                allowGrouping: true,
                allowEditing: false
            }
        ]
    }).dxDataGrid("instance");

    $("#autoExpand").dxCheckBox({
        value: false,
        text: "Раскрыть все группы",
        onValueChanged: function(data) {
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
    });

    // Счета
var dataGridBill = $("#gridContainerBill").dxDataGrid({
        dataSource: {
            store:storeBills
        },
        stateStoring: {
            enabled: true
        },
        showColumnLines: true,
        showRowLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        sorting: {
            mode: "multiple"
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Найти..."
        },
        selection: {
            mode: "multiple"
        },
        export: {
            enabled: true,
            fileName: "fail",
            allowExportSelectedData: true
        },
        grouping: {
            autoExpandAll: false,
            contextMenuEnabled: true
        },
        columnChooser: {
            enabled: true,
            mode:  'select'
        },
        scrolling: {
            mode: "standard"
        },
        groupPanel: {
            visible: true
        },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },

        summary: {
            groupItems: [{
                summaryType: "count",
                displayFormat: "Счетов: {0}"
            }]
        },

        editing: {
            mode: 'cell',
            allowUpdating: true,
            texts: {
                confirmDeleteMessage: null
            }
        },

        onRowPrepared: function(e) {

            if (e.rowType == "data" && e.data.dl_exp != 0){
                e.rowElement.addClass('col-deep-orange');
            }
        },

        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            logName = moment().format('x');

            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        $.ajax({
                            type: 'GET',
                            cache: false,
                            data: {'log': logName},
                            url: '/finance/buh/bills/load_from_api',
                            beforeSend: function(){

                                var popupOptions = {
                                    width: "90%",
                                    height: 150,
                                    contentTemplate: function () {
                                        return $("<div />").append(
                                                $("<div id='progressBarStatus'></div>")
                                            );
                                    },
                                    showCloseButton: false,
                                    showTitle: true,
                                    title: "Загрузка данных их системы <<Моё Дело>>",
                                    visible: false,
                                    dragEnabled: false,
                                    closeOnOutsideClick: false,
                                    closeOnBackButton: false
                                };
                                var $popupContainer = $("<div />").addClass("popup").appendTo($("#popup"));
                                popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
                                popup.show();

                                progress_bar = $("#progressBarStatus").dxProgressBar({
                                    min: 0,
                                    max: 100,
                                    width: "100%",
                                    statusFormat:  function(value) {
                                        return "Прогресс: " + (value * 100).toFixed(1) + "%";
                                    },
                                    onComplete: function (e) {
                                        clearInterval(progress_timer);
                                        popup.hide();
                                        popup.dispose();
                                        progress_bar.dispose();
                                        $("#popup").html('')
                                        dataGrid.refresh();
                                        swal("Ура!", "Данные успешно загружены", "success");
                                        $.ajax({
                                                type: 'GET',
                                                cache: false,
                                                dataType: 'json',
                                                url: '/finance/buh/bills/json_get_data_for_infoboxes',
                                                beforeSend: function(){
                                                    $('#unpaid_bills .number, #deadline_bills .number').hide();
                                                },
                                                success: function (data) {
                                                    $('#unpaid_bills .number').text(data.unpaid_bills).show();
                                                    $('#deadline_bills .number').text(data.deadline_bills).show();
                                            },
                                                error: function(jqXHR, textStatus, errorThrown){
                                                    console.info(textStatus);
                                                }
                                            });
                                    }
                                }).dxProgressBar("instance");

                                progress_timer = setInterval(upgrade_progress_bar, 3000);
                            },
                            success: function (data) {

                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                console.info(textStatus);
                            }
                        });

                    }
                }
            });
        },

        columnFixing: {
            enabled: true
        },

        columns: [
            {
                caption: "ID",
                dataField: "id",
                allowGrouping: false,
                allowEditing: false,
                allowHeaderFiltering: false
            },  {
                caption: "Номер счёта",
                dataField: "number",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: false
            },  {
                caption: "Просрочено?",
                dataField: "dl_exp",
                allowGrouping: true,
                allowEditing: false,
                alignment: 'center',
                cellTemplate: function(container, options) {
                    var html = '';
                    if(options.data.dl_exp == 1){
                        html = '<i class="glyphicon glyphicon-fire font-22" aria-hidden="true" title="Просрочено"></i>'
                    }
                    container.html(html);
                },
                customizeText: function (cellInfo) {
                    switch (cellInfo.value) {
                        case 1:
                            return 'Просрочено'
                            break;
                        case 0:
                            return 'Нет'
                            break;
                    }
                }
            },  {
                caption: "Дата",
                dataField: "doc_date",
                dataType: 'date',
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Дедлайн",
                dataField: "doc_dd_date",
                dataType: 'date',
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Создан",
                dataField: "doc_create_date",
                dataType: 'date',
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Изменён",
                dataField: "doc_modify_date",
                dataType: 'date',
                allowGrouping: true,
                allowEditing: false
            },  {
                caption: "Оплата",
                dataField: "status_id",
                allowGrouping: true,
                allowEditing: false,
                alignment: 'center',
                cellTemplate: function(container, options) {
                    var html = '';
                    if(options.data.status_id == 6){
                        html = '<i class="material-icons col-green" title="Оплачено">done_all</i>';
                    }
                    if(options.data.status_id == 5){
                        html = '<i class="material-icons col-orange" title="Частично оплачено">opacity</i>';
                        html += '<button type="button" class="btn bg-green btn-xs pull-right force_close" data-id="' +
                            options.data.id +
                            '">\n<i class="material-icons">done</i>\n' +
                            '</button>';
                    }
                    container.html(html);
                },
                customizeText: function (cellInfo) {
                    switch (cellInfo.value) {
                        case 4:
                            return 'Не оплачено'
                            break;
                        case 5:
                            return 'Частично оплачено'
                            break;
                        case 6:
                            return 'Оплачено'
                            break;
                    }
                }
            },  {
                caption: "Коммент",
                dataField: "info",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: false
            },  {
                caption: "Сумма",
                dataField: "sum",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: false
            },  {
                caption: "Оплачено",
                dataField: "paid_sum",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: false
            },  {
                caption: "Контрагент",
                dataField: "kontragent_name",
                allowGrouping: true,
                allowEditing: false,
                headerFilter: {
                    allowSearch: true
                },
            }/*,  {
                caption: "Покрытие",
                dataField: "is_covered",
                allowGrouping: true,
                allowEditing: false
            }*/,  {
                caption: "Ссылка",
                dataField: "online",
                allowGrouping: true,
                allowEditing: false,
                allowHeaderFiltering: false,
                alignment: 'center',
                cellTemplate: function(container, options) {
                    var base_path = 'https://www.moedelo.org/';
                    var link = $("<a />").attr('href', base_path + options.data.online)
                        .attr('target', '_new' + options.data.id)
                        .html('<i class="material-icons">open_in_new</i>')
                        .addClass('col-teal');
                    container.html(link);
                }
            },  {
                caption: "Тип",
                dataField: "type_id",
                allowGrouping: true,
                allowEditing: false,
                cellTemplate: function(container, options) {
                    var text = '?';
                    if(options.data.type_id == 1){
                        text = 'об.';
                    }
                    if(options.data.type_id == 2){
                        text = 'с/д';
                    }
                    container.html(text);
                }
            },  {
                caption: "ТИП НДС",
                dataField: "nds_id",
                allowGrouping: true,
                allowEditing: false,
                cellTemplate: function(container, options) {
                    var text = '?';
                    if(options.data.nds_id == 1){
                        text = 'не нач.';
                    }
                    if(options.data.nds_id == 2){
                        text = 'сверху';
                    }
                    if(options.data.nds_id == 3){
                        text = 'в т/ч';
                    }
                    container.html(text);
                }
            }

        ]
    }).dxDataGrid("instance");

    $("#autoExpandBill").dxCheckBox({
        value: false,
        text: "Раскрыть все группы",
        onValueChanged: function(data) {
            dataGridBill.option("grouping.autoExpandAll", data.value);
        }
    });

    var upgrade_progress_bar = function upgrade_progress_bar() {
        $.ajax({
            type: 'GET',
            cache: false,
            dataType: 'json',
            data: {'log': logName},
            url: '/finance/buh/bills/load_from_api_progress',
            success: function (data) {
                progress_bar.option("value", data.progress);
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.info(textStatus);
            }
        });
    }

    $("#gridContainerBill").on('click', '.force_close', function(){
        var $this = $(this);
        var id = $this.data('id');

        $.ajax({
          url: '/finance/buh/bills/force_close',
          data: {
              'id':  id
          },

          beforeSend: function(){
            $this.attr('disabled', 'disabled');
          },
          success: function(data){
              dataGridBill.refresh();
          },
          dataType: 'json'
        });

    });

});


});