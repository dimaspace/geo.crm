$(function () {




$.when(
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-generic.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-gregorian.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/numbers.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/currencies.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/dateFields.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/likelySubtags.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/timeData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/weekData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/currencyData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/calendarData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/numberingSystems.json")
).then(function () {
    //The following code converts the got results into an array
    return [].slice.apply( arguments, [0] ).map(function( result ) {
        return result[ 0 ];
    });
}).then(
    Globalize.load //loads data held in each array item to Globalize
).then(function(){

DevExpress.config({ defaultCurrency: 'RUB' });
Globalize.locale(navigator.language || navigator.browserLanguage);



var formatterRUB = Globalize.currencyFormatter( "RUB" );




var store = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/stats/projects/json");
    }
});

$(function(){

    var pivotGridChart = $("#projectsChart").dxChart({
        commonSeriesSettings: {
            type: "spline"
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (args) {
                var valueText = (args.seriesName.indexOf("Total") != -1) ?
                        Globalize.formatCurrency(args.originalValue,
                            "RUB", { maximumFractionDigits: 0 }) :
                        args.originalValue;

                return {
                    html: args.originalArgument + ": " + args.seriesName + "<div class='currency'>"
                        + valueText + "</div>"
                };
            }
        },
        size: {
            height: 320
        },
        adaptiveLayout: {
            width: 450
        },
        onLegendClick: function (e) {
            var series = e.target;
            if (series.isVisible()) {
                series.hide();
            } else {
                series.show();
            }
        }
    }).dxChart("instance");

    /*$("#projectsChartrangeSelector").dxRangeSelector({
        size: {
            height: 120
        },
        margin: {
            left: 10
        },
        scale: {
            minorTickCount:1
        },
        dataSource: store,
        chart: {
            series: series,
            palette: "Harmony Light"
        },
        behavior: {
            callValueChanged: "onMoving"
        },
        onValueChanged: function (e) {
            var zoomedChart = $("#projectsChart").dxChart("instance");
            zoomedChart.zoomArgument(e.value[0], e.value[1]);
        }
    });*/

    var pivotGridInstance = $("#projectsTable").dxPivotGrid({

        dataFieldArea: 'row',
        allowSortingBySummary: true,
        allowSorting: true,
        allowFiltering: true,
        allowExpandAll: true,
        //height: 440,
        showBorders: true,
        export: {
            enabled: true,
            fileName: 'projects'
        },
        fieldChooser: {
            enabled: true,
            allowSearch: true,
            layout: 1,
            width: 800
        },
        fieldPanel: {
            visible: true
        },
        /*scrolling: {
            mode: "virtual"
        },*/
        headerFilter: {
            allowSearch: true
        },
        stateStoring: {
            enabled: true
        },
        dataSource: {
            fields: [{
                dataField: "id",
                visible: false
            },{
                caption: "Регион",
                width: 120,
                dataField: "region",
                area: "row"
            }, {
                caption: "Исполнитель",
                width: 120,
                dataField: "worker",
                //area: "filter"
            }, {
                caption: "Опл. Б.",
                width: 120,
                dataField: "pay_b",
                //area: "filter"
            }, {
                caption: "Опл. И.",
                width: 120,
                dataField: "pay_w",
                //area: "filter"
            }, {
                caption: "РФ",
                dataField: "rf",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                displayFolder: 'РФ'
            }, {
                caption: "РФ ($ ФС)",
                dataField: "rf_referral",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                displayFolder: 'РФ'
            }, {
                caption: "РФ ($ GEO)",
                dataField: "rf_geo",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data",
                displayFolder: 'РФ'
            }, {
                caption: "Гонорар Ф",
                dataField: "amount_photo",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data"
            }, {
                caption: "Гонорар К",
                dataField: "amount_cords",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data"
            }, {
                caption: "Заработано",
                dataField: "amount_office",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data"
            }, {
                caption: "Стоимость",
                dataField: "amount",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data"
            }, {
                caption: "Стоимость без налогов",
                dataField: "amount_minus_tax",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "data"
            }, {
                caption: "Место",
                dataField: "place",
                dataType: "string",
                summaryType: "sum",
                area: "filter"
            }, {
                caption: "Клиент",
                dataField: "client",
                dataType: "string",
                summaryType: "sum",
                area: "filter"
            }, {
                caption: "Г-р CC",
                dataField: "amount_ss",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                //area: "filter",

            }, {
                caption: "Г-р ФС",
                dataField: "amount_referral",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                //area: "filter"
            }, {
                caption: "Съёмок",
                dataField: "project_num",
                dataType: "number",
                summaryType: "sum",
                area: "data"
            }, {
                caption: "От GEO(шт)",
                dataField: "project_num_geo",
                dataType: "number",
                summaryType: "sum",
                area: "data"
            }, {
                caption: "Свои(шт)",
                dataField: "project_num_hs",
                dataType: "number",
                summaryType: "sum",
                area: "data"
            }, {
                caption: "От GEO($)",
                dataField: "amount_geo",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "filter"
            }, {
                caption: "Свои($)",
                dataField: "amount_hs",
                dataType: "number",
                summaryType: "sum",
                format: function (value) {
                    return formatterRUB(value);
                },
                area: "filter"
            }, {
                caption: "От GEO(% от $)",
                dataField: "amount_geo",
                dataType: "number",
                summaryType: 'custom',
                calculateSummaryValue: function(e){
                    var result = (e.value("amount_geo")/e.value("amount") * 100).toFixed(2);
                    return result ? result : 0;
                },
                format: function(value){
                    return value + '%';
                },
                area: "data"
            }, {
                caption: "Свои(% от $)",
                dataField: "amount_hs",
                dataType: "number",
                summaryType: 'custom',
                calculateSummaryValue: function(e){
                    var result = 100 - (e.value("amount_geo")/e.value("amount") * 100).toFixed(2);
                    return result ? result : 0;
                },
                format: function(value){
                    return value + '%';
                },
                area: "data"
            }, {
                caption: "От GEO(% от шт)",
                dataField: "project_num_geo",
                dataType: "number",
                summaryType: 'custom',
                calculateSummaryValue: function(e){
                    var result = (e.value("project_num_geo")/e.value("project_num") * 100).toFixed(2);
                    return result ? result : 0;
                },
                format: function(value){
                    return value + '%';
                },
                area: "data"
            }, {
                caption: "Свои(% от шт)",
                dataField: "project_num_hs",
                dataType: "number",
                summaryType: 'custom',
                calculateSummaryValue: function(e){
                    var result = 100 - (e.value("project_num_geo")/e.value("project_num") * 100).toFixed(2);
                    return result ? result : 0;
                },
                format: function(value){
                    return value + '%';
                },
                area: "data"
            }, {
                caption: "РФ % ФС($)",
                dataField: "rf_referral",
                dataType: "number",
                summaryType: 'custom',
                calculateSummaryValue: function(e){
                    var result = (e.value("rf_referral")/e.value("rf") * 100).toFixed(2);
                    return result ? result : 0;
                },
                format: function(value){
                    return value + '%';
                },
                area: "filter",
                displayFolder: 'РФ'
            }, {
                caption: "РФ % GEO($)",
                dataField: "rf_geo",
                dataType: "number",
                summaryType: 'custom',
                calculateSummaryValue: function(e){
                    var result = 100 - (e.value("rf_referral")/e.value("rf") * 100).toFixed(2);
                    return result ? result : 0;
                },
                format: function(value){
                    return value + '%';
                },
                area: "filter",
                displayFolder: 'РФ'
            }, {
                caption: "Своя съёмка?",
                dataField: "is_referral_hs",
                dataType: "string",
                area: "filter",
                displayFolder: 'Признак'
            }, {
                caption: "ФС съёмка?",
                dataField: "is_referral",
                dataType: "string",
                area: "filter",
                displayFolder: 'Признак'
            }, {
                caption: "CС съёмка?",
                dataField: "is_ss",
                dataType: "string",
                area: "filter",
                displayFolder: 'Признак'
            }, {
                groupName: "date",
                caption: "Месяц",
                groupInterval: "month",
                visible: true
            }, {
                groupName: "date",
                caption: "Квартал",
                groupInterval: "quarter",
                visible: false
            }, {
                groupName: "date",
                caption: "Год",
                groupInterval: "year",
                expanded: true
            }, {
                dataField: "date",
                dataType: "date",
                caption: "Дата",
                area: "column"
            }, {
                caption: "Выбор месяцев",
                area: 'filter',
                dataField: 'date',
                groupInterval: 'month',
                dataType: 'date',
                filterType: 'include',
            }],
            store: store
        }
    }).dxPivotGrid("instance");

   pivotGridInstance.bindChart(pivotGridChart, {
        dataFieldsDisplayMode: "splitPanes",
        alternateDataFields: false
    });


});


})





});