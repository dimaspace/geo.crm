$(function () {




$.when(
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-generic.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/ca-gregorian.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/numbers.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/currencies.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/main/ru/dateFields.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/likelySubtags.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/timeData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/weekData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/currencyData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/calendarData.json"),
    $.getJSON("/plugins/DevExpress/js/cldr/supplemental/numberingSystems.json")
).then(function () {
    //The following code converts the got results into an array
    return [].slice.apply( arguments, [0] ).map(function( result ) {
        return result[ 0 ];
    });
}).then(
    Globalize.load //loads data held in each array item to Globalize
).then(function(){

DevExpress.config({ defaultCurrency: 'RUB' });
Globalize.locale(navigator.language || navigator.browserLanguage);



var formatterRUB = Globalize.currencyFormatter( "RUB" );
var formatterDATE = Globalize.dateFormatter({ datetime: "short" });


var store = new DevExpress.data.CustomStore({
    loadMode: "raw",
    load: function() {
        return $.getJSON("/finance/cash_flows/json");
    }
});

$(function(){

var dataGrid = $("#gridContainer").dxDataGrid({
        dataSource: {
            store:store
        },
        showBorders: true,
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        sorting: {
            mode: "multiple"
        },
        scrolling: {
            mode: "virtual"
        },
        groupPanel: {
            visible: true
        },
        summary: {
            groupItems: [{
                column: "id",
                summaryType: "count",
                displayFormat: "Заявок: {0}",
            }, {
                column: "amount",
                summaryType: "sum",
                displayFormat: "{0}",
                showInGroupFooter: false,
                alignByColumn: true
            }, {
                column: "carried",
                summaryType: "sum",
                displayFormat: "{0}",
                showInGroupFooter: false,
                alignByColumn: true
            }, {
                column: "left",
                summaryType: "sum",
                displayFormat: "{0}",
                showInGroupFooter: false,
                alignByColumn: true
            }]
        },
        columns: [
            {
                caption: "ID",
                dataField: "id",
                allowGrouping: false,
                width: 70
            },
            {
                caption: "UID",
                dataField: "user_id",
                allowGrouping: false,
                width: 70
            },
            {
                caption: "Юзер",
                dataField: "user_name",
                groupIndex: 0
            }, {
                caption: "Комментарий",
                dataField: "comment"
            }, {
                caption: "Тип",
                dataField: "type",
                cellTemplate: function (element, info) {
                    if(info.value == 0){
                        element.append('<span class="minus_value">Пополнение</span>');
                    }else{
                        element.append('<span class="plus_value">Вывод</span>');
                    }
                }
            },
            {
                caption: "Сумма",
                dataField: "amount",
                /*customizeText: function (cellInfo) {
                    return formatterRUB(cellInfo.value);
                }*/
            }, {
                caption: "Состыковано",
                dataField: "carried",
                /*customizeText: function (cellInfo) {
                    return formatterRUB(cellInfo.value);
                }*/
            }, {
                caption: "Остаток",
                dataField: "left",
                /*customizeText: function (cellInfo) {
                    return formatterRUB(cellInfo.value);
                }*/
            }, {
                caption: "Создано",
                dataField: "created_at",
                /*dataType: "datetime",
                customizeText: function (cellInfo) {
                    return formatterDATE(cellInfo.value);
                }*/
            }, {
                caption: "Последняя стыковка",
                dataField: "updated_at",
                dataType: "datetime",
                /*customizeText: function (cellInfo) {
                    return formatterDATE(cellInfo.value);
                }*/
            }
        ]
    }).dxDataGrid("instance");

    $("#autoExpand").dxCheckBox({
        value: true,
        text: "Раскрыть все группы",
        onValueChanged: function(data) {
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
    });

});


})





});