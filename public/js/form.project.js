var project_range_function;
var clone_project_form;
$(function () {

    var $time_line_input = $("#time_line");
    var $time_line_start = $("#time_line_start");
    var $time_line_end = $("#time_line_end");

    var $visibility_list_row = $("#project_visibility_list_row");
    var $visibility_id = $('#project_visibility_id');
    var $visibility_list = $('#project_visibility_list');

    var table = $('#cord_dash_table').DataTable({
        fixedHeader: true,
        paging: false,
        scrollX: true,
        dom: 'lfrtp',

        colReorder: true,

        stateSave: true,
        "stateDuration": 60 * 60 * 24 * 365 * 3,

        scrollY:        '40vh',
        scrollCollapse: true,

        /*columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0
        }],

        order: [ 3, 'asc' ],*/

        language: {
            "url": "/plugins/jquery-datatable/ru.lang"
        }
    });

    $('#cord_dash_table tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    // Фильтры

    var $project_date_range = $('#project-date-range');
    var $project_date_range_start = $('#project-date-range-start');
    var $project_date_range_end= $('#project-date-range-end');

    project_range_function = function (start, end) {

        $project_date_range.find('span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));

        // noinspection NonAsciiCharacters
        $project_date_range.daterangepicker({
            "startDate": start,
            "endDate": end,
            "ranges": {
                'Сегодня': [moment().startOf('day'), moment().endOf('day')],
                'Завтра': [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')],
                '3 дня вперёд': [moment().startOf('day'), moment().add(3, 'days').endOf('day')],
                'Неделя вперёд': [moment().startOf('day'), moment().add(7, 'days').endOf('day')],
                'Неделя назад': [moment().startOf('day').subtract(7, 'days'), moment().endOf('day')],
                /*'Этот месяц': [moment().startOf('month'), moment().endOf('month')],*/
                'Месяц вперёд': [moment().startOf('day'), moment().add(1, 'month').endOf('day')],
                '+/- 10 дней': [moment().subtract(10, 'days').startOf('day'), moment().add(10, 'days').endOf('day')]

            },
            "locale": {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Вручную",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            }
        }, function (start, end) {
            $project_date_range.find('span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
            $project_date_range_start.val(start.format('YYYY-MM-DD'));
            $project_date_range_end.val(end.format('YYYY-MM-DD'));

            apple_project_filter();
        });
    };

    $('.project-regions select, .project-statuses select, .project-workers select, .project-staff select').on('hidden.bs.select', function(event){
        var $rt = $(event.currentTarget);
        if($rt.data('changed') == 'true'){
            apple_project_filter();
        }
    });

    $('.project-regions select, .project-statuses select, .project-workers select, .project-staff select').on('changed.bs.select', function(event){
        var $rt = $(event.currentTarget);
        $rt.data('changed', 'true');
    });

    function apple_project_filter(){
        if($('#projects_calendar').length > 0){
            var filter_query = $('#projects_filter_form').serialize();
            //window.storeCalendarProjectsFilterQuery = filter_query;
            console.log(filter_query);
            //console.log(projects_calendar);
            $('#projects_filter_form #projects_list_mode').val('calendar').parent().submit();
            //window.dataSourceCalendarProjects.reload();
        }else{
            $('#projects_filter_form').submit();
        }
    }

    init_project_filter();


/*	$('#add_project').on('shown.bs.modal', function() {

		$('#project_date_start').bootstrapMaterialDatePicker({
			format: 'DD.MM.YYYY HH:mm',
			clearButton: true,
			weekStart: 1,
			time: true,
            switchOnClick: true
		});

		$('#project_date_start').bootstrapMaterialDatePicker({ weekStart : 0 }).on('change', function(e, date){
            $('#project_date_end').bootstrapMaterialDatePicker('setMinDate', date);
        });

		$('#project_date_end').bootstrapMaterialDatePicker({
			format: 'DD.MM.YYYY HH:mm',
			clearButton: true,
			weekStart: 1,
			time: true,
            switchOnClick: true
		});
		$('#project_date_end').bootstrapMaterialDatePicker({ weekStart : 0 }).on('change', function(e, date){
            $('#project_date_start').bootstrapMaterialDatePicker('setMaxDate', date);
        });

		$('#project_date_deadline').bootstrapMaterialDatePicker({
			format: 'DD.MM.YYYY',
			clearButton: true,
			weekStart: 1,
			time: false,
            switchOnClick: true
		});

	})*/

    var dx_deadline2 = $("#project_date_deadline2").dxNumberBox({
        inputAttr: {
            name: 'project_deadline2'
        },
        mode: 'number',
        min: 0,
        showSpinButtons: true,
        format: "+#0 ч",
        width: 200
    }).dxNumberBox("instance");
    $('#add_project').on('click', '.deadline2-set', function(){
        var val = $(this).data('value');
        dx_deadline2.option('value', val);
    });

    $('#add_project').on('hide.bs.modal', function() {
        $('#project_date_start').bootstrapMaterialDatePicker('setMaxDate', new Date(2100, 1, 1));
    });

    function render_night_row(date){
        var sdate_moment = moment(date.format("X"), "X");

        var sdate = sdate_moment .format('Do MMMM YYYY');
        $('#night_row .alert .sdate').html(sdate);

        var weekday_num = sdate_moment .format('d');

        $('#night_row .alert .nw_day').addClass('hidden');
        $('#night_row .alert .nw_day.nw_' + weekday_num ).removeClass('hidden');
    }

    $('#project_date_deadline').bootstrapMaterialDatePicker({
        format: 'DD.MM.YYYY HH:mm',
        clearButton: true,
        weekStart: 1,
        time: true,
        switchOnClick: true
    });

    $("#time_line").ionRangeSlider({
        type: "double",
        min: +moment().startOf('day').add(6, 'hours').format("X"),
        max: +moment().add(1, 'days').startOf('day').add(6, 'hours').format("X"),
        from: +moment().subtract(1, "hours").format("X"),
        grid: true,
        drag_interval: true,
        force_edges: true,
        step: 60*15,
        grid_num: 4,
        prettify: function (num) {
            var m = moment(num, "X").locale("ru");
            if(m.format("HH:mm") === '00:00'){
                return '<b class="midnight"><span class="sdate">' + m.format("D MMMM, ") + '</span>' + m.format("HH:mm") + '</b>';
            }else{
                return '<span class="sdate">' + m.format("D MMMM, ") + '</span>' + m.format("HH:mm") + '';
            }

        },
        onStart: function (data) {
        },
        onChange: function (data) {
            $time_line_input.data('changed', 'true');
            time_line_changed(data);
        },
        onFinish: function (data) {
            time_line_changed_final(data);
        },
        onUpdate: function (data) {
            time_line_changed(data);
        }
    });

    function time_line_changed(data){

            var start_h = +moment(data.from, 'X').format('H');
            var end_h = +moment(data.to, 'X').format('H');
            var start_d_real = +moment(data.from, 'X').format('D');
            var end_d_real = +moment(data.to, 'X').format('D');
            var date_start = $('#project_date_start').val();
            var start_d_club = +moment(date_start, 'DD.MM.YYYY').format('D');

            if( ( start_h > 22 ) || ( start_d_real !== start_d_club ) || ( start_d_real !== end_d_real ) || (start_h > end_h) ){

                $('#night_row').removeClass('hidden');
                $time_line_input.data('nigthmode', 'true');
                $('#time_line_row .irs-bar').addClass('midnight');
            }else{
                $('#night_row').addClass('hidden');
                $time_line_input.data('nigthmode', 'false');
                $('#time_line_row .irs-bar').removeClass('midnight');
            }
            $time_line_input.data('from', data.from);
            $time_line_input.data('to', data.to);

            $time_line_start.val(moment(data.from, 'X').format('DD.MM.YYYY HH:mm'));
            $time_line_end.val(moment(data.to, 'X').format('DD.MM.YYYY HH:mm'));

            add_project_last_time_from = data.from;
            add_project_last_time_duration = data.to - data.from;

            $('#project_time_start').val(moment(data.from, 'X').format('H:mm'));


    }

    function time_line_changed_final(data){
        var dead_line_date = $('#project_date_deadline').val();
        var is_auto_set = $('#project_date_deadline').data('isAutoSet');
        var skip = $('#project_date_deadline').data('skip');

        if(skip !== true){
            if(dead_line_date && is_auto_set !== true){
                swal({
                    title: "Автоматическая установка дедлайна",
                    text: "Дата дедлайна уже установлена, можно ли её изменить автоматически?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, установить!",
                    cancelButtonText: "Оставить старую",
                    //closeOnConfirm: false
                }).then( function (isConfirm) {
                    if(isConfirm.value){
                        $('#project_date_deadline').data('isAutoSet', true);
                        swal("Дедлайн изменён!", "Последущие изменения будут применяться автоматически", "info");
                        time_line_changed_final(data);
                    }else{
                        $('#project_date_deadline').data('skip', true);
                    }
                });
            }else{
                var dl_moment = moment(data.to, 'X').add(24, 'h');

                $('#project_date_deadline').bootstrapMaterialDatePicker('setDate', dl_moment);
                $('#project_date_deadline').val(dl_moment.format('DD.MM.YYYY HH:mm'));
                $('#project_date_deadline').data('isAutoSet', true);
            }
        }
    }

    var $time_line = $time_line_input.data("ionRangeSlider");

    $('#project_time_unknown').on('change', function(event) {
        $ct = $(this);
        if($ct.prop("checked")){
            $time_line.update({
                disable: true
            });
            $('#night_row').addClass('hidden');
        }else{
            var date = moment($('#project_date_start').val(), "DD.MM.YYYY").startOf('day');
            //alert(date.format('DD.MM.YYYY HH:mm'));
            $('#project_date_start').trigger('beforeChange', [date]).trigger('change', [date]);

            $time_line.update({
                disable: false
            });
        }

    });


    var add_project_last_date;
    var add_project_last_time_from;
    var add_project_last_time_duration;

    $('#add_project').on('show.bs.modal', function(event) {
      var $rt = $(event.relatedTarget);
      var mode = $rt.data('mode');

      $('#project_date_start').val('');
      $time_line_input.data('changed', false).val('');
      $('#project_time_unknown').prop('checked', false);
      $('#time_line_row').addClass('hidden');
      $('#night_row').addClass('hidden');
      $('#deny_text_block').addClass('hidden');


        $('#project_date_start').bootstrapMaterialDatePicker({
            format: 'DD.MM.YYYY',
            clearButton: true,
            weekStart: 1,
            time: false,
            switchOnClick: true
        }).on('beforeChange', function(event, date){
            $val = $(this).val()
            if($val){
                 add_project_last_date =  moment($val, 'DD.MM.YYYY').startOf('day');
            }else{
                 add_project_last_date = date.startOf('day');
            }

        }).on('change', function(e, date){
            var min = +moment(date.format("X"), "X").startOf('day').add(6, 'hours').format("X");
            var max =  +moment(date.format("X"), "X").add(1, 'days').startOf('day').add(6, 'hours').format("X");
            var from =  +moment(date.format("X"), "X").hour(12).minute(0).format("X");
            var to =  +moment(date.format("X"), "X").hour(14).minute(0).format("X");

            if($time_line_input.data('changed') == false) {

                add_project_last_time_from = from;
                add_project_last_time_duration = to - from;

                $time_line.update({
                    min: min,
                    max: max,
                    from: from,
                    to: to
                });

                time_line_changed_final({'to': to})

                $('#time_line_row').removeClass('hidden');
            }else{

                var day_range = (date.startOf('day').unix() - add_project_last_date.unix()) / 3600 / 24;

                from = moment(add_project_last_time_from, 'X').add(day_range, 'days');
                to =  from.clone().add(add_project_last_time_duration, 'seconds');

                $time_line.update({
                    min: min,
                    max: max,
                    from: from.unix(),
                    to: to.unix()
                });

                time_line_changed_final({'to': to.unix()})
            }

            render_night_row(date);

        });



      if(mode === 'new'){
            new_project_form();
      }
      if(mode === 'edit'){
          var id =  $rt.data('id');
          edit_project_form(id);
      }
    });

    var $vk_send = $('#form-project .vk-send');
    /*$vk_send.click(function(){
        var $rt = $(event.currentTarget);
        var project_id = $('#form-project #project_id').val();
        var user_id = $('#form-project #project_worker_id').val();

        if(project_id != 0) {
            $.ajax({
                url: '/project/send_to_vk',
                type: "POST",
                cache: false,
                data: {
                    project_id: project_id,
                    user_id: user_id
                },
                beforeSend: function () {
                    $vk_send.removeClass('hidden').addClass('load').prop('disabled', true).prop('title', 'Загрузка...');
                },
                success: function (data) {
                    $vk_send.removeClass('hidden bg-red bg-teal load').addClass('bg-light-green').prop('disabled', false).prop('title', 'Сообщение отправлено');
                }
            });
        }
    });*/


    window.vkSend = function(){
        var $rt = $(event.currentTarget);
        var project_id = $rt.data('projectId');

        if(project_id != 0) {
            $.ajax({
                url: '/project/send_to_vk',
                type: "POST",
                cache: false,
                data: {
                    project_id: project_id
                },
                beforeSend: function ($a, settings) {
                    if($rt.hasClass('vk-resend')){
                        if(!confirm('Сообщение уже было отправлено! Отправить ещё одно?')){
                            return false;
                        }else{
                            settings.data = settings.data + '&resend=1';
                        }
                    }
                    $rt.addClass('load').prop('disabled', true).prop('title', 'Загрузка...');
                },
                success: function (data) {
                    if(data.error){
                        $rt.removeClass('load').prop('disabled', false);
                        swal({
                            title: 'Сбой отправки!',
                            html: data.error,
                            type: 'error',
                            showCloseButton: true,
                            showConfirmButton: false,
                            onClose: function(){
                                $('#vk_msg_copy').remove();
                                $('.vk_msg_copy_button').remove();
                            }
                        });
                    }else{
                        $rt.removeClass('load').addClass('hidden').prop('disabled', false).prop('title', 'Сообщение отправлено');
                        $rt.parent().prepend('<i class="fa fa-vk col-blue font-20" aria-hidden="true" title="Отправлена информация вконтакте"></i>');
                        if(projects_calendar){
                            projects_calendar.getDataSource().reload().then(r => {});
                        }
                        swal("Сообщение отправлено!", '', "success");
                    }

                }
            });
        }


    };
    var $vk_send_in_table = $('#cord_dash_table .vk-send');
    $vk_send_in_table.click(vkSend);


    $("body").on("click", ".vk_msg_copy_button", function () {
        var text_to_copy = $('#vk_msg_copy').html();
        CopyToClipboard(text_to_copy);
    });
    $("body").on("click", ".vk_msg_deny_copy_button", function () {
        CopyToClipboardNew($('#deny_text_to_client'));
    });

    $('#project_worker_id').change(function(){
        var $rt = $(event.currentTarget);
        var user_id = $rt.val();
        process_vk_send(user_id);

    });

    $visibility_id.change(function(event){
        var $rt = $(event.currentTarget);
        var $selected = $rt.find('option:selected');
        var type = $selected.data('type');

        if(type == 'list') {
            $visibility_list_row.removeClass('hidden');
        }else{
            $visibility_list_row.addClass('hidden');
        }

    });

    function get_calc_amount(){

        var data = {};

        data.worker_id = $('#project_worker_id').val();
        data.payment_type_id_b = $('#project_payment_type_id_b').val();
        data.payment_type_id_w = $('#project_payment_type_id_w').val();
        data.amount = $('#project_amount').val();
        data.amount_to_office = $('#project_amount_to_office').val();

        data.ssmanager_id = $('#project_ssmanager_id').val();
        data.ssmanager_royalty = $('#project_ssmanager_royalty').val();

        data.ss_mode = $('#project_ss_mode').val();
        data.ss_base = $('#project_ss_base').val();

        data.referral_id = $('#project_referral_id').val();
        data.referral_royalty = $('#project_referral_royalty').val();


        data.project_id = 1;

        $.ajax({
            url: '/project/calc_amount',
            type: "POST",
            cache: false,
            data: data,
            beforeSend: function () {
                $('.finance_info .preloader').removeClass('hidden');
                $('.finance_info .finance_data').addClass('invisible');
            },
            success: function (data) {
                $('.finance_info .preloader').addClass('hidden');
                $('.finance_info .finance_data').removeClass('invisible');

                $.each(data.result, function(key, value){
                    $('.finance_info .finance_data span.finance_' + key).html(value);
                });
                $('.finance_info .finance_data')
            }
        });
    }

    var project_rf_fields = [
        '#project_worker_id', '#project_amount', '#project_payment_type_id_b', '#project_payment_type_id_w',
        '#project_ssmanager_id', '#project_ssmanager_royalty',
        '#project_referral_id', '#project_referral_royalty', '#project_ss_mode', '#project_ss_base'
    ];
    $(project_rf_fields.join(', ')).change(get_calc_amount);
    var project_rf_text = ['#project_amount', '#project_amount_to_office', '#project_ssmanager_royalty', '#project_referral_royalty'];
    $(project_rf_text.join(', ')).keyup(get_calc_amount);

    function process_vk_send(user_id) {
        if (user_id != 0) {
            $.ajax({
                url: '/project/get_user_vk',
                type: "POST",
                cache: false,
                data: {
                    user_id: user_id
                },
                beforeSend: function () {
                    $vk_send.removeClass('hidden').addClass('load').prop('disabled', true).prop('title', 'Загрузка...');
                },
                success: function (data) {
                    if (data.error) {
                        swal("Ошибка", data.error, "error");
                        $vk_send.addClass('hidden');
                    }
                    if (data.result) {
                        $vk_send.removeClass('load bg-red bg-light-green').addClass('bg-teal').prop('disabled', false).prop('title', 'Отправить сообщение в ВК');
                    } else {
                        $vk_send.removeClass('load bg-teal bg-light-green').addClass('bg-red').prop('disabled', true).prop('title', 'Пользователь не привязал профиль ВК');
                    }

                }
            });
        } else {
            $vk_send.addClass('hidden').removeClass('load').prop('disabled', false);
        }
    }


    $('#cord_dash_table tbody tr').on('dblclick', function(event){
        var $rt = $(event.currentTarget);
        $('#add_project').modal('show');
        var id =  $rt.data('id');
        edit_project_form(id);
    });

    $('#cord_dash_table tbody tr td.id').on('click', function(event){
        var $rt = $(event.currentTarget);
        $('#add_project').modal('show');
        var id =  $rt.parent().data('id');
        edit_project_form(id);
    });

    function new_project_form(){
        $('.page-loader-wrapper').fadeIn();

        var $fp = $('#form-project');
        var $sb = $('#add_project button.submit');

        var $cb = $('#add_project button.clone');
        $cb.addClass('hidden');

        $vk_send.addClass('hidden');

        $('#time_line_row').addClass('hidden');
        $visibility_list_row.addClass('hidden');

        $fp.trigger('reset');
        $fp.find('select').selectpicker('refresh');
        $sb.text('СОЗДАТЬ');
        $fp.attr('action', '/project/add');

        var $bb = $('#add_project .boss_buttons');
        $bb.addClass('hidden');

        $('#project_date_deadline').data('isAutoSet', false).data('skip', false);

        dx_deadline2.option('value', 44);
        dx_deadline2.repaint();

        $('#add_project #vk_user_info').html('');
        $('#project_client_vk_id').parent().removeClass('focused error success');

        rebuld_worker_list([], false);
        reorder_visibility_citylist();

        $('.page-loader-wrapper').fadeOut();
    }

    window.edit_project_form = function(project_id){

        $('#project_date_start').bootstrapMaterialDatePicker({
            format: 'DD.MM.YYYY',
            clearButton: true,
            weekStart: 1,
            time: false,
            switchOnClick: true
        });

        /* Получаем данные о проекте */
        $.ajax({
            url: '/project/get_one',
            type: "POST",
            cache: false,
            data: {
                project_id: project_id
            },
            beforeSend: function () {
                $('.page-loader-wrapper').fadeIn();

                $('#project_date_deadline').data('isAutoSet', false).data('skip', false);

                var $cb = $('#add_project button.clone');
                $cb.removeClass('hidden');
            },
            success: function (data) {
                 var $fp = $('#form-project');
                 var $sb = $('#add_project button.submit');

                $fp.trigger('reset');
                $fp.find('select').selectpicker('refresh');
                $sb.text('РЕДАКТИРОВАТЬ');
                $fp.attr('action', '/project/edit');

                var $bb = $('#add_project .boss_buttons');
                $bb.removeClass('hidden');
                $bb.find('.history').attr('href', '/logs/project/' + data.project.id);
                //$bb.find('.history').attr('href', '/project/' + data.project.id + '/transactions/');

                var project = data.project;
                var cords = data.cords;
                var ssmanager = data.ssmanager;
                var referral = data.referral;


                $.each(project, function(key, val){

                    if(key === 'client_vk_id'){
                        if(val === 0){
                            val = '';
                        }
                        $('#project_' + key).val(val);
                    }else if(key === 'cords'){
                         $.each(cords, function(key, val){

                             var select_id = 'project_cord_' + key + '_id';

                             $('#' + select_id).val(val);
                         });


                    }else if(key === 'date_deadline'){
                        if(val){
                            var date = moment(val, "YYYY-MM-DD HH:mm");
                            $('#project_' + key).bootstrapMaterialDatePicker('setDate', date);
                        }
                    }else if(key === 'time_unknown'){
                        if(project.date_start == null){
                            val = 1;
                        }
                        if(val){
                            $('#project_' + key).prop('checked', 1);
                                $time_line.update({
                                    disable: true
                                });
                                $('#night_row').addClass('hidden');
                        }else{
                            $('#project_' + key).prop('checked', 0);
                                $time_line.update({
                                    disable: false
                                });
                        }

                    }else if(key === 'visibility_list'){

                        $visibility_list.selectpicker('val', val.split(','));

                    }else if(key === 'ssmanager'){
                        if(ssmanager && ssmanager.user_id){
                            $('#project_ssmanager_id').val(ssmanager.user_id);
                        }
                        if(ssmanager && ssmanager.royalty){
                            $('#project_ssmanager_royalty').val(ssmanager.royalty);
                        }
                    }else if(key === 'referral'){
                        if(referral && referral.user_id) {
                            $('#project_referral_id').val(referral.user_id);
                        }
                        if(referral && referral.user_id){
                            $('#project_referral_royalty').val(referral.royalty);
                        }

                    }else if(key === 'deadline2'){
                        dx_deadline2.option('value', val);
                        dx_deadline2.repaint();

                    }else{
                        if(key !== 'date_start' && key !== 'date_start_real' && key !== 'date_end'){
                            $fp.find('#project_' + key).val(val);
                        }

                    }
                });

                if(project.date_start){
                    var date = moment(project.date_start, "YYYY-MM-DD").startOf('day');
                    render_night_row( date);
                    $('#project_date_start').bootstrapMaterialDatePicker('setDate', date);

                    $time_line.update({
                        min: +moment(date.format("X"), "X").startOf('day').add(6, 'hours').format("X"),
                        max:  +moment(date.format("X"), "X").add(1, 'days').startOf('day').add(6, 'hours').format("X")
                    });
                    $time_line_input.data('changed', true);
                }
                if(project.date_start_real !== null){
                    $time_line.update({
                        from: +moment(project.date_start_real, 'YYYY-MM-DD HH:mm').format('X'),
                    });
                }
                if(project.date_end !== null){
                    $time_line.update({
                        to: +moment(project.date_end, 'YYYY-MM-DD HH:mm').format('X'),
                    });
                }


                $('#add_project #vk_user_info').html('');
                $('#project_client_vk_id').parent().removeClass('focused error success');


                rebuld_worker_list(data.worker_desire, data.worker_weight);
                reorder_visibility_citylist();

                $('#time_line_row').removeClass('hidden');
                $visibility_id.trigger('change');

                process_vk_send(project.worker_id);

                $fp.find('select').selectpicker('refresh');

                $('.page-loader-wrapper').fadeOut();

                get_calc_amount();
            }
        });


    }


    function reorder_visibility_citylist(){
        var $fp = $('#form-project');
        var $select_list = $fp.find('#project_visibility_list');
        var $select_list_groups = $select_list.find('optgroup');

        var region_code_selected = $('#project_region_code').val();
        $select_list_groups.sortElements(function(a, b){
            var $a = $(a);
            var $b = $(b);
            var weight_a = 1;
            var weight_b = 1;
            if($a.data('rcode') == region_code_selected){
                weight_a = 2;
            }

            if($b.data('rcode') == region_code_selected){
                weight_b = 2;
            }

            if(weight_a === weight_b){
                return $a.prop('label') < $b.prop('label') ? 1 : -1;
            }

            return weight_a < weight_b ? 1 : -1;
        });

        $select_list.selectpicker('refresh');
    }

    function rebuld_worker_list(desire_info, worker_weight){
        var $fp = $('#form-project');
        var $worker_list = $fp.find('#project_worker_id');
        var $worker_list_groups = $worker_list.find('optgroup');

        $worker_list_groups.find('option').each(function(){
            var $this = $(this);
            var id = $this.val();
            $this.removeClass('desire_1 desire_2 desire_3 desire_4 desire_5');
            if(desire_info[id]){
                $this.addClass('desire_' + desire_info[id]);
            }


        });

        $worker_list_groups.each(function(){
            var $this = $(this);
            $this.find('option').sortElements(function(a, b){

                var $a = $(a);
                var $b = $(b);
                var weight_a, weight_b;
                if(worker_weight){
                    var id_a = $a.val();
                    var id_b = $b.val();
                    weight_a = worker_weight[id_a] ? worker_weight[id_a] : 0;
                    weight_b = worker_weight[id_b] ? worker_weight[id_b] : 0;
                }else{
                    weight_a = $a.data('weight');
                    weight_b = $b.data('weight');
                }
                return weight_a < weight_b ? 1 : -1;
            });
        });

        reorder_worker_citylist();

    }

    function reorder_worker_citylist(){
        var $fp = $('#form-project');
        var $worker_list = $fp.find('#project_worker_id');
        var $worker_list_groups = $worker_list.find('optgroup');

        var region_code_selected = $('#project_region_code').val();
        $worker_list_groups.sortElements(function(a, b){
            var $a = $(a);
            var $b = $(b);
            var weight_a = 1;
            var weight_b = 1;
            if($a.data('rcode') == region_code_selected){
                weight_a = 2;
            }

            if($b.data('rcode') == region_code_selected){
                weight_b = 2;
            }

            if(weight_a === weight_b){
                return $a.prop('label') < $b.prop('label') ? 1 : -1;
            }

            return weight_a < weight_b ? 1 : -1;
        });

        $worker_list.selectpicker('refresh');
        reorder_visibility_citylist();
    }

    $('#project_region_code').change(reorder_worker_citylist);
    clone_project_form = function(){


        var $fp = $('#form-project');
        var $sb = $('#add_project button.submit');
        var $cb = $('#add_project button.clone');

        $vk_send.addClass('hidden');
        $cb.addClass('hidden');

        $('#time_line_row').addClass('hidden');
        $('#night_row').addClass('hidden');
        $visibility_list_row.addClass('hidden');

        $time_line_input.data('changed', false);

        $sb.text('СОХРАНИТЬ КЛОН');
        $fp.attr('action', '/project/add');

        $('#project_date_deadline, #project_date_start, #project_time_start, #project_real_start, #project_real_end, #project_download_link, #project_geo_link').val('');
        $('#project_worker_id').selectpicker('val', 0);
        $('#project_status_id').selectpicker('val', 0);
        $('#project_pass_type_id').selectpicker('val', 0);

        $('#project_date_deadline').data('isAutoSet', false).data('skip', false);

        get_calc_amount();
    }

    $(".project_make_paid_button").click(function (event) {
        event.stopPropagation();
        $this = $(this);
        var project_id = $this.data('projectId');
        swal({
            title: "Проект #" + project_id,
            text: "Вы уверены, что хотите поменить этот проект оплаченым?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, оплачен!",
            cancelButtonText: "Отмена",
            //closeOnConfirm: false
        }).then( function (result) {
             if(result.value){
                $('#project_make_paid_form_' + project_id).submit();
             }
        });

    });

    var get_vk_user_id_by_link = function(){
        var $this = $(this);
        var $vk_user_info = $('#add_project #vk_user_info');
        var val = $this.val();
        $.ajax({
            url: '/project/get_vk_user_id_by_link',
            type: "POST",
            cache: false,
            data: {
                link: val,
                region_code: $('#project_region_code').val()
            },
            dataType: 'json',
            beforeSend: function () {
                $this.prop('disabled', true);
                $this.parent().removeClass('focused error success');
            },
            success: function (data) {
                $this.prop('disabled', false);
                if(data.user_info === false){
                    swal("ОШИБКА!", "Неверная ссылка на профиль", "error");
                    $this.val('');
                    $this.parent().addClass('focused error');
                    $vk_user_info.html('<span class="col-red"><i class="material-icons font-28">error</i> Ошибка</span>');
                }else{
                    var vk_id = 0;
                    if(data.user_info.uid){
                        vk_id = data.user_info.uid;
                    }else{
                        vk_id = data.user_info.id;
                    }
                    $this.val(vk_id);
                    $vk_user_info.html(data.user_info.first_name + ' ' + data.user_info.last_name);
                    $vk_user_info.append('<img class="pull-left" src="'+ data.user_info.photo_50 +'">');
                    $this.parent().addClass('focused success');
                    if(data.user_info.allow === false){
                        var text_for_client =
                            'Приветствуем, у Вас нет активных диалогов с нашей группой @' + data.user_info.group_screen_name +
                            ' (' + data.user_info.group_name + ')' +
                            ".\n Просим Вас написать в чат группы сообщение «Привет», что поможет нам автоматически уведомлять вас о статусах проекта.\n" +
                            data.user_info.group_dialog_link ;
                        $('#deny_text_to_client').text(text_for_client);
                        $('#deny_text_block').removeClass('hidden');
                        swal({
                            title: 'Внимание!',
                            type: 'warning',
                            html: 'Пользователь не давал разрешение писать из группы этого региона!<br><br>',
                            showCloseButton: true,
                        });
                    }
                }
            },
            error: function (data) {
                $this.prop('disabled', false);
                swal("Что то пошло не так!", "Не удалось получить информацию о профиле клиента<br>Возможно и с доставкой сообщений будут проблемы", "error");
            }
        });
    }

    $("#project_client_vk_id").blur(get_vk_user_id_by_link);

});