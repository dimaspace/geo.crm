<?php
/*
 * @TODO Обновить Zizaco/entrust до 1.8 при релизе
 */
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Media;
use App\User;
use Spatie\Tags\Tag;
Route::domain(config('portfolio.sites.main'))->middleware(['generate-menu:portfolio'])->group(function () {
    Route::get('/{code}', 'RedirectController@redirect')->name('shorturl.redirect');

    Route::get('/', 'Portfolio\HomeController@index')->name('extraPortfolioMainPage');

    Route::get('/team/{user}', function (Request $request, User $user) {
        if($user->hasMedia('portfolio')){
            $portfolio = $user->media()->collection('portfolio')->with('tags')->has('tags', '>', 0)->get();

            $tags = collect([]);
            foreach($portfolio as $item){
                $tags = $tags->merge($item->tags);
            }
            $tags = $tags->unique('id');

            $price = $user->price;

            return view('extra_sites.portfolio.user_page', [
                'portfolio' => $portfolio,
                'tags' => $tags,
                'prices' => $price,
                'user' => $user,
                'user_prev' => $user->prev(),
                'user_next' => $user->next(),
            ]);

        }else{
            abort(404);
        }
    })->name('extraPortfolioUserPage');
});
Auth::routes();

use App\Project;
Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'DashboardController@index');

    Route::get('/profile', 'ProfileController@index');
    Route::post('/profile', 'ProfileController@saveProfile');

    Route::get('/profile/notify/', 'ProfileController@Notify');
    Route::post('/profile/notify/', 'ProfileController@saveNotify');

    Route::get('/profile/busy/', 'ProfileController@Busy');
    Route::get('/profile/busy/json', 'ProfileController@loadBusy');
    Route::post('/profile/busy/', 'ProfileController@saveBusy');


    Route::get('/profile/balance', 'ProfileController@balance');
    Route::post('/profile/balance/add_cash_flow', 'ProfileController@AddCashFlow');
    Route::post('/profile/balance/cash_flow_transaction_accept', 'ProfileController@CashFlowTransactionAccept');

    Route::post('/profile/price', 'ProfileController@savePrice');

    Route::get('/profile/portfolio', 'PortfolioController@photoIndex')->name('portfolioPhotoIndex');
    Route::get('/profile/portfolio/add', 'PortfolioController@photoAdd')->name('portfolioPhotoAdd');
    Route::put('/profile/portfolio/add', 'PortfolioController@photoAddSubmit')->name('portfolioPhotoAddSubmit');
    Route::get('/profile/portfolio/tags/by_type_{type}', 'PortfolioController@getTags');
    Route::get('/profile/portfolio/by_type_{type}/json', 'PortfolioController@getSelfItems');
    Route::get('/profile/portfolio/{id}', 'PortfolioController@getSelfItem')->where('id', '[0-9]+');
    Route::post('/profile/portfolio/{id}', 'PortfolioController@photoSelfEdit')->name('portfolioSelfPhotoEdit')->where('id', '[0-9]+');
    Route::get('/profile/portfolio/remove', 'PortfolioController@photoRemove')->name('portfolioPhotoRemove');

    Route::get('/moderate/portfolio', 'PortfolioController@censorIndex')->name('portfolioModerateIndex');
    Route::get('/moderate/portfolio/by_type_{type}/json', 'PortfolioController@getItems');
    Route::get('/moderate/portfolio/get_users_with_portfolio/by_type_{type}/json', 'PortfolioController@getUsers');
    Route::get('/moderate/portfolio/get_photographers/json', 'PortfolioController@getPhotographers');
    Route::get('/moderate/portfolio/{id}', 'PortfolioController@getItem');
    Route::post('/moderate/portfolio/{id}', 'PortfolioController@photoEdit')->name('portfolioPhotoEdit')->where('id', '[0-9]+');
    Route::post('/moderate/portfolio/vote/{id}', 'PortfolioController@Vote')->name('portfolioVote')->where('id', '[0-9]+');
    Route::get('/storage/media/{id}/conversions/photo-{conversion}.jpg', 'PortfolioController@notFoundFile');

    Route::get('/home', 'HomeController@index');

    //Route::post('/telegram/hook', 'ExcelController@ExportTransactions');

    Route::get('/import/reports', 'ImportController@Reports');
    Route::post('/import/reports', 'ImportController@ReportsUpload');
    Route::post('/import/reports/save', 'ImportController@ReportsSave');

    Route::get('/newsletter/add', 'NewsletterController@Add');
    Route::post('/newsletter/send', 'NewsletterController@Send');

    Route::get('/dispatch/massvk', 'DispatchController@MassVkExcelImport');
    Route::get('/dispatch/failed', 'DispatchController@failedMsg');
    Route::post('/dispatch/massvk', 'DispatchController@MassVkSend');

    Route::get('/jobs/failed', 'DispatchController@jobsFailed');
    Route::get('/jobs/failed/json', 'DispatchController@jobsFailedJson');
    Route::get('/jobs/failed/delete/{id}', 'DispatchController@jobsFailedDelete');
    Route::get('/jobs/failed/retry/{id}', 'DispatchController@jobsFailedRetry');

    Route::any('/bx24api/find_contacts', 'BxApiController@findContacts');
    Route::any('/bx24api/find_company', 'BxApiController@findCompany');
    Route::any('/bx24api/get_company_by_id', 'BxApiController@getCompany');

    Route::post('/project/add', 'ProjectController@add');
    Route::post('/project/edit', 'ProjectController@edit');
    Route::post('/project/get_one', 'ProjectController@getOne');
    Route::post('/project/calc_amount', 'ProjectController@calcAmount');
    Route::post('/project/get_user_vk', 'ProjectController@getUserVK');
    Route::post('/project/send_to_vk', 'ProjectController@sendProjectToVK');
    Route::post('/project/get_vk_user_id_by_link', 'ProjectController@getVkUserIdByLink');
    Route::post('/project/make_paid', 'ProjectController@makePaid');

    Route::get('/projects/filtered/json', 'DashboardController@getFilteredProjects');
    Route::get('/projects/workers/json', 'UsersController@getUsersJson');

    Route::get('/regions/json', 'RegionsController@getRegionsJson');

    Route::get('/users/photo', 'UsersController@photo');
    Route::get('/users/all', 'UsersController@all');
    Route::get('/users/photo/get', 'UsersController@photoGetOne');
    Route::post('/users/photo/add', 'UsersController@photoAdd');
    Route::post('/users/photo/edit', 'UsersController@photoEdit');
    Route::get('/users/login/{id}', 'UsersController@login');

    Route::get('/users/cord', 'UsersController@cord');
    Route::get('/users/manager', 'UsersController@manager');

    Route::get('/users/profile/{id}', 'UsersController@profile');
    Route::post('/transaction/add', 'UsersController@addTransaction');

    Route::post('/work_market/add_desire', 'WorkMarketController@desireAdd');
    Route::get('/worker/add_link', 'ProjectController@addLink');


    Route::get('/excel/export', 'ExcelExportController@index');
    Route::post('/excel/export/projects', 'ExcelExportController@projects');
    Route::post('/excel/export/transactions', 'ExcelExportController@transactions');

    Route::get('/stats/projects', 'StatsController@projects');
    Route::get('/stats/projects_self_photo', 'StatsController@projectsSelfPhoto');
    Route::get('/stats/projects/json', 'StatsController@projectsJson');
    Route::get('/finance/buh/json', 'StatsController@projectsBuhJson');

    Route::get('/tables/projects_self_photo', 'TablesController@projectsSelfPhoto');
    Route::get('/tables/lookups/statuses.json', 'TablesController@projectStatusesJson');

    Route::get('/finance/cash_flows', 'FinanceController@cashFlows');
    Route::get('/finance/cash_flows/json', 'FinanceController@cashFlowsJson');
    Route::get('/finance/buh', 'FinanceController@buhProjects');
    Route::get('/finance/buh/bills/json', 'FinanceController@billsJson');
    Route::get('/finance/buh/bills/load_from_api', 'FinanceController@billsLoadFromApi');
    Route::get('/finance/buh/bills/load_from_api_progress', 'FinanceController@billsLoadFromApiProgress');
    Route::get('/finance/buh/bills/json_get_data_for_infoboxes', 'FinanceController@billsGetDataForInfoboxesJson');
    Route::get('/finance/buh/bills/force_close', 'FinanceController@billsForceClose');

    Route::get('/finance/transactions', 'FinanceController@transactions');
    Route::get('/finance/transactions/json', 'FinanceController@transactionsJson');

    Route::get('/settings/ext_services', 'SettingsController@externalServices');
    Route::get('/settings/ext_services/vk', 'SettingsController@externalServicesSave');

    Route::get('/short_url', 'ShortUrlController@create')->name('shorturl.url.create');
    Route::post('/short_url', 'ShortUrlController@store')->name('shorturl.url.store');
    Route::get('/short_url/{id}/edit', 'ShortUrlController@edit')->name('shorturl.url.edit');
    Route::put('/short_url/{id}', 'ShortUrlController@update')->name('shorturl.url.update');
    Route::delete('/short_url/{id}', 'ShortUrlController@destroy')->name('shorturl.url.destroy');
    Route::get('/short_url/list', 'ShortUrlController@index')->name('shorturl.url.index');

    Route::get('/logs/projects', 'LogProjects@view');
    Route::get('/logs/project/{id}', 'LogProjects@viewProject');
    Route::get('/logs/projects/json', 'LogProjects@getAllJson');

    Route::get('/test', function(){
        $project = Project::find(1);
        dd([
            'amount' => $project->amount,
            'amount_p' => $project->calc_amount_photo(),
            'tax' =>  $project->calc_tax(),
            'ref' => $project->calc_referral(),
            'ss' => $project->calc_ss_fee(),
            'rsb' => $project->calc_ed_fee()
        ]);
    });

});

// Публичные страницы без авторизации
    Route::get('/project/{uuid}', 'ProjectController@pageForClient')->name('projectPageForClient');

Route::any('/bot.php', 'BotTelegramController@index');
Route::any('/bot', 'BotTelegramController@index');
Route::any('/bottest.php', 'BotTestTelegramController@index');