<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('media:s3', function () {

    $medias = App\Media::where('disk', 'media')->get();
    $bar = $this->output->createProgressBar(count($medias));
    $bar->start();
    $failed = [];
    $fm = new Spatie\MediaLibrary\FileManipulator();
    foreach($medias as $media){

        if( file_exists($media->getPath())){
            $file_content = file_get_contents($media->getPath());
            $s3_name = $media->id .'/' . $media->file_name;
            Storage::disk('s3')->put($s3_name, $file_content);
            $model = $media->model;
            $media->disk = 's3';
            $media->save();
            $fm->createDerivedFiles($media);
        }else{
            $failed[] = $media;
        }
        $bar->advance();
    }
    $bar->finish();

    $this->info('Finished');
    $this->info('Failed: ' . count($failed));

    /*foreach($failed as $media){
        $this->error('#' . $media->id . ' not found');
        $this->error($media->getPath());
        $this->info('');
    }*/

})->describe('Перенос медиа библиотеки в s3');

Artisan::command('media:s3_old', function () {

    $medias = App\Media::where('disk', 'media')->get();
    $bar = $this->output->createProgressBar(count($medias));
    $bar->start();
    $failed = [];
    foreach($medias as $media){
        if( file_exists($media->getPath())){
            $model = $media->model;
            $media->move($model);
        }else{
            $failed[] = $media;
        }
        $bar->advance();
    }
    $bar->finish();

    $this->info('Finished');
    $this->info('Failed: ' . count($failed));

    foreach($failed as $media){
        $this->error('#' . $media->id . ' not found');
        $this->error($media->getPath());
        $this->info('');
    }

})->describe('Перенос медиа библиотеки в s3');
