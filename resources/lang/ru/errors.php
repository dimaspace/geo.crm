<?php

return [

    'token'   => [
        'mismatch' => 'Форма устарела. По соображениям безопастности данные из неё не могут быть приняты...'
    ]

];
