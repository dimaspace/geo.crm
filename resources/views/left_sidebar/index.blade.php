<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="/images/user.png" width="48" height="48" alt="User"/>
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true"
                 aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="/profile"><i class="material-icons">person</i>Профиль</a></li>
                    <!--<li><a href="/profile/password"><i class="material-icons">lock</i>Сменить пароль</a></li>-->
                    <li><a href="/profile/notify/"><i class="material-icons">notifications_active</i>Уведомления</a></li>
                    <li role="seperator" class="divider"></li>
                    <li>

                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                    class="material-icons">input</i>Выйти</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="active">
                <a href="{{ url('/') }}">
                    <i class="material-icons">home</i>
                    <span>Стартовая панель</span>
                </a>
            </li>
            <li class="header">ВАШ ПРОФИЛЬ</li>

                @role('photo')
                    <li>
                        <a href="/profile/busy"><i class="material-icons">wdate_range</i><span>Ваша занятость</span></a>
                    </li>
                @endrole
                <li>
                    <a href="/profile"><i class="material-icons">person</i><span>Профиль</span></a>
                </li>
                @role('photo')
                    <li>
                        <a href="/profile/portfolio"><i class="material-icons">burst_mode</i><span>Ваше портфолио</span></a>
                    </li>
                @endrole
                <li>
                    <a href="/profile/notify/"><i class="material-icons">notifications_active</i><span>Уведомления</span></a>
                </li>
                @permission('view_balance_stats')
                    <li>
                        <a href="/profile/balance/"><i class="material-icons">attach_money</i><span>История баланса</span></a>
                    </li>
                @endpermission

            @if(Entrust::can('portfolio_moderate'))
                @include('left_sidebar.censor.moderate')
            @endif()

            @role('admin')
                @include('left_sidebar.admin.acl')
            @endrole()

            <!--<li class="header">СТАТИСТИКА</li>
            <li>
                <a href="#">
                    <i class="material-icons">help_outline</i>
                    <span>####</span>
                </a>
            </li>-->
            @permission('view_users')
            <li class="header">СОТРУДНИКИ</li>
            <li>
                    <a href="/users/photo"><i class="material-icons">camera_alt</i><span>Фотографы</span></a>
                    <a href="/users/all"><i class="material-icons">people</i><span>Все</span></a>
                    <!--<a href="/users/cord"><i class="material-icons">rv_hookup</i><span>Координаторы</span></a>
                    <a href="/users/manager"><i class="material-icons">child_care</i><span>Менеджеры</span></a>-->
            </li>
            @endpermission
            @permission('view_booker')
                @include('left_sidebar.booker.unpaid')
            @endpermission
            @permission('view_finance')
                @include('left_sidebar.admin.finance')
            @endpermission
            @permission('view_support')
            <li class="header">ПОДДЕРЖКА</li>
                @permission('view_tickets')
                <li>
                        <a href="/tickets"><i class="material-icons">camera_alt</i><span>Тикет-система</span></a>
                </li>
                @endpermission
            @endpermission

            @permission('self_photo_projects_stats')
            <li class="header">ВАШИ ПРОЕКТЫ</li>
                <li>
                    <a href="/tables/projects_self_photo"><i class="material-icons">list</i><span>Ваши проекты</span></a>
                </li>
            @endpermission()

            <li class="header">СТАТИСТИКА</li>
            @permission('all_projects_stats')
            @include('left_sidebar.admin.stats')
            @endpermission()
            @permission('self_photo_projects_stats')
                <li>
                    <a href="/stats/projects_self_photo"><i class="material-icons">contact_mail</i><span>Ваши проекты</span></a>
                </li>
            @endpermission()


            @permission('excel')
                @include('left_sidebar.admin.excel')
            @endpermission()

            @permission('newsletter_add')
                @include('left_sidebar.admin.newsletters')
            @endpermission()

            @permission('dispatch')
                @include('left_sidebar.admin.dispatch')
            @endpermission()

            @permission('sys_settings')
            @include('left_sidebar.admin.settings')
            @endpermission()

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            <?php echo date('Y') ?> &copy; Geo CRM
        </div>
        <div class="version">
            <b>Version: </b> 0.2 ALFA
        </div>
    </div>
    <!-- #Footer -->
</aside>