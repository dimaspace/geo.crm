<li class="header">ACL</li>
<li>
    <a href="/entrust/users"><i class="material-icons">perm_identity</i><span>Юзеры</span></a>
    <a href="/entrust/roles"><i class="material-icons">group</i><span>Роли</span></a>
    <a href="/entrust/permissions"><i class="material-icons">security</i><span>Привилегии</span></a>
</li>