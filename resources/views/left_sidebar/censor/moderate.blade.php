<li class="header">МОДЕРАЦИЯ</li>
@if(Entrust::can('portfolio_moderate'))
<li>
    <a href="{{ route('portfolioModerateIndex') }}"><i class="material-icons">burst_mode</i><span>Портфолио</span></a>
</li>
@endif
@permission('short_url')
<li>
    <a href="{{ route('shorturl.url.index') }}"><i class="material-icons">directions</i><span>Короткие ссылки</span></a>
</li>
@endpermission