@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                ВЫБОР КАНАЛОВ УВЕДОМЛЕНИЙ
            </h2>
        </div>
        <div class="body">
            <form id="notify_channels" method="POST" action="/profile/notify">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">

                        <h2 class="card-inside-title">E-mail</h2>
                        <div class="form-group">

                              <div class="switch">
                                <label>
                                  Off
                                  <input type="checkbox" id="channel_email" name="channels[email]" @if(in_array('email',$channels)) checked @endif >
                                  <span class="lever"></span>
                                  On
                                </label>
                              </div>

                        </div>

                        <h2 class="card-inside-title">Telergam</h2>
                        <div class="form-group">

                            @if($telegram_user_status == false)
                            <div class="card">


                            <div class="body bg-red">
                                ВЫ НЕ ПРИВЯЗАЛИ СВОЙ АККАУНТ К TELEGRAM!
                                                <button type="button" class="btn btn-xs bg-black waves-effect waves-light pull-right" data-toggle="collapse" data-target="#telegram_connect_info" aria-expanded="false" aria-controls="telegram_connect_info">
                                                    ПРИВЯЗАТЬ
                                                </button>
                            </div>
                            <div class="body collapse" id="telegram_connect_info">
                                <b>Ваш личный код для бота:</b> {{$telegram_user_code}}
                                <h4>Способы привязки</h4>
                                <p>Существует несколько способов привязки, выберите один наиболее удобный для вас.</p>
                                <div class="panel-group" id="accordion_telegram_connect" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-col-black">
                                                <div class="panel-heading" role="tab" id="heading_atc_1">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion_telegram_connect" href="#collapse_atc_1" aria-expanded="false" aria-controls="collapse_atc_1">
                                                            #1 Отсканировать QR-код (для планшетов и телефонов)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_atc_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_atc_1">
                                                    <div class="panel-body">
                                                        <ol>
                                                            <li>Запустите на вашем телефоне/планшете приложение для сканирования QR-кодов</li>
                                                            <li>
                                                                <p>Отсканируйте этот код</p>
                                                                <p class="text-center m-b-15">{!! $telegram_user_auth_link_qr_code !!}</p>
                                                            </li>
                                                            <li>Перейдите по полученной после сканирования ссылке. Если устроство спросит: "Чем открыть ссылку?" - выберите Telegram</li>
                                                            <li>В открывшемся диалоге с ботом нажмите на кнопку Начать/Старт/Start</li>
                                                            <li>Всё, Telegram подключен!</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-col-black">
                                                <div class="panel-heading" role="tab" id="heading_atc_2">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_telegram_connect" href="#collapse_atc_2" aria-expanded="false"
                                                           aria-controls="collapse_atc_2">
                                                            #2 Перейти по ссылке (если используете телеграм на компьютере)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_atc_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_atc_2">
                                                    <div class="panel-body">
                                                        <ol>
                                                            <li>Перейдите <a href="{{ $telegram_user_auth_link }}" target="_new">по этой ссылке</a>. Если устроство спросит: "Чем открыть ссылку?" - выберите Telegram Desktop или Telegram Web</li>
                                                            <li>В открывшемся диалоге с ботом нажмите на кнопку Начать/Старт/Start</li>
                                                            <li>Всё, Telegram подключен!</li>
                                                        </ol>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-col-black">
                                                <div class="panel-heading" role="tab" id="heading_atc_3">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_telegram_connect" href="#collapse_atc_3" aria-expanded="false"
                                                           aria-controls="collapse_atc_3">
                                                            #3 Вручную
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_atc_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_atc_3">
                                                    <div class="panel-body">
                                                        <ol>
                                                            <li>Найдите в телеграмм пользователя <b>GeoCRM_Bot</b></li>
                                                            <li>Перейдите в диалог с ним</li>
                                                            <li>Нажмите кнопку Начать/Старт/Start</li>
                                                            <li>Робот сообщит, что не знает кто вы и спросит код авторизации</li>
                                                            <li>Отправьте ему код <i>{{ $telegram_user_code }}</i></li>
                                                            <li>Всё, Telegram подключен!</li>
                                                        </ol>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="switch">
                                <label>
                                    Off
                                    <input type="checkbox" id="channel_telegram" name="channels[telegram]" @if(in_array('telegram',$channels)) checked @endif >
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>
                        @endif



                        </div>

                        <h2 class="card-inside-title">Вконтакте</h2>
                        <div class="form-group">
                            <div class="alert bg-grey">
                                ВРЕМЕННО НЕДОСТУПНО
                            </div>
                        </div>

                        <h2 class="card-inside-title">Facebook</h2>
                        <div class="form-group">
                            <div class="alert bg-grey">
                                ВРЕМЕННО НЕДОСТУПНО
                            </div>
                        </div>

                        <h2 class="card-inside-title">Уведомления в браузере</h2>
                        <div class="form-group">
                            <div class="alert bg-grey">
                                ВРЕМЕННО НЕДОСТУПНО
                            </div>
                        </div>



                        <div class="form-group">
                            <button class="btn bg-teal waves-effect" type="submit">СОХРАНИТЬ</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
                swal("Ура!", "{{ session('success') }}", "success");
            @endif
            @if (session('error'))
                swal("Ошибка!", "{{ session('error') }}", "error");
            @endif
        });
    </script>

@endsection