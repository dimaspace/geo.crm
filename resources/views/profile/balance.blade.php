@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>ИСТОРИЯ БАЛАНСА</h2>

        </div>
        <div class="body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#photo_cashflow_list" data-toggle="tab">ИСТОРИЯ ВВОД/ВЫВОД</a></li>
                <li role="presentation"><a href="#photo_transactions" data-toggle="tab">ТРАНЗАКЦИИ</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade in active" id="photo_cashflow_list">
                    <div class="panel-group" id="accordion_cf" role="tablist" aria-multiselectable="true">
                        @forelse(App\CashFlow::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get() as $cash_flow)
                            <div class="panel cash-flow-list {{ $cash_flow->type == 1 ? 'panel-col-deep-orange' : 'panel-col-light-green' }}">
                                <div class="panel-heading" role="tab" id="headingCF_{{ $cash_flow->id }}">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion_cf" href="#collapseCF_{{ $cash_flow->id }}" aria-expanded="true" aria-controls="collapseCF_{{ $cash_flow->id }}">
                                            #{{ $cash_flow->id }}
                                            <i class="material-icons" title="{{ $cash_flow->type == 1 ? 'Заявка на вывод' : 'Заявка на пополнение' }}">{{ $cash_flow->type == 1 ? 'file_upload' : 'file_download' }}</i>
                                            [{{ $cash_flow->created_at->timezone(config('project.city_time_zone'))->format('d.m.y') }}]
                                            {{ $cash_flow->comment }}
                                            <span class="pull-right">
                                                        <span class="badge {{ $cash_flow->carried == 0 ? 'bg-red' : ($cash_flow->carried == $cash_flow->amount ? 'bg-green' : 'bg-orange') }}">{{ number_format($cash_flow->carried) }}</span> из <span class="badge bg-green">{{ number_format($cash_flow->amount) }}</span>
                                                    </span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseCF_{{ $cash_flow->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCF_{{ $cash_flow->id }}">
                                    <div class="panel-body">
                                        <div class="list-group">
                                            @forelse($cash_flow->transactions()->get() as $transaction)
                                                <div class="list-group-item">
                                                    <span class="badge {{$transaction->value < 0 ? 'bg-red' : 'bg-green' }}">{{$transaction->value > 0 ? '+' : '' }}{{ $transaction->value }}</span>
                                                    <h4 class="list-group-item-heading">Транзакция #{{$transaction->id}} от {{ $cash_flow->created_at->timezone(config('project.city_time_zone'))->format('d.m.y') }}</h4>
                                                    <p class="list-group-item-text">
                                                        <i class="material-icons {{$transaction->accepted ? 'col-teal' : 'col-grey' }} font-40 pull-left m-r-15 m-b-15">{{$transaction->accepted ? 'done' : 'query_builder' }}</i>
                                                        {!! $transaction->descCompiled() !!}<br>
                                                        Связанная транзакция: #{{ $transaction->cross_t_id }}<br>
                                                    </p>
                                                </div>
                                            @empty
                                                Транзакции отсутствуют
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @empty
                            <div class="panel-heading">
                                <h4 class="panel-title">ЗАЯВОК НЕТ</h4>
                            </div>
                        @endforelse
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="photo_transactions">

                    <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
                        @forelse($transactions as $transaction)
                            @if($transaction->project)
                                <div class="panel transactions-list panel-col-clear">
                                    <div class="panel-heading" role="tab" id="heading_{{ $transaction->project->id }}">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion_{{ $transaction->project->id }}" href="#collapse_{{ $transaction->project->id }}" aria-expanded="false"
                                               aria-controls="collapse_{{ $transaction->project->id }}">
                                                @if($transactions_by_project[$transaction->project_id]->sum('value') > 0)
                                                    <i class="material-icons col-teal font-30">add_circle</i>
                                                @else
                                                    <i class="material-icons col-red font-30">remove_circle</i>
                                                @endif
                                                <i class="material-icons">{{ Config::get('project.category_icon')[$transaction->project->category_id] }}</i>#{{ $transaction->id }} [{{ $transaction->created_at->format('d.m.y') }}] Съёмка в {{ $transaction->project->place_cached }}
                                                <span class="badge {{ $transactions_by_project[$transaction->project_id]->sum('value') > 0 ? 'bg-green' : 'bg-red' }}  pull-right">@if($transactions_by_project[$transaction->project_id]->sum('value') > 0) + @endif {{ number_format($transactions_by_project[$transaction->project_id]->sum('value'), 2) }}</span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_{{ $transaction->project->id }}" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="headingThree_17">
                                        <div class="panel-body">
                                            <b>ID проекта</b>: {{ $transaction->project->id }}<br>
                                            <b>Дата проекта</b>: {{ $transaction->project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y H:i') }}<br>
                                            <b>Регион</b>: {{ $transaction->project->region_code }}<br>
                                            <b>Тип проекта</b>: {{ Config::get('project.category')[$transaction->project->category_id] }}<br>
                                            <b>Клиент</b>: {{ $transaction->project->client_cached }}<br>
                                            <b>Описание</b>: {{ $transaction->project->desc_public }}<br>
                                            <b>Стоимость</b>: {{ $transaction->project->calc_amount_photo_whis_tax() }}<br>
                                            <b>Тип оплаты</b>: {{ Config::get('project.payment_type_w')[$transaction->project->payment_type_id_w] }}<br><br>
                                            <ul class="list-group">
                                                @if(isset($transactions_by_project[$transaction->project->id]))
                                                    @foreach($transactions_by_project[$transaction->project->id] as $transaction)
                                                        <li class="list-group-item">
                                                            @if($transaction->value > 0)
                                                                <i class="material-icons col-teal font-30">add_circle</i>
                                                            @else
                                                                <i class="material-icons col-red font-30">remove_circle</i>
                                                            @endif

                                                            {{ $transaction->getType() }} <span class="badge {{ $transaction->value > 0 ? "bg-green" : "bg-red" }}">@if($transaction->value > 0) + @endif {{ number_format($transaction->value, 2) }}</span></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @else

                                <div class="panel transactions-list {{ $transaction->accepted == 1 ? $transaction->value < 0 ? 'panel-col-deep-orange' : 'panel-col-light-green' : 'panel-col-grey' }}">
                                    <div class="panel-heading" role="tab" id="heading_single_{{ $transaction->id }}">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion_single_{{ $transaction->id }}" href="#collapse_single_{{ $transaction->id }}" aria-expanded="false"
                                               aria-controls="collapse_single_{{ $transaction->id }}">
                                                <span class="bg-teal"><i class="material-icons col-teal font-30">{{ $transaction->value > 0 ? 'add_circle' : 'remove_circle' }}</i></span>
                                                @if($transaction->getCrTypeCode() == 'MANUAL')
                                                    <i class="material-icons" title="Создана вручную">build</i>
                                                @endif
                                                <i class="material-icons" title="{{ $transaction->getType() }}">{{ $transaction->getTypeIcon() }}</i>#{{ $transaction->id }} [{{ $transaction->created_at->timezone(config('project.city_time_zone'))->format('d.m.y') }}] <span class="hidden-xs">{!! $transaction->descCompiled() !!}</span>
                                                <span class="badge {{ $transaction->value > 0 ? "bg-green" : "bg-red" }} pull-right">@if($transaction->value > 0) + @endif {{ number_format($transaction->value, 2) }}</span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_single_{{ $transaction->id }}" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="headingThree_17">
                                        <div class="pd-5">
                                            {{ $transaction->created_at->timezone(config('project.city_time_zone'))->timezone(config('project.city_time_zone'))->format('d.m.y') }}<br>
                                            {!! $transaction->descCompiled() !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @empty
                            <div class="panel-heading">
                                <h4 class="panel-title">ОПЕРАЦИИ НЕ ОБНАРУЖЕНЫ</h4>
                            </div>
                        @endforelse
                    </div>

                </div>

            </div>

        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
                swal("Ура!", "{{ session('success') }}", "success");
            @endif
        });
    </script>

@endsection

@push('js_custom')
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?1506" charset="utf-8"></script>
    <script type="text/javascript">
      VK.init({apiId: 6219683});
    </script>

<script type="text/javascript">
@if(!trim(Auth::user()->vk_id))
  VK.Widgets.Auth("vk_auth", {"width":300,"onAuth":vk_auth_callback});
@endif
</script>
@endpush