@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>КАЛЕНДАРЬ ВАШЕЙ ЗАНЯТОСТИ</h2>
            <!--<ul class="header-dropdown m-r--5">
                <li>
                    <button class="btn">
                        <i class="material-icons col-teal">add</i>
                    </button>
                </li>
            </ul>-->
        </div>
        <div class="body">




        <div id="add_range_form">
            <div id="busy_date"></div>
            <div id="busy_time"></div>
            <div id="busy_comment"></div>
            <div>
                <div id="busy_status"></div>
                <div id="busy_submit"></div>
            </div>
        </div>

            <br><br><br>

        <div id="add_range_popup"></div>
    </div>
        <div id="my_calendar"></div>
    <script type="text/javascript">

    </script>

@endsection

@push('js')

    <!--<script src="/plugins/fullcalendar/jquery.min.js"></script>
    <script type="text/javascript">var jQuery_3_3_1 = jQuery.noConflict();</script>
    <script src="/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script src="/plugins/fullcalendar/locale-all.js"></script>
    <script src="/plugins/fullcalendar/scheduler.min.js"></script>-->
@endpush

@push('js_custom')
    <script>
        $(function(){
            var $busy_status = $("#busy_status").dxSelectBox({
                displayExpr: "title",
                valueExpr: "code",
                dataSource: [
                    {
                        code: -3,
                        title: 'не снимаю'
                    },{
                        code: 0,
                        title: 'не знаю'
                    },{
                        code: 3,
                        title: 'снимаю'
                    },
                ],
                value: 0
            }).dxSelectBox("instance");
            var $busy_date = $("#busy_date").dxDateBox({
                type: "date",
                value: new Date(),
                onValueChanged: function(e){
                    var start = moment(e.value).startOf('day');
                    var end = start.clone().endOf('day');

                    var _start = start.clone().add(12, 'hours');
                    var _end = start.clone().add(14, 'hours');

                    $busy_time.option('scale', {
                        startValue: start.toDate(),
                        endValue: end.toDate(),
                        minorTickInterval: "minute",
                        tickInterval: "hour",
                        minRange: "minute",
                        maxRange: "day",
                        minorTick: {
                            visible: false,
                        }
                    });
                    $busy_time.setValue([
                        _start.toDate(),
                        _end.toDate()]
                    )
                }
            }).dxDateBox("instance");
            var $busy_time = $("#busy_time").dxRangeSelector({
                margin: {
                    bottom: 50
                },
                value: [
                    moment().startOf('day').add(12, 'hours').toDate(),
                    moment().startOf('day').add(14, 'hours').toDate()
                ],
                scale: {
                    startValue: moment().startOf('day').toDate(),
                    endValue: moment().add(1, 'days').startOf('day').toDate(),
                    minorTickInterval: "hour",
                    tickInterval: "hour",
                    minRange: "minute",
                    maxRange: "day",
                    minorTick: {
                        visible: false,
                    }
                },
                sliderMarker: {
                    format: "shortDateShortTime"
                },
                //value: [new Date(2011, 1, 5), new Date(2011, 2, 5)],
                title: "Укажите период времени"
            }).dxRangeSelector("instance");

            var $busy_comment = $("#busy_comment").dxTextArea({
                autoResizeEnabled: true,
                placeholder: 'Комментарий',
            }).dxTextArea("instance");

            var busySubmitButtonIndicator;
            $("#busy_submit").dxButton({
                //icon: "check",
                type: "success",
                text: "Добавить",
                onClick: function(e) {
                    e.component.option("text", "Сохранение");
                    e.component.option('disabled', true);
                    busySubmitButtonIndicator.option("visible", true);
                    var status = $busy_status.option('value');
                    var comment = $busy_comment.option('value');
                    var time_range = $busy_time.getValue();
                    var time_start = moment(time_range[0]).unix();
                    var time_end = moment(time_range[1]).unix();
                    $.ajax({
                        url: '/profile/busy/',
                        type: "POST",
                        data: {
                            from: time_start,
                            to: time_end,
                            status: status,
                            comment: comment
                        },
                        success: function(data){
                            $myCalendar.getDataSource().reload();
                            busySubmitButtonIndicator.option("visible", false);
                            e.component.option("text", "Добавить");
                            e.component.option("disabled", false);
                            $busy_comment.option("value", '');
                        }
                    });


                },
                template: function(data, container) {
                    $("<div class='button-indicator'></div><span class='dx-button-text'>" + data.text + "</span>").appendTo(container);
                    busySubmitButtonIndicator = container.find(".button-indicator").dxLoadIndicator({
                        visible: false
                    }).dxLoadIndicator("instance");

                }
            });

            var storeMyCalendar = new DevExpress.data.CustomStore({
                cacheRawData: false,
                load: function(e) {
                    var query = "/profile/busy/json?";
                    var filter_query = '';
                    if(e.dxScheduler){
                        filter_query += 'start=' + moment(e.dxScheduler.startDate).unix();
                        filter_query += '&end=' + moment(e.dxScheduler.endDate).unix()
                    }
                   query = query + filter_query;

                    return $.getJSON(query);
                }
            });

            var $myCalendar = $("#my_calendar").dxScheduler({
                editing: {
                    allowAdding: false,
                    allowDeleting: false,
                    allowUpdating: false,
                    allowResizing: false,
                    allowDragging: false
                },
                adaptivityEnabled: true,
                dataSource:  storeMyCalendar,//'/profile/busy/json?',
                startDateExpr: "startDate",
                endDateExpr: "endDate",
                remoteFiltering: true,
                timeZone: window.cfg_time_zone,
                views: ["day", "week", "month", "agenda"],
                currentView: "day",
                firstDayOfWeek: 1,
                currentDate: new Date(),
                //startDayHour: 9,
                //height: 1048,
                onAppointmentDblClick: function(e){
                    e.cancel = true;
                },
                onAppointmentClick: function(e){
                    e.cancel = true;
                },
                onContentReady(e){
                    if(e.component.option('currentView') === "month"){
                        e.component.option('height', 700);
                    }else{
                        e.component.option('height', 'auto');
                    }
                },

                appointmentTemplate: function(data, index, container) {
                    container.addClass('event_' + data.busy);
                    var time = moment(data.startDate).format('HH:mm');
                    time += '-' + moment(data.endDate).format('HH:mm');
                    return $("<div class='busy_text'>" + time + ' '+ data.text + "<br>" + data.comment + "</div>");
                },
            }).dxScheduler("instance");

            function sendRequest(url, method, data, return_full = false) {
                var d = $.Deferred();

                method = method || "GET";



                $.ajax(url, {
                    method: method || "GET",
                    data: data,
                    cache: false,
                    xhrFields: { withCredentials: true }
                }).done(function(result) {
                    d.resolve(method === "GET" ? (result.data ? result.data: result ) : result);
                }).fail(function(xhr) {
                    d.reject(xhr.responseJSON ? xhr.responseJSON.message : xhr.statusText);
                });

                return d.promise();
            }

        });
    </script>
    <!--<script src="{{ mix('js/profile/my_calendar_fullcalendar.js') }}"></script>-->
@endpush

@push('css')
    <!--<link rel="stylesheet" href="/plugins/fullcalendar/fullcalendar.min.css" type="text/css">
    <link rel="stylesheet" href="/plugins/fullcalendar/scheduler.min.css" type="text/css">-->
@endpush

