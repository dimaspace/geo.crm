@extends('layouts.main')

@section('content')

    <div class="card">
        <!--<div class="header">
            <h2>
                ОБЩАЯ ИНФОРМАЦИЯ
            </h2>
        </div>-->
        <div class="body">
            <ul class="nav nav-tabs tab-col-teal" role="tablist">
                <li role="presentation" class="{{ request('active_tab', 'home') == 'home' ? 'active' : '' }}">
                    <a href="#home" data-toggle="tab">
                        <i class="material-icons">home</i> Общие
                    </a>
                </li>
                <li role="presentation" class="{{ request('active_tab', '') == 'avatar' ? 'active' : ''  }}">
                    <a href="#avatar" data-toggle="tab">
                        <i class="material-icons">face</i> Ваше фото
                    </a>
                </li>
                <li role="presentation" class="{{ request('active_tab', '') == 'price' ? 'active' : ''  }}">
                    <a href="#price" data-toggle="tab">
                        <i class="material-icons">attach_money</i> Расценки
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade {{ request('active_tab', 'home') == 'home' ? 'in active' : ''  }}" id="home">
                    <form id="profile" method="POST" action="/profile">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-4">

                                <label for="email" class="form-label">E-Mail</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="email" id="email" value="{{ Auth::user()->email }}"
                                               class="form-control" required>
                                    </div>
                                </div>

                                <label for="name" class="form-label">Имя, Фамилия</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" id="name" value="{{ Auth::user()->name }}"
                                               class="form-control"
                                               required>
                                    </div>
                                </div>

                                <label for="about" class="form-label">О себе(на страницу портфолио)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea name="about" id="about" class="form-control no-resize auto-growth">{{ Auth::user()->info->about }}</textarea>
                                    </div>
                                </div>

                                <label for="phone" class="form-label">Телефон</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="phone" id="phone"
                                               value="@if(Auth::user()->info){{ Auth::user()->info->phone }}@endif"
                                               class="form-control" required>
                                    </div>
                                </div>

                                <label for="geo_login" class="form-label">Ссылка на ваш профиль GEO</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="geo_login" id="geo_login"
                                               value="@if(Auth::user()->info){{ Auth::user()->info->geo_login }}@endif"
                                               class="form-control" required>
                                    </div>
                                </div>

                                <label for="geo_login" class="form-label">Привязка профиля VK</label>
                                <div class="form-group">
                                    <input type="hidden" name="vk_id" id="vk_id"
                                           value="@if(trim(Auth::user()->vk_id)){{ Auth::user()->vk_id }}@endif"
                                           class="form-control" required>
                                    @if(trim(Auth::user()->vk_id))
                                        <div class="form-line col-grey"><i
                                                    class="material-icons col-teal font-20">done</i> {{ Auth::user()->vk_id }}
                                        </div>
                                    @else
                                    <!-- VK Widget -->
                                        <div id="vk_auth"></div>
                                    @endif

                                </div>


                                <div class="form-group">
                                    <button class="btn bg-teal waves-effect" type="submit">СОХРАНИТЬ</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade {{ request('active_tab', '') == 'avatar' ? 'in active'  : '' }}" id="avatar">
                    Тут будет загрузка фото
                </div>
                <div role="tabpanel" class="tab-pane fade {{ request('active_tab', '') == 'price' ? 'in active'  : '' }}" id="price">
                    <form id="profile" method="POST" action="/profile/price">
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class=" col-sm-12">
                                <input type="submit" class="btn btn-lg bg-teal">
                            </div>
                            @foreach(\Spatie\Tags\Tag::getWithType('photo') as $tag)
                                <div class=" col-xm-12 col-sm-6 col-md-4 col-lg-3">
                                    <label for="tag_price_{{ $tag->id }}" class="form-label">{{ $tag->name }}</label>
                                    <div class="input-group input-group-sm">
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="tags[{{ $tag->id }}]" class="filled-in"
                                               id="tag_{{ $tag->id }}" data-tag-id="{{ $tag->id }}"
                                               {{ Auth::user()->getPriceByTagId($tag->id) ? 'checked' : '' }}
                                        >
                                        <label for="tag_{{ $tag->id }}"></label>
                                    </span>
                                        <div class="form-line">
                                            <input type="text" name="tag_price[{{ $tag->id }}]"
                                                   id="tag_price_{{ $tag->id }}" value="{{ Auth::user()->getPriceByTagId($tag->id) ?? '' }}" class="form-control tag_price"
                                                    data-tag-id="{{ $tag->id }}">
                                        </div>
                                        <span class="input-group-addon">руб/час</span>
                                    </div>
                                </div>
                            @endforeach
                            <div class=" col-sm-12">
                                <input type="submit" class="btn btn-lg bg-teal">
                            </div>

                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
            swal("Ура!", "{{ session('success') }}", "success");
            @endif
        });
    </script>

@endsection

@push('js_custom')
    <script src="/plugins/jquery-input-number-format/jquery-inputformat.min.js"></script>
    <script>
        $('.tag_price').inputNumber({
            allowDecimals: false,
            allowNegative: false,
            allowLeadingZero: false,
            thousandSep: '  ',
            maxDecimalDigits: 0
        });
        $('.tag_price').on('keyup', function () {
            var $this = $(this);
            var value = $this.val();
            var tag_id = $this.data('tag-id');
            var $check_box = $('#tag_' + tag_id);
            console.log($check_box.prop('checked'));
            if (value != '' && value != 0) {
                console.log('не пусто')
                $check_box.prop('checked', true);
            } else {
                console.log('пусто')
                $check_box.prop('checked', false);
            }
        });
    </script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?1506" charset="utf-8"></script>
    <script type="text/javascript">
        VK.init({apiId: 6219683});
    </script>

    <script type="text/javascript">
        @if(!trim(Auth::user()->vk_id))
        VK.Widgets.Auth("vk_auth", {"width": 300, "onAuth": vk_auth_callback});
        @endif
    </script>
@endpush