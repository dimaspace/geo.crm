@extends('layouts.sign2')

@section('content')

    <form id="sign_up" method="POST" action="/profile">
        {{ csrf_field() }}
        <input type="hidden" name="register" value="1">
        <div class="msg">Укажите данные о себе</div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">email</i>
            </span>
            <div class="form-line">
                <p class="font-24">{{ Auth::user()->email }}</p>
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">person</i>
            </span>
            <div class="form-line">
                <input type="text" class="form-control" name="name" placeholder="Имя, Фамилия" required autofocus>
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">phone</i>
            </span>
            <div class="form-line">
                <input type="text" class="form-control" name="phone" placeholder="Телефон" required>
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">link</i>
            </span>
            <div class="form-line">
                <input type="text" class="form-control" name="geo_login" placeholder="Ссылка на ваш профиль GEO" required>
            </div>
        </div>


        <button class="btn btn-block btn-lg bg-teal waves-effect" type="submit">ВОЙТИ</button>

    </form>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
        });
    </script>

@endsection