<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Ошибка @yield('code')</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,latin-ext" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ mix('css/style.css') }}" rel="stylesheet">
</head>

<body class="five-zero-zero">
    <div class="five-zero-zero-container">

        <div class="error-code">УПС!</div>
        <div class="error-message">ЧТО-ТО ПО ПОШЛО СОВСЕМ НЕ ТАК.</div>
        <div class="button-place hidden">
            <a href="javascript:send_raven_report()" class="btn btn-default btn-lg waves-effect"><i class="material-icons">message</i><span>ОТПРАВИТЬ ОТЧЁТ</span></a>
        </div>

    @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
        <div class="sentry-error-id">ID ошибки: {{ Sentry::getLastEventID() }}</div>

        <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

        <script>
            function send_raven_report(){
                Raven.showReportDialog({
                    eventId: '{{ Sentry::getLastEventID() }}',
                    dsn: 'https://88a4a74f160f4f388402b0f1dc467ddf@sentry.io/1426133',
                    title: 'В системе произошёл непредвиденный сбой',
                    lang: 'ru'
                });
            }
        </script>
    @endif

    </div>

    <!-- Jquery Core Js -->
    <script src="/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/plugins/node-waves/waves.js"></script>
</body>

</html>