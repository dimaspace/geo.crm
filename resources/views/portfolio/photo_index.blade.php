@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <div id="portfolio_photo_toolbar"></div>
            <div id="portfolio_photo_tile"></div>
            <div id="portfolio_photo_popup"></div>
            <div id="portfolio_photo_loader"></div>
            <div id="portfolio_gal"></div>
        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
                swal("Ура!", "{{ session('success') }}", "success");
            @endif
            @if ($errors->any())
                swal("Ошибка!", "{{ $errors->first() }}", "error");
            @endif
        });
    </script>

@endsection

@push('js_custom')
            @javascript('portfolio_moderate', $moderate_mode);
            @javascript('portfolio_vote', $voter_mode);
            @javascript('portfolio_edit_tags', $edit_mode);
    <script src="/plugins/jquery-colorbox/jquery.colorbox.js"></script>

    <script src="{{ mix('js/profile/portfolio.js') }}"></script>
@endpush

@push('css')
    <link type="text/css" rel="stylesheet" href="/plugins/jquery-colorbox/skins/3/colorbox.css" />
@endpush