@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <form enctype='multipart/form-data' method='post' action="{{ route('portfolioPhotoAddSubmit') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div id="form_photo_add"></div>
                <div class="content" id="selected-files"></div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
                swal("Ура!", "{{ session('success') }}", "success");
            @endif
            @if ($errors->any())
                swal("Ошибка!", "{{ $errors->first() }}", "error");
            @endif
        });
    </script>

@endsection

@push('js_custom')
    @javascript('portfolio_upload_by_another_user', $upload_by_another_user);
    <script src="{{ mix('js/profile/portfolio.js') }}"></script>
@endpush