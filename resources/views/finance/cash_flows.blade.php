@extends('layouts.main')

@section('content')

<div class="card">
    <div class="body">
        <div id="gridContainer"></div>
        <div class="options">
            <div class="caption">Options</div>
            <div class="option">
                <div id="autoExpand"></div>
            </div>
        </div>
    </div>
</div>

@push('js_custom')
    <script src="/js/finance/cash_flows.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

@endsection