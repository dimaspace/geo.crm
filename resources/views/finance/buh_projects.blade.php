@extends('layouts.main')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="info-box bg-deep-orange" id="unpaid_bills">
                <div class="icon">
                    <i class="material-icons">description</i>
                </div>
                <div class="content">
                    <div class="text">
                        НЕОПЛАЧЕННЫХ СЧЕТОВ
                    </div>
                    <div class="number">
                        {{ $unpaid_bills_num }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="info-box bg-red" id="deadline_bills">
                <div class="icon">
                    <i class="glyphicon glyphicon-fire font-26" aria-hidden="true" title="Просрочено"></i>
                </div>
                <div class="content">
                    <div class="text">
                        ПРОСРОЧЕННЫХ СЧЕТОВ
                    </div>
                    <div class="number">
                        {{ $deadline_exp_unpaid_bills_num }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="card">
        <div class="header">
            <h2>
                НЕОПЛАЧЕННЫЕ СЧЕТА
            </h2>
        </div>
        <div class="body">

            <div id="gridContainerBill"></div>
            <div class="options">
                <div class="caption">Настройки</div>
                <div class="option">
                    <div id="autoExpandBill"></div>
                </div>
            </div>

        </div>
        <div id="popup"></div>

    </div>

    <div class="card">
        <div class="header">
            <h2>
                ПРОЕКТЫ
            </h2>
        </div>
        <div class="body">

            <div id="gridContainer"></div>
            <div class="options">
                <div class="caption">Настройки</div>
                <div class="option">
                    <div id="autoExpand"></div>
                </div>
            </div>

        </div>


@push('js_custom')
    <script src="/js/stats/buh.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        @if (session('success'))
            swal("Ура!", "{!! session('success')  !!}", "success");
        @endif
        @if (session('error'))
            swal("Ошибка!", "{!! session('error')  !!}", "error");
        @endif
    });
</script>

@endsection