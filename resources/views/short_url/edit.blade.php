@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                СОКРАЩАТЕЛЬ ССЫЛОК
            </h2>
        </div>
        <div class="body">
            <div class="col-12">
                @if (session('short_url'))
                    <div class="alert bg-teal" role="alert">
                        Ваш короткий URL: <a class="font-weight-bold alert-link" href="{{ session('short_url') }}"
                                             title="Ваш короткий Url">{{ session('short_url') }}</a> (<a
                                class="copy-clipboard alert-link" href="javascript:void(0);"
                                data-clipboard-text="{{ session('short_url') }}">Скопировать</a>)
                    </div>
                @endif
                <form method="POST" action="{{ route('shorturl.url.update', $url->id) }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="input-group">
                        <div class="form-line" style="display: inline">
                            <input type="text"
                                   class="form-control form-control-lg {{ $errors->has('url') ? 'is-invalid' : '' }}"
                                   id="url" name="url" placeholder="Paste an url" aria-label="Paste an url"
                                   value="{{ old('url', $url->url) }}">
                        </div>
                        <div class="input-group-append">
                            <button class="btn bg-teal" type="submit">Сохранить</button>
                        </div>
                    </div>
                    @if ($errors->has('url'))
                        <small id="url-error" class="form-text text-danger">
                            {{ $errors->first('url') }}
                        </small>
                    @endif

                    <div class="col-4">
                        <div class="form-group">
                            <label for="code">Кастомный код (не обязательно)</label>
                            <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                   id="code" name="code" placeholder="Set your custom alias"
                                   value="{{ old('code', $url->code) }}">
                            @if ($errors->has('code'))
                                <small id="code-error" class="form-text text-danger">
                                    {{ $errors->first('code') }}
                                </small>
                            @endif
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@push('js_custom')
    <script src="/plugins/clipboard/clipboard.min.js"></script>
    <script>
        var clipboard = new ClipboardJS('.copy-clipboard');

        clipboard.on('success', function (e) {
            e.trigger.innerText = 'Скопировано!';
            DevExpress.ui.dialog.alert('Ссылка скопирована!');
        });
    </script>
@endpush