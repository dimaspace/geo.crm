@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                СОКРАЩАТЕЛЬ ССЫЛОК
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="{{ route('shorturl.url.create') }}"><i
                                class="material-icons col-teal">add_box</i></a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="col-8 offset-2">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-condensed">
                        <tr>
                            <th></th>
                            <th>Url</th>
                            <th>Сокращение</th>
                        </tr>
                        @foreach ($urls as $url)
                            <tr>
                                <td width="180px" class="align-center">
                                    <button class="btn btn-sm bg-teal copy-clipboard"
                                            data-clipboard-text="{{ route('shorturl.redirect', $url->code) }}"
                                            title="Копировать"><i class="material-icons">content_copy</i></button>
                                    <a class="btn btn-sm btn-primary" href="{{ route('shorturl.url.edit', $url->id) }}"
                                       role="button" title="Править"><i class="material-icons">mode_edit</i></a>
                                    <form method="POST" action="{{ route('shorturl.url.destroy', $url->id) }}"
                                          style="display: inline">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button class="btn btn-sm btn-danger" href="#" role="button" title="Удалить"><i
                                                    class="material-icons">delete_forever</i></button>
                                    </form>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <div class="form-line" style="display: inline">
                                            <input type="text" class="form-control" value="{{ $url->url }}">
                                        </div>
                                    </div>

                                </td>
                                <td><a href="{{ route('shorturl.redirect', $url->code) }}">{{ $url->code }}</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                {{ $urls->links() }}
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        form {
            display: inline-block;
        }

        .wrapper {
            min-height: 100vh;
        }

        .pagination {
            justify-content: flex-end;
        }
    </style>
@endpush

@push('js_custom')
    <script src="/plugins/clipboard/clipboard.min.js"></script>
    <script>
        var clipboard = new ClipboardJS('.copy-clipboard');

        clipboard.on('success', function (e) {
            e.trigger.innerText = 'Скопировано!';
            DevExpress.ui.dialog.alert('Ссылка скопирована!');
        });

        @if (session('short_url'))
        DevExpress.ui.dialog.alert('Ссылка удалена!');
        @endif

    </script>
@endpush