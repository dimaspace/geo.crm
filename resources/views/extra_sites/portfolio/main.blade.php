
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Geo Portfolio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="/portfolio/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|Roboto+Slab:700|Roboto:400,500,700|Ubuntu:400,700|Vollkorn:700" rel="stylesheet">
        @stack('css')
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="/portfolio/images/favicon.ico">
    </head>
    <body>
        <div class="loader">
            <div class="tm-loader">
                <div id="circle"></div>
            </div>
        </div>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            @include('extra_sites.portfolio.header')
            <!-- Header   end-->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!--=============== content-holder ===============-->
                <div class="content-holder elem scale-bg2 transition3">
                    <!-- Page title -->
                    <div class="dynamic-title"></div>
                    <!-- Page title  end-->
                    <!--  Navigation -->
                    @include('extra_sites.portfolio.left_menu')
                    <!--  Navigation end -->
                    <!--  Content  -->
                    <div class="content ">
                        @yield('content')
                        <section class="no-padding no-border no-bg">
                            <div class="fixed-info-container">
                                <!-- Filters-->
                                <div class="filter-holder filter-vis-column">
                                    <div class="gallery-filters at">
                                        <a href="{{ $all_tags_link }}" class="gallery-filter {{ $all_tags_link_active ? 'gallery-filter-active' : '' }}"  data-filter=".all_tags">ВСЕ ТЕГИ<span class="filter-count"></span></a>
                                        @foreach($tags as $tag)
                                            <a href="{{ $tag->link }}" class="gallery-filter {{ $tag->selected ? 'gallery-filter-active' : '' }}" data-filter=".{{ $tag->slug }}">{{ $tag->name }}<span class="filter-count"></span></a>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- Filters end  -->
                            </div>
                            <!-- fixed-info-container  end -->
                            <!-- resize-carousel-holder -->
                            <div class="resize-carousel-holder vis-info">
                                <!-- gallery-items -->
                                <div class="gallery-items hid-port-info grid-small-pad">
                                    <!-- 1 -->
                                    @foreach($items as $item)
                                    <div class="gallery-item {{ $item->tags()->get()->pluck('slug')->implode(' ') }} author_{{ $item->model->id }} all_tags all_authors">
                                        <div class="grid-item-holder">
                                            <div class="box-item">
                                                <a href="{{ $item->getUrl('main') }}" class="portfolio-item magnific"><img  src="{{ $item->getUrl('thumb_fit_250') }}"   alt=""></a>
                                            </div>
                                            <div class="grid-item">
                                                <h3><a href="{{ route('extraPortfolioUserPage', ['user' => $item->model->id]) }}" class="portfolio-link">{{ $item->model->is_staff ? $item->model->name : 'Внештатный фотограф #' . $item->model->id }}</a></h3>
                                                <span>{{ $item->tags()->get()->pluck('name')->implode(', ') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                    <!-- 1 end -->
                                    {{ $items->appends(['filter' => $filter])->links() }}
                                </div>
                                <!-- end gallery items -->
                                <!-- Filters-->
                                <div class="filter-holder filter-nvis-column">
                                    <div class="gallery-filters at">
                                        <a href="{{ $all_users_link }}" class="gallery-filter  {{ $all_users_link_active ? 'gallery-filter-active' : '' }}" data-filter=".all_authors">Все<span class="filter-count"></span></a>
                                        @foreach($users as $user)
                                            <a href="{{ $user->link }}" class="{{ $user->is_staff ? '' : 'user_un_staff'}} gallery-filter {{ $user->selected ? 'gallery-filter-active' : '' }}" data-filter=".author_{{ $user->id }}">{{ $user->is_staff ? $user->name : 'Внештатный фотограф #' . $user->id }}<span class="filter-count"></span></a>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- filters end -->
                                <!--  filter-button-->
                                <div class="filter-button vis-fc">ФИЛЬТР ФОТОГРАФОВ</div>

                            </div>
                        </section>
                    </div>
                    <!--  Content  end -->
                    <!-- share  -->
                    <div class="share-inner">
                        <div class="share-container  isShare"  data-share="['vk','ok','facebook','googleplus','twitter']"></div>
                        <div class="close-share"></div>
                    </div>
                    <!-- share end -->
                </div>
                <!-- Content holder  end -->
            </div>
            <!-- wrapper end -->
            <!--=============== footer ===============-->
            <footer>
                <div class="policy-box">
                    <span>&#169; Geometria {{ Carbon\Carbon::now()->format('Y') }}  /  Za kopirovanie presleduem i vse takoe. </span>
                </div>
                <!--<div class="footer-social">
                    <ul>
                        <li><a href="#" target="_blank" ><i class="fa fa-vk"></i><span>vk</span></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i><span>facebook</span></a></li>
                    </ul>
                </div>-->
            </footer>
            <!-- footer end -->
        </div>
        <!-- Main end -->
        <!--=============== google map ===============-->
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script>-->
        <!--=============== scripts  ===============-->
        <!--<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>-->
        <script type="text/javascript" src="/portfolio/js/all.js"></script>
        @stack('js')
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        @include('extra_sites.portfolio.partials.vk_widgets')
    </body>
</html>