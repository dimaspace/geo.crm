
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Geo Portfolio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="/portfolio/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|Roboto+Slab:700|Roboto:400,700|Ubuntu:400,700|Vollkorn:700" rel="stylesheet">
        @stack('css')
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="/portfolio/images/favicon.ico">
    </head>
    <body>
        <div class="loader">
            <div class="tm-loader">
                <div id="circle"></div>
            </div>
        </div>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header>
                <!-- Nav button-->
                <!--<div class="nav-button">
                    <span  class="nos"></span>
                    <span class="ncs"></span>
                    <span class="nbs"></span>
                </div>-->
                <!-- Nav button end -->
                <!-- Logo-->
                <div class="logo-holder">
                    <a href="/" class="ajax"><img src="/portfolio/images/logo_geo.png" alt=""></a>
                </div>
                <!-- Logo  end-->
                <!-- Header  title -->
                <div class="header-title">
                    <h2><a class="ajax" href="#"></a></h2>
                </div>
                <!-- Header  title  end-->
                <!-- share -->
                <div class="show-share isShare">
                    <span>&nbsp;</span>
                    <i class="fa fa-share-alt fa-2x"></i>
                </div>
                <!-- share  end-->
            </header>
            <!-- Header   end-->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!--=============== content-holder ===============-->
                <div class="content-holder elem scale-bg2 transition3">
                    <!-- Page title -->
                    <!--<div class="dynamic-title">Geo Portfolio</div>-->
                    <!-- Page title  end-->
                    <!--  Navigation -->
                    <div class="nav-overlay"></div>
                    {{--<div class="nav-inner isDown">
                        <nav>
                            <ul>
                                <li class="subnav">
                                    <a href="index.html" class="ajax">Home</a>
                                    <!--  Subnav  -->
                                    <ul>
                                        <li>
                                            <a href="index.html" class="ajax">Slideshow </a>
                                        </li>
                                        <li>
                                            <a href="index2.html" class="ajax">Multi Slideshow 4</a>
                                        </li>
                                        <li>
                                            <a href="index3.html" class="ajax">Multi Slideshow 3</a>
                                        </li>
                                        <li>
                                            <a href="index4.html" class="ajax">Video </a>
                                        </li>
                                        <li>
                                            <a href="index5.html" class="ajax">Single image</a>
                                        </li>
                                        <li>
                                            <a href="index6.html" class="ajax">Slider </a>
                                        </li>
                                        <li>
                                            <a href="index7.html" class="ajax">Visible Menu</a>
                                        </li>
                                    </ul>
                                    <!--  Subnav  end-->
                                </li>
                                <li><a href="about.html" class="ajax"> About </a></li>
                                <li class="subnav">
                                    <a href="portfolio.html" class="ajax active">Work</a>
                                    <!--  Subnav  -->
                                    <ul>
                                        <li>
                                            <a href="portfolio.html" class="ajax">Style 1</a>
                                        </li>
                                        <li>
                                            <a href="portfolio2.html" class="ajax">Style 2</a>
                                        </li>
                                        <li>
                                            <a href="portfolio3.html" class="ajax" >Style 3</a>
                                        </li>
                                        <li>
                                            <a href="portfolio4.html" class="ajax" >Style 4</a>
                                        </li>
                                        <li>
                                            <a href="portfolio5.html" class="ajax" >Style 5</a>
                                        </li>
                                        <li>
                                            <a href="portfolio6.html" class="ajax" >Style 6</a>
                                        </li>
                                    </ul>
                                    <!--  Subnav  end-->
                                </li>
                                <li><a href="services.html" class="ajax">Services</a></li>
                                <li><a href="contact.html" class="ajax">Contact</a></li>
                                <li class="subnav">
                                    <a href="blog.html" class="ajax">Journal</a>
                                    <!--  Subnav  -->
                                    <ul>
                                        <li>
                                            <a href="blog.html" class="ajax">4 column</a>
                                        </li>
                                        <li>
                                            <a href="blog2.html" class="ajax">3 column</a>
                                        </li>
                                        <li>
                                            <a href="blog-single.html" class="ajax">Single </a>
                                        </li>
                                    </ul>
                                    <!--  Subnav  end-->
                                </li>
                                <li class="subnav">
                                    <a href="#">Portfolio single</a>
                                    <!--  Subnav  -->
                                    <ul>
                                        <li>
                                            <a href="portfolio-single.html" class="ajax">Style 1</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single2.html" class="ajax">Style 2</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single3.html" class="ajax" >Style 3</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single4.html" class="ajax" >Style 4</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single5.html" class="ajax" >Style 5</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single6.html" class="ajax" >Style 6</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single7.html" class="ajax" >Style 7</a>
                                        </li>
                                        <li>
                                            <a href="404.html" class="ajax" >404</a>
                                        </li>
                                    </ul>
                                    <!--  Subnav  end-->
                                </li>
                            </ul>
                        </nav>--}}
                    </div>
                    <!--  Navigation end -->
                    <!--  Content  -->
                    <div class="content full-height no-padding">
                        @yield('content')
                        <section class="no-padding no-border no-bg">
                            <div class="fixed-info-container">
                            <!--  content-nav
                            <div class="content-nav">
                                <ul>
                                    <li>
                                        @if($user_prev)
                                            <a href="{{ route('extraPortfolioUserPage', ['user' => $user_prev->id]) }}" class="ajax ln"><i class="fa fa fa-angle-left"></i></a>
                                        @else
                                            &nbsp;
                                        @endif
                                    </li>
                                    <li>
                                        <div class="list">
                                            <a href="portfolio.html" class="ajax">
                                            <span>
                                            <i class="b1 c1"></i><i class="b1 c2"></i><i class="b1 c3"></i>
                                            <i class="b2 c1"></i><i class="b2 c2"></i><i class="b2 c3"></i>
                                            <i class="b3 c1"></i><i class="b3 c2"></i><i class="b3 c3"></i>
                                            </span></a>
                                        </div>
                                    </li>
                                    <li>
                                        @if($user_next)
                                            <a href="{{ route('extraPortfolioUserPage', ['user' => $user_next->id]) }}" class="ajax rn"><i class="fa fa fa-angle-right"></i></a>
                                        @else
                                            &nbsp;
                                        @endif
                                    </li>
                                </ul>
                            </div>
                              content-nav end-->
                            <h3>{{ $user->is_staff ? $user->name : 'Внештатный фотограф #' . $user->id }}</h3>
                            <div class="separator"></div>
                            <div class="clearfix"></div>
                            <p>{{ nl2br($user->info->about) }}</p>
                            <h4>Виды съёмок</h4>
                            <ul class="project-details">
                                @forelse($prices as $price)
                                <li><span>{{$price->tag->name}} :</span> {{ $price->price }} {{ $price->type_for_front }}</li>
                                @empty
                                    не указаны
                                @endforelse
                            </ul>

                            </div>
                            <!-- fixed-info-container  end -->
                        <!--  resize-carousel-holder-->
                        <div class="resize-carousel-holder vis-info gallery-horizontal-holder">
                            <!--  gallery_horizontal-->
                            <div id="gallery_horizontal" class="gallery_horizontal owl_carousel">
                                @forelse($portfolio as $item)
                                <!-- gallery Item-->
                                <div class="horizontal_item">
                                    <div class="zoomimage"><img src="{{ $item->getUrl('main') }}" class="intense" alt=""><i class="fa fa-expand"></i></div>
                                    <img src="{{ $item->getUrl('main') }}" alt="">
                                    <div class="show-info">
                                        <span>{{ $item->tags()->get()->pluck('name')->implode(', ') }}</span>
                                        <!--<div class="tooltip-info">
                                            <h5>Тэги</h5>
                                            <p>{{ $item->tags()->get()->pluck('name')->implode(',&nbsp;&nbsp;') }}</p>
                                        </div>-->
                                    </div>
                                </div>
                                <!-- gallery item end-->
                                @empty
                                @endforelse

                            </div>
                            <!--  resize-carousel-holder-->
                            <!--  navigation -->
                            <div class="customNavigation">
                                <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
                                <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <!--  navigation end-->
                        </div>
                        <!--  gallery_horizontal end-->

                        </section>
                    </div>
                    <!--  Content  end -->
                    <!-- share  -->
                    <div class="share-inner">
                        <div class="share-container  isShare"  data-share="['vk','ok','facebook','googleplus','twitter']"></div>
                        <div class="close-share"></div>
                    </div>
                    <!-- share end -->
                </div>
                <!-- Content holder  end -->
            </div>
            <!-- wrapper end -->
            <!--=============== footer ===============-->
            <footer>
                <div class="policy-box">
                    <span>&#169; Geometria {{ Carbon\Carbon::now()->format('Y') }}  /  Za kopirovanie presleduem i vse takoe. </span>
                </div>
                <!--<div class="footer-social">
                    <ul>
                        <li><a href="#" target="_blank" ><i class="fa fa-vk"></i><span>vk</span></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i><span>facebook</span></a></li>
                    </ul>
                </div>-->
            </footer>
            <!-- footer end -->
        </div>
        <!-- Main end -->
        <!--=============== google map ===============-->
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script>-->
        <!--=============== scripts  ===============-->
        <!--<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>-->
        <script type="text/javascript" src="/portfolio/js/all.js"></script>
        @stack('js')
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </body>
</html>