    <script src="https://vk.com/js/api/openapi.js?162" type="text/javascript"></script>
    <div id="vk_community_messages"></div>

<span class="vk_msg_button fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-comments fa-stack-1x fa-inverse"></i>
</span>


    <script type="text/javascript">
        var vk_msg_widget = VK.Widgets.CommunityMessages("vk_community_messages", {{ config('project.vk_groups_id')[config('project.region_default')] }}, {
            widgetPosition: 'right',
            buttonType: 'no_button'
        });


        $('.vk_msg_button').click(function () {
            vk_msg_widget.expand({
                welcomeScreen: 1,
            });
        });

    </script>