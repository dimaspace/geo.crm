<header>
    <!-- Nav button-->
    <div class="nav-button">
        <span  class="nos"></span>
        <span class="ncs"></span>
        <span class="nbs"></span>
    </div>
    <!-- Nav button end -->
    <!-- Logo-->
    <div class="logo-holder">
        <a href="/" class=""><img src="/portfolio/images/logo_geo.png" alt=""></a>
    </div>
    <!-- Logo  end-->
    <!-- Header  title -->
    <div class="header-title">
        <h2><a class="" href="#"></a></h2>
    </div>
    <!-- Header  title  end-->
    <!-- share -->
    <div class="show-share isShare">
        <span>&nbsp;</span>
        <i class="fa fa-share-alt fa-2x"></i>
    </div>
    <!-- share  end-->
</header>