<div class="nav-overlay"></div>
<div class="nav-inner isDown">
    <nav>
        <ul>
            @foreach(Menu::get('PortfolioMainMenu')->whereParent(null) as $main_menu_item)
                <li  class="{{ $main_menu_item->attr('subnav') ? 'subnav' : '' }}">
                    <a
                        @if(!$main_menu_item->attr('subnav') )
                                href="{{ $main_menu_item->url() }}"
                        @endif
                    >
                        {{ $main_menu_item->title }}
                    </a>
                    @if($main_menu_item->hasChildren())
                        <ul>
                        @foreach($main_menu_item->children() as $main_menu_children_item)
                            <li>
                                <a href="{{ $main_menu_children_item->url() }}">{{ $main_menu_children_item->title }}</a>
                            </li>
                        @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
</div>