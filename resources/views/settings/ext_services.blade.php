@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>VK.COM</h2>
        </div>
        <div class="body">
            <div class="alert bg-red">
                ВНИМАНИЕ! Получать тоукен только под контролем администратора CRM!
            </div>
            <div class="row">
                <div class="col-sm-4"><b>Ссылка для получения api-token</b></div>
                <div class="col-sm-8">
                    <a href="https://oauth.vk.com/authorize?client_id=5062925&scope=78850&redirect_uri=https://oauth.vk.com/blank.html&display=page&response_type=token&v=5.80&revoke=1" target="_new">Получить TOKEN</a>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
                swal("Ура!", "{{ session('success') }}", "success");
            @endif
        });
    </script>

@endsection

@push('js_custom')
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?1506" charset="utf-8"></script>
    <script type="text/javascript">
      VK.init({apiId: 6219683});
    </script>

<script type="text/javascript">
@if(!trim(Auth::user()->vk_id))
  VK.Widgets.Auth("vk_auth", {"width":300,"onAuth":vk_auth_callback});
@endif
</script>
@endpush