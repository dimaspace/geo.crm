@extends('layouts.by_token')

@section('content')
    <h2>{{$project->place_cached}}  {{ $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') }}</h2>

    <div class="msg">Проект завершён</div>

    <div class="links">
        <a class="btn bg-teal waves-effect" target="_new{{$project->id}}" href="{{$project->download_link}}"><i class="material-icons">file_download</i><span>СКАЧАТЬ АРХИВ С ФОТОГРАФИЯМИ</span></a>

        @if($project->geo_type_id === 2)
            <a class="btn bg-teal waves-effect m-l-35" target="_geo{{$project->id}}" href="{{$project->geo_link}}"><i class="material-icons">photo_camera</i><span>ПРОСМОТРЕТЬ ОТЧЁТ НА GEO</span></a>
        @endif
    </div>

    @if($project->geo_type_id === 0)
        <div class="msg">Сожалеем, но проект ещё не опубликован на портале =(</div>
    @endif
@endsection