@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <div id="projectsTablePhoto"></div>

        </div>
    </div>


@push('js_custom')
    <script src="/js/tables/projects_table_photo.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

@endsection