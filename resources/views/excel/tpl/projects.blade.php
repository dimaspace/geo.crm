<table border="1">
    <tr>
        <th>#</th>
        <th>Регион</th>
        <th>Тип</th>
        <th>Статус</th>
        <th>Начало</th>
        <th>Конец</th>
        <th>Время неизвестно</th>

        <th>Место</th>
        <th>Клиент</th>
        <th>Описание</th>
        <th>Скр. Описание</th>

        <th>Сумма</th>
        <th>Заработок редакции</th>
        <th>Оплата координаторов</th>

        <th>Дедлайн</th>
        <th>Задержка</th>

        <th>Время выгрузки</th>

        <th>Оплата фот</th>
        <th>Оплата бух</th>
        <th>Исполнитель</th>

        <th>Принимающий</th>
        <th>Распределяющий</th>
        <th>Контролирующий</th>

    </tr>
    @foreach($projects as $key => $project)
    <tr>
        <td valign="top" style="wrap-text: true">{{ $project->id }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->region_code }}</td>
        <td valign="top" style="wrap-text: true">{{ Config::get('project.category')[$project->category_id] }}</td>
        <td valign="top" style="wrap-text: true">{{ Config::get('project.status')[$project->status_id] }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->date_start ? $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : '' }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->date_end ? $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : '' }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->time_unknown ? 'да' : 'нет' }}</td>


        <td valign="top" style="wrap-text: true">{{ $project->place_cached }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->client_cached }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->desc_public }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->desc_hidden }}</td>

        <td valign="top" style="wrap-text: true">{{ $project->amount }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->calc_ed_fee() - $project->calc_referral() - $project->calc_all_cords_royalty()  }}</td>
        <td valign="top" style="wrap-text: true">{{ $project->calc_all_cords_royalty()  }}</td>

        <td valign="top" style="wrap-text: true">{{ $project->date_deadline ? $project->date_deadline->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : '' }}</td>
        <td valign="top" style="wrap-text: true">{{ ( $project->date_deadline && $project->upload_at) ?  $project->date_deadline < $project->upload_at ? $project->date_deadline->diffInMinutes($project->upload_at, false) : 0  : '' }}</td>

        <td valign="top" style="wrap-text: true">{{ $project->upload_at ? $project->upload_at->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') : '' }}</td>

        <td valign="top" style="wrap-text: true">{{ Config::get('project.payment_type_w')[$project->payment_type_id_w] }}</td>
        <td valign="top" style="wrap-text: true">{{ Config::get('project.payment_type_b')[$project->payment_type_id_b] }}</td>
        <td valign="top" style="wrap-text: true">@if($pw = $project->worker()){{ $pw->name }}@endif</td>

        <td valign="top" style="wrap-text: true">
            @foreach($project->getCordsByType('prin') as $cord)
                @if (!$loop->first)<br>@endif
                {{ $cord->user->name }}
            @endforeach
        </td>
        <td valign="top" style="wrap-text: true">
            @foreach($project->getCordsByType('rasp') as $cord)
                @if (!$loop->first)<br>@endif
                {{ $cord->user->name }}<br>
            @endforeach
        </td>
        <td valign="top" style="wrap-text: true">
            @foreach($project->getCordsByType('control') as $cord)
                @if (!$loop->first)<br>@endif
                {{ $cord->user->name }}<br>
            @endforeach
        </td>

    </tr>
    @endforeach
</table>