<table border="1">
    <tr>
        <th>ID</th>
        <th>CROSS ID</th>
        <th>Подтверждена?</th>
        <th>Пользователь</th>
        <th>Создано</th>
        <th>Тип</th>
        <th></th>
        <th>Сумма</th>
        <th>Коммент</th>

        <th>ID Проекта</th>
        <th>ID заявки</th>

        <th>Тип создания</th>

    </tr>
    @foreach($transactions as $key => $transaction)
    <tr>
        <td valign="top" style="wrap-text: true">{{ $transaction->id }}</td>
        <td valign="top" style="wrap-text: true">{{ $transaction->cross_t_id }}</td>
        <td valign="top" style="wrap-text: true;
                background-color: {{ $transaction->accepted ? '#7cff7c' : '#fd8585' }};
                color: {{ $transaction->accepted ? '#01aa01' : '#aa0101' }};">
            {{ $transaction->accepted ? 'Да' : 'Нет' }}
        </td>
        <td valign="top" style="wrap-text: true">{{ App\User::getNameById($transaction->user_id) }}</td>
        <td valign="top" style="wrap-text: true">{{ $transaction->created_at->timezone(config('project.city_time_zone'))->format('d.m.Y H:i') }}</td>
        <!--<td valign="top" style="wrap-text: true">{{ App\User::getNameById($transaction->creator_id) }}</td>-->
        <td valign="top" style="wrap-text: true;
                color: {{ $transaction->getTypeColor() }}">
            {{ $transaction->getType() }}
        </td>
        <td valign="top" style="wrap-text: true;
                color: {{ $transaction->value > 0 ? '#298d02' : '#f90101' }}">
            {{ $transaction->value > 0 ? '↑' : '↓' }}
        </td>
        <td valign="top" style="wrap-text: true;
                color: {{ $transaction->value > 0 ? '#298d02' : '#f90101' }}">
            {{ $transaction->value }}
        </td>
        <td valign="top" style="wrap-text: true">{{ strip_tags($transaction->descCompiled()) }}</td>
        <td valign="top" style="wrap-text: true">{{ $transaction->project_id }}</td>
        <td valign="top" style="wrap-text: true">{{ $transaction->cash_flow_id }}</td>
        <td valign="top" style="wrap-text: true">{{ $transaction->getCrTypeCode() }}</td>

    </tr>
    @endforeach
</table>