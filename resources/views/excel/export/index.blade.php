@extends('layouts.main')

@section('content')

<div class="card">
    <div class="header">
        <h2>
            ЭКСПОРТ В EXCEL
        </h2>
    </div>
    <div class="body">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active">
                <a href="#excel_tab_export_projects" data-toggle="tab">
                    ПРОЕКТЫ
                </a>
            </li>
            <li role="presentation">
                <a href="#excel_tab_export_transactions" data-toggle="tab">
                    ТРАНЗАКЦИИ
                </a>
            </li>

        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="excel_tab_export_projects">
                <form id="excel_export_projects" method="post" action="/excel/export/projects">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="date_start" type="hidden" id="project-date-range-start">
                                    <input name="date_end" type="hidden" id="project-date-range-end">

                                    <div id="project-date-range">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="submit" class="btn btn-lg m-l-15 waves-effect bg-teal">ЭКСПОРТ</button>
                        </div>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="excel_tab_export_transactions">
                <form id="excel_export_transactions" method="post" action="/excel/export/transactions">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="date_start" type="hidden" id="transactions-date-range-start">
                                    <input name="date_end" type="hidden" id="transactions-date-range-end">

                                    <div id="transactions-date-range">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="submit" class="btn btn-lg m-l-15 waves-effect bg-teal">ЭКСПОРТ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

@push('js_custom')
    <script src="/js/form.export.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {

        @if (session('success'))
            swal("Ура!", "{{ session('success') }}", "success");
        @endif

        @if (session('error'))
            swal("Ошибка!", "{{ session('error') }}", "error");
        @endif

    });
</script>

@endsection