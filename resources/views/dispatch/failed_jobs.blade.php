@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                ПРОВАЛЕННЫЕ ЗАДАЧИ
            </h2>
        </div>
        <div class="body">

            <div id="gridContainer"></div>
            <div class="options">
                <div class="caption">Настройки</div>
                <div class="option">
                    <div id="autoExpand"></div>
                </div>
            </div>

        </div>
    </div>

@push('js_custom')
    <script src="/js/jobs/jobs.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        @if (session('success'))
            showNotification('bg-green', "<p>Ура!<br>{!! session('success') !!}<p>", 1000, 'top', 'right');
        @endif
        @if (session('error'))
            showNotification('bg-red', "<p>Ошибка!<br>{!! session('error') !!}<p>", 1000, 'top', 'right');
        @endif
    });
</script>

@endsection