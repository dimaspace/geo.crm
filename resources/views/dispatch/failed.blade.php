@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                НЕДОСТАВЛЕННЫЕ РАССЫЛКИ И УВЕДОМЛЕНИЯ
            </h2>
        </div>
        <div class="body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-condensed" id="cord_dash_table" style="background-color: #fff;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Когда</th>
                                    <th>Канал</th>
                                    <th>Получатель</th>
                                    <th>Сообщение</th>
                                    <th>Проблема</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($failed as $fail)
                                    <tr>
                                        <td>{{ $fail->id }}</td>
                                        <td>{{ $fail->created_at->format('d.m.Y H:i') }}</td>
                                        <td>{{ $fail->channel }}</td>
                                        <td><a href="https://vk.com/im?sel={{ $fail->to }}" target="_new">{{ $fail->to }}</a></td>
                                        <td title="{{ $fail->msg }}">
                                            <button type="button" class="btn bg-teal vk_msg_copy_button">
                                                <i class="material-icons">file_copy</i>
                                            </button>
                                            <textarea>{{ $fail->msg }}</textarea>
                                        </td>
                                        <td>{{ $fail->problem }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

        </div>
    </div>

@push('js_custom')
    <script type="text/javascript">
        $("body").on("click", ".vk_msg_copy_button", function (e) {
            var text_to_copy = $(this).parent().find('textarea').val();
            CopyToClipboard(text_to_copy);
        });
    </script>
@endpush

@endsection