@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                ПАРАМЕТРЫ РАССЫЛКИ
            </h2>
        </div>
        <div class="body">

            <div id="add_dispatch">
                @form( $form )
                <button type="button" class="btn bg-teal waves-effect submit" onClick="$('#add_dispatch form').submit()">СОХРАНИТЬ</button>
            </div>

        </div>
    </div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        @if (session('success'))
            swal("Ура!", "{!! session('success')  !!}", "success");
        @endif
        @if (session('error'))
            swal("Ошибка!", "{!! session('error')  !!}", "error");
        @endif
    });
</script>

@endsection