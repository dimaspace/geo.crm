Доброго времени суток.<br>
{{  $booker_name  }} подтвердил ваш платёж №{{ $transaction_info->id  }} в регионе {{  $region_info->title }}.<br><br>

Сумма платежа: <b>{{ number_format($transaction_info->amount, 2)  }} руб.</b><br>
Текущий баланс: <b>{{  number_format($region_info->balance, 2) }} руб.</b><br><br>

C уважением, команда системы GeoRegions.
