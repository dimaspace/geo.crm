	<input
		type="{{ $type }}"
		name="{{ $field->name }}"
		id="{{ $field->name }}"
		class="form-control {{ array_get($field->getAttributes(), 'class') }}"
		value="{{ $field->value }}"
		@foreach(array_except($field->getAttributes()->all(), ['class', 'value']) as $key => $value)
			@if($value === true)
				{{ $key }}
			@else
				{{ $key }}="{{ $value }}"
			@endif
		@endforeach
	>