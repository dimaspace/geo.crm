<div class="form-group {{ ($errors->has($field->name) ? 'has-error' : '') }}">
	<label for="{{ $field->name }}" class="col-sm-2 control-label">{{ $field->label }}</label>
	<div class="col-sm-10">
	<div class="form-line">

	<textarea
		type="{{ $type }}"
	 	rows="4"
		name="{{ $field->name }}"
		id="{{ $field->name }}"
		class="form-control  no-resize {{ array_get($field->getAttributes(), 'class') }}"
		@foreach(array_except($field->getAttributes()->all(), ['class', 'value']) as $key => $value)
			@if($value === true)
				{{ $key }}
			@else
				{{ $key }}="{{ $value }}"
			@endif
		@endforeach
	>{{ $field->value }}</textarea></div>
	@if($errors->has($field->name))
		@foreach($errors->get($field->name) as $error)
			<span class="help-block">{{ $error }}</span>
		@endforeach
	@endif
	</div>
</div>