<div class="form-group {{ ($errors->has($field->name) ? 'has-error' : '') }}">
	<label for="{{ $field->name }}" class="col-sm-2 control-label {{ ($errors->has($field->name) ? 'col-red' : '') }}">{{ $field->label }}</label>
	<div class="col-sm-10">
	<div class="form-line">
	<input
		type="{{ $type }}"
		name="{{ $field->name }}"
		id="{{ $field->name }}"
		class="form-control {{ array_get($field->getAttributes(), 'class') }}"
		value="{{ $field->value }}"
		@foreach(array_except($field->getAttributes()->all(), ['class', 'value']) as $key => $value)
			@if($value === true)
				{{ $key }}
			@else
				{{ $key }}="{{ $value }}"
			@endif
		@endforeach
	></div>
		@if($errors->has($field->name))
			@foreach($errors->get($field->name) as $error)
				<label id="{{ $field->getAttributes()->get('id') }}-error" class="error" for="{{ $field->getAttributes()->get('id') }}">{{ $error }}</label>
			@endforeach
		@endif
	</div>
</div>