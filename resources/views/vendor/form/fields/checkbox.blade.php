<div class="form-group {{ ($errors->has($field->name) ? 'has-error' : '') }}">
	<label class="col-sm-2 control-label">{{ $field->label }}</label>
	<div class="col-sm-10">
	@foreach($field->options() as $checkbox_num => $option)
		<div class="checkbox">
				<input
					type="checkbox"
					class="filled-in"
					name="{{ $field->name }}[]"
					id="{{ $field->name }}_{{ $checkbox_num }}"
					value="{{ $option['value'] }}"
					{{ $field->hasValueOf(array_get($option, 'value')) ? 'checked' : '' }}
					@foreach(array_except($field->getAttributes()->all(), ['value', 'name']) as $key => $value)
						@if($value === true)
							{{ $key }}
						@else
							{{ $key }}="{{ $value }}"
						@endif
					@endforeach
				>
			<label for="{{ $field->name }}_{{ $checkbox_num }}">{{ $option['label'] }}</label>

		</div>
	@endforeach
	</div>
	
	@if($errors->has($field->name))
		@foreach($errors->get($field->name) as $error)
			<span class="help-block">{{ $error }}</span>
		@endforeach
	@endif
</div>