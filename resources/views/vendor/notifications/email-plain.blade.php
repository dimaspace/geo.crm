<?php

if (! empty($greeting)) {
    echo $greeting, "\n\n";
} else {
    echo $level == 'error' ? 'Упс!' : 'Приветствуем!', "\n\n";
}

if (! empty($introLines)) {
    echo implode("\n", $introLines), "\n\n";
}

if (isset($actionText)) {
    echo "{$actionText}: {$actionUrl}", "\n\n";
}

if (! empty($outroLines)) {
    echo implode("\n", $outroLines), "\n\n";
}

echo 'C уважением,', "\n";
echo config('app.name'), "\n";

echo 'Внимание! Это письмо отправлено автоматически с технического адреса, не нужно на него отвечать!', "\n";
echo 'Если вы хотите связаться с администрацией, то напишите на e-mail ';
echo config('mail.reply.address'), "\n";
