@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <div id="projectsTable"></div>

        </div>
    </div>

    <div class="card">
        <div class="body">
            <div id="projectsChart"></div>
            <div id="projectsChartrangeSelector"></div>
            <div id="projectsPopup"></div>
        </div>
    </div>

@push('js_custom')
    <script src="/js/stats/projects_self_photo.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

@endsection