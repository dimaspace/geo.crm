@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <div id="projectsTable"></div>

        </div>
    </div>

    <div class="card">
        <div class="body">
            <div id="projectsChart"></div>
            <div id="projectsChartrangeSelector"></div>
        </div>
    </div>

@push('js_custom')
    <script src="/js/stats/projects.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush

@endsection