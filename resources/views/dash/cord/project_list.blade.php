    <div class="row clearfix">
            <div class="btn-group btn-group-justified info-boxes-links">

                    <a href="{{ $projects_filter_alternate === 'all' ? '/' : '/?projects_filter_alternate=all' }}" class="hidden-xs hidden-sm btn btn-link bg-grey btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'all' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['all'] }}</span>&nbsp;ВСЕ</a>

                    <a href="{{ $projects_filter_alternate === 'new' ? '/' : '/?projects_filter_alternate=new' }}" class="hidden-xs btn btn-link bg-light-green btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'new' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['new'] }}</span>&nbsp;НОВЫЕ</a>

                    <a href="{{ $projects_filter_alternate === 'unassigned24' ? '/' : '/?projects_filter_alternate=unassigned24' }}" class="btn btn-link bg-red btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'unassigned24' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['unassigned24'] }}</span>&nbsp;СРОЧНО НАЗ-ТЬ</a>

                    <a href="{{ $projects_filter_alternate === 'unassigned96' ? '/' : '/?projects_filter_alternate=unassigned96' }}" class="hidden-xs btn btn-link bg-orange btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'unassigned96' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['unassigned96'] }}</span>&nbsp;НАЗНАЧИТЬ!</a>

                    <a href="{{ $projects_filter_alternate === 'unassigned' ? '/' : '/?projects_filter_alternate=unassigned' }}" class="hidden-xs btn btn-link bg-amber btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'unassigned' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['unassigned'] }}</span>&nbsp;НЕ НАЗНАЧЕНО</a>

                    <a href="{{ $projects_filter_alternate === 'projects_in_work' ? '/' : '/?projects_filter_alternate=projects_in_work' }}" class="hidden-xs hidden-sm btn btn-link bg-indigo btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'projects_in_work' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['projects_in_work'] }}</span>&nbsp;В РАБОТЕ</a>

                    <a href="{{ $projects_filter_alternate === 'unknow_date' ? '/' : '/?projects_filter_alternate=unknow_date' }}" class="hidden-xs hidden-sm btn btn-link bg-brown btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'unknow_date' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['unknow_date'] }}</span>&nbsp;БЕЗ ДАТЫ</a>

                    <a href="{{ $projects_filter_alternate === 'deadline_fail' ? '/' : '/?projects_filter_alternate=deadline_fail' }}" class="btn btn-link bg-deep-orange btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'deadline_fail' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['deadline_fail'] }}</span>&nbsp;ДД</a>

                    <a href="{{ $projects_filter_alternate === 'deadline2_fail' ? '/' : '/?projects_filter_alternate=deadline2_fail' }}" class="btn btn-link bg-pink btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'deadline2_fail' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['deadline2_fail'] }}</span>&nbsp;ДД2</a>

                    <a href="{{ $projects_filter_alternate === 'deadline2_fail_client' ? '/' : '/?projects_filter_alternate=deadline2_fail_client' }}" class="hidden-xs btn btn-link bg-deep-orange btn-sm btn-block waves-effect {{ $projects_filter_alternate === 'deadline2_fail_client' ? 'active' : '' }}" type="button"><i class="material-icons pull-right close">close</i><span class="badge bg-white col-black">{{ $info_boxes['deadline2_fail_client'] }}</span>&nbsp;ДД2K</a>

            </div>
    </div>
    <!--
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box-4 bg-light-green hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">new_releases</i>
                </div>
                <div class="content">
                    <div class="text">НОВЫЕ ЗАЯВКИ</div>
                    <div class="number">{{ $info_boxes['new'] }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box-4 bg-orange hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">swap_horiz</i>
                </div>
                <div class="content">
                    <div class="text">НАДО РАСПРЕДЕЛИТЬ</div>
                    <div class="number">{{ $info_boxes['unassigned'] }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box-4 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">devices</i>
                </div>
                <div class="content">
                    <div class="text">ПРОЕКТОВ В РАБОТЕ</div>
                    <div class="number">{{ $info_boxes['projects_in_work'] }}</div>
                </div>
            </div>
        </div>
    </div>-->

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>СПИСОК СЪЁМОК</h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <div class="btn-group">
                                <button type="button" class="btn bg-teal dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="true">
                                    РЕЖИМ <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li class=" {{$projects_list_mode == 'table' ? 'active' : ''}}">
                                        <a href="/?{{$filters_link}}&projects_list_mode=table"
                                           class=" waves-effect waves-block"><i class="material-icons">reorder</i>
                                            Таблица</a>
                                    </li>
                                    <li class=" {{$projects_list_mode == 'calendar' ? 'active' : ''}}">
                                        <a href="/?{{$filters_link}}&projects_list_mode=calendar"
                                           class=" waves-effect waves-block"><i class="material-icons">event_note</i>Календарь</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="modal" data-target="#add_project" data-mode="new"><i
                                        class="material-icons col-teal font-40">add_box</i></a>
                        </li>
                    </ul>
                </div>
                <div class="body">

                    <div class="project-filter clearfix">
                        <form id="projects_filter_form" method="GET">
                            <input name="projects_list_mode" id="projects_list_mode" type="hidden" value="table">
                            <input name="projects_filter[start]" type="hidden" id="project-date-range-start" value="{{$projects_filter['start']}}">
                            <input name="projects_filter[end]" type="hidden" id="project-date-range-end" value="{{$projects_filter['end']}}">

                            <div id="project-date-range">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                            <div class="project-regions">
                                <select name="projects_filter[regions][]" multiple class="form-control" data-changed="false">
                                    @foreach(App\Region::whereIn('code', Config::get('project.regions'))->orderBy('title', 'desc')->get() as $region)
                                    <option value="{{ $region->code }}" @if(!isset($projects_filter['regions']) || (isset($projects_filter['regions']) && in_array($region->code, $projects_filter['regions']))) selected @endif>{{ $region->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="project-statuses">
                                <select name="projects_filter[statuses][]" multiple class="form-control" data-changed="false" data-none-selected-text="Статусы не выбраны">
                                    @foreach(Config::get('project.status') as $key => $status)
                                        <option value="{{ $key }}" @if(isset($projects_filter['statuses']) && in_array($key, $projects_filter['statuses'])) selected @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="project-workers">
                                <select name="projects_filter[workers][]" multiple class="form-control" data-changed="false" data-live-search="true" data-none-selected-text="Фотографы не выбраны">
                                    <option value="0" @if(isset($projects_filter['workers']) && in_array(0, $projects_filter['workers'])) selected @endif>не назначен</option>
                                    @foreach(App\User::whereHidden(0)->whereHas('roles', function($query){
                                            $query->where('name', 'photo');
                                        })->get(['id', 'name']) as $user)
                                        <option value="{{ $user->id }}" @if(isset($projects_filter['workers']) && in_array($user->id, $projects_filter['workers'])) selected @endif>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if(isset($projects_list_mode) && $projects_list_mode == 'calendar')
                                <div class="project-staff">
                                    <select name="projects_filter[staff][]" multiple class="form-control" data-changed="false" data-none-selected-text="Штатность не выбрана">
                                        <option value="1" @if(isset($projects_filter['staff']) && in_array(1, $projects_filter['staff'])) selected @endif>Штатные</option>
                                        <option value="0" @if(isset($projects_filter['staff']) && in_array(0, $projects_filter['staff'])) selected @endif>Не штатные</option>
                                    </select>
                                </div>
                            @endif
                        </form>
                    </div>

                    @if($projects_list_mode == 'table')
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-condensed" id="cord_dash_table" style="background-color: #fff;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <td></td>
                                    <th>Город</th>
                                    <th>Биржа / статус</th>
                                    <th>&nbsp;&nbsp;Дата&nbsp;&nbsp;</th>
                                    <th>Время съёмки</th>
                                    <th>Место</th>

                                    <th>Заказчик</th>
                                    <th>Описание</th>
                                    <th>Описание скрытое</th>
                                    <th>SMM пост для сарафана</th>
                                    <th>Кому доступно</th>
                                    <th>Вид оплаты для исполнителя</th>
                                    <th>Вид оплаты для бухгалтера</th>

                                    <th>Исполнитель</th>
                                    <th>Оплата/час</th>
                                    <th>Стоимость</th>
                                    <th>Сум. с уч. налог. выч.(%)</th>
                                    <th>Сдача материала</th>

                                    <th>Geo</th>
                                    <th>Дата сдачи матер.</th>
                                    <th>Ссылки на скачивание фото</th>
                                    <th>СС</th>
                                    <th>Реферрал(ФС)</th>
                                    <th>Принимающий координатор</th>

                                    <th>Распределяющий координатор</th>
                                    <th>Контролирующий координатор</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr data-id="{{ $project->id }}" data-mode="edit" class="project_status_{{ $project->status_id  }}">
                                    <td class="id">#{{ $project->id }}</td>
                                    <td class="text-center info_cell">
                                        @if(in_array($project->status_id , Config::get('project.fail_status')))
                                            <i class="emoji font-20">&#128169;</i>
                                        @else
                                            {{-- <button class="btn bg-teal btn-sm project-edit-link" data-toggle="modal"  data-id="{{ $project->id }}" data-target="#add_project"  data-mode="edit"><i class="material-icons">mode_edit</i></button> --}}

                                            @if($project->status_id != Config::get('project.closed_status'))
                                                @if(!$pw = $project->worker())
                                                    <i class="fa fa-user-times col-red" aria-hidden="true" title="Не указан исполнитель"></i>
                                                @endif

                                                @if($project->vk_msg_id)
                                                    <i class="fa fa-vk col-blue font-20  @if($pw && $pw->vk_id) vk-send @endif vk-resend" data-project-id="{{ $project->id }}" aria-hidden="true" title="Отправлена информация вконтакте"></i>
                                                @else
                                                    @if($pw && $pw->vk_id)
                                                            <button type="button" class="btn bg-teal waves-effect vk-send" title="Отправить информация через вконтакт" data-project-id="{{ $project->id }}">
                                                                <i class="fa fa-vk" aria-hidden="true"></i>
                                                            </button>
                                                    @elseif($pw && $pw->vk_id == false)
                                                        <i class="fa fa-vk col-red font-20" aria-hidden="true" title="У исполнителя не привязан контакт"></i>
                                                    @endif
                                                @endif
                                            @endif


                                            @if($project->geo_type_id == 0)
                                                @if($project->date_deadline && $project->date_deadline < Carbon\Carbon::now())
                                                    <i class="fa fa-google col-red" aria-hidden="true" title="Просрочена публикация на геометрии"></i>
                                                @else
                                                    <i class="fa fa-google" aria-hidden="true" title="Нужно выложить на геометрию"></i>
                                                @endif
                                            @endif

                                            @if($project->date_deadline && $project->date_deadline < Carbon\Carbon::now() && in_array($project->status_id, $cfg_active_status))
                                                <i class="glyphicon glyphicon-fire col-red font-26" aria-hidden="true" title="Просрочено"></i>
                                            @else
                                                @if($project->date_deadline && $project->date_deadline->startOfDay() == Carbon\Carbon::now()->startOfDay() && in_array($project->status_id, $cfg_active_status))
                                                    <i class="glyphicon glyphicon-fire col-orange" aria-hidden="true" title="Крайний срок сегодня"></i>
                                                @endif
                                            @endif
                                        @endif


                                        @if(Auth::user()->can('manage_payments'))
                                            @if($project->po_at_once == 1)
                                                @if($project->po_later_on == 1)
                                                    <i class="material-icons col-green" title="Проведена 2-я фаза транзакций">filter_2</i>
                                                @else
                                                    <i class="material-icons col-green" title="Проведена 1-я фаза транзакций">filter_1</i>
                                                @endif
                                            @endif

                                            @if($project->pay_status_id == 0 &&  $project->payment_type_id_b == 1)
                                                <form action="/project/make_paid" method="POST" id="project_make_paid_form_{{ $project->id }}">{{ csrf_field() }}<input type="hidden" name="project_id" value="{{ $project->id }}"></form>
                                                <button class="btn bg-teal btn-xs mg-2 project_make_paid_button" data-project-id="{{ $project->id }}"><i class="material-icons"  title="Пометить оплаченым">attach_money</i></button>
                                            @endif

                                            @if($project->pay_status_id == 1 && ($project->payment_type_id_w == 1 || $project->payment_type_id_b == 1))
                                                <i class="material-icons col-green">attach_money</i>
                                            @endif
                                        @else
                                            @if($project->pay_status_id == 0 &&  $project->payment_type_id_b == 1)
                                                <i class="material-icons col-grey" title="Ожидание оплаты от клиента">money_off</i>
                                            @endif

                                            @if($project->pay_status_id == 1 && ($project->payment_type_id_w == 1 || $project->payment_type_id_b == 1))
                                                <i class="material-icons col-green" title="Оплачено">attach_money</i>
                                            @endif
                                        @endif

                                        @if($project->client_notified === 1)
                                                <i class="material-icons col-green" title="Отправлено заказчику роботом">contact_mail</i>
                                        @endif

                                    </td>

                                    <td>{{ $project->region_code }}</td>
                                    <td class="@if($project->status_id == 0) bg-light-green @endif">
                                        {{ Config::get('project.status')[$project->status_id]}}
                                        @if($project->status_id == Config::get('project.worker_assigned_status') && $project->worker_id)
                                            <br><span class="label font-12 bg-teal">{{ App\User::getNameById($project->worker_id) }}</span>
                                        @endif
                                    </td>
                                    <td>@if($project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') }}@else ? @endif<br>@if($project->date_end){{ $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.y') }}@endif</td>
                                    <td>@if($project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('H:i') }}@else ? @endif<br>@if($project->date_end){{ $project->date_end->timezone(config('project.city_time_zone'))->format('H:i') }}@endif</td>
                                    <td>{{ $project->place_cached }}</td>

                                    <td>{{ $project->client_cached }}</td>
                                    <td>{{ str_limit($project->desc_public, 50) }}
                                        @if(Illuminate\Support\Str::length($project->desc_public) > 50)

                                            <button tabindex="0" class="btn bg-teal btn-xs"
                                                    data-trigger="focus" data-container="body" data-toggle="popover"
                                                    data-content="{{  $project->desc_public }}" onclick="event.stopPropagation()">
                                                подробнее...
                                            </button>

                                        @endif
                                    </td>
                                    <td>{{ str_limit($project->desc_hidden, 50) }}
                                        @if(Illuminate\Support\Str::length($project->desc_hidden) > 50)

                                            <button tabindex="0" class="btn bg-teal btn-xs"
                                                    data-trigger="focus" data-container="body" data-toggle="popover"
                                                    data-content="{{  $project->desc_hidden }}" onclick="event.stopPropagation()">
                                                подробнее...
                                            </button>

                                        @endif
                                    </td>
                                    <td>{{ Config::get('project.smm_type')[$project->smm_type_id] }}</td>
                                    <td>
                                        {{ Config::get('project.visibility')[$project->visibility_id] }}
                                        @if(in_array($project->visibility_id, Config::get('project.visibility_list')))
                                            <br>
                                            @foreach(explode(',', $project->visibility_list) as $user_id)
                                                <span class="label {{ $project->visibility_id == 3 ? 'bg-red' : 'bg-green' }}">{{ App\User::getNameById($user_id) }}</span>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{ Config::get('project.payment_type_w')[$project->payment_type_id_w] }}</td>
                                    <td>{{ Config::get('project.payment_type_b')[$project->payment_type_id_b] }}</td>

                                    <td>@if($project->worker_id){{ App\User::getNameById($project->worker_id) }}@else не назначен @endif</td>
                                    <td>{{ $project->amount_per_hour }}</td>
                                    <td>{{ $project->amount }}</td>
                                    <td>{{ $project->amount_with_tax }}</td>
                                    <td>{{ Config::get('project.pass_type')[$project->pass_type_id] }}</td>

                                    <td>{{ Config::get('project.geo_type')[$project->geo_type_id] }}</td>
                                    <td>@if($project->date_deadline){{ $project->date_deadline->timezone(config('project.city_time_zone'))->format('d.m.y H:i') }}@else ? @endif</td>
                                    <td>@if(trim($project->download_link))<a target="new" href="{{ $project->download_link }}"><i class="material-icons">cloud_download</i></a>@endif</td>
                                    <td>
                                        @if($project->ssmanager)
                                            {{ $project->ssmanager->user->name }} ({{ $project->ssmanager->royalty }}%)
                                        @endif
                                    </td>

                                    <td>
                                        @if($project->referral)
                                            {{ $project->referral->user->name }} ({{ $project->referral->royalty }}%)
                                        @endif
                                    </td>
                                    <td>@forelse($project->getCordsByType('prin') as $cord) [{{ App\User::getNameById($cord->user_id) }}] @empty не назначен @endforelse</td>

                                    <td>@forelse($project->getCordsByType('rasp') as $cord) [{{ App\User::getNameById($cord->user_id) }}] @empty не назначен @endforelse</td>
                                    <td>@forelse($project->getCordsByType('control') as $cord) [{{ App\User::getNameById($cord->user_id) }}] @empty не назначен @endforelse</td>


                                </tr>
                            @endforeach

                            <tbody>

                        </table>
                    </div>
                    @endif

                    @if(isset($projects_list_mode) && $projects_list_mode == 'calendar')
                        <div id="projects_calendar"></div>
                        <br><br>
                        <div id="calendar-options-layout"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@permission('manage_projects_list')
    <script type="text/javascript">
        function init_project_filter() {
            var start = moment("{{$projects_filter['start']}}", "YYYY-MM-DD").startOf('day');
            var end = moment("{{$projects_filter['end']}}", "YYYY-MM-DD").endOf('day');
            project_range_function(start, end);
        }
    </script>
@endpermission

@push('js_custom')
    <script src="{{ mix('js/form.project.js') }}"></script>
    @if(isset($projects_list_mode) && $projects_list_mode == 'calendar')
        <script src="{{ mix('js/form.project.calendar.js') }}"></script>
    @endif

    @javascript('project_categories', config('project.category'))
    @javascript('project_regions', $project_regions);
    @javascript('payment_types_w', config('project.payment_type_w'));
    @javascript('payment_types_b', config('project.payment_type_b'));



@endpush