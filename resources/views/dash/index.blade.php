@extends('layouts.main')

@section('content')

@role('cord')
    <div class="block-header">
        <h4>ПАНЕЛЬ КООРДИНАТОРА</h4>
    </div>
@endrole
@permission('manage_projects_list')
    @include('dash.cord.project_list')
@endpermission

@role('photo')
    <div class="block-header">
        <h4>ПАНЕЛЬ ФОТОГРАФА</h4>
    </div>
@endrole


@permission('view_balance_stats')

    <div class="row clearfix">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="info-box bg-{{ $user->balance_color }}">
                <div class="icon">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text">
                        ТЕКУЩИЙ БАЛАНС
                        <button class="btn btn-xs bg-green hidden-xs hidden-sm user_recharge_money">
                            <i class="material-icons">file_download</i>&nbsp;ПОПОЛНИТЬ
                        </button>
                        @if($user->balanceCached() > 0)
                        &nbsp;&nbsp;<button class="btn btn-xs bg-red hidden-xs hidden-sm user_take_money">
                            <i class="material-icons">file_upload</i>&nbsp;ВЫВЕСТИ
                           </button>
                        @endif
                    </div>
                    <div class="number">
                        {{ number_format($user->balanceCached(), 2) }}&nbsp;&#8381;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="info-box bg-{{ $user->threshold > 0 ? $user->balance_color : 'grey' }}">
                <div class="icon">
                    <i class="material-icons">gavel</i>
                </div>
                <div class="content">
                    <div class="text">ПОРОГ БЛОКИРОВКИ</div>
                    <div class="number">
                         {{ $user->threshold > 0 ? '-' . number_format($user->threshold, 0) : 0 }}&nbsp;&#8381;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="info-box bg-grey">
                <div class="icon">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="content">
                    <div class="text">ОЖИДАЕМЫЙ БАЛАНС</div>
                    <div class="number">
                       [НЕДОСТУПНО]
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <form action="/profile/balance/add_cash_flow" method="POST" id="user_recharge_money_form">
                {{ csrf_field() }}
                <input type="hidden" name="amount">
                <input type="hidden" name="comment">
                <input type="hidden" name="type" value="{{ App\CashFlow::type_by_code('RECHARGE') }}">
            </form>
                <button class="btn btn-lg bg-blue hidden-lg hidden-md m-b-30 pd-15 user_recharge_money" style="width: 100%;">
                    <i class="material-icons">file_download</i>&nbsp;&nbsp;&nbsp; ПОПОЛНИТЬ
                </button>
        </div>
            @if($user->balanceCached() > 0)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <form action="/profile/balance/add_cash_flow" method="POST" id="user_take_money_form">
                        {{ csrf_field() }}
                        <input type="hidden" name="amount">
                        <input type="hidden" name="comment">
                        <input type="hidden" name="type" value="{{ App\CashFlow::type_by_code('TAKE') }}">
                    </form>
                        <button class="btn btn-lg bg-blue hidden-lg hidden-md m-b-30 pd-15 user_take_money" style="width: 100%">
                            <i class="material-icons font-16">file_upload</i>&nbsp;&nbsp;&nbsp;ВЫВЕСТИ
                        </button>
                </div>
            @endif

    </div>

    @if($cf_transactions_not_accepted && count($cf_transactions_not_accepted) > 0)

        <div class="card">
            <div class="header bg-orange cursor-pointer orange-blink" data-toggle="collapse" data-target="#cf_transactions_not_accepted">
                <h2>ВЫ ИЛИ ВАМ ДОЛЖНЫ ПЕРЕДАТЬ ДЕНЬГИ</h2>
                <small>НЕИСПОЛНЕНЫХ ЗАЯВОК: <b>{{ count($cf_transactions_not_accepted) }}</b></small>
                <ul class="header-dropdown m-r--5 font-40">
                    <li class="dropdown">
                        <a href="#cf_transactions_not_accepted" class="dropdown-toggle" data-toggle="collapse" role="button">
                            <i class="material-icons" style="font-size: 40px;">arrow_downward</i>
                        </a>
                    </li>
                </ul>
            </div>

                <div class="body collapse" id="cf_transactions_not_accepted">
                    @foreach($cf_transactions_not_accepted as $activeCF)
                        <div class="card">
                            <div class="header">
                                @if($activeCF->value < 0)
                                <form action="/profile/balance/cash_flow_transaction_accept" method="POST" onSubmit="return confirm('Вы уверены что получили эти деньги?')">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="transaction_id" value="{{ $activeCF->id }}">
                                    <button type="submit" class="btn bg-red pull-right"><i class="material-icons">done</i> ПОЛУЧИЛ</button>
                                </form>
                                @endif
                                <h2>
                                    <i class="fa font-24 {{ $activeCF->value < 0 ? 'col-green fa-sign-in' : 'col-deep-orange fa-sign-out' }}"></i>&nbsp;
                                    Заявка #{{ $activeCF->cash_flow_id }} от {{ isset($activeCF->cash_flow) ? $activeCF->cash_flow->created_at->format('d.m.y') : '[ошибка]' }}
                                </h2>

                            </div>
                            <div class="body">
                                Дата стыковки заявок: {{ isset($activeCF->cash_flow) ? $activeCF->created_at->format('d.m.y H:i') : '[ошибка]' }}<br><br>
                                {!! $activeCF->descCompiled() !!}
                                @if($activeCF->value > 0 && $activeCF->cross && $activeCF->cross->cash_flow)
                                    <h5>КОММЕНТАРИЙ ПОЛУЧАТЕЛЯ</h5>
                                    <div class="bg-light-yellow pd-10">
                                    {{ $activeCF->cross->cash_flow->comment }}
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endforeach
                </div>
        </div>

    @endif

    <div class="alert bg-black">
        <i class="material-icons p-r-10">warning</i>&nbsp;&nbsp;История баланса перенесена в раздел <b>Ваш профиль > История баланса</b>
    </div>

@endpermission

@permission('view_work_market')
    @include('dash.photo.work_market')
@endpermission

@permission('manage_projects_list')
    <div class="modal fade" id="add_project" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Создание/Редактирование проекта</h4>
                </div>
                <div class="modal-footer">
                    <span class="boss_buttons m-r-20">
                        @permission('logs_projects')
                        <a href="" target="_new" class="btn bg-indigo history" data-toggle="tooltip" data-original-title="История">
                            <i class="material-icons">history</i>
                        </a>
                        @endpermission
                        @permission('view_project_transactions')
                        <a href="" target="_new" class="btn bg-indigo transactions" data-toggle="tooltip" data-original-title="Транзакции связанные с проектом">
                            <i class="material-icons">attach_money</i>
                        </a>
                        @endpermission
                    </span>
                    <button type="button" class="btn bg-teal waves-effect clone" onClick="clone_project_form()">КЛОНИРОВАТЬ</button>
                    <button type="button" class="btn bg-teal waves-effect submit" onClick="$('#form-project').submit()">СОХРАНИТЬ</button>
                    <button type="button" class="btn bg-teal waves-effect" data-dismiss="modal">ОТМЕНА</button>
                </div>
                <div class="modal-body">
                    <form role="form" class="form-horizontal" method="POST" action="/project/add" id="form-project">
                        {{ csrf_field() }}
                        <div class="modal-icon loading hidden"></div>
                        <input type="hidden" value="" id="project_id" name="project_id">


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_worker_id">Исполнитель:</label>
                            <div class="col-sm-9">
                                <select name="project_worker_id" id="project_worker_id"
                                        class="form-control" data-live-search="true">
                                    <option value="0">не назначен</option>
                                    @foreach($workers_grouped as $region_code => $workers)
                                        <optgroup label="{{ $regions_by_code[$region_code] }}" data-rcode="{{ $region_code }}">
                                            @foreach($workers as $worker)
                                                <option value="{{ $worker->id }}" data-weight="{{ $loop->count - $loop->index }}" data-tokens="{{ $worker->name }} {{ $region_code }} {{ $regions_by_code[$region_code] }}" data-content="@if($worker->is_staff)<span class='glyphicon glyphicon-ok col-green'></span>@else<span class='glyphicon glyphicon-remove col-grey'></span>@endif {!! $worker->isDebt() ? "<span class='font-line-through'>" . $worker->name . "</span>" : $worker->name !!} @if($region_code)<span class='label bg-teal'>{{ $region_code }}</span>&nbsp;@endif<span class='label bg-{{ $worker->balance_color }}'>{{ number_format($worker->balanceCached(), 0) }}&nbsp;&#8381;</span><span class='desire'></span>">{{ $worker->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn bg-teal waves-effect hidden vk-send" data-toggle="tooltip" data-placement="top" data-original-title="">
                                    <i class="fa fa-vk" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <br>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_status_id">Статус проекта:</label>
                            <div class="col-sm-10">
                                <select name="project_status_id" id="project_status_id"
                                        class="form-control">
                                    @foreach(Config::get('project.status') as $key => $status)
                                        <option value="{{ $key }}" @if($key == 1 || $key == 99) class="hidden" @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                       <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_date_start">Дата начала съёмки:</label>
                            <div class="col-sm-10">
                                <div class='input-group'>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                                    <div class="form-line">
                                        <input type='text' name="project_date_start" class="form-control datepicker_default"
                                               id='project_date_start'/>
                                        <input type='hidden' name="project_time_start" class="form-control datepicker_default"
                                               id='project_time_start'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <div class="form-group hidden" id="time_line_row">
                            <label class="col-sm-2 control-label" for="time_line">Время:</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="project_time_unknown" id="project_time_unknown" class="filled-in">
                                <label for="project_time_unknown">Время неизвестно</label>
                                <input id="time_line" type="text" name="project_real_dates" value="" data-nigthmode="false" data-changed="false" />
                                <input id="time_line_start" type="hidden" name="project_real_start" value=""/>
                                <input id="time_line_end" type="hidden" name="project_real_end" value=""/>
                            </div>
                        </div>

                        <div class="form-group hidden" id="night_row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="alert bg-red text-center">
                                    <b>Внимание!!</b> Ночная съёмка, убедитесь что указана дата "по афише"!<br>
                                    Съёмка запланирована на ночь
                                    <span class="badge bg-amber font-20">
                                    @foreach(Config::get('project.week_night') as $key => $night_week)
                                        <span class="nw_day nw_{{ $key }} hidden">{{ $night_week }}</span>
                                    @endforeach
                                        <span class="sdate"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        {{--<div class="form-group">
                            <label class="col-sm-2 control-label" for="project_date_end">Дата/Время конца съёмки:</label>
                            <div class="col-sm-10">
                                <div class='input-group'>
                                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                                    <div class="form-line">
                                        <input type='text' name="project_date_end" class="form-control datepicker_default"
                                               id='project_date_end'/>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="project_date_deadline">Дедлайн:</label>
                            <div class="col-sm-10">
                                <div class='input-group'>
                                   <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                                    <div class="form-line">
                                        <input type='text' name="project_date_deadline"
                                               class="form-control datepicker_default" id='project_date_deadline'/>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="project_date_deadline2">Дедлайн 2:</label>
                            <div class="col-sm-10">

                                <div class="pull-left" id='project_date_deadline2'></div>
                                <div class="m-l-15 pull-left">
                                    <button type="button" class="btn bg-teal deadline2-set" data-value="0">
                                        <i class="material-icons">add</i>
                                        <span>0 ч</span>
                                    </button>
                                    <button type="button" class="btn bg-teal deadline2-set" data-value="44">
                                        <i class="material-icons">add</i>
                                        <span>44 ч</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_region_code">Регион:</label>
                            <div class="col-sm-10">
                                <select name="project_region_code" id="project_region_code"
                                        class="form-control">
                                    @foreach(App\Region::whereIn('code', Config::get('project.regions'))->orderBy('title', 'desc')->get() as $region)
                                    <option value="{{ $region->code }}">{{ $region->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_place_cached">Место:</label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <input type="text" id="project_place_cached" name="project_place_cached" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_client_cached">Клиент:

                            </label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <input type="text" id="project_client_cached" name="project_client_cached" class="form-control">
                                </div>


                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_client_vk_id">Профиль клиента в VK:

                            </label>
                            <div class="col-sm-8">
                                <div class="form-line">
                                    <input type="text" id="project_client_vk_id" name="project_client_vk_id" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <span class="font-bold col-teal font-18 m-l-10" id="vk_user_info"></span>
                            </div>
                        </div>

                        <div class="form-group hidden" id="deny_text_block">
                            <label class="col-sm-2 control-label" for="deny_text_to_client">Текст сообщения о закрытом доступе:</label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <textarea id="deny_text_to_client" name="deny_text_to_client" class="form-control">
                                    </textarea>
                                    <button type="button" class="btn btn-xs bg-teal pull-left vk_msg_deny_copy_button"><i class="material-icons" title="Скопировать текст">file_copy</i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_client_email">E-mail клиента:

                            </label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <input type="text" id="project_client_email" name="project_client_email" class="form-control">
                                </div>


                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_category_id">Раздел:</label>
                            <div class="col-sm-10">
                                <select name="project_category_id" id="project_category_id"
                                        class="form-control">
                                    @foreach(Config::get('project.category') as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_desc_public">Описание:</label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <textarea id="project_desc_public" name="project_desc_public"
                                              class="form-control no-resize auto-growth"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="project_desc_hidden">Описание<br/>(скрытое):</label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <textarea id="project_desc_hidden" name="project_desc_hidden"
                                              class="form-control no-resize"></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_amount_per_hour">Оплата в час:</label>
                            <div class="col-sm-10">
                                <div class='input-group'>
                                    <div class="form-line">
                                    <input type="text" id="project_amount_per_hour" name="project_amount_per_hour" value=""
                                           class="form-control input-text input-width-200">
                                    </div>
                                    <span class="input-group-addon">
                                руб.
                            </span>
                                </div>
                                <br>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_amount">Стоимость:</label>
                            <div class="col-sm-10">
                                <div class='input-group'>
                                    <div class="form-line">
                                    <input type="text" id="project_amount" name="project_amount" value=""
                                           class="form-control input-text input-width-200">
                                    </div>
                                    <span class="input-group-addon">
                                руб.
                            </span>
                                </div>

                                <!--<div class="alert alert-info" role="alert">Оплата фотографу: <strong id="new_order_price_photo">???</strong> [формулы не указаны]</div>-->
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_amount_to_office">Сдать в редакцию:</label>
                            <div class="col-sm-10">
                                <div class='input-group'>
                                    <div class="form-line">
                                    <input type="text" id="project_amount_to_office" name="project_amount_to_office" value=""
                                           class="form-control input-text input-width-200">
                                    </div>
                                    <span class="input-group-addon">
                                руб.
                            </span>
                                </div>

                            </div>

                        </div>

                        <div class="form-group">
                            <div class="finance_info col-sm-10 col-sm-push-2">
                                <div class="panel-body bg-light-yellow m-b-30">
                                    <div class="preloader pull-left hidden">
                                        <div class="spinner-layer pl-black">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="finance_data invisible">
                                        На руки фотографу: <span class="finance_amount_photo font-bold"></span>&nbsp;руб.<br>
                                        Сдать в редакцию: <span class="finance_amount_to_office font-bold"></span>&nbsp;руб.<br>
                                        Налоги: <span class="finance_tax font-bold"></span>&nbsp;руб.<br>
                                        <div class="{{ config('project.defaults.show_rf') ? '' : 'hidden' }}">
                                        Редсбор: <span class="finance_ed_fee font-bold"></span>&nbsp;руб.<br>
                                        </div>
                                        ФС: <span class="finance_ref font-bold"></span>&nbsp;руб.<br>
                                        СС: <span class="finance_ss_fee font-bold"></span>&nbsp;руб.<br>
                                        <div class="{{ config('project.defaults.show_rf') ? '' : 'hidden' }}">
                                        Редфонд: <span class="finance_rf font-bold"></span>%<br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_payment_type_id_w"><i
                                        class="fa fa-credit-card fa-lg fa-pull-left" aria-hidden="true"></i>Вид оплаты
                                для исполнителя:</label>
                            <div class="col-sm-10">
                                <select name="project_payment_type_id_w" id="project_payment_type_id_w"
                                        class="form-control">
                                    @foreach(Config::get('project.payment_type_w') as $key => $type)
                                        <option value="{{ $key }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_payment_type_id_b"><i
                                        class="fa fa-credit-card fa-lg fa-pull-left" aria-hidden="true"></i>Вид оплаты
                                для бух.:</label>
                            <div class="col-sm-10">
                                <select name="project_payment_type_id_b" id="project_payment_type_id_b"
                                        class="form-control">
                                    @foreach(Config::get('project.payment_type_b') as $key => $type)
                                        <option value="{{ $key }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_pass_type_id">Сдача заказчику:</label>
                            <div class="col-sm-10">
                                <select name="project_pass_type_id" id="project_pass_type_id"
                                        class="form-control">
                                    @foreach(Config::get('project.pass_type') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_geo_type_id">Geo Статус:</label>
                            <div class="col-sm-10">
                                <select name="project_geo_type_id" id="project_geo_type_id"
                                        class="form-control">
                                    @foreach(Config::get('project.geo_type') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_smm_type_id">SMM:</label>
                            <div class="col-sm-10">
                                <select name="project_smm_type_id" id="project_smm_type_id"
                                        class="form-control">
                                    @foreach(Config::get('project.smm_type') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_visibility_id">Кому доступно:</label>
                            <div class="col-sm-10">
                                <select name="project_visibility_id" id="project_visibility_id"
                                        class="form-control input-width-300">
                                    @foreach(Config::get('project.visibility') as $key => $status)
                                        <option value="{{ $key }}" data-type="{{ in_array($key, Config::get('project.visibility_list')) ? 'list' : 'default' }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-10 m-t-30 col-xs-push-2 hidden" id="project_visibility_list_row">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                    <div class="form-line">
                                        <label class="control-label" for="project_visibility_list">Кому именно?</label>
                                        <select name="project_visibility_list[]" multiple="multiple" id="project_visibility_list"
                                                class="form-control input-width-300" data-live-search="true">
                                            @foreach($workers_grouped as $region_code => $workers)
                                                <optgroup label="{{ $regions_by_code[$region_code] }}" data-rcode="{{ $region_code }}">
                                                    @foreach($workers as $worker)
                                                        <option value="{{ $worker->id }}" data-tokens="{{ $worker->name }} {{ $region_code }} {{ $regions_by_code[$region_code] }}" data-content="@if($worker->is_staff)<span class='glyphicon glyphicon-ok col-green'></span>@else<span class='glyphicon glyphicon-remove col-grey'></span>@endif {{ $worker->name }} @if($region_code)<span class='label bg-teal'>{{ $region_code }}</span>@endif">{{ $worker->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12"><br><h4>РЕФЕРРАЛ(ФС)</h4><br></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="project_referral_id">Кто привёл?:</label>
                            <div class="col-sm-9">
                                <select name="project_referral_id" id="project_referral_id"
                                        class="form-control"  data-live-search="true">
                                    <option value="0">никто</option>
                                    @foreach(App\User::whereHidden(0)->get(['id', 'name']) as $cord)
                                        <option value="{{ $cord->id }}">{{ $cord->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="project_referral_royalty">Процент:</label>
                            <div class="col-sm-9">
                                <div class='input-group'>
                                <div class="form-line">
                                <input type="text" id="project_referral_royalty" name="project_referral_royalty"
                                       class="form-control input-text input-width-full">
                                </div>
                                <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="finance_info col-sm-9 col-sm-push-3">
                                <div class="panel-body bg-light-yellow">
                                    <div class="preloader pl-size-xs pull-left hidden">
                                        <div class="spinner-layer pl-black">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="finance_data invisible">
                                        ФС: <span class="finance_ref font-bold"></span>&nbsp;руб.<br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12"><br><h4>ДОП. ОПЦИИ</h4><br></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="project_ssmanager_id">Менеджер сложной
                                съёмки:</label>
                            <div class="col-sm-9">
                                <select name="project_ssmanager_id" id="project_ssmanager_id"
                                        class="form-control">
                                    <option value="0">не нужен</option>
                                    @foreach(App\User::whereHidden(0)->whereHas('roles', function($query){
                                            $query->where('name', 'manager');
                                        })->get(['id', 'name']) as $cord)
                                        <option value="{{ $cord->id }}">{{ $cord->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="project_ssmanager_royalty">Процент
                                менеджера:</label>
                            <div class="col-sm-9">
                                 <div class='input-group'>

                                <div class="form-line">
                                <input type="text" id="project_ssmanager_royalty" name="project_ssmanager_royalty"
                                       class="form-control">
                                </div>
                                <span class="input-group-addon">%</span>
                                 </div>
                            </div>
                            <div class="finance_info col-sm-9 col-sm-push-3">
                                <div class="panel-body bg-light-yellow">
                                    <div class="preloader pl-size-xs pull-left hidden">
                                        <div class="spinner-layer pl-black">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="finance_data invisible">
                                        СС: <span class="finance_ss_fee font-bold"></span>&nbsp;руб.<br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_ss_mode">Режим СС:</label>
                            <div class="col-sm-10">
                                <select name="project_ss_mode" id="project_ss_mode"
                                        class="form-control">
                                    <option value="0" {{ config('project.defaults.ss_mode') == 0 ? 'selected' : '' }}>CC + РС</option>
                                    <option value="1" {{ config('project.defaults.ss_mode') == 1 ? 'selected' : '' }}>CC включён в РС</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_ss_base">Считать СС от:</label>
                            <div class="col-sm-10">
                                <select name="project_ss_base" id="project_ss_base"
                                        class="form-control">
                                    <option value="0" {{ config('project.defaults.ss_base') == 0 ? 'selected' : '' }}>Стоимости</option>
                                    <option value="1" {{ config('project.defaults.ss_base') == 1 ? 'selected' : '' }}>Прибыли</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-12"><br><h4>КООРДИНАТОРЫ</h4><br></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_cord_prin_id">Принимающий к.:</label>
                            <div class="col-sm-10">
                                <select multiple="multiple" name="project_cord[prin][]" id="project_cord_prin_id"
                                        class="form-control">
                                    @foreach(App\User::whereHidden(0)->whereHas('roles', function($query){
                                            $query->where('name', 'cord');
                                        })->get(['id', 'name']) as $cord)
                                        <option value="{{ $cord->id }}">{{ $cord->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_cord_rasp_id">Распределяющий к.:</label>
                            <div class="col-sm-10">
                                <select multiple="multiple" name="project_cord[rasp][]" id="project_cord_rasp_id"
                                        class="form-control">
                                    @foreach(App\User::whereHidden(0)->whereHas('roles', function($query){
                                            $query->where('name', 'cord');
                                        })->get(['id', 'name']) as $cord)
                                        <option value="{{ $cord->id }}">{{ $cord->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_cord_control_id">Контролирующий к.:</label>
                            <div class="col-sm-10">
                                <select multiple="multiple" name="project_cord[control][]" id="project_cord_control_id"
                                        class="form-control">
                                    @foreach(App\User::whereHidden(0)->whereHas('roles', function($query){
                                            $query->where('name', 'cord');
                                        })->get(['id', 'name']) as $cord)
                                        <option value="{{ $cord->id }}">{{ $cord->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_download_link">Ссылка на файлы:</label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <input type="text" id="project_download_link" name="project_download_link" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="project_geo_link">Ссылка на GEO:</label>
                            <div class="col-sm-10">
                                <div class="form-line">
                                    <input type="text" id="project_geo_link" name="project_geo_link" class="form-control">
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-teal waves-effect submit" onClick="$('#form-project').submit()">СОХРАНИТЬ</button>
                    <button type="button" class="btn bg-teal waves-effect" data-dismiss="modal">ОТМЕНА</button>
                </div>
            </div>
        </div>
    </div>

<div id="deny_text_to_client_copy"></div>

@endpermission

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            @if (session('success'))
            swal("Ура!", "{{ session('success') }}", "success");
            @endif
            @if (session('error'))
            swal("УПС!", "{{ session('error') }}", "error");
            @endif

        });
    </script>


@endsection

@push('js_custom')

    <script src="/plugins/jquery-datatable/jquery.dataTables.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="https://cdn.datatables.net/colreorder/1.4.1/js/dataTables.colReorder.min.js?v={{ config('app.js_version_prefix') }}"></script>

    <script src="/plugins/jquery-sort-element/jquery.sortElements.js?v={{ config('app.js_version_prefix') }}"></script>

@endpush

@push('css')
    <link href="/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/colreorder/1.4.1/css/colReorder.dataTables.min.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
@endpush