
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>ТЕКУЩИЕ ПРОЕКТЫ</h2>
            </div>
            <div class="body">

                <div class="table-responsive _hidden-xs">
                    <table id="now_project_list_table" class="table table-bordered table-striped table-condensed" style="background-color: #fff;">
                        <thead>
                            <tr>
                                <th>Действия</th>
                                <th>ID</th>
                                <th>Тип</th>
                                <th>Статус проекта</th>
                                <th>Когда съёмка:</th>
                                <th>Дедлайн:</th>
                                <th>Регион</th>
                                <th>Клиент</th>
                                <th>Место</th>
                                <th>Описание</th>
                                <th>Оплата в час</th>
                                <th>Стоимость</th>
                                <th>Вид оплаты</th>
                                <th>Сдача заказчику</th>
                                <th>Geo Статус</th>
                                <th>CC</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($photo_now_project_list as $project)
                            <tr>
                                <td class="text-center">

                                @if(trim($project->download_link) === '')
                                    <button type="button" class="btn btn-xs bg-teal waves-effect mg-5 pd-5" title="Ссылка на архив с материалом" onClick="showPromptMessage('Укажите ссылку файлы отчёта', {{ $project->id }}, 'down')">
                                        <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                    </button>
                                @endif


                                @if($project->geo_type_id === 0)
                                    <button type="button" class="btn btn-xs bg-teal waves-effect mg-5 pd-5" title="Ссылка на фотоотчёт на ГЕО" onClick="showPromptMessage('Укажите ссылку на отчёт на Геометрии', {{ $project->id }}, 'geo')">
                                        <i class="fa fa-google" aria-hidden="true"></i>
                                    </button>
                                @endif

                                </td>
                                <td>{{ $project->id }}</td>
                                <td><i class="material-icons" title="{{ Config::get('project.category')[$project->category_id] }}">{{ Config::get('project.category_icon')[$project->category_id] }}</i></td>
                                <td>

                                    <div>
                                        @if(trim($project->download_link) === '')
                                            <i class="material-icons col-grey">query_builder</i><span class="icon-label">Ожидается ссылка на материал</span>
                                        @else
                                            <i class="material-icons col-green">done</i><span class="icon-label">Ссылка на материал отправлена</span>
                                        @endif
                                    </div>

                                    <div>
                                        @if($project->geo_type_id === 0)
                                            <i class="material-icons col-grey">query_builder</i><span class="icon-label">Ожидается ссылка на ГЕО</span>
                                        @elseif($project->geo_type_id === 2)
                                            <i class="material-icons col-green">done</i><span class="icon-label">Ссылка на ГЕО отправлена</span>
                                        @endif
                                    </div>

                                    <div>
                                        @if($project->pass_type_id === 0)
                                            <i class="material-icons col-grey">query_builder</i><span class="icon-label">Ожидается отправка заказчику</span>
                                        @elseif($project->pass_type_id === 2)
                                            <i class="material-icons col-green">done</i><span class="icon-label">Отправлено заказчику</span>
                                        @endif
                                    </div>

                                </td>
                                <td>
                                    @if($project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') }}@else ? @endif&nbsp;@if(!$project->time_unknown && $project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('H:i') }}@else ?:? @endif<br>@if($project->date_end){{ $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.y') }}@endif&nbsp;@if(!$project->time_unknown && $project->date_end){{ $project->date_end->timezone(config('project.city_time_zone'))->format('H:i') }}@else ?:? @endif
                                </td>
                                <td>
                                    @if($project->date_deadline){{ $project->date_deadline->timezone(config('project.city_time_zone'))->format('d.m.y H:i') }}@else ? @endif
                                </td>
                                <td>{{ $project->region_code }}</td>
                                <td>{{ $project->client_cached }}</td>
                                <td>{{ $project->place_cached }}</td>
                                <td>{{ str_limit($project->desc_public, 50) }}
                                    @if(Illuminate\Support\Str::length($project->desc_public) > 50)

                                        <a href="#" tabindex="0" class="btn bg-teal btn-xs"
                                                data-trigger="focus" data-container="body" data-toggle="popover"
                                                data-content="{{  $project->desc_public }}" onclick="event.stopPropagation()">
                                            подробнее...
                                        </a>

                                    @endif
                                </td>
                                <td>{{ $project->amount_per_hour }}</td>
                                <td>{{ $project->calc_amount_photo_whis_tax() }}</td>
                                <td>
                                    {{ Config::get('project.payment_type_w')[$project->payment_type_id_w] }}
                                    {{ ($project->payment_type_id_w == 0 && $project->payment_type_id_b > 0) ? '(ИскНАЛ!)' : '' }}
                                </td>
                                <td>{{ Config::get('project.pass_type')[$project->pass_type_id] }}</td>
                                <td>{{ Config::get('project.geo_type')[$project->geo_type_id] }}</td>

                                <td>
                                    @if($project->ssmanager)
                                        {{ $project->ssmanager->user->name }} ({{ $project->ssmanager->royalty }}%)
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
            </div>
        </div>
    </div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>БИРЖА</h2>
            </div>
            <div class="body">

                @if(Auth::user()->isDebt())

                    <div class="info-box-2 bg-red">
                        <div class="icon">
                            <i class="material-icons">sentiment_very_dissatisfied</i>
                        </div>
                        <div class="content">
                            <div class="text">ВЫ ПРИВЫСИЛИ ПОРОГ БЛОКИРОВКИ</div>
                            <div class="number">Биржа проектов недоступна</div>
                        </div>
                    </div>

                @else
                <div class="table-responsive _hidden-xs">
                    <table id="photo_work_market_project_list_table" class="table table-bordered table-striped table-condensed" style="background-color: #fff;">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Тип</th>
                                <th>Когда съёмка:</th>
                                <th>Дедлайн:</th>
                                <th>Регион</th>
                                <th>Клиент</th>
                                <th>Место</th>
                                <th>Описание</th>
                                <th>Оплата в час</th>
                                <th>Стоимость</th>
                                <th>Вид оплаты</th>
                                <th>Сдача заказчику</th>
                                <th>Geo Статус</th>
                                <th>CC</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($photo_work_market_project_list as $project)
                                <tr>
                                    <td class="text-center market-vote">
                                        <input type="number" value="{{ $project->desireAuth ? $project->desireAuth->rating : 0 }}" name="desirability[{{ $project->id }}]" id="desirability_{{ $project->id }}" data-project-id="{{ $project->id }}" class="rating" data-empty-value="0" data-clearable="отказаться" data-icon-lib="fa" data-active-icon="fa-heart" data-inactive-icon="fa-heart-o" data-clearable-icon="fa-minus-circle"/>
                                    </td>
                                    <td>{{ $project->id }}</td>
                                    <td><i class="material-icons" title="{{ Config::get('project.category')[$project->category_id] }}">{{ Config::get('project.category_icon')[$project->category_id] }}</i></td>
                                    <td>
                                        @if($project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') }}@else ? @endif&nbsp;@if(!$project->time_unknown && $project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('H:i') }}@else ?:? @endif<br>@if($project->date_end){{ $project->date_end->timezone(config('project.city_time_zone'))->format('d.m.y') }}@endif&nbsp;@if(!$project->time_unknown && $project->date_end){{ $project->date_end->timezone(config('project.city_time_zone'))->format('H:i') }}@else ?:? @endif
                                    </td>
                                    <td>
                                        @if($project->date_deadline){{ $project->date_deadline->timezone(config('project.city_time_zone'))->format('d.m.y H:i') }}@else ? @endif
                                    </td>
                                    <td>{{ $project->region_code }}</td>
                                    <td>{{ $project->client_cached }}</td>
                                    <td>{{ $project->place_cached }}</td>
                                    <td>{{ $project->desc_public }}</td>
                                    <td>{{ $project->amount_per_hour }}</td>
                                    <td>{{ $project->calc_amount_photo_whis_tax() }}</td>
                                    <td>{{ Config::get('project.payment_type_w')[$project->payment_type_id_w] }}</td>
                                    <td>{{ Config::get('project.pass_type')[$project->pass_type_id] }}</td>
                                    <td>{{ Config::get('project.geo_type')[$project->geo_type_id] }}</td>

                                    <td>
                                        @if($project->ssmanager)
                                            {{ $project->ssmanager->user->name }} ({{ $project->ssmanager->royalty }}%)
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>


                <div class="panel-group hidden" id="accordion_work_market" role="tablist" aria-multiselectable="true">
                    @foreach($photo_work_market_project_list as $project)
                    <div class="panel panel-col-green">
                        <div class="panel-heading" role="tab" id="project_{{ $project->id }}">
                            <h4 class="panel-title">
                                <a role="button" class="font-14" data-toggle="collapse" data-parent="#accordion_work_market" href="#collapse_project_{{ $project->id }}" aria-expanded="true" aria-controls="collapse_project_{{ $project->id }}">
                                    <i class="material-icons font-14" title="{{ Config::get('project.category')[$project->category_id] }}">{{ Config::get('project.category_icon')[$project->category_id] }}</i>
                                    @if($project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y') }}@else ? @endif&nbsp;@if(!$project->time_unknown && $project->date_start){{ $project->date_start->timezone(config('project.city_time_zone'))->format('H:i') }}@else ?:? @endif
                                    {{ $project->place_cached }}
                                    <span class="badge bg-cyan pull-right">{{ $project->calc_amount_photo_whis_tax() }}</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_project_{{ $project->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_project_{{ $project->id }}">
                            <div class="panel-body">
                                <div class="text-center market-vote">
                                    <input type="number" value="{{ $project->desireAuth ? $project->desireAuth->rating : 0 }}" name="desirability[{{ $project->id }}]" id="desirability_mobile_{{ $project->id }}" data-project-id="{{ $project->id }}" class="rating" data-empty-value="0" data-clearable="отказаться" data-icon-lib="fa" data-active-icon="fa-heart" data-inactive-icon="fa-heart-o" data-clearable-icon="fa-minus-circle"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                @endif

            </div>
            </div>
        </div>
    </div>



@push('js_custom')
    <script src="/plugins/star-rating/bootstrap-rating-input.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/js/form.market.js?v={{ config('app.js_version_prefix') }}"></script>
@endpush