@extends('layouts.sign')

<!-- Main Content -->
@section('content')
<body class="fp-page @if ($errors->has('email')) bg-red @else bg-teal @endif">
    <div class="fp-box">
        <div class="logo">
            <span class="logo-img"></span>
            <small>Geometria Coordination System</small>
        </div>
        <div class="card">
            @if ($errors->has('email'))
            <div class="alert bg-black align-center">{{ $errors->first('email') }}</div>
            @endif
            @if (session('status'))
                <div class="alert bg-black text-center">
                    {{ session('status') }}
                </div>
            @endif
            <div class="body">
                <form id="forgot_password" method="POST" action="{{ url('/password/email') }}">
                    {{ csrf_field() }}
                    @if (!session('status'))
                        <div class="msg">
                            Введите указанный при регистрации e-mail.<br> Мы отправим вам письмо с ссылкой для создания нового пароля.
                        </div>
                    @endif
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line{{ $errors->has('email') ? ' focused error' : '' }}">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-teal waves-effect" type="submit">СБРОСИТЬ МОЙ ПАРОЛЬ</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="{{ url('/login') }}">Войти</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
