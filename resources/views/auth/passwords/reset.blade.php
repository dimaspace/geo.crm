@extends('layouts.sign')

@section('content')
<body class="signup-page @if ($errors->has('email') or $errors->has('password') or $errors->has('password_confirmation')) bg-red @else bg-teal @endif">
    <div class="signup-box">
        <div class="logo">
            <span class="logo-img"></span>
            <small>Geometria Coordination System</small>
        </div>
        <div class="card">
            @if (session('status'))
                <div class="alert bg-black text-center">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->has('email'))
                <div class="alert bg-black text-center">
                    {{ $errors->first('email') }}
                </div>
            @endif
            @if ($errors->has('password'))
                <div class="alert bg-black text-center">
                    {{ $errors->first('password') }}
                </div>
            @endif
            @if ($errors->has('password_confirmation'))
                <div class="alert bg-black text-center">
                    {{ $errors->first('password_confirmation') }}
                </div>
            @endif
            <div class="body">
                <form id="sign_up" method="POST" action="{{ url('/password/reset') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    @if (!$errors->has('email') and !$errors->has('password') and !$errors->has('password_confirmation'))
                        <div class="msg">Укажите новый пароль</div>
                    @endif
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line{{ $errors->has('email') ? ' focused error' : '' }}">
                            <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="Email" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line{{ $errors->has('password') ? ' focused error' : '' }}{{ $errors->has('password_confirmation') ? ' focused error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="Пароль" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line{{ $errors->has('password') ? ' focused error' : '' }}{{ $errors->has('password_confirmation') ? ' focused error' : '' }}">
                            <input type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="Подтверждение пароля" required>
                        </div>
                    </div>
                    <button class="btn btn-block btn-lg bg-teal waves-effect" type="submit">СМЕНИТЬ ПАРОЛЬ</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="{{ url('/login') }}">Войти</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
