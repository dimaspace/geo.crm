@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                ПАРАМЕТРЫ РАССЫЛКИ
            </h2>
        </div>
        <div class="body">
                            <form class="form-horizontal" method="POST" action="/newsletter/send">
                                {{ csrf_field() }}
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_msg">Обладателям каких ролей разослать?</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">

                                                @foreach(App\Role::get() as $role)
                                                    <input type="checkbox" name="roles[]" value="{{$role->name}}" id="md_checkbox_{{$role->name}}" class="filled-in chk-col-teal" @if($role->name == 'boss') checked @endif />
                                                    <label class="m-r-45" for="md_checkbox_{{$role->name}}">@if($role->display_name){{$role->display_name}}@else{{$role->name}}@endif</label>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_msg">Фильтр регионов</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">

                            <input type="checkbox" name="region_filter_on" value="1" id="region_filter_on" class="filled-in chk-col-teal"  />
                            <label class="m-r-45" for="region_filter_on">Активировать фильтрацию по регионам</label>

                            <div id="newsletter_regions_aria" class="hidden">
                                <select id="newsletter_regions" name="newsletter_regions[]" class="ms" multiple="multiple">
                                    <optgroup label="Регионы без грыппы">
                                        @foreach(App\Region::orderBy('title')->get() as $region)
                                            <option value="{{ $region->id }}">{{ $region->title }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_msg">Текст сообщения на почту</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea class="tinymce" id="email_msg" name="email_msg"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="telegram_msg">Текст сообщения в Telegram</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="6" class="form-control no-resize" id="telegram_msg" name="telegram_msg"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn bg-teal">РАЗОСЛАТЬ</button>
                                    </div>
                                </div>
                            </form>




        </div>
    </div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        @if (session('success'))
            swal("Ура!", "{{ session('success') }}", "success");
        @endif
    });
</script>

@endsection