<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ config('app.name', 'GeoCRM') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset(config('app.favicon')) }}?v={{ config('app.js_version_prefix') }}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,latin-ext" rel="stylesheet">

    <!-- Font -->
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css?v={{ config('app.js_version_prefix') }}">

    <!-- Bootstrap Core Css -->
    <link href="/plugins/bootstrap/css/bootstrap.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/plugins/node-waves/waves.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/plugins/animate-css/animate.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="/plugins/morrisjs/morris.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ mix('css/style.css') }}" rel="stylesheet">
    @if(config('app.color') == 'test')
    <link href="/css/test-mode.css" rel="stylesheet">
    @endif

    <!-- Bootstrap Select Css -->
    <link href="/plugins/bootstrap-select/css/bootstrap-select.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/css/themes/all-themes.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Moment Plugin Js -->
    <script src="/plugins/momentjs/moment-with-locales.js?v={{ config('app.js_version_prefix') }}?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Range Slider Css -->
    <link href="/plugins/ion-rangeslider/css/ion.rangeSlider.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />
    <link href="/plugins/ion-rangeslider/css/ion.rangeSlider.skinFlat.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/plugins/sweetalert/sweetalert2.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="/plugins/dropzone/dropzone.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="/plugins/multi-select/css/multi-select.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">

    <!-- Light Gallery Plugin Css -->
    <link href="/plugins/light-gallery/css/lightgallery.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">

    <!-- Chosen-->
    <link rel="stylesheet" type="text/css" href="/plugins/chosen/chosen.css?v={{ config('app.js_version_prefix') }}">

    <!-- Bootstrap Daterangepicker -->
    <link rel="stylesheet" type="text/css" href="/plugins/bootstrap-daterangepicker/daterangepicker.css?v={{ config('app.js_version_prefix') }}">

    <!-- DevExpress -->
    <link rel="stylesheet" type="text/css" href="{{ mix('plugins/DevExpress/css/dx.common.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ mix('plugins/DevExpress/css/dx.light.css') }}" />

    <script src="{{ mix('js/geocrm.js') }}" type="text/javascript"></script>

     @stack('css')

     @stack('js')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>

<body class="{{ config('app.color') == 'teal' ? 'theme-black' : 'theme-pink' }} @if(isset($body_class)){{ $body_class }}@endif">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-teal">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Загрузка...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="Поиск пока не пашет :)">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                @if(config('app.color') != 'test')
                    <a class="navbar-brand" href="/"><span class="logo"></span></a>
                @else
                    <a class="navbar-brand" href="/">ТЕСТОВЫЙ СЕРВЕР <i class="material-icons">announcement</i></a>
                @endif
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                    <!-- Users Online -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">supervisor_account</i>
                            <span class="label-count" id="navbar_online_num">0</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">СЕЙЧАС В CRM</li>
                            <li class="body">
                                <ul class="menu online" id="navbar_online_list">
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Users Online -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">0</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">УВЕДОМЛЕНИЯ</li>
                            <li class="body">
                                <ul class="menu">
                                    <li class="font-14 text-center p-t-20 col-grey">
                                        уведомлений нет
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <li class="pull-right"><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="bottom" data-original-title="Выйти"><i class="material-icons">input</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        @include('left_sidebar.index')
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{  $PAGE_TITLE or ''  }}</h2>
            </div>
                @yield('content')

        </div>
    </section>



    <!-- Jquery Core Js -->
    <script src="/plugins/jquery/jquery.min.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="/plugins/bootstrap/js/bootstrap.min.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Select Plugin Js -->
    <script src="/plugins/bootstrap-select/js/bootstrap-select.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/bootstrap-select/js/i18n/defaults-ru_RU.min.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/plugins/jquery-slimscroll/jquery.slimscroll.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/plugins/node-waves/waves.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/plugins/jquery-countto/jquery.countTo.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Morris Plugin Js -->
    <script src="/plugins/raphael/raphael.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/morrisjs/morris.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- ChartJs -->
    <script src="/plugins/chartjs/Chart.bundle.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="/plugins/flot-charts/jquery.flot.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/flot-charts/jquery.flot.resize.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/flot-charts/jquery.flot.pie.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/flot-charts/jquery.flot.categories.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/flot-charts/jquery.flot.time.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="/plugins/jquery-sparkline/jquery.sparkline.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Tooltip -->
    <script src="/js/pages/ui/tooltips-popovers.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="/plugins/multi-select/js/jquery.multi-select.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="/plugins/quicksearch/js/jquery.quicksearch.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/plugins/jquery-validation/jquery.validate.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- RangeSlider Plugin Js -->
    <script src="/plugins/ion-rangeslider/js/ion.rangeSlider.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- SweetAlert Plugin Js -->
    <!--<script src="/plugins/sweetalert/sweetalert.min.js?v={{ config('app.js_version_prefix') }}"></script>-->
    <script src="/plugins/sweetalert/sweetalert2.all.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- TinyMCE -->
    <script src="/plugins/tinymce/tinymce.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Autosize Plugin Js -->
    <script src="/plugins/autosize/autosize.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Dropzone Plugin Js -->
    <script src="/plugins/dropzone/dropzone.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/plugins/light-gallery/js/lightgallery-all.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="/plugins/bootstrap-notify/bootstrap-notify.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Cookie Js -->
    <script src="/plugins/cookie/js.cookie.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Chosen -->
    <script src="/plugins/chosen/chosen.jquery.min.js?v={{ config('app.js_version_prefix') }}"></script>

    <!-- Bootstrap Daterangepicker -->
    <script src="/plugins/bootstrap-daterangepicker/daterangepicker.js?v={{ config('app.js_version_prefix') }}"></script>

     @stack('js_plugins')

    <!-- Custom Js -->
    <script src="{{ mix('js/admin.js') }}"></script>
    <script src="{{ mix('js/forms.js') }}"></script>

    <!-- Jquery Core Js
    <script src="/plugins/jquery/jquery-3.2.1.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script type="text/javascript">
        var jQuery_3_2_1 = $.noConflict(true);
    </script>-->


<!--    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script> -->
<!--<script src="https://js.pusher.com/5.0/pusher.min.js"></script>-->
    <script src="{{ mix('js/echo.init.js') }}"></script>

    <script src="{{ mix('js/echo.sockets.js') }}"></script>

    <!-- DevExpress -->
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/dx.all.js') }}"></script>
    <script src="https://unpkg.com/devextreme-intl@18.2/dist/devextreme-intl.min.js"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/localization/dx.messages.ru.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/dx.custom.global.options.js') }}"></script>
    <!-- Globalize scripts -->
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/cldr.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/cldr/event.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/cldr/supplemental.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/globalize.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/globalize/message.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/globalize/number.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/globalize/currency.js') }}"></script>
    <script type="text/javascript" src="{{ mix('plugins/DevExpress/js/globalize/date.js') }}"></script>

    <script>
DevExpress.config({ defaultCurrency: 'RUB' });
DevExpress.localization.locale(navigator.language || navigator.browserLanguage);
window.cfg_time_zone = '{{ config('project.city_time_zone') }}';
window.cfg_time_zone_momentjs = '{{ config('project.cfg_time_zone_momentjs', '+03:00') }}';
    </script>

    @stack('js_custom')
    @stack('mustache')
    @javascript('auth_user', auth()->user());
    <script type="text/javascript">
            @if(Auth::user()->isDebt())
            showNotification('bg-red', '<p>Привышен лимит блокировки!<br>В ближайшее время вас перестанут назначать на съёмки!<p>', 10000, 'top', 'center');
            @endif
    </script>


    <!-- Demo Js -->
    <script src="{{ mix('js/demo.js') }}"></script>
    @yield('footer')



</body>

</html>



