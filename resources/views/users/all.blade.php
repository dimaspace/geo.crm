@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="header">
            <h2>
                ВСЕ ПОЛЬЗОВАТЕЛИ
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="#" data-toggle="modal" data-target="#add_photoman" data-mode="new"><i
                                class="material-icons col-teal">add_box</i></a>
                </li>
            </ul>
        </div>
        <div class="body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-condensed" id="users_datatable" data-searching="true">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Баланс</th>
                                    <th>Баланс(кэш)</th>
                                    <th>Имя</th>
                                    <th>E-mail</th>
                                    <th>Роли</th>
                                    <th>Регион</th>
                                    <th>VK ID</th>
                                    <th>Skype</th>
                                    <th>Instagramm</th>
                                    <th>Скрыт?</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse(App\User::get() as $user)
                                <tr data-id="{{ $user->id }}" data-mode="edit">
                                    <td>{{ $user->id }}</td>
                                    <td class="text-center col-{{  $user->balance() >= 0 ? 'teal' : ($user->isDebt() ? 'red' : 'orange') }}">
                                        {{ number_format($user->balance(), 0) }}<br>
                                        <span class="label label-default" title="Порог блокировки">
                                        {{ $user->threshold > 0 ? '-' . number_format($user->threshold, 0) : 0 }}
                                        </span>
                                    </td>
                                    <td>{{ $user->balanceCached() }}</td>
                                    <td><a href="/users/profile/{{ $user->id }}">{{ $user->name }}</a></td>
                                    <td>
                                        @if(preg_match('/@/', $user->email)) {{ $user->email }} @else <span class="col-red">не указан</span> @endif
                                        @permission('free_login')
                                            &nbsp;<a href="/users/login/{{ $user->id }}"><i class="material-icons col-teal pull-right">directions_run</i></a>
                                        @endpermission
                                    </td>
                                    <td>@forelse($user->roles as $region)<span class="bg-teal font-12 pd-5 mg-2">{{ trim($region->display_name) != "" ? $region->display_name : $region->name}}</span>@empty <span class="col-red">без роли</span> @endforelse</td>
                                    <td>
                                        @if(!$user->primary_region)
                                            <div class="alert bg-red">Нет главного региона!</div>
                                        @else
                                            @forelse($user->regions as $region)
                                                <span class="{{ ($region->id != $user->primary_region->id) ? 'bg-teal' : 'bg-pink' }} font-12 pd-5 mg-2">{{ $region->title }}</span>
                                            @empty
                                                <span class="col-red">без региона</span>
                                            @endforelse
                                        @endif
                                    </td>
                                    <td>{{ $user->vk_id }}</td>
                                    <td>@if($user->info && $user->info->skype){{ $user->info->skype }}@endif</td>
                                    <td>@if($user->info && $user->info->instagramm){{ $user->info->instagramm }}@endif</td>
                                    <td class="text-center"><i class="material-icons {{ $user->hidden ? 'col-grey' : 'col-teal' }}">{{ $user->hidden ? 'visibility_off' : 'visibility' }}</i></td>
                                </tr>
                                @empty
                                <tr>
                                    <td>Никого нет :(</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

    <div class="modal fade" id="add_photoman" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Создание/Редактирование фотографа</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-teal waves-effect submit" onClick="$('#add_photoman form').submit()">СОХРАНИТЬ</button>
                    <button type="button" class="btn bg-teal waves-effect" data-dismiss="modal">ОТМЕНА</button>
                </div>
                <div class="modal-body">
            @form($form_edit)
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-teal waves-effect submit" onClick="$('#add_photoman form').submit()">СОХРАНИТЬ</button>
                    <button type="button" class="btn bg-teal waves-effect" data-dismiss="modal">ОТМЕНА</button>
                </div>
            </div>
        </div>
    </div>

        </div>
    </div>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            autosize($('textarea.auto-growth'));
            @if (session('success'))
                swal("Ура!", "{{ session('success') }}", "success");
            @endif

            @if (session('errors'))
                swal("Ошибка!", "" +
            @foreach($errors->all() as $error)
                "{{ $error }}<br>" +
            @endforeach
                "", "error");
                $('#add_photoman').modal('show');
            @endif
        });
    </script>

@endsection

@push('js_custom')

    <script src="/plugins/jquery-datatable/jquery.dataTables.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="https://cdn.datatables.net/colreorder/1.4.1/js/dataTables.colReorder.min.js?v={{ config('app.js_version_prefix') }}"></script>

    <script src="/plugins/jquery-sort-element/jquery.sortElements.js?v={{ config('app.js_version_prefix') }}"></script>
    <script src="/js/form.users.js"></script>
@endpush

@push('css')
    <link href="/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/colreorder/1.4.1/css/colReorder.dataTables.min.css?v={{ config('app.js_version_prefix') }}" rel="stylesheet">
@endpush