@extends('layouts.main')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-{{ $user->balance_color }}">
                <div class="icon">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text">ТЕКУЩИЙ БАЛАНС</div>
                    <div class="number">
                        {{ number_format($user->balance(), 2) }}&nbsp;&#8381;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="info-box bg-{{ $user->threshold > 0 ? $user->balance_color : 'grey' }}">
                <div class="icon">
                    <i class="material-icons">gavel</i>
                </div>
                <div class="content">
                    <div class="text">ПОРОГ БЛОКИРОВКИ</div>
                    <div class="number">
                        {{ $user->threshold > 0 ? '-' . number_format($user->threshold, 0) : 0 }}&nbsp;&#8381;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-grey">
                <div class="icon">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="content">
                    <div class="text">ОЖИДАЕМЫЙ БАЛАНС</div>
                    <div class="number">
                       [НЕДОСТУПНО]
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>ИСТОРИЯ БАЛАНСА @permission('manual_transactions')<a href="#collapse_add" type="button" class="btn btn-xs bg-teal pull-right" role="button" data-toggle="collapse" aria-controls="collapse_add"><i class="material-icons">add</i></a>@endpermission</h2>

                </div>
                <div class="body">
                    @permission('manual_transactions')
                        <div id="collapse_add" class="collapse">
                            <form action="/transaction/add" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>С кого снять</label>
                                                <select name="from" class="form-control" data-live-search="true">
                                                    @foreach(App\User::get() as $key => $user_row)
                                                        <option @if($user_row->id == $user->id) selected @endif value="{{ $user_row->id }}">{{ $user_row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>Кому положить</label>
                                                <select name="to" class="form-control" data-live-search="true">
                                                    @foreach(App\User::get() as $key => $user_row)
                                                        <option @if($user_row->id == $user->id) selected @endif value="{{ $user_row->id }}">{{ $user_row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>Сумма</label>
                                                <input name="value" type="text" class="form-control" placeholder="100.00">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>Комментарий</label>
                                                <textarea name="comment" class="form-control" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>Тип транзакции</label>
                                                <select name="type" class="form-control">
                                                    @foreach(App\BalanceTransaction::_getTypes() as $key => $type)
                                                        @if(App\BalanceTransaction::_getTypeIdByCode($key) >= 5000)
                                                        <option value="{{ $key }}">{{ $type }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                                <input type="submit" value="СОЗДАТЬ">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endpermission

                    <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
                    @forelse($transactions as $transaction)
                        @if($transaction->project)
                            <div class="panel transactions-list panel-col-clear">
                                <div class="panel-heading" role="tab" id="heading_{{ $transaction->project->id }}">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion_{{ $transaction->project->id }}" href="#collapse_{{ $transaction->project->id }}" aria-expanded="false"
                                           aria-controls="collapse_{{ $transaction->project->id }}">
                                            <i class="material-icons">{{ Config::get('project.category_icon')[$transaction->project->category_id] }}</i> [{{ $transaction->created_at->format('d.m.y') }}] Съёмка в {{ $transaction->project->place_cached }}
                                            <span class="badge {{ $transactions_by_project[$transaction->project_id]->sum('value') > 0 ? 'bg-green' : 'bg-red' }}  pull-right">@if($transactions_by_project[$transaction->project_id]->sum('value') > 0) + @endif {{ number_format($transactions_by_project[$transaction->project_id]->sum('value'), 2) }}</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse_{{ $transaction->project->id }}" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingThree_17">
                                    <div class="panel-body">
                                        <b>ID проекта</b>: {{ $transaction->project->id }}<br>
                                        <b>Дата проекта</b>: {{ $transaction->project->date_start->timezone(config('project.city_time_zone'))->format('d.m.y H:i') }}<br><br>
                                        <ul class="list-group">
                                            @if(isset($transactions_by_project[$transaction->project->id]))
                                                @foreach($transactions_by_project[$transaction->project->id] as $transaction)
                                                    <li class="list-group-item">
                                                        <span class="badge {{ $transaction->value > 0 ? "bg-green" : "bg-red" }}">@if($transaction->value > 0) + @endif {{ number_format($transaction->value, 2) }}</span>

                                                        <span class="{{ $transaction->value > 0 ? 'col-teal' : 'col-red' }}">
                                                        #{{ $transaction->id }}

                                                            @if($transaction->cross)
                                                                {!! $transaction->cross->user ?
                                                                    '[' . $transaction->cross->user->name
                                                                    . '<a href = "/users/profile/'. $transaction->cross->user_id .'"><i class="material-icons font-18">person</i></a>]' :
                                                                    '[неизвестный пользователь ID #' . $transaction->cross->user_id . ']'
                                                                !!}
                                                            @else
                                                                не найдена связанная транзакция
                                                            @endif

                                                         {{ $transaction->getType() }}
                                                        </span>
                                                        @if(trim($transaction->desc) != '')
                                                            <br> {{ $transaction->desc }}
                                                        @endif
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @else

                            <div class="panel transactions-list {{ $transaction->accepted == 1 ? $transaction->value < 0 ? 'panel-col-deep-orange' : 'panel-col-teal' : 'panel-col-grey' }}">
                                <div class="panel-heading" role="tab" id="heading_single_{{ $transaction->id }}">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion_single_{{ $transaction->id }}" href="#collapse_single_{{ $transaction->id }}" aria-expanded="false"
                                           aria-controls="collapse_single_{{ $transaction->id }}">
                                            <span class="badge {{ $transaction->value > 0 ? "bg-green" : "bg-red" }} pull-right">@if($transaction->value > 0) + @endif {{ number_format($transaction->value, 2) }}</span>
                                            @if($transaction->getCrTypeCode() == 'MANUAL')
                                                <i class="material-icons" title="Создана вручную">build</i>
                                            @endif
                                            <i class="material-icons" title="{{ $transaction->getType() }}">{{ $transaction->getTypeIcon() }}</i>#{{ $transaction->id }} [{{ $transaction->created_at->timezone(config('project.city_time_zone'))->format('d.m.y') }}] @if($transaction->type != 6000)<span class="hidden-xs">{!! $transaction->descCompiled() !!}</span>@endif

                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse_single_{{ $transaction->id }}" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingThree_17">
                                    <div class="pd-5">
                                        {{ $transaction->created_at->timezone(config('project.city_time_zone'))->format('d.m.y') }}<br>
                                        {!! $transaction->descCompiled() !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    @empty
                        <div class="panel-heading">
                            <h4 class="panel-title">ОПЕРАЦИИ НЕ ОБНАРУЖЕНЫ</h4>
                        </div>
                    @endforelse
                    </div>




                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            @if (session('success'))
            swal("Ура!", "{{ session('success') }}", "success");
            @endif
            @if (session('error'))
            swal("УПС!", "{{ session('error') }}", "error");
            @endif

        });
    </script>

@endsection

