@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <div id="logsTable"></div>

        </div>
    </div>

@push('js_custom')
    <script src="{{ mix('js/logs/projects.js') }}"></script>
@endpush

@endsection