@extends('layouts.main')

@section('content')

    <div class="card">
        <div class="body">
            <div id="logsTable"></div>

        </div>
    </div>

@push('js_custom')
    <script type="text/javascript">
        window.by_project_id = '{{ $project_id }}';
    </script>
    <script src="{{ mix('js/logs/project.js') }}"></script>
@endpush

@endsection