@extends('layouts.main')

@section('content')
<div class="card">
    <div class="header">
        <h2>
            РЕПОРТАЖИ
        </h2>
    </div>
    <div class="body">
        <h2 class="card-inside-title">Месяц</h2>
        <select class="form-control show-tick" id="excel-import-report-month">
            @for ($i = 1; $i < 13; $i++)
              <option value="{{ $i }}">{{ Carbon\Carbon::createFromDate(null, $i, 1)->format('F') }}</option>
            @endfor
        </select>
        <h2 class="card-inside-title">Excel файл данных</h2>
        <form action="/import/reports" class="dropzone" method="POST" id="excel-import-report-dropzone">
            {{ csrf_field() }}
            <div class="dz-message">
                <div class="drag-icon-cph">
                    <i class="material-icons">touch_app</i>
                </div>
                <h5>Перетащите сюда файл или кликните для загрузки.</h5>
            </div>
            <div class="fallback">
                <input name="file" type="file"/>
            </div>
        </form>
    </div>
</div>
@endsection