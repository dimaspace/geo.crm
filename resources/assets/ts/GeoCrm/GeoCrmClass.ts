import {_Mustache} from './_MustacheClass'

class GeoCrm {
    mustache: _Mustache

    constructor(){
        this.mustache = new _Mustache();
    }
}

export {GeoCrm}