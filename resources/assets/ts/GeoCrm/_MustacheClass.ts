import * as Mustache from 'mustache';
import * as $ from "jquery";

class _Mustache {
    private tpls: Object = {};

    public addTpl(tplName: string): void {
        let tpl = $('#'+ tplName).html();
        if(tpl === undefined){
            tpl = '';
        }
        this.tpls[tplName] = tpl;
    }

    public getTpl(tplName: string): string {
        let tpl: string = '';
        if(tplName in this.tpls){
            tpl = this.tpls[tplName];
        }
        return tpl;
    }

    public render(tplName: string, data: Object = {}): string {
        return Mustache.render(this.getTpl(tplName), data);
    }
}
export {_Mustache}