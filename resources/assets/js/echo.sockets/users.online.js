window.users_online = [];
window.users_online.num = 0;
window.users_online.list = [];

var $navbar_online_num = $('#navbar_online_num');
var $navbar_online_list = $('#navbar_online_list');

LaraEcho.join('users.online')
    .here((users) => {
        window.users_online.num = users.length;
        window.users_online.list = [];
        $(users).each(function(i, user){
            window.users_online.list[user.id] = user;
            update_users_online_info();
        });
        update_users_online_info();
    })
    .joining((user) => {
        window.users_online.num++;
        window.users_online.list[user.id] = user;
        update_users_online_info();
    })
    .leaving((user) => {
        window.users_online.num--;
        delete window.users_online.list[user.id];
        update_users_online_info();
    });

function update_users_online_info(){
    $navbar_online_num.text(window.users_online.num);
    $navbar_online_list.find('li').remove();
    window.users_online.list.forEach(function(user, i){
            var tpl = '<li><div class="menu-info"><h4>' + user.name + '</h4></div></li>';
            $navbar_online_list.append(tpl);
    });
}