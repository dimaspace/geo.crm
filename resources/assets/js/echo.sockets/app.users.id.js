$(function () {
    LaraEcho.private('App.User.' + window.auth_user.id)
        .notification((notification) => {
            if (notification.type == "App\\Notifications\\SysWorks") {
                var color = notification.color;
                var msg = notification.msg;
                var msg2 = notification.msg2;
                var title = notification.title;
                var time = notification.time;
                var minute = 1000 * 60;
                var minute_5 = null;
                var icon = notification.icon;
                showWarning(msg, title, icon, color, minute_5, time);
                if(time){
                    if(msg2 && time && time > 0){
                        setTimeout(function(){
                                showWarning(msg2, title, icon, color, minute_5, 0, true);
                            },
                            time * minute
                        );
                    }
                    for(var t = 1; t < notification.time; t++){
                        var timer = function(t){
                            setTimeout(function(){
                                    showWarning(msg, title, icon, color, minute_5, time - t);
                                },
                                t * minute
                            );
                        };
                        timer(t);
                    }
                }
            }else{
                showNotification(null, notification.msg, 10 * 1000);
            }
        });
});

