<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Busy;
use Carbon\Carbon;
use Spatie\Period;
use Spatie\Period\Visualizer;

class BusyTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOverlaps()
    {
        $periods = [
            [
                'user_id' => 1,
                'started_at' => Carbon::create(2020, 1, 25,9, 0),
                'ended_at'=> Carbon::create(2020, 1, 25,13, 0),
                'busy' => -3
            ],[
                'user_id' => 1,
                'started_at' => Carbon::create(2020, 1, 25,13, 0),
                'ended_at'=> Carbon::create(2020, 1, 25,19, 0),
                'busy' => -3
            ],[
                'user_id' => 1,
                'started_at' => Carbon::create(2020, 1, 25,11, 0),
                'ended_at'=> Carbon::create(2020, 1, 25,15, 0),
                'busy' => 0
            ],[
                'user_id' => 1,
                'started_at' => Carbon::create(2020, 1, 25,10, 30),
                'ended_at'=> Carbon::create(2020, 1, 25,11, 0),
                'busy' => 3
            ]
        ];
        foreach ($periods as $period){
            Busy::create($period)->save();
        }

        $per = Busy::orderBy('started_at')->get()->toArray();

        $this->assertDatabaseMissing('busies', $periods[0]);
        $this->assertDatabaseMissing('busies', $periods[1]);

        $this->assertDatabaseHas('busies', $periods[2]);
        $this->assertDatabaseHas('busies', $periods[3]);

    }
}
