<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_extras', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('project_id')->default(0)->nullable();
            $table->integer('user_id')->default(0)->nullable();
            $table->string('type_extras')->default('ss')->nullable();
            $table->decimal('royalty', 5, 2)->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_extras');
    }
}
