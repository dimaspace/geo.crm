<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoeDeloSaleBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moe_delo_sale_bills', function (Blueprint $table) {
            $table->integer('id')->primary()->unique();
            $table->string('number')->default('');
            $table->dateTime('doc_date')->nullable();
            $table->dateTime('doc_dd_date')->nullable();
            $table->dateTime('doc_create_date')->nullable();
            $table->dateTime('doc_modify_date')->nullable();
            $table->string('doc_modify_user')->default('');
            $table->text('online')->nullable();
            $table->integer('type_id')->default(0); // 1 - обычный, 2 - счёт-договор
            $table->integer('status_id')->default(4); // 4 - не оп, 5 - ч. оп, 6 - оплачен
            $table->integer('kontragent_id')->default(0);
            $table->integer('project_id')->default(0);
            $table->text('info')->nullable();
            $table->integer('nds_id')->default(0); // 1 - не нач, 2 - сверху, 3 - в том числе
            $table->integer('is_covered')->default(0);
            $table->float('sum',8, 2)->default(0);
            $table->float('paid_sum',8, 2)->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moe_delo_sale_bills');
    }
}
