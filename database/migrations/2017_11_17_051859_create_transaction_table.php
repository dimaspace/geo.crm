<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->float('value')->default(0);

            $table->integer('type')->length(6)->unsigned()->default(0);
            $table->integer('cr_type')->length(6)->unsigned()->default(0);

            $table->text('desc')->nullable();

            $table->boolean('accepted')->default(0);

            $table->integer('user_id')->unsigned()->index();
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('creator_id')->unsigned()->index()->nullable();
            //$table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('project_id')->unsigned()->index()->nullable();
            //$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade')->onUpdate('cascade');

            $table->float('last_balance')->default(0);
            $table->float('now_balance')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       /* Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign('transactions_user_id_foreign');
            $table->drop();
        });*/
    }
}
