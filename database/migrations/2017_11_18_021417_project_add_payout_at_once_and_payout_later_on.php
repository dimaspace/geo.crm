<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectAddPayoutAtOnceAndPayoutLaterOn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function ($table) {
            $table->boolean('po_at_once')->after('pay_status_id')->default(0)->nullable();
            $table->boolean('po_later_on')->after('pay_status_id')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('projects', function ($table) {
            $table->dropColumn('po_at_once');
            $table->dropColumn('po_later_on');
        });
    }
}
