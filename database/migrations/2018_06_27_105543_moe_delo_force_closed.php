<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoeDeloForceClosed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moe_delo_sale_bills', function ($table) {
            $table->integer('is_force_closed')->after('status_id')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moe_delo_sale_bills', function ($table) {
            $table->dropColumn('is_force_closed');
        });
    }
}
