<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailedDispatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_dispatches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('channel')->default('unknow');
            $table->string('to')->default('');
            $table->text('msg')->nullable();
            $table->text('problem')->nullable();
            $table->tinyInteger('hide')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_dispatches');
    }
}
