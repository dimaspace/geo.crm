<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('busies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->default(0)->nullable();
            $table->dateTime('started_at');
            $table->dateTime('ended_at');
            $table->tinyInteger('busy')->default(0);
            $table->text('comment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('busies');
    }
}
