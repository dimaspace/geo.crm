<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectAddAdvancedFinanceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function ($table) {
            $table->integer('ss_mode')->after('payment_type_id_b')->default(0)->nullable();
            $table->integer('ss_base')->after('payment_type_id_b')->default(0)->nullable();
            $table->float('amount_to_office', 8, 2)->after('amount')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function ($table) {
            $table->dropColumn('ss_mode');
            $table->dropColumn('ss_base');
            $table->dropColumn('amount_to_office');
        });
    }
}
