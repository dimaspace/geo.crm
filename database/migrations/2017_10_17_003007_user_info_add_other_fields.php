<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserInfoAddOtherFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_info', function ($table) {
            $table->string('skype')->default('')->nullable();
            $table->string('instagramm')->default('')->nullable();
            $table->integer('in_g')->default(0)->nullable();
            $table->text('bank_card')->nullable();
            $table->text('passport')->nullable();
            $table->string('size')->default('')->nullable();
            $table->text('camera_body')->nullable();
            $table->text('camera_glasses')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('users', function ($table) {
            $table->dropColumn('skype');
            $table->dropColumn('instagramm');
            $table->integer('in_g');
            $table->dropColumn('bank_card');
            $table->dropColumn('passport');
            $table->dropColumn('size');
            $table->dropColumn('camera_body');
            $table->dropColumn('camera_glasses');
        });*/
    }
}
