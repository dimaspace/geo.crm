<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_cords', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('project_id')->default(0)->nullable();
            $table->integer('user_id')->default(0)->nullable();
            $table->string('type_code')->default('prin')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_cords');
    }
}
