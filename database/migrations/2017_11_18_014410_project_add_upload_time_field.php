<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectAddUploadTimeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function ($table) {
            $table->dateTime('upload_at')->after('updated_at')->nullable();
            $table->dateTime('moderated_at')->after('upload_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('projects', function ($table) {
            $table->dropColumn('upload_at');
            $table->dropColumn('moderated_at');
        });
    }
}
