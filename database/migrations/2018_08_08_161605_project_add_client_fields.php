<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectAddClientFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function ($table) {
            $table->integer('client_notified')->after('client_cached')->default(0);
            $table->integer('client_vk_id')->after('client_cached')->default(0)->nullable();
            $table->string('client_email')->after('client_cached')->default('')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function ($table) {
            $table->dropColumn('client_email');
            $table->dropColumn('client_vk_id');
            $table->dropColumn('client_notified');
        });
    }
}
