<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('creator_id')->default(0)->nullable();

            $table->string('region_code')->default('')->nullable();

            $table->string('category_id')->default('')->nullable();

            $table->integer('status_id')->default(0)->nullable();

            $table->dateTime('date_start')->nullable();
            $table->dateTime('date_end')->nullable();
            //$table->time('time_start')->nullable();
            //$table->time('time_end')->nullable();
            $table->date('date_deadline')->nullable();

            $table->integer('place_id')->default(0)->nullable();
            $table->string('place_cached')->default('')->nullable();

            $table->integer('client_id')->default(0)->nullable();
            $table->string('client_cached')->default('')->nullable();

            $table->text('desc_public')->nullable();
            $table->text('desc_hidden')->nullable();

            $table->float('amount_per_hour', 8, 2)->nullable();
            $table->float('amount', 8, 2)->nullable();
            $table->float('amount_with_tax', 8, 2)->nullable();

            $table->integer('payment_type_id_w')->default(0)->nullable();
            $table->integer('payment_type_id_b')->default(0)->nullable();

            $table->integer('worker_id')->default(0)->nullable();

            $table->integer('pass_type_id')->default(0)->nullable();
            $table->integer('geo_type_id')->default(0)->nullable();
            $table->integer('smm_type_id')->default(0)->nullable();

            $table->text('download_link')->nullable();

            $table->integer('referral_id')->default(0)->nullable();
            $table->integer('extra_service_id')->default(0)->nullable();

            $table->integer('visibility_id')->default(1)->nullable();

            $table->integer('pay_status_id')->default(0)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
