<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MassAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->index('category_id');
            $table->index('status_id');
            $table->index('place_id');
            $table->index('client_id');
            $table->index('worker_id');
            $table->index('date_start');
            $table->index('date_start_real');
            $table->index('date_end');
            $table->index('date_deadline');


        });

        Schema::table('project_cords', function (Blueprint $table) {
            $table->index('project_id');
            $table->index('user_id');
            $table->index('type_code');
        });

        Schema::table('project_desires', function (Blueprint $table) {
            $table->index('project_id');
            $table->index('user_id');
        });

        Schema::table('project_extras', function (Blueprint $table) {
            $table->index('project_id');
            $table->index('user_id');
            $table->index('type_extras');
        });

        Schema::table('project_referrals', function (Blueprint $table) {
            $table->index('project_id');
            $table->index('user_id');
        });

        Schema::table('regions', function (Blueprint $table) {
            $table->index('code');
            $table->index('geo_id');
        });

        Schema::table('user_region', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('region_id');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->index('cash_flow_id');
            $table->index('cross_t_id');
            $table->index('created_at');
            $table->index('updated_at');
        });

        Schema::table('cash_flows', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('type');
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('projects', function (Blueprint $table) {
            $table->dropIndex('category_id');
            $table->dropIndex('status_id');
            $table->dropIndex('place_id');
            $table->dropIndex('client_id');
            $table->dropIndex('worker_id');
            $table->dropIndex('date_start');
            $table->dropIndex('date_start_real');
            $table->dropIndex('date_end');
            $table->dropIndex('date_deadline');


        });

        Schema::table('project_cords', function (Blueprint $table) {
            $table->dropIndex('project_id');
            $table->dropIndex('user_id');
            $table->dropIndex('type_code');
        });

        Schema::table('project_desires', function (Blueprint $table) {
            $table->dropIndex('project_id');
            $table->dropIndex('user_id');
        });

        Schema::table('project_extras', function (Blueprint $table) {
            $table->dropIndex('project_id');
            $table->dropIndex('user_id');
            $table->dropIndex('type_extras');
        });

        Schema::table('project_referrals', function (Blueprint $table) {
            $table->dropIndex('project_id');
            $table->dropIndex('user_id');
        });

        Schema::table('regions', function (Blueprint $table) {
            $table->dropIndex('code');
            $table->dropIndex('geo_id');
        });

        Schema::table('user_region', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('region_id');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropIndex('cash_flow_id');
            $table->dropIndex('cross_t_id');
            $table->dropIndex('created_at');
            $table->dropIndex('updated_at');
        });

        Schema::table('cash_flows', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('type');
            $table->dropIndex('created_at');
            $table->dropIndex('updated_at');
        });*/
    }
}
