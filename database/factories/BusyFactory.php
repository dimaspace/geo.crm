<?php

use Faker\Generator as Faker;

$factory->define(App\Busy::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'started_at' => null,
        'ended_at'=> null,
        'busy' => -3
    ];
});
